<%@ taglib uri="/struts-tags" prefix="s"%>
<%@page import="org.jfree.data.KeyedValues"%>
<%@page import="org.jfree.data.DataUtilities"%>
<%@page import="org.jfree.data.DefaultKeyedValues"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page  import="java.awt.*" %>
<%@ page  import="java.io.*" %>
<%@ page  import="org.jfree.chart.*" %>
<%@ page  import="org.jfree.chart.axis.*" %>
<%@ page  import="org.jfree.chart.entity.*" %>
<%@ page  import="org.jfree.chart.labels.*" %>
<%@ page  import="org.jfree.chart.plot.*" %>
<%@ page  import="org.jfree.chart.renderer.category.*" %>
<%@ page  import="org.jfree.chart.urls.*" %>
<%@ page  import="org.jfree.data.category.*" %>
<%@ page  import="org.jfree.data.general.*" %>

<%
Double todayachived1=(Double)request.getAttribute("todayachived1");
Double weekachived1=(Double)request.getAttribute("weekachived1");
Double monthachived1=(Double)request.getAttribute("monthachived1");

final DefaultKeyedValues data = new DefaultKeyedValues();
data.addValue("DAY",todayachived1 );
data.addValue("WEEK",weekachived1);
data.addValue("MONTH",monthachived1);


final KeyedValues cumulative = DataUtilities.getCumulativePercentages(data);
final CategoryDataset dataset = DatasetUtilities.createCategoryDataset("Achived", data);
        
            JFreeChart chart = null;
            BarRenderer renderer = null;
            CategoryPlot plot = null;
            final CategoryAxis categoryAxis = new CategoryAxis("Achvied");
            final ValueAxis valueAxis = new NumberAxis("Targets(%)");
            
          
            renderer = new BarRenderer();
         
            plot = new CategoryPlot(dataset, categoryAxis, valueAxis,
            renderer);
                
            plot.setOrientation(PlotOrientation.VERTICAL);
                    
            chart = new JFreeChart("Target Graph", JFreeChart.DEFAULT_TITLE_FONT,
            plot, true);
            
            
            chart.setBackgroundPaint(new Color(249, 231, 236));
            Paint p1 = new GradientPaint(
                    0.0f, 0.0f, new Color(16, 89, 172), 0.0f, 0.0f, new Color
                   (201, 201, 244));
            renderer.setSeriesPaint(1, p1);
                   
            plot.setRenderer(renderer);
              
            
            try {
                final ChartRenderingInfo info = new ChartRenderingInfo
                (new StandardEntityCollection());
                final File file1 = new File("images/barchart.png");
                ChartUtilities.saveChartAsPNG(file1, chart, 300, 300, info);
            } catch (Exception e) {
                out.println(e);
            }
%>
<html>
<head>
<style type="text/css">



</style>
<script type="text/javascript">
//location.reload("#");
//window.location.href="#";
</script>

</head>
<body >
<p style="color: red;font-size: 18px;text-align: center;">Targets</p>


<div class="#"> 
        <div style="width:150px; height:auto; float:left; margin-left:10px;">
         Today Target
        </div>
      	<div style="width:160px; height:auto; float:left; margin-left:10px;">
         Weekly Target
        </div>
        
        <div style="width:150px; height:auto; float:left; margin-left:10px;">
        Monthly Target
        </div>
       
       
       
      
</div>

<div class="">

      <div style="width:230px; height:auto; float:left; margin-left:10px;">
     
     <s:property value="objTargetDTO1.today_target"/>
     <input type="hidden" id="today_target" value="<s:property value=" objtargetdto1.today_target"/>">
    
     </div>
        
       <div style="width:160px; height:auto; float:left; margin-left:-26px;">
    
     <s:property value="objTargetDTO1.week_target" />
     <input type="hidden" id="week_target" value="<s:property value=" objtargetdto1.week_target"/>"/>
    
     </div>
       
        <div style="width:150px; height:auto; float:left; margin-left:10px;">
   
         <s:property value="objTargetDTO1.month_target"/>
      <input type="hidden" id="month_target" value="<s:property value=" objtargetdto1.month_target"/>"/>
     
     </div>
       
       <div style="width:150px; height:auto; float:left; margin-left:10px;color: red;">
   
         Targets
      
     
       </div>
       
</div>



<div class="">

      <div style="width:230px; height:auto; float:left; margin-left:10px;">
     
     <s:property value="objTargetDTO.day_count"/>
      <input type="hidden" id="day_count" value="<s:property value=" objtargetdto.day_count"/>"/>
    
      </div>
        
       <div style="width:160px; height:auto; float:left; margin-left:-26px;">
    
     <s:property value="objTargetDTO.week_count"/>
     <input type="hidden" id="week_count" value="<s:property value=" objtargetdto.week_count"/>"/>
    
       </div>
       
        <div style="width:150px; height:auto; float:left; margin-left:10px;">
   
         <s:property value="objTargetDTO.month_count"/>
      <input type="hidden" id="month_count" value="<s:property value=" objtargetdto.month_count"/>"/>
     
</div>
       
       <div style="width:150px; height:auto; float:left; margin-left:10px;color: red;">
   
        Achieved Targets
      
     
       </div>
   
       
</div>


<div class="newmyprofile_userfrofile_businessname">

      <div style="width:170px; height:auto; float:left; margin-left:10px;">
   
      
       
<img src="images/barchart.png" width="300" height="300" alt="">

        
       
      
      
     
       </div>
   
       
</div>





</body></html>
