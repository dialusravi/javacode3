<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css/jquery-ui123.css" />	 
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script src="js/jquery-min.js"></script>

</head>
<body>
<div class="userappointments_line"></div>
<div class="userappointments_inner">
<a style="cursor: pointer;color:#008abb; margin: 0 0 0 50px; text-decoration: none;" href="adminHome.jsp" id="home_link">  Home </a>
<img src="images/appointment_missed.png" width="30" height="30" style="float:left; margin-left:25px;"><span style="float:left; margin-top:3px; margin-left:10px;"> To Day Your Appointments</span> </div>
<div class="userappointments_line"></div>
<div class="userappointments_line" style="margin-top:10px;"></div>

<div class="userappointments">
	<div class="userbusinessname">
    Business Name
    </div>
    <div class="user_appointmentplace">
    Appointment Place
    </div>
    <div class="user_appointmenttime">
    Appointment Time
    </div>
    <div class="user_appointmentfixedby">
    Fixed by
    </div>
</div>

<div class="userappointments_line"></div>
<s:if test="%{objAppointmentDTOList.size!=0}">
<s:iterator value="objAppointmentDTOList" id="a">
<div class="userappointments_inner1">
<div class="userbusinessname">

 <s:if test="%{#a.businessName!=null && #a.businessName!=''}">
	   		 <s:property value="#a.businessName"/>
	    </s:if>
	    <s:else>
	   		 ........
	    </s:else>

  </div>
  
    <div class="user_appointmentplace">
    <s:if test="%{#a.businessArea!=null && #a.businessArea!=''}">
	   		 <s:property value="#a.businessArea"/>
	    </s:if>
	    <s:else>
	   		 ........
	    </s:else>
    </div>
    
    <div class="user_appointmenttime">
    
    <s:if test="%{#a.appointmentDateTime!=null && #a.appointmentDateTime!=''}">
	   		 <s:property value="#a.appointmentDateTime"/>
	    </s:if>
	    <s:else>
	   		 ........
	    </s:else>
  
    </div>
    <div class="user_appointmentfixedby">
    <s:if test="%{#a.telecallerName!=null && #a.telecallerName!=''}">
    <s:property value="#a.telecallerName"/>
    </s:if>
    <s:else>
    ........
    </s:else>
    </div>
    <div class="user_appointmentalert">
    
    <a href="loadAppointments.action?appointmentCity=<s:property value="#a.businessCity"/>&appointmentId=<s:property value="#a.appointmentId"/>&type=one&appointmentPlace=<s:property value="#a.businessArea"/>&tellcallername=<s:property value="#a.telecallerName"/>&usertype=admin&append_todaydate=<s:property value="#session.todaydate" />" id="allot" style="color:#fff; font-weight:bold; background-color:#008abb; text-align:center; padding:5px; border:none;">Allot</a> 
   <%-- <a href="loadAppointments.action?appointmentCity=<s:property value="#a.businessCity"/>&appointmentId=<s:property value="#a.appointmentId"/>&type=one&appointmentPlace=<s:property value="#a.businessArea"/>&tellcallername=<s:property value="#a.telecallerName"/>&usertype=admin" id="allot" style="color:#fff; font-weight:bold; background-color:#008abb; text-align:center; padding:5px; border:none;">Allot</a>--%>
    </div>

</div>

</s:iterator>
</s:if>
<s:else>
<div class="userappointments_inner1" style="color:red;font-family: georgia;font-weight: bold">Sorry No Records Are  Available....</div>
</s:else>
</body>
</html>