<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css/jquery-ui123.css" />	 
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script src="js/jquery-min.js"></script>
<link rel="stylesheet" href="css/jquery-ui123.css" />	 
 <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui.min.js"></script>
 <script type="text/javascript">
  function edit(mobile)
  {
$("#mysms").hide();
$("#show_sms_div").load("ShowSmsAction.action?mobile="+mobile);
$("#show_sms_div").show();
  } 
  </script>

</head>
<body>

<div id="mysms">
<p style="color: red;font-size: 18px;text-align: center;">Leads Sent</p>
<div class="userappointments_line"></div>

<div class="userappointments_line"></div>
<div class="userappointments_line" style="margin-top:10px;"></div>
<div class="userappointments">
	<div class="userbusinessname" style="width:21px">
      id
    </div>
    
    <div class="user_appointmentplace" style="width:165px">
    Business Name
    </div>
    
    <div class="user_appointmenttime" style="width:120px">
    Mobile
    </div>
    
     <div class="user_appointmenttime" style="width:120px">
   SMS Count
    </div>
    
</div>

<div class="userappointments_line"></div>
<s:if test="%{objArrayList.size!=0}">
<s:iterator value="objArrayList" id="a">
<div class="userappointments_inner1">

    <div class="userbusinessname" style="width:21px">
       <s:property value="#a.sms_id"/>
    </div>
  
    <div class="user_appointmentplace" style="width:165px;">
      <s:property value="#a.business_name"/>
    </div>
    
    <div class="user_appointmenttime" style="width:120px">
        <s:property value="#a.mobile"/>
    </div>
    
    
    <div class="user_appointmenttime" style="width:218px">
      <a  href="#" onclick="edit('<s:property value="#a.mobile"/>')" style="background-color: #008abb; border: none; color: #fff; height: 14px; padding:5px; float: left; cursor: pointer;">View SMS Count(<s:property value="#a.leads_count"/>)</a>
    </div>
   
</div>

</s:iterator>
</s:if>
<s:else>
<div class="userappointments_inner1" style="color:red;font-family: georgia;font-weight: bold">Sorry No Records Are  Available....</div>
</s:else>
</div>
<div id="show_sms_div"></div>
</body>
</html>