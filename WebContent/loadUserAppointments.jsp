<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />

</head>
<body>
<div class="load_appointments">
 <s:if test="%{objAppointmentDTOList.size!=0}">
	<div class="load_appointments_headding">
    <img src="images/appointment_missed.png" width="30" height="30" style="float:left; margin-left:15px;"> 
    <span style="float:left; margin-left:10px; margin-top:4px;">To Day Your Appointments</span>
    </div>
   
    <s:iterator value="objAppointmentDTOList" id="a">
    <div class="load_appointments_inner"  onclick="getAppointmentDetails('<s:property value="#a.appointmentId"/>')">
    <div class="load_appointments_inner101">
    	<div class="load_appointments_innername">
       <s:property value="#a.businessName"/>
        </div>
        <div class="load_appointments_innerplece">
        Near : <s:property value="#a.businessArea"/>
        </div>
        <div class="load_appointments_innertime">
       On : <s:property value="#a.appointmentDateTime"/>
        </div>
      </div>
        <div class="load_appointments_inner122" >
        <input type="button" value="Click Here To Process" style="margin-left: -150px;position: absolute;"> </div>
        
  </div>
  </s:iterator>
    </s:if>
    <s:else>
     <div class="load_appointments_inner" style="color:red;font-family: georgia;font-weight: bold;">
     	Sorry There are No Appointments Till Now................
     </div>
    </s:else>
    <div style="clear:both"></div>
</div>


</body>
</html>