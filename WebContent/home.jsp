<%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sales Tracking</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />
<script src="js/jquery-min.js"></script>
<script type="text/javascript" src="date_time.js"></script>
<script type="text/javascript">
function date_time(id)
{
        date = new Date;
        year = date.getFullYear();
        month = date.getMonth();
        months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'Jully', 'August', 'September', 'October', 'November', 'December');
        d = date.getDate();
        day = date.getDay();
        days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
        h = date.getHours();
        if(h<10)
        {
                h = "0"+h;
        }
        m = date.getMinutes();
        if(m<10)
        {
                m = "0"+m;
        }
        s = date.getSeconds();
        if(s<10)
        {
                s = "0"+s;
        }
        result =  ''+d+' '  +months[month]+'  '+year+' -  '+h+':'+m+':'+s;
        document.getElementById(id).innerHTML = result;
        setTimeout('date_time("'+id+'");','1000');
        return true;
}
$(document).ready(function()
{
	     // find the input fields and apply the time select to them.
	     $('#login_links').show();
	     $('#banner_div').show();
	     $('#more_links').hide();
	     $('#userlogin_div').hide();
	     $('#adminlogin_div').hide(); 
	    
	    $('#username').attr("placeholder", "Please Enter User Name");
	    $('#password').attr("placeholder", "Please Enter Password");
	    $('#adminusername').attr("placeholder", "Please Enter User Name");
	    $('#adminpassword').attr("placeholder", "Please Enter Password");
	    
	    $("#userlogin_link").click(function()
	    {
	    	
			 $('#banner_div').hide();
			 $('#login_links').hide();
		     $('#more_links').show();
		    
		     $('#userlogin_div').show();
		     $('#adminlogin_div').hide();
	 	});
	    $("#adminlogin_link").click(function()
	    	    {
	    	    	
			    	 $('#banner_div').hide();
					 $('#login_links').hide();
				     $('#more_links').show();
				     $('#userlogin_div').hide();
				    
				     $('#adminlogin_div').show();
	    	 	});
    
    
});

</script>
</head>

<body>
<div class="top_bg">
	<div class="topbg_inner">
   	  <div class="topbg_inner_left">
        	<div class="topbg_inner_left_top">
            	SALES TRACKING
            </div>
            <div class="topbg_inner_left_bottom">
             <span id="date_time"></span>
            <script type="text/javascript">window.onload = date_time('date_time');</script>
          </div>
      </div>
      <div class="topbg_inner_right">
        	<div class="topbg_inner_right_left">
       	  <img src="images/dialus_small.png" width="28" height="27" /> </div>
            <div class="topbg_inner_right_right">
            9666 888 888
            </div>
      </div>
    </div>
</div>
<div style="clear:both"></div>
<div class="wrapper">
<div class="logopart">
	<div class="logopart_left">
    <img src="images/logo.jpg" width="225" height="90" /> 
    </div>
    <div class="logopart_right">
    	<ul>
        
        <li>About Us</li>
         <li>|</li>
        <li>Services</li>
         <li>|</li>
        <li>Gallery</li>
          <li>|</li>
        <li>Products</li>
          <li>|</li>
        <li>Contact Us</li>
        </ul>
    </div>
</div>
<div class="contentpart">
	<div class="content_left">
    	
        <div id="login_links">
	        <div class="content_left_headding">
		  <div class="content_left_text">
            Login Menu
          </div>
      </div>
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;" id="adminlogin_link" >Admin Login</a>
	        	</div>
	       </div>
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;" id="userlogin_link">User Login</a>
	        	</div>
	       </div>
      </div>
      <div id="more_links">
      <div class="content_left_headding">
		  <div class="content_left_text">
            Dialuz Updates
          </div>
      </div>
		       <div class="smallheadding_conleft_inner">
		       	<div class="smallheadding_conleft_inner_left">
		         <img src="images/arrow.jpg" width="5" height="8" /> 
		        </div>
		        <div></div>
		        <div class="smallheadding_conleft_inner_right">
		         Admin Login
		        </div>
		       </div>
		       <div class="smallheadding_conleft_inner">
		       	<div class="smallheadding_conleft_inner_left">
		         <img src="images/arrow.jpg" width="5" height="8" /> 
		        </div>
		        
		        <div class="smallheadding_conleft_inner_right">
		         Admin Login
		        </div>
		       </div>
		       <div class="smallheadding_conleft_inner">
		       	<div class="smallheadding_conleft_inner_left">
		         <img src="images/arrow.jpg" width="5" height="8" /> 
		        </div>
		        
		        <div class="smallheadding_conleft_inner_right">
		         Admin Login
		        </div>
		       </div>
		       <div class="smallheadding_conleft_inner">
		       	<div class="smallheadding_conleft_inner_left">
		         <img src="images/arrow.jpg" width="5" height="8" /> 
		        </div>
		        
		        <div class="smallheadding_conleft_inner_right">
		         Admin Login
		        </div>
		       </div>
		       <div class="smallheadding_conleft_inner">
		       	<div class="smallheadding_conleft_inner_left">
		         <img src="images/arrow.jpg" width="5" height="8" /> 
		        </div>
		        
		        <div class="smallheadding_conleft_inner_right">
		         Admin Login
		        </div>
		       </div>
		       <div class="smallheadding_conleft_inner">
		       	<div class="smallheadding_conleft_inner_left">
		         <img src="images/arrow.jpg" width="5" height="8" /> 
		        </div>
		        
		        <div class="smallheadding_conleft_inner_right">
		         Admin Login
		        </div>
		       </div>
		       <div class="smallheadding_conleft_inner">
		       	<div class="smallheadding_conleft_inner_left">
		         <img src="images/arrow.jpg" width="5" height="8" /> 
		        </div>
		        
		        <div class="smallheadding_conleft_inner_right">
		         Admin Login
		        </div>
		       </div>
		    </div>
    </div>
    <div class="content_right">
     <s:if test="%{#request.success_msg !=null && #request.success_msg!=' '}">
								            <s:property value="%{#request.success_msg}"/>
	</s:if>
	   	  <div class="banner_part" id="banner_div">
	        	<div class="banner_part_left">
	         	 	<img src="images/banner.jpg" width="423" height="311" />
	       	    </div>
	            <div class="banner_part_right">
	            	<img src="images/banner_left.jpg" width="277" height="314" />
	            </div>
	            
	            <div class="banner_part">
	             </div>
	            
	      </div>
	      <div class="main_div">
	      
	      <div id="userlogin_div">
			      	<s:form action="Login" theme="simple">
					    	
					        <div class="main_div_middle">
					         <div class="main_div_middle_textfields_main" style="text-align:center;background: #008ABB;width: 100%;color:#fff;padding: 8px 0px;margin-top:-10px;border-top-left-radius:10px;border-top-right-radius:10px;">
								           Please Enter User  Login Details
								        </div>
						        <div class="main_div_middle_textfields">
								       
							           <div class="main_div_middle_textfields_main">
								           	<div class="main_div_middle_textfields_main_left">
								       	   		<img src="images/username.jpg" width="44" height="39" /> 
								            </div>
								            <div class="main_div_middle_textfields_main_right">
								           		  <s:textfield  name="" id="username"  cssStyle="width:280px; border:1px solid #008abb; padding:11px 0 10px 3px" ></s:textfield>
								            </div>
							           </div>
							           
							           <div class="main_div_middle_textfields_main">
								           	<div class="main_div_middle_textfields_main_left">
								       	    	 <img src="images/password.jpg" width="44" height="39" /> 
								            </div>
								            <div class="main_div_middle_textfields_main_right">
								            	<s:textfield  name=""  id="password"  cssStyle="width:280px; border:1px solid #008abb; padding:11px 0 10px 3px"  ></s:textfield>
								            </div>
							           </div>
							           
							           <div class="main_div_middle_textfields_main">
							          			 <s:submit   value="Login" cssStyle="color:#fff; font-weight:bold; background-color:#008abb; text-align:center; padding:5px 0 5px 0; width:328px; border:none" />
							           </div>
							           <div class="main_div_middle_textfields_main" style="font-size:12px">
							          		<s:checkbox  name=""   cssStyle="margin-left:95px; float:left"/>  <span style="float:left; margin-left:5px">Forgot Password</span>
							           </div>
							           <div style="clear:both"></div>
						            </div>
						            
						            
						          
						        </div>
					     
					     </s:form>
				</div>
				
				   <div id="adminlogin_div">
			      	<s:form action="Login" theme="simple">
					    	
					        <div class="main_div_middle">
					         <div class="main_div_middle_textfields_main" style="text-align:center;background: #008ABB;width: 100%;color:#fff;padding: 8px 0px;margin-top:-10px;border-top-left-radius:10px;border-top-right-radius:10px;">
								           Please Enter Admin  Login Details
								        </div>
						        <div class="main_div_middle_textfields">
								        
							           <div class="main_div_middle_textfields_main">
								           	<div class="main_div_middle_textfields_main_left">
								       	   		<img src="images/username.jpg" width="44" height="39" /> 
								            </div>
								            <div class="main_div_middle_textfields_main_right">
								           		  <s:textfield  name="" id="adminusername"  cssStyle="width:280px; border:1px solid #008abb; padding:11px 0 10px 3px" ></s:textfield>
								            </div>
							           </div>
							           
							           <div class="main_div_middle_textfields_main">
								           	<div class="main_div_middle_textfields_main_left">
								       	    	 <img src="images/password.jpg" width="44" height="39" /> 
								            </div>
								            <div class="main_div_middle_textfields_main_right">
								            	<s:textfield  name=""  id="adminpassword"  cssStyle="width:280px; border:1px solid #008abb; padding:11px 0 10px 3px"  ></s:textfield>
								            </div>
							           </div>
							           
							           <div class="main_div_middle_textfields_main">
							          			 <s:submit   value="Login" cssStyle="color:#fff; font-weight:bold; background-color:#008abb; text-align:center; padding:5px 0 5px 0; width:328px; border:none" />
							           </div>
							           <div class="main_div_middle_textfields_main" style="font-size:12px">
							          		<s:checkbox  name=""   cssStyle="margin-left:95px; float:left"/>  <span style="float:left; margin-left:5px">Forgot Password</span>
							           </div>
							           <div style="clear:both"></div>
						            </div>
						            
						            
						          
						        </div>
					        
					     </s:form>
				</div>
	   		 </div>
	   
	      
  </div>
</div>
</div>
<div class="fotter">
	<div class="fotter_inner">
     All Rights Reserved - Copyright © Dialus.com 2010-2013
    </div>
</div>
</body>
</html>
