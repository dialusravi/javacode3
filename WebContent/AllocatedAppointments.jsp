<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css/jquery-ui123.css" />	 
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script src="js/jquery-min.js"></script>

</head>
<body>
<div class="userappointments_line"></div>

<div class="userappointments_line"></div>
<div class="userappointments_line" style="margin-top:10px;"></div>

<div class="userappointments">
	
    <div class="user_appointmentplace" style="width:120px">
    Business Name
    </div>
    
    <div class="user_appointmentfixedby" style="width:135px;">
   Appointment Date 
    </div>
      <div class="user_appointmentfixedby" style="width:135px;">
     SalesPersonName
    </div>
     <div class="user_appointmentfixedby" style="width:135px;">
     Business City
    </div>
    <div class="user_appointmentfixedby" style="width:135px;">
     Business Area
    </div>
 
</div>

<div class="userappointments_line"></div>
<s:if test="%{objNamesList.size!=0}">
<s:iterator value="objNamesList" id="a">
<div class="userappointments_inner1">
  <div class="userbusinessname" title="<s:property value="#a.businessName"/>" style="width:120px;overflow: hidden;">
  
  
   <s:if test="%{#a.businessName!=null && #a.businessName!=''}">
	   		 <s:property value="#a.businessName"/>
	    </s:if>
	    <s:else>
	   		 ........
	    </s:else>
   
   

  </div>
  
  
  
    <div class="user_appointmentplace" title="<s:property value="#a.date"/>" style="width:135px;overflow: hidden;">
      <s:if test="%{#a.date!=null && #a.date!=''}">
    
    
    <s:property value="#a.date"/>
    </s:if>
     <s:else>
	   		 ........
	    </s:else>
    </div>
    
    <div class="user_appointmenttime" title="<s:property value="#a.name"/>" style="width:135px;overflow: hidden;">
    
     <s:if test="%{#a.name!=null && #a.name!=''}">
    
    
    <s:property value="#a.name"/>
    </s:if>
      <s:else>
	   		 ........
	    </s:else>
    </div>
    
    
    <div class="user_appointmenttime" title="<s:property value="#a.businessCity"/>" style="width:135px;overflow: hidden;">
    
    <s:if test="%{#a.businessCity!=null && #a.businessCity!=''}">
    <s:property value="#a.businessCity"/>
    </s:if>
    <s:else>
	   		 ........
	    </s:else>
    </div>
    <div class="user_appointmenttime" title=" <s:property value="#a.businessArea"/>" style="width:135px;overflow: hidden;">
    
    <s:if test="%{#a.businessArea!=null && #a.businessArea!=''}">
    <s:property value="#a.businessArea"/>
    </s:if>
      <s:else>
	   		 ........
	    </s:else>
    </div>
    
</div>

</s:iterator>
</s:if>
<s:else>
<div class="userappointments_inner1" style="color:red;font-family: georgia;font-weight: bold">Sorry No Records Are  Available....</div>
</s:else>
</body>
</html>