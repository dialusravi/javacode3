<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />
<script src="js/jquery-min.js"></script>
<link rel="stylesheet" href="css/jquery-ui123.css" />	 
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/redmond/jquery-ui.css" />

<!-- Plugin files below -->
<link rel="stylesheet" type="text/css" href="css/jquery.ptTimeSelect.css" />
<script type="text/javascript" src="js/jquery.ptTimeSelect.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<link href="http://dialuscss.dialus.com/dialus.css" rel="stylesheet" type="text/css" media="all"/>
 <script type="text/javascript" src="js/salesTrackingValid.js"></script>
<script>
$(function() {
	$( ".datepicker1" ).datepicker();
	});
	
	
function changeCategoryValue(){
	 
	 
	 var e = document.getElementById("businesscategory");
	 var selected = e.options[e.selectedIndex].value;
	
	 
	 if(selected=="other"){
		 
		  
		  $("#myOwnCategory").show();
		 if (window.XMLHttpRequest)
		  {
		   // code for IE7+, Firefox, Chrome, Opera, Safari
		   xmlhttp=new XMLHttpRequest();
		  }else{
		   // code for IE6, IE5
		   xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		 xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		   {
			
			
			 document.getElementById("subcategory").innerHTML = xmlhttp.responseText;
			 $('#subsubcategory').hide();
		   }
		  };
		  xmlhttp.open("GET","subCategory.action?allcategoryid="+selected,true);
		  xmlhttp.send();
		 
	 }


	 if(selected!=null && selected!="other")
		 {
		 $("#myOwnCategory").hide();
	 if (window.XMLHttpRequest)
	  {
	   // code for IE7+, Firefox, Chrome, Opera, Safari
	   xmlhttp=new XMLHttpRequest();
	  }else{
	   // code for IE6, IE5
	   xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	 xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	   {
		
		
		 document.getElementById("subcategory").innerHTML = xmlhttp.responseText;
		 $('#subsubcategory').hide();
	   }
	  };
	  xmlhttp.open("GET","subCategory.action?allcategoryid="+selected,true);
	  xmlhttp.send();
	 }
	
	 
}
	
	jQuery(function(){
		 
	
		 $("#country123").autocomplete("AutoCompleteAction.action");
		 $('#businessState').autocomplete('CityautoCompleteAction.action?variable=state');
		 $('#businessCity').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businessState: function () {
				                return $("#businessState").val();
				            },
				            
				            
				            variable: function () {
				                return "city";
				            }
				
				        }
					}
			
			
			);
			
			/*area starts here  */
			
			
			
			/*Start of area auto complete  */
		$('#businessArea').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businesCity: function () {
				                return $("#businessCity").val();
				            },
				            
				            
				            variable: function () {
				                return "area";
				            }

				        }
					}
			
			
			);
			
			
			
	
			
			
		});
	
	
	</script>
<script type="text/javascript">


$(document).ready(function(){
        	
     
        	$("#myOwnCategory").hide();
            // find the input fields and apply the time select to them.
            $('#appointmentTime').ptTimeSelect();
            $("#callbackdate_div_date").hide();
            $("#check_callback").click(function()
            {
            	if($('#check_callback').is(':checked'))
            	{
            		//attr('checked')
	                $("#callback_div").hide();
	                $("#callbackdate_div_date").show();
	                document.getElementById('fix_appointment_but').value='Save As Call Back';
	               // $("#fix_appointment_but").html("Save As Call Back");
	                $('#check_save').removeAttr('checked');
	                $('#check_fix').removeAttr('checked');
	                
            	}
            	else
           		{
            		 $("#callback_div").show();
 	                $("#callbackdate_div_date").hide();
 	               document.getElementById('fix_appointment_but').value='Fix Appointment';
 	               	//$("#fix_appointment_but").html("Fix Appointment");
 	               $('#check_save').removeAttr('checked');
 	              $('#check_fix').removeAttr('checked');
           		}
            	
                
            });
            
            $("#check_save").click(function()
                    {
                    	if($('#check_save').is(':checked'))
                    	{
                    		//attr('checked')
        	                $("#callback_div").hide();
        	                $("#callbackdate_div_date").hide();
        	                document.getElementById('fix_appointment_but').value='Add (or) Save Data';
        	              //  $("#fix_appointment_but").html("Add(or)Save Data");
        	                $('#check_callback').removeAttr('checked');
        	                $('#check_fix').removeAttr('checked');
                    	}
                    	else
                   		{
                    		 $("#callback_div").show();
         	                $("#callbackdate_div_date").hide();
         	               document.getElementById('fix_appointment_but').value='Fix Appointment';
         	              // $("#fix_appointment_but").html("Fix Appointment");
         	              $('#check_callback').removeAttr('checked');
         	             $('#check_fix').removeAttr('checked');
         	             // $('#').prop('checked', false); 
                   		}
                        
                    });
            
            
            $("#check_fix").click(function()
                    {
                    	if($('#check_fix').is(':checked'))
                    	{
                    		//attr('checked')
        	                $("#callback_div").show();
        	                $("#callbackdate_div_date").hide();
        	                document.getElementById('fix_appointment_but').value='Fix Appointment';
        	              //  $("#fix_appointment_but").html("Add(or)Save Data");
        	                $('#check_callback').removeAttr('checked');
        	                $('#check_save').removeAttr('checked');
                    	}
                    	else
                   		{
                    		 $("#callback_div").hide();
         	                $("#callbackdate_div_date").hide();
         	               document.getElementById('fix_appointment_but').value='Fix Appointment';
         	              // $("#fix_appointment_but").html("Fix Appointment");
         	              $('#check_callback').removeAttr('checked');
         	             $('#check_save').removeAttr('checked');
         	             // $('#').prop('checked', false); 
                   		}
                        
                    });
            
            
            
            $('.datepicker').datepicker({minDate: 0});
        });
    </script>
      <script>
  $(function()
  {
    $( ".datepicker" ).datepicker();
  });

	$("#callStatus1").change(function()
	{ 
        if( $(this).is(":checked") )
        { 
        	$('#fixappointment_div').hide();
			$('#banner_div').hide();
			$('#callstatus_div').hide();
			$('#appoint_msg').hide();
			$('#appoint_div').show();
			$('#callbackdate').hide();
			$('#callbackdate').val("");
			$('#appointmentfrom_div').show();
			
			
        }
    });
	$("#callStatus2").change(function()
	{ 
        if( $(this).is(":checked") )
        { 
        	var call=$('#callStatus2').val();
        	$('#hiddencallStatus').val(call);
        
        	$('#fixappointment_div').hide();
			$('#banner_div').hide();
			$('#callstatus_div').hide();
			$('#appoint_div').show();
			$('#appointmentfrom_div').hide();
			$('.black_overlay').show();
			$('#callbackdate').hide();
			$('#callbackdate').val("");
			$('#appointment_status').show();
			
			
        }
    });
	$("#callStatus3").change(function()
	{ 
        if( $(this).is(":checked") )
        { 
        	var call=$('#callStatus3').val();
        	$('#hiddencallStatus').val(call);
        	
        	$('#fixappointment_div').hide();
			$('#banner_div').hide();
			$('#callstatus_div').hide();
			$('#appoint_div').show();
			$('#appointmentfrom_div').hide();
			$('.black_overlay').show();
			$('#callbackdate').hide();
			$('#callbackdate').val("");
			$('#appointment_status').show();
			
        }
    });
	$("#callStatus4").change(function()
	{ 
        if( $(this).is(":checked") )
        { 
        	var call=$('#callStatus4').val();
        	$('#hiddencallStatus').val(call);
        	
        	$('#fixappointment_div').hide();
			$('#banner_div').hide();
			$('#callstatus_div').hide();
			$('#appoint_div').show();
			$('#appointmentfrom_div').hide();
			$('.black_overlay').show();
			$('#callbackdate').hide();
			$('#callbackdate').val("");
			$('#appointment_status').show();
			
        }
    });
	$("#callStatus5").change(function()
			{ 
		        if( $(this).is(":checked") )
		        { 
		        	var call=$('#callStatus5').val();
		        	$('#hiddencallStatus').val(call);
		        	$('#fixappointment_div').hide();
					$('#banner_div').hide();
					$('#callstatus_div').hide();
					$('#appoint_div').show();
					$('#appointmentfrom_div').hide();
					$('.black_overlay').show();
					$('#appointment_status').show();
					$('#callbackdate').show();
					
					
		        }
		    });
	
	$("#callStatus6").change(function()
			{ 
		        if( $(this).is(":checked") )
		        { 
		        	var call=$('#callStatus6').val();
		        	$('#hiddencallStatus1').val(call);
		        
		        	$('#Sales_Closed_Div').hide();
		        	$('#fixappointment_div').hide();
					$('#banner_div').hide();
					$('#callstatus_div').hide();
					$('#appoint_div').show();
					$('#appointmentfrom_div').hide();
					$('.black_overlay').show();
					$('#callbackdate').hide();
					$('#callbackdate').val("");
					$('#appointment_status').hide();
					$('#dialedDiv').show();
					
					
		        }
		    });
			
			$("#callStatus7").change(function()
					{ 
				        if( $(this).is(":checked") )
				        { 
				        	var call=$('#callStatus7').val();
				        	$('#hiddencallStatus2').val(call);
				        
				        	$('#fixappointment_div').hide();
							$('#banner_div').hide();
							$('#callstatus_div').hide();
							$('#appoint_div').show();
							$('#appointmentfrom_div').hide();
							$('.black_overlay').show();
							$('#callbackdate').hide();
							$('#callbackdate').val("");
							$('#appointment_status').hide();
							$('#dialedDiv').hide();
							$('#Sales_Closed_Div').show();
							
							
				        }
				    });
		 $("#callStatus8").change(function()
					{ 
				        if( $(this).is(":checked") )
				        { 
				        	var call=$('#callStatus8').val();
				        	$('#hiddencallStatus').val(call);
				        
				        	

				        	$('#Sales_Closed_Div').hide();
				        	$('#dialedDiv').hide();
				        	$('#fixappointment_div').hide();
							$('#banner_div').hide();
							$('#callstatus_div').hide();
							$('#appoint_div').show();
							$('#appointmentfrom_div').hide();
							$('.black_overlay').show();
							$('#callbackdate').hide();
							$('#callbackdate').val("");
							$('#appointment_status').show();
									
				        }
				    });
	
	
	$(".closeRatingBox").click(function()
	{
			
			$('#fixappointment_div').hide();
			$('#callstatus_div').hide();
			$('#appoint_div').hide();
			$('#appointmentfrom_div').hide();
			$('#appointment_status').fadeOut(1000);
			$('.white_content').fadeOut(1000);
			$('.black_overlay').fadeOut(1000);
			$('#banner_div').hide();	  	
			$('#fixappointment_div').show();
			$('#appoint_div').hide();
			 $("#phoneno").val("");
			 $("#mobileno").val("");
			 $("#mailid").val("");
	
		
	 });
  </script>
  
</head>
<body>
					 <s:if test="%{objAppointmentDTOList.size==0}">
						<div class="callstatus_headding"  style="text-align: center;width:600px;" id="appoint_msg"> <font style="color: red;font-family: georgia;font-weight: bold;">No Appointment Found So Far With This Business Detail.....</font></div>
					</s:if>
					
						<div  class="callstatus"  id="callstatus_div" >
							<div class="callstatusInner">
                                    	<div class="callstatusInner_left">
                                      		 <img src="images/callstatusimg.jpg" width="130" height="38"> 
                                        </div>
                                        
                                         
	                                       
	                                        <div class="callstatusInner_right">
	                							<div class="callstatusInner_right_main">
	                    							<div class="callstatusInner_right_main_left">
	                    								<input type="radio" id="callStatus1" name="callStatus" value="0" class="call">
	                    							</div>
	                    							 <div class="callstatusInner_right_main_right">
	                       								 Answered
	                        						 </div>
	                        					</div>
	                        					<div class="callstatusInner_right_main">
	                    							<div class="callstatusInner_right_main_left">
	                    								<input type="radio"  name="callStatus" value="1" id="callStatus2"  class="call"> 
	                    							</div>
	                    							 <div class="callstatusInner_right_main_right">
	                       								 Rejected
	                        						 </div>
	                        					</div>
	                        					<div class="callstatusInner_right_main">
	                    							<div class="callstatusInner_right_main_left">
	                    								<input type="radio"  name="callStatus" value="2" id="callStatus3"  class="call"> 
	                    							</div>
	                    							 <div class="callstatusInner_right_main_right">
	                       								Not Reachable
	                        						 </div>
	                        					</div>
	                                         	<div class="callstatusInner_right_main">
	                    							<div class="callstatusInner_right_main_left">
	                    								<input type="radio"  name="callStatus" value="3" id="callStatus4"  class="call">
	                    							</div>
	                    							 <div class="callstatusInner_right_main_right">
	                       								Others
	                        						 </div>
	                        					</div>
	                        					<div class="callstatusInner_right_main">
	                    							<div class="callstatusInner_right_main_left">
	                    								<input type="radio"  name="callStatus" value="5" id="callStatus5"  class="call">
	                    							</div>
	                    							 <div class="callstatusInner_right_main_right">
	                       								Call Back
	                        						 </div>
	                        					</div>
	                        					<div class="callstatusInner_right_main">
	                    							<div class="callstatusInner_right_main_left">
	                    								<input type="radio"  name="Dialedcalls" value="6" id="callStatus6"  class="call">
	                    							</div>
	                    							 <div class="callstatusInner_right_main_right">
	                       							Dialed(Not Answered)
	                        						 </div>
	                        					</div>
	                        						                        					
	                        					<div class="callstatusInner_right_main">
	                    							<div class="callstatusInner_right_main_left">
	                    								<input type="radio"  name="SalesClosed" value="7" id="callStatus7"  class="call">
	                    							</div>
	                    							 <div class="callstatusInner_right_main_right">
	                       								Sales Closed
	                        						 </div>
	                        					</div>
	                        					
	                        						<div class="callstatusInner_right_main">
	                    							<div class="callstatusInner_right_main_left">
	                    								<input type="radio"  name="NotRelevant" value="8" id="callStatus8"  class="call">
	                    							</div>
	                    							 <div class="callstatusInner_right_main_right">
	                       								Not Relevant
	                        						 </div>
	                        					</div>
	                                         </div>
	                                         </div>
	                                        
                         </div>
                          <div class="black_overlay"></div>
                           <div id="appointment_status" class="white_content" style="display:none;">
	    						 <a href = "#" class = "closeRatingBox" style="float:right;">
									<img src="http://dialusimages.dialus.com/images/close_4.png" alt="" style="margin-right:-15px; margin-top:-15px;" /></a>
								     	<s:form action="submitCallStatus">
									     	<s:hidden name="objCheckAppointmentDTO.callStatus" id="hiddencallStatus"></s:hidden>
									     	<s:hidden name="objCheckAppointmentDTO.businessMobile" value="%{#request.mobile}"></s:hidden>
									     	<s:hidden name="objCheckAppointmentDTO.businessPhone" value="%{#request.phoneno}"></s:hidden>
									     	<s:hidden name="objCheckAppointmentDTO.businessMail" value="%{#request.mailid}"></s:hidden>
									     		
									     	<s:textarea name="objCheckAppointmentDTO.comments" cssStyle="width:400px;height:70px;" id="call_comments"  label="Comments" ></s:textarea>
									     	<s:textfield  name="objCheckAppointmentDTO.appointmentDate" cssClass="datepicker" readonly="true"  label="Call Back Date "  id="callbackdate"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
								     	<s:submit id="comments_but"></s:submit>
								     	
								     	</s:form>
	     
	     				 </div>  
	     				 
	     				 <div id="dialedDiv" class="white_content" style="display:none;">
	     			 
	    						 <a href = "#" class = "closeRatingBox" style="float:right;">
									<img src="http://dialusimages.dialus.com/images/close_4.png" alt="" style="margin-right:-15px; margin-top:-15px;" /></a>
								     	<s:form action="submitCallStatus1">
									     	<s:hidden name="objCheckAppointmentDTO.callStatus" id="hiddencallStatus1"></s:hidden>
									     	<s:hidden name="objCheckAppointmentDTO.businessMobile" value="%{#request.mobile}"></s:hidden>
									     	<s:hidden name="objCheckAppointmentDTO.businessPhone" value="%{#request.phoneno}"></s:hidden>
									     	<s:hidden name="objCheckAppointmentDTO.businessMail" value="%{#request.mailid}"></s:hidden>
									     		
									     	<s:textarea name="objCheckAppointmentDTO.comments" cssStyle="width:400px;height:70px;" id="call_comments"  label="Comments" ></s:textarea>
									     	<s:textfield  name="objCheckAppointmentDTO.appointmentDate" cssClass="datepicker1" readonly="true"  label="Dialed "  id="datepicker1"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
								     	<s:submit id="comments_but1"></s:submit>
								     	
								     	</s:form>
	     
	     				</div> 
	     				
	     				
	     					<div id="Sales_Closed_Div" class="white_content" style="display:none;">
	     			 
	    						 <a href = "#" class = "closeRatingBox" style="float:right;">
									<img src="http://dialusimages.dialus.com/images/close_4.png" alt="" style="margin-right:-15px; margin-top:-15px;" /></a>
								     	<s:form action="submitCallStatus2">
									     	<s:hidden name="objCheckAppointmentDTO.callStatus" id="hiddencallStatus2"></s:hidden>
									     	<s:hidden name="objCheckAppointmentDTO.businessMobile" value="%{#request.mobile}"></s:hidden>
									     	<s:hidden name="objCheckAppointmentDTO.businessPhone" value="%{#request.phoneno}"></s:hidden>
									     	<s:hidden name="objCheckAppointmentDTO.businessMail" value="%{#request.mailid}"></s:hidden>
									     		
									     	<s:textarea name="objCheckAppointmentDTO.comments" cssStyle="width:400px;height:70px;" id="call_comments"  label="Comments" ></s:textarea>
									     	<s:textfield  name="objCheckAppointmentDTO.appointmentDate" cssClass="datepicker1" readonly="true"  label="Dialed "   cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
								     	  	<s:submit id="comments_but2"></s:submit>
								     	
								     	</s:form>
	     
	     				</div>
	     				         
				
					<div id="appointmentfrom_div" style="display:none;">
			      	<s:form action="storeAppointmentDetails" theme="simple">
					    	<s:hidden name="usertype" value="user"></s:hidden>
					    	<s:hidden name="appointmentId" value="%{objCheckAppointmentDTO.appointmentId}"></s:hidden>
					        <div class="main_div_middle">
					         	<div class="main_div_middle_textfields_main" style="text-align:center;background: #008ABB;width: 100%;color:#fff;padding: 8px 0px;margin-top:-10px;border-top-left-radius:10px;border-top-right-radius:10px;">
								           Appointments
								</div>
                                
                                <div class="appointmentform">
                                <div class="appointmentform_left1">
                                	
                                	
                                	<div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Business Name
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.businessName" maxlength="135" id="businessName" value="%{objCheckAppointmentDTO.businessName}" cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                
                                    
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                      <b style="color:red">&lowast;</b> Business State
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.businessState" maxlength="45"  value="%{objCheckAppointmentDTO.businessState}"  id="businessState"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                      <b style="color:red">&lowast;</b> Business City
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.businessCity" maxlength="45"  value="%{objCheckAppointmentDTO.businessCity}"  id="businessCity"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Keywords :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                  <input type="text" id="country123" name="country" style="width:177px"/>
	                              <input type="button" value="add" id="add123"/>
                                        </div>
                                 </div>
                                    
                               <div class="appointmentform_left">
                                    	    
                                     <div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>
                                     </div>
                                                  	
                                    	
                                    	                                        
                                        <div class="appointmentform_left_right">
                                        <div id="result_div"></div>
                                 <p style="color:#ff6600" id="p_id"></p>  
        	                     <s:hidden name="keyword" id="keyword_field"  accesskey="b" />
                                        </div>
                               </div>
                                    
                                    
                                    
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Business Area
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.businessArea" maxlength="45"  value="%{objCheckAppointmentDTO.businessArea}"  id="businessArea"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Business Mobile 
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.businessMobile" maxlength="10"  value="%{objCheckAppointmentDTO.businessMobile}"   id="businessMobile"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                      Business Phone
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.businessPhone" maxlength="15"  value="%{objCheckAppointmentDTO.businessPhone}"  id="businessPhone"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left" style="display: none;">
                                    	<div class="appointmentform_left_left">
                                       Business Phone1
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.businessPhone1" maxlength="15"  value="%{objCheckAppointmentDTO.businessPhone1}"  id="businessPhone1"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                      <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       Business Email
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.businessMail" maxlength="50"  value="%{objCheckAppointmentDTO.businessMail}"  id="businessMail"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    </div>
                                  <div class="appointmentform_right">
                                  <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Contact Number
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.contactNumber" maxlength="15" value="%{objCheckAppointmentDTO.contactNumber}"  id="contactNumber"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Contact Person
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.contactPerson" maxlength="43"  value="%{objCheckAppointmentDTO.contactPerson}"  id="contactPerson"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <%-- <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       Appointment State
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.appointmentState" maxlength="45"  value="%{objCheckAppointmentDTO.appointmentState}"   id="appointmentState"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       Appointment City
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.appointmentCity" maxlength="45"  value="%{objCheckAppointmentDTO.appointmentCity}"   id="appointmentCity"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       Appointment Place
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.appointmentPlace" maxlength="50"   value="%{objCheckAppointmentDTO.appointmentPlace}"   id="appointmentPlace"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div> --%>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left" style="width:230px;text-align: right;">
                                       			<b style="color:red;white-space: nowrap;">Check Here if Call Back</b>
                                        </div>
                                        
                                        <div class="appointmentform_left_right" style="width:20px;">
                                        <input type="checkbox" id="check_callback"/>
                                        </div>
                                    </div>
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left" style="width:230px;text-align: right;">
                                       			<b style="color:red;white-space: nowrap;">Check Here if Save Data</b>
                                        </div>
                                        
                                        <div class="appointmentform_left_right" style="width:20px;">
                                        <input type="checkbox" id="check_save"/>
                                        </div>
                                    </div>
                                      <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left" style="width:230px;text-align: right;">
                                       			<b style="color:red;white-space: nowrap;">Check Here if Fix Appointment</b>
                                        </div>
                                        
                                        <div class="appointmentform_left_right" style="width:20px;">
                                        <input type="checkbox" id="check_fix" />
                                        </div>
                                    </div>
                                    <div id="callbackdate_div_date" style="display:none;">
                                    <div class="appointmentform_left">
	                                    	<div class="appointmentform_left_left">
	                                       <b style="color:red">&lowast;</b>CallBack Date
	                                        </div>
	                                        
	                                        <div class="appointmentform_left_right">
	                                        <s:textfield  name="objCheckAppointmentDTO.callBackDate" cssClass="datepicker" readonly="true" value="%{objCheckAppointmentDTO.callBackDate}"  id="callBackDate"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
	                                        </div>
	                                    </div>
                                    </div>
                                    <div id="callback_div" style="display: none;">
	                                     <div class="appointmentform_left">
	                                    	<div class="appointmentform_left_left">
	                                       <b style="color:red">&lowast;</b>Appointment Date
	                                        </div>
	                                        
	                                        <div class="appointmentform_left_right">
	                                        <s:textfield  name="objCheckAppointmentDTO.appointmentDate" cssClass="datepicker" readonly="true" value="%{objCheckAppointmentDTO.appointmentDate}"  id="appointmentDate"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
	                                        </div>
	                                    </div>
	                                     <div class="appointmentform_left">
	                                    	<div class="appointmentform_left_left">
	                                       <b style="color:red">&lowast;</b>Appointment Time
	                                        </div>
	                                        
	                                        <div class="appointmentform_left_right">
	                                        <s:textfield  name="objCheckAppointmentDTO.appointmentTime" readonly="true"  value="%{objCheckAppointmentDTO.appointmentTime}"   id="appointmentTime"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
	                                        </div>
	                                    </div>
                                    </div>
                                    
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        Comments
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textarea  name="objCheckAppointmentDTO.comments" value="%{objCheckAppointmentDTO.comments}"  id="comments"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textarea>
                                        </div>
                                    </div>
                                    
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       Call Status
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        
                                      
                                        Answered<input type="radio" checked="checked"  name="objCheckAppointmentDTO.callStatus" value="0">
                                        
                                      
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                 
                                <div class="appointmentform" style="margin-top:15px;">
                                <s:if test="%{objCheckAppointmentDTO.businessName!=null && objCheckAppointmentDTO.businessName!=''}">					   
                                 <input type="button" onclick="javascript:history.back();"  value="Back" style="color:#fff; font-weight:bold; background-color:#008abb; text-align:center; padding:5px 0 5px 0; width:328px; border:none; margin:0 0 0 250px;" />
                                </s:if>
                                <s:else>
                                 	<s:submit   value="Fix Appointment" id="fix_appointment_but" cssStyle="color:#fff; font-weight:bold; background-color:#008abb; text-align:center; padding:5px 0 5px 0; width:328px; border:none; margin:0 0 0 250px;" />
                                </s:else>
                                </div>
                                
                                
                                
                                
					        </div>
					     
					     </s:form>
			 </div>
				
</body>
</html>