<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css/jquery-ui123.css" />	 
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script src="js/jquery-min.js"></script>
<link rel="stylesheet" href="css/jquery-ui123.css" />	 
 <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui.min.js"></script>
 <script>
  $(function(){
    $( ".datepicker" ).datepicker();
    $(".datepicker").datepicker("setDate", new Date);
    $("#click").click(function(){
    	 $("#datewise_sms").load("DateWiseSmsDetails.action?fdate="+$("#datepicker3").val()+"&tdate="+$("#datepicker4").val());
    	 $("#sms_count").hide();
    });
   
  });
  </script>
</head>
<body>
<p style="color: red;font-size: 18px;text-align: center;">SMS Details</p>
<div style="color:red;size: 3px;"  id="sms_count">Today SMS Count::<s:property value="today_smscount"/></div>
<div id="datewise_sms">
<div class="userappointments_line"></div>

<div class="userappointments_line"></div>
<div class="userappointments_line" style="margin-top:10px;"></div>

<div>
 
                    <div style="width:100px;margin-left: 20px;">
                     Enter From Date: 
                     </div>
                     <div>
                     <s:textfield   cssClass="datepicker" readonly="true"  name="fdate" id="datepicker3"  />
                     </div>
                     
                     <div style="width:100px;margin-right:392px;margin-top:-19px;">
                     Enter To Date: 
                      </div>
                      <div style="width:100px;margin-right:392px;margin-top:-19px;">
                      <s:textfield  name="tdate" id="datepicker4" readonly="true"  cssClass="datepicker" />
                      </div>
                      <div style="width:24px;margin-top:18px;">
                         <s:submit id="click"/>
                         </div>
         
                    

</div>

<div class="userappointments">
	<div class="userbusinessname" style="width:21px">
      id
    </div>
    <div class="user_appointmentplace" style="width:165px">
    Business Name
    </div>
    <div class="user_appointmenttime" style="width:120px">
    Mobile
    </div>
    <div class="user_appointmentfixedby" style="width:218px;">
    SMS
    </div>
    <div class="user_appointmentfixedby" style="width:135px;">
    Date and Time
    </div>
</div>

<div class="userappointments_line"></div>
<s:if test="%{objArrayList.size!=0}">
<s:iterator value="objArrayList" id="a">
<div class="userappointments_inner1">
   <div class="userbusinessname" style="width:21px">
   
    <s:property value="#a.sms_id"/>

  </div>
    <div class="user_appointmentplace" style="width:165px;">
    <s:property value="#a.business_name"/>
    </div>
    <div class="user_appointmenttime" style="width:120px">
    <s:property value="#a.mobile"/>
    </div>
    
    
    <div class="user_appointmenttime" style="width:218px">
    <s:property value="#a.msgContent"/>
    </div>
   
    <div class="user_appointmenttime" style="width:135px">
    <s:property value="#a.st"/>
    </div>
</div>

</s:iterator>
</s:if>
<s:else>
<div class="userappointments_inner1" style="color:red;font-family: georgia;font-weight: bold">Sorry No Records Are  Available....</div>
</s:else>
</div>
</body>
</html>