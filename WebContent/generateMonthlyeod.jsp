<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />

</head>
<body>
<div class="userappointments_line"></div>
<div class="userappointments_inner">
<img src="images/appointment_missed.png" width="30" height="30" style="float:left; margin-left:25px;">
<span style="float:left; margin-top:3px; margin-left:10px;font-family:georgia;">Monthly  Report</span>
<span style="float:right; margin-top:3px; margin-left:10px;font-family:georgia;">
<s:if test="%{objCheckAppointmentDTOList.size!=0}"><a href="sendEod.action?type=monthly">
<input value="Send" type="button"></a></s:if></span> </div>
<div class="userappointments_line"></div>
<div class="userappointments_line" style="margin-top:10px;"></div>

<div class="userappointments">
	<div class="userbusinessname" style="width:140px;">
    Business Name
    </div>
    <div class="user_appointmentplace">
    Appointment Place
    </div>
      
    <div class="user_appointmenttime">
    Appointment Time
    </div>
    <div class="user_appointmentfixedby">
   Business Dev Officer
    </div>
 
    <div class="user_appointmentplace" style="width:140px;">
    Status
    </div>
    
</div>

<div class="userappointments_line"></div>
<s:if test="%{objCheckAppointmentDTOList.size!=0}">
<!-- overflow-y: scroll;overflow-x: hidden; height:450px; width: 788px; -->
<div style="height: 300px; overflow-y: scroll;overflow-x: hidden; width: 720px; float:left" >
<s:iterator value="objCheckAppointmentDTOList" id="a">
<div class="userappointments_inner1">
<div class="userbusinessname" style="width:140px;">
    <s:property value="#a.businessName"/>

  </div>
    <div class="user_appointmentplace">
    <s:property value="#a.appointmentPlace"/>
    </div>
     
    <div class="user_appointmenttime">
    <s:property value="#a.appointmentDateTime"/>
    </div>
    <div class="user_appointmentfixedby">
	    <s:if test="%{#a.salesPersonName!=null && #a.salesPersonName!=''}">
	   		 <s:property value="#a.salesPersonName"/>
	    </s:if>
	    <s:else>
	   		 ........
	    </s:else>
    </div>
     
    <div class="user_appointmenttime" style="width:140px;text-overflow: ellipsis;">
      <s:if test="%{#a.status==2}">
    	Completed
    </s:if>
    <s:if test="%{#a.status==3}">
    	Pending
    </s:if>
    <s:if test="%{#a.status==4}">
    	Rejected
    </s:if>
    </div>

</div>

</s:iterator>
</div>
</s:if>
<s:else>
<div class="userappointments_inner1" style="color:red;font-family: georgia;font-weight: bold">Sorry No Records Are  Available....</div>
</s:else>
</body>
</html>