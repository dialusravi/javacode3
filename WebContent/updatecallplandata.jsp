<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<s:form action="checkAppointment" theme="simple">
					    	<s:hidden name="usertype" value="telecaller"></s:hidden>
					        <div class="main_div_middle">
					         	<div class="main_div_middle_textfields_main" style="text-align:center;background: #008ABB;width: 100%;color:#fff;padding: 8px 0px;margin-top:-10px;border-top-left-radius:10px;border-top-right-radius:10px;">
								         Search (or) Add  Data
								</div>
						        <div class="main_div_middle_textfields" style="float:left; width:600px;">
								       
							           <div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:180px; text-align:right; margin-left:60px;">
								       	   		<b style="color:red">&lowast;</b>Enter Mobile Number :
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								           		  <s:textfield  name="objCheckAppointmentDTO.businessMobile" maxlength="10"  id="mobileno" value="%{objBusinessDetailDTO.mobile}" cssStyle="width:280px; border:1px solid #008abb; padding:11px 0 10px 3px" ></s:textfield>
								            </div>
							           </div>
							               <div class="main_div_middle_textfields_main" style="width:600px; text-align:center; margin:0; font-size:13px">
                                           (Or)
                                           </div>
							           <div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:180px; text-align:right; margin-left:60px;">
								       	    	<b style="color:red">&lowast;</b>Enter Phone Number :
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								            	<s:textfield  name="objCheckAppointmentDTO.businessPhone"  maxlength="15"  id="phoneno" value="%{objBusinessDetailDTO.ph1}" cssStyle="width:280px; border:1px solid #008abb; padding:11px 0 10px 3px"  ></s:textfield>
								            </div>
							           </div>
							           <div class="main_div_middle_textfields_main" style="width:600px; text-align:center; margin:0; font-size:13px">
                                           (Or)
                                           </div>
							             <div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:180px; text-align:right; margin-left:60px;">
								       	    	<b style="color:red">&lowast;</b>Enter Email Id :
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								            	<s:textfield  name="objCheckAppointmentDTO.businessMail"  maxlength="60"  id="mailid"  cssStyle="width:280px; border:1px solid #008abb; padding:11px 0 10px 3px"  ></s:textfield>
								            </div>
							           </div>
							          
							           <div class="main_div_middle_textfields_main" style="width:340px; margin-left:230px;">
							          			 <input type="submit" id="retrieveAppointment1"  value="Retrive Data" style="color:#fff; font-weight:bold; background-color:#008abb; text-align:center; padding:5px 0 5px 0; width:328px; border:none" />
							           </div>
							           
							           <div style="clear:both"></div>
						            </div>
						            
						            
						        </div>
					     
					     </s:form>
</body>
</html>