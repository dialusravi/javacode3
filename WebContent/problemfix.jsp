 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="js/jquery-min.js"></script>
<link href="http://dialuscss.dialus.com/dialus.css" rel="stylesheet" type="text/css" media="all"/>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">

.newmyprofile_lastupdate{ width:201px; height:auto; float:left; border:1px solid #ccc; padding:8px 0 8px 0}
.newmyprofile_lastupdate_top{ width:165px; height:auto; float:left; margin:5px 0 0 15px; color:#ff6600;}
.newmyprofile_lastupdate_top_review{ width:52px; height:auto; float:left; padding:5px; background-color:#018dbf; color:#fff; font-weight:bold}
.newmyprofile_maincontentpart_right{ width:725px; height:auto; float:right; border:1px solid #d6d6d6; padding:3px 3px 3px 3px}
.newmyprofile_userfrofile{ width:725px; height:auto; float:right; background-color:#d3d3d3;  padding:2px; padding:8px 0 8px 0; font-size:12px; color:#fff;}

.newmyprofile_userfrofile1{ width:725px; height:auto; float:right; background-color:#f1fafd;  padding:2px; padding:6px 0 6px 0; margin-top:12px;} 

.newmyprofile_userfrofile_businessname{ width:70px; height:auto; float:left; margin-left:10px; }
.newmyprofile_userfrofile_businessCategory{ width:70px; height:auto; float:left; margin-left:15px}
.newmyprofile_userfrofile_businessCity{ width:70px; height:auto; float:left; margin-left:15px}

.newmyprofile_userfrofile_businessusername{ width:70px; height:auto; float:left; margin-left:15px;}

.new_changepassword_main{ width:574px; height:auto; background-color:#fff; border:1px solid #dfdede; border-radius: 20px 20px 20px 20px; margin-top:160px;}
 

</style>

</head>
<body>
<span style="color:blue;"> Fixed Count:<s:property value="%{objTargetDTO.fixcount}"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Unfixed Count:<s:property value="%{objTargetDTO.unfixcount}"/></span>
<div class="newmyprofile_userfrofile" style="background: #008abb" >

      <div class="newmyprofile_userfrofile_businessname" style="width:50px;">
       S.NO
        </div>
      	<div class="newmyprofile_userfrofile_businessname" style="width:120px;">
        Date
        </div>
        
        <div class="newmyprofile_userfrofile_businessCategory" style="width:120px;">
      Subject
        </div>
        
       <div class="newmyprofile_userfrofile_businessusername" style="width:160px;">
     Description
        </div>
         
         <div class="newmyprofile_userfrofile_businessusername" style="width:70px;">
       Status
        </div>
      
      
      </div>
<s:if test="objArrayList.size>0">
<s:iterator value="objArrayList" id="a" status="Status">
<div class="newmyprofile_userfrofile1" id="mydiv">
     
     
      <div class="newmyprofile_userfrofile_businessname"    style="width:50px;overflow: hidden;">
  
  			
	   		 <s:property value="%{#Status.count}"/>
	   
       </div>
        
      	<div class="newmyprofile_userfrofile_businessname"  title="<s:property value="#a.date"/>"  style="width:120px;overflow: hidden;">
      	
  			<s:property value="#a.date"/>
        </div>
        
        <div class="newmyprofile_userfrofile_businessCategory" title="<s:property value="#a.subject"/>" style="width:120px;overflow: hidden;">
       
        <s:property value="#a.subject"/>
        	
        </div>
    
        <div class="newmyprofile_userfrofile_businessusername" title="<s:property value="#a.description"/>" style="width:160px;overflow: hidden;">
      	
      		<s:property value="#a.description"/>
      		
        </div>
     
        <div class="newmyprofile_userfrofile_businessusername"  style="width:70px;overflow: hidden;">
         <s:if test="%{#a.username==null || username==''}">
                		<s:if test="%{#a.status==0 || status==null}">
      		   				  <span style="color:red;">Not Fixed</span>
      		           </s:if>
      		     	   <s:else>
      		                  <span style="color:red;">  Fixed </span>
      		       	  </s:else>
          </s:if>
         <s:else>
                    <s:if test="%{#a.status==0 || status==null}">
      		                 <span style="color:red;cursor:pointer;" id="venki" title="Not Fixed" onclick="forfixed('<s:property value="#a.id"/>','<s:property value="#a.username"/>');">Not Fixed</span>
      		        </s:if>
      		        <s:elseif test="%{#a.status==1}">
      		                  <span style="color:red;">Fixed </span>
      		        </s:elseif>
         </s:else>
         </div>
          
         <s:if test="%{#a.username!=null || username!=''}">
        <div class="newmyprofile_userfrofile_businessusername" style="width:80px;overflow: hidden;">
          <s:property value="#a.username"/>
      	  </div>
         </s:if>
         
    
        
</div>


 </s:iterator>
</s:if>
<s:else>
<div class="userappointments_inner1" style="color:red;font-family: georgia;font-weight: bold">Sorry No Records Are  Available....</div>
</s:else>
   
</body>
</head>
</html>