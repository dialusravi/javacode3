<%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sales Tracking</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />
<script src="js/jquery-min.js"></script>
<link rel="stylesheet" href="css/jquery-ui123.css" />	 
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
 <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/redmond/jquery-ui.css" />

<!-- Plugin files below -->
<link rel="stylesheet" type="text/css" href="css/jquery.ptTimeSelect.css" />
<script type="text/javascript" src="js/jquery.ptTimeSelect.js"></script> 
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<link href="http://dialuscss.dialus.com/dialus.css" rel="stylesheet" type="text/css" media="all"/>
<script type="text/javascript" src="js/salesTrackingValid.js"></script>
<script>
function getContactDetails()
{
	var salesPersonName=$('#salesPersonName').val();
	
	$('#salesPersonContactDetails').load("getContactDetailsAction.action?salesPersonName="+salesPersonName);
	loadContactDetails();
	
}
function loadContactDetails()
{
	setTimeout(function () { loadContactDetails1(); }, 200);
}
function loadContactDetails1()
{
	var salesPersonContactNo =$('#salesPersonContactNo1').val();
	var salesPersonArea =$('#salesPersonArea1').val();
	var salesPersonCity =$('#salesPersonCity1').val();
	var salesPersonState =$('#salesPersonState1').val();
	$('#salesPersonContactNo').val(salesPersonContactNo);
	$('#salesPersonArea').val(salesPersonArea);
	$('#salesPersonCity').val(salesPersonCity);
	$('#salesPersonState').val(salesPersonState);
}
	
	jQuery(function(){
		 
		
			
		$(".keyword_id").autocomplete("AutoCompleteAction.action");
		 $('#businessState').autocomplete('CityautoCompleteAction.action?variable=state');
			$('#businessCity').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businessState: function () {
				                return $("#businessState").val();
				            },
				            
				            
				            variable: function () {
				                return "city";
				            }
				
				        }
					}
			
			
			);
			
			/*area starts here  */
			
			
			
			/*Start of area auto complete  */
		$('#businessArea').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businesCity: function () {
				                return $("#businessCity").val();
				            },
				            
				            
				            variable: function () {
				                return "area";
				            }

				        }
					}
			
			
			);
			/* end of area autocomplete */
			
			
			/* START of served area autocomplete   */
			
			
			
		
			/* End of SERVED AREA AUTOCOMPLETE */
			
			
		$('#appointmentState').autocomplete('CityautoCompleteAction.action?variable=state');
		$('#appointmentCity').autocomplete('CityautoCompleteAction.action',
				
				{
			  extraParams:
				
			        {
				  businessState: function () {
			                return $("#appointmentState").val();
			            },
			            
			            
			            variable: function () {
			                return "city";
			            }
			
			        }
				}
		
		
		);
		
		/*area starts here  */
		
		
		
		/*Start of area auto complete  */
	$('#appointmentPlace').autocomplete('CityautoCompleteAction.action',
				
				{
			  extraParams:
				
			        {
				  businesCity: function () {
			                return $("#appointmentCity").val();
			            },
			            
			            
			            variable: function () {
			                return "area";
			            }

			        }
				}
		
		
		);
		/* end of area autocomplete */
		
		
		/* START of served area autocomplete   */
		
			
		});
	</script>
<script type="text/javascript">
        $(document).ready(function()
        {
        	  $('#appointmentTime').attr("placeholder", "Click This For Time");
            // find the input fields and apply the time select to them.
            $('#appointmentTime').ptTimeSelect();
            
            $('.datepicker').datepicker({minDate: 0});
        });
    </script> 
     <script>
  $(function()
  {
    $( ".datepicker" ).datepicker();
    $(".datepicker").datepicker("setDate", new Date);
    
  });
  </script> 


<script type="text/javascript">
function date_time(id)
{
        date = new Date;
        year = date.getFullYear();
        month = date.getMonth();
        months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'Jully', 'August', 'September', 'October', 'November', 'December');
        d = date.getDate();
        day = date.getDay();
        days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
        h = date.getHours();
        if(h<10)
        {
                h = "0"+h;
        }
        m = date.getMinutes();
        if(m<10)
        {
                m = "0"+m;
        }
        s = date.getSeconds();
        if(s<10)
        {
                s = "0"+s;
        }
        result =  ''+d+' '  +months[month]+'  '+year+' -  '+h+':'+m+':'+s;
        document.getElementById(id).innerHTML = result;
        setTimeout('date_time("'+id+'");','1000');
        return true;
}
</script>
		
</head>

<body>
<div id="appointmentform_div"></div>
<div id="salesPersonContactDetails"></div>
<div class="top_bg">
	<div class="topbg_inner">
   	  <div class="topbg_inner_left">
        	<div class="topbg_inner_left_top">
            	SALES TRACKING
            </div>
            <div class="topbg_inner_left_bottom">
             <span id="date_time"></span>
            <script type="text/javascript">window.onload = date_time('date_time');</script>
          </div>
      </div>
      <div class="topbg_inner_right">
        	<div class="topbg_inner_right_left">
       	  <img src="images/dialus_small.png" width="28" height="27" /> </div>
            <div class="topbg_inner_right_right">
            9666 888 888
            </div>
      </div>
    </div>
</div>
<div style="clear:both"></div>
<div class="wrapper">
<div class="logopart">
	<div class="logopart_left">
    <img src="images/logo.jpg" width="225" height="90" /> 
    </div>
    <!-- <div class="logopart_right">
     
	
    	<ul>
        
        <li>About Us</li>
         <li>|</li>
        <li>Services</li>
         <li>|</li>
        <li>Gallery</li>
          <li>|</li>
        <li>Products</li>
          <li>|</li>
        <li>Contact Us</li>
        </ul>
    </div> -->
</div>

<div class="contentpart">
	<div class="content_left">
    	
        <div id="login_links">
	         <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" href="adminHome.jsp" id="home_link" >Home</a>
	        		 
	        	</div>
	       </div>
      </div>
      
    </div>
    <div class="content_right">
    		 <s:if test="%{objSalesPersonsList.size()!=0}">
					
						<s:form action="allotAppointment" theme="simple">
					    	<s:hidden name="usertype" value="user"></s:hidden>
					    	<s:hidden name="todaydate" value="%{append_todaydate}" />
					    	<s:hidden name="objCheckAppointmentDTO.appointmentId" value="%{objCheckAppointmentDTO.appointmentId}"></s:hidden>
					    
					    	
					        <div class="main_div_middle">
					         	<div class="main_div_middle_textfields_main" style="text-align:center;background: #008ABB;width: 100%;color:#fff;padding: 8px 0px;margin-top:-10px;border-top-left-radius:10px;border-top-right-radius:10px;">
								           Appointments:<s:property value="tellcallername"/>
								</div>
                                
                                <div class="appointmentform">
                                <div class="appointmentform_left1">
                                	<div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Business Name
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.businessName" readonly="true"  maxlength="130" id="businessName" value="%{objCheckAppointmentDTO.businessName}" cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                   <%--  <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Business Category
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.businessCategory"  readonly="true"   maxlength="140"  value="%{objCheckAppointmentDTO.businessCategory}"  id="username"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Sub Category
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.businesssubcategory"  readonly="true"  maxlength="140"  value="%{objCheckAppointmentDTO.businesssubcategory}"  id="businessSubCategory"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Other Category
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.businessOtherCategory"  readonly="true"  maxlength="130"  value="%{objCheckAppointmentDTO.businessOtherCategory}"  id="businessOtherCategory"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div> --%>
                                 
                                 
                               
                                 <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Keyword1
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.keyword1"     maxlength="45"  value="%{objCheckAppointmentDTO.keyword1}"  cssClass="keyword_id"   cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Keyword2
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.keyword2"    maxlength="45"  value="%{objCheckAppointmentDTO.keyword2}"  cssClass="keyword_id"   cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Keyword3
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.keyword3"     maxlength="45"  value="%{objCheckAppointmentDTO.keyword3}"  cssClass="keyword_id"   cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Keyword4
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.keyword4"     maxlength="45"  value="%{objCheckAppointmentDTO.keyword4}" cssClass="keyword_id"   cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Keyword5
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.keyword5"   maxlength="45"  value="%{objCheckAppointmentDTO.keyword5}"  cssClass="keyword_id"   cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                              
                                 
                                    
                                    
                                    
                                    
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Business State
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.businessState"  readonly="true"   maxlength="45"  value="%{objCheckAppointmentDTO.businessState}"  id="businessState"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                      <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Business City
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.businessCity"  readonly="true"   maxlength="45"  value="%{objCheckAppointmentDTO.businessCity}"  id="businessCity"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Business Area
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.businessArea"  readonly="true"   maxlength="45"  value="%{objCheckAppointmentDTO.businessArea}"  id="businessArea"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Business Mobile 
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.businessMobile" readonly="true"  maxlength="10"   value="%{objCheckAppointmentDTO.businessMobile}"   id="businessMobile"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                      Business Phone
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.businessPhone" readonly="true"  maxlength="10"   value="%{objCheckAppointmentDTO.businessPhone}"  id="businessPhone"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       Business Phone1
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.businessPhone1"  readonly="true"  maxlength="10"  value="%{objCheckAppointmentDTO.businessPhone1}"  id="businessPhone1"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                      <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       Business Email
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.businessMail" readonly="true"  maxlength="45"   value="%{objCheckAppointmentDTO.businessMail}"  id="businessMail"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                     <!-- <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                      View SalesPerson Location
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                      <input type="button" id="location_button" value="Load Location" onclick="loadMap()">
                                      <div id="map_div"></div>
                                        </div>
                                    </div> -->
                                    </div>
                                  <div class="appointmentform_right">
                                  <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Contact Number
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.contactNumber" readonly="true"  maxlength="10"  value="%{objCheckAppointmentDTO.contactNumber}"  id="contactNumber"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Contact Person
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.contactPerson" readonly="true"  maxlength="43"  value="%{objCheckAppointmentDTO.contactPerson}"  id="contactPerson"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                     <%-- <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       Appointment State
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.appointmentState" readonly="true"  maxlength="45"  value="%{objCheckAppointmentDTO.appointmentState}"   id="appointmentState"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       Appointment City
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.appointmentCity"  readonly="true"   maxlength="45"  value="%{objCheckAppointmentDTO.appointmentCity}"   id="appointmentCity"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       Appointment Place
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.appointmentPlace"  readonly="true"  maxlength="45"  value="%{objCheckAppointmentDTO.appointmentPlace}"   id="appointmentPlace"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div> --%>
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Appointment Date
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.appointmentDate"  readonly="true"    value="%{objCheckAppointmentDTO.appointmentDate}" cssClass="datepicker"  id="appointmentDate"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Appointment Time
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.appointmentTime"  readonly="true"    value="%{objCheckAppointmentDTO.appointmentTime}"   id="appointmentTime"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Business Dev Officer Name
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                     
                                     	   <select  onchange="getContactDetails()" name="objCheckAppointmentDTO.salesPersonName"  id="salesPersonName" style="width:150px; border:1px solid #008abb; padding:5px 0" >
                                     	   <option value="123">-----Select-----</option>
                                     	   <s:iterator value="objSalesPersonsList">
                                     	   <option value="<s:property />"><s:property /></option>
                                     	   </s:iterator>
                                     	   </select>
                                        </div>
                                    </div>
                                      <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Business Dev Officer State
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                     <s:textfield  name="objCheckAppointmentDTO.salesPersonState" readonly="true"   id="salesPersonState"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Business Dev Officer City
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                     <s:textfield  name="objCheckAppointmentDTO.salesPersonCity" readonly="true"   id="salesPersonCity"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Business Dev Officer Area
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                     <s:textfield  name="objCheckAppointmentDTO.salesPersonArea"  readonly="true"   id="salesPersonArea"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Business Dev Officer Contact No
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                     <s:textfield  name="objCheckAppointmentDTO.salesPersonContactNo" readonly="true"   id="salesPersonContactNo"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        Comments
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textarea  name="objCheckAppointmentDTO.comments" value="%{objCheckAppointmentDTO.comments}"  readonly="true"   id="comments"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textarea>
                                        </div>
                                    </div>
                                    
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       Call Status
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:if test="%{objCheckAppointmentDTO.callStatus==0}">
                                       		 <s:textfield  name="objCheckAppointmentDTO.callStatus" value="Answered"  readonly="true"   id="callStatus"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </s:if>
                                        <s:elseif test="%{objCheckAppointmentDTO.callStatus==1}">
                                        	<s:textfield  name="objCheckAppointmentDTO.callStatus" value="Rejected" readonly="true"  id="callStatus"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </s:elseif>
                                        <s:elseif test="%{objCheckAppointmentDTO.callStatus==2}">
                                        	<s:textfield  name="objCheckAppointmentDTO.callStatus" value="Not Reachble" readonly="true"  id="callStatus"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </s:elseif>
                                        <s:elseif test="%{objCheckAppointmentDTO.callStatus==3}">
                                        	<s:textfield  name="objCheckAppointmentDTO.callStatus" value="Others" readonly="true"  id="callStatus"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </s:elseif>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                 
                                <div class="appointmentform" style="margin-top:15px;">
                               
                               
                                 	<s:submit  id="allot_appointment"  value="Allot Appointment" cssStyle="color:#fff; font-weight:bold; background-color:#008abb; text-align:center; padding:5px 0 5px 0; width:328px; border:none; margin:0 0 0 250px;" />
                               
                                </div>
                                
                                
                                
                                
					        </div>
					     
					     </s:form>
					
					
					
					
					
					</s:if>
					     <s:else>
					    
							<div style="text-align: center;width:600px;float:none;color: red;font-family: georgia;font-weight: bold;" id="success_msg"> <s:property value="%{#request.success_msg}"/></div>
								
					     </s:else>
    		 
	     
  </div>
</div>
</div>
<div class="fotter">
	<div class="fotter_inner">
     All Rights Reserved - Copyright ｩ Dialus.com 2010-2013
    </div>
</div>
</body>
</html>
