<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <%@ taglib uri="/struts-tags" prefix="s" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
  <link href="css/css.css" rel="stylesheet" type="text/css" media="all" />
  <link rel="stylesheet" href="css/jquery-ui123.css" />	 
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui.min.js"></script>
 
 
  <script>
  $(function() {
    $( ".datepicker" ).datepicker();
    $(".datepicker").datepicker("setDate", new Date);
  });
  </script>
	  
<style>
		.ac_results {
			padding: 0px;
			border: 1px solid #84a10b;
			background-color: #84a10b;
			overflow: hidden;
		}
		
		.ac_results ul {
			width: 100%;
			list-style-position: outside;
			list-style: none;
			padding: 0;
			margin: 0;
		}
		
		.ac_results li {
			margin: 0px;
			padding: 2px 5px;
			cursor: default;
			display: block;
			color: #fff;
			font-family:verdana;
			/* 
			if width will be 100% horizontal scrollbar will apear 
			when scroll mode will be used
			*/
			/*width: 100%;*/
			font-size: 12px;
			/* 
			it is very important, if line-height not setted or setted 
			in relative units scroll will be broken in firefox
			*/
			line-height: 16px;
			overflow: hidden;
		
		}
		
		.ac_loading {
			background: white url('../images/indicator.gif') right center no-repeat;
		}
		
		.ac_odd {
			background-color: #84a10b;
			color: #ffffff;
		}
		
		.ac_over {
			background-color: #5a6b13;
			color: #ffffff;
		}
		
		
		.input_text{
			font-family:Arial, Helvetica, sans-serif;
			font-size:12px;
			border:1px solid #84a10b;
			padding:2px;
			width:150px;
			color:#000;
			background:white url(../images/search.png) no-repeat 3px 2px;
			padding-left:17px;
		}

</style>
  <script src="js/jquery.autocomplete.js"></script>
 </head>


<body> 
 <div class="new_changepassword_main_middle_textfields_right" >
                    <table>
                    <tr>
                    	<!-- <td>Enter Data Processor Name<b style="color:red">&lowast;</b></td> -->
                    	<td>Enter From Date</td>
                    	<td style="white-space: nowrap;">Enter To Date</td>
                    </tr>
                    <tr>
                 		 <td><s:textfield   cssClass="datepicker" readonly="true"  name="fdate" id="datepicker3" value=""/></td>
                 		 <td> <s:textfield  name="tdate" id="datepicker4" readonly="true"  cssClass="datepicker"/></td>
                 	</tr>
                 	</table>
                    </div>
      
       
       
      
         
</body>
</html>