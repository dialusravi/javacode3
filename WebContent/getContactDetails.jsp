<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<s:hidden name="salesPersonContactNo1" id="salesPersonContactNo1" value="%{objCheckAppointmentDTO.salesPersonContactNo}"></s:hidden>
<s:hidden name="salesPersonArea1" id="salesPersonArea1" value="%{objCheckAppointmentDTO.salesPersonArea}"></s:hidden>
<s:hidden name="salesPersonCity1" id="salesPersonCity1" value="%{objCheckAppointmentDTO.salesPersonCity}"></s:hidden>
<s:hidden name="salesPersonLatitude1" id="salesPersonLatitude1"  value="%{objCheckAppointmentDTO.salesPersonLatitude}"></s:hidden>
<s:hidden name="salesPersonLongitude1" id="salesPersonLongitude1"  value="%{objCheckAppointmentDTO.salesPersonLongitude}"></s:hidden>
<s:hidden name="salesPersonState1" id="salesPersonState1"  value="%{objCheckAppointmentDTO.salesPersonState}"></s:hidden>
</body>
</html>