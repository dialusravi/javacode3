<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@taglib uri="/struts-tags" prefix="s" %>
      <style type="text/css">
.white_content1{		
	display: none;	
	position: fixed;
		top: 26%;
			left: 50%; margin-left:-250px;
			width: 40%;
			height: auto;
			padding: 16px;
			border: 5px solid #008abb;
			background-color:#fff;
				z-index: 99999;
				box-shadow: 0px 0px 20px #008abb;
				-moz-box-shadow: 0px 0px 20px #999; /* Firefox */
			    -webkit-box-shadow: 0px 0px 20px #999; /* Safari, Chrome */
				border-radius:20px 20px 20px 20px ;
				-moz-border-radius:20px 20px 20px 20px ;
			  -webkit-border-radius:20px 20px 20px 20px ;
			   
			overflow: auto;
		}
		
		
.black_overlay1{
			display: none;
			position:fixed;
			top: 0%;
			left: 0%;
			width: 100%;
			height: 100%;
			background-color:black;
			-moz-opacity: 0.8;
			opacity:.70;
			filter: alpha(opacity=30);
		        }

.newmyprofile_lastupdate{ width:201px; height:auto; float:left; border:1px solid #ccc; padding:8px 0 8px 0}
.newmyprofile_lastupdate_top{ width:165px; height:auto; float:left; margin:5px 0 0 15px; color:#ff6600;}
.newmyprofile_lastupdate_top_review{ width:52px; height:auto; float:left; padding:5px; background-color:#018dbf; color:#fff; font-weight:bold}
.newmyprofile_maincontentpart_right{ width:725px; height:auto; float:right; border:1px solid #d6d6d6; padding:3px 3px 3px 3px}
.newmyprofile_userfrofile{ width:725px; height:auto; float:right; background-color:#d3d3d3;  padding:2px; padding:8px 0 8px 0; font-size:12px; color:#fff;}

.newmyprofile_userfrofile1{ width:725px; height:auto; float:right; background-color:#f1fafd;  padding:2px; padding:6px 0 6px 0; margin-top:12px;} 

.newmyprofile_userfrofile_businessname{ width:70px; height:auto; float:left; margin-left:10px; }
.newmyprofile_userfrofile_ContactPerson{ width:70px; height:auto; float:left; margin-left:15px}
.newmyprofile_userfrofile_BusinessMobile{ width:70px; height:auto; float:left; margin-left:15px}

.newmyprofile_userfrofile_CallbackDate{ width:70px; height:auto; float:left; margin-left:15px;}
.newmyprofile_userfrofile_Comments{ width:70px; height:auto; float:left; margin-left:15px;}

 

</style>
   <p style="color: red;font-size: 18px;text-align: center;">Renewal Data</p>   
    
   <div>
      <div class="newmyprofile_userfrofile" style="background: #008abb">
      
      <div class="newmyprofile_userfrofile_businessname" style="width:20px;">
        SNO
        </div>
      	<div class="newmyprofile_userfrofile_businessname" style="width:130px;">
        BusinessName
        </div>
        
        <div class="newmyprofile_userfrofile_ContactPerson" style="width:130px;">
       ContactPerson
        </div>
        
        <div class="newmyprofile_userfrofile_BusinessMobile" style="width:100px;">
      BusinessMobile
        </div>
        
         <div class="newmyprofile_userfrofile_CallbackDate" style="width:90px;">
       CallbackDate
        </div>
       <div class="newmyprofile_userfrofile_Comments" style="width:95px;">
      Comments
        </div>
        
      </div>
     
 <s:if test="#objArrayList.size()>0">
 <s:iterator value="objArrayList" id="a" status="c"> 
<div class="newmyprofile_userfrofile1" id="mydiv">
     
     
     <div class="newmyprofile_userfrofile_businessname"    style="width:20px;overflow: hidden;">
      	
  			 <s:property value="%{#c.count}" />.
	   </div>
        
      	<div class="newmyprofile_userfrofile_businessname"  title="<s:property value="#a.business_name" />"  style="width:130px;overflow: hidden;">
      	
  			 <s:property value="#a.business_name" />
	   </div>
        
        <div class="newmyprofile_userfrofile_ContactPerson" title="<s:property value="#a.contact_person" />" style="width:130px;overflow: hidden;">
        	<s:property value="#a.contact_person" />
	 
        </div>
        
        <div class="newmyprofile_userfrofile_BusinessMobile" title="<s:property value="#a.business_mobile" />" style="width:100px;overflow: hidden;">
       
      		<s:property value="#a.business_mobile" />
        </div>
        <div class="newmyprofile_userfrofile_CallbackDate" title=" <s:property value="#a.callback_date" />" style="width:80px;overflow: hidden;">
        
	 <s:property value="#a.callback_date" />
	    
	   
        
      		
        </div>
        
        
        <div class="newmyprofile_userfrofile_Comments" title="<s:property value="#a.comments" /> " style="width:95px;overflow: hidden;">
      		<s:property value="#a.comments" /> 
        </div>
       
        
</div>

 </s:iterator>
</s:if>
<s:else>

<p style="color: red;font-size: 16px;text-align: center;">Sorry There Is No Renewal Data</p>
</s:else>
</div>
      
     
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      

<%-- <s:iterator value="objArrayList"   id="a">

 <s:property value="#a.business_name" />
<s:property value="#a.contact_person" />
<s:property value="#a.business_mobile" />
<s:property value="#a.callback_date" />
<s:property value="#a.comments" /> 

</s:iterator> --%>

