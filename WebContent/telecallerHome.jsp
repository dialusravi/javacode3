<%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sales Tracking</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />

<script src="js/jquery-min.js"></script>
<script type="text/javascript" src="date_time.js"></script>

<link rel="stylesheet" href="css/jquery-ui123.css" />	 
<link href="http://dialuscss.dialus.com/dialus.css" rel="stylesheet" type="text/css" media="all"/>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/salesTrackingValid.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.ptTimeSelect.css" />
<script type="text/javascript" src="js/jquery.ptTimeSelect.js"></script>
<script type="text/javascript" src="http://dialusbanners.dialus.com/jquery.autocomplete.js"></script>
 <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/redmond/jquery-ui.css" />
 
 <script>
$(document).ready(function()
{
		
		$(function()
		{
		  $( ".reportTodate123" ).datepicker();
		});
		
		
});
</script>
<script>

function submit_button_clicked()
{
	
		 $(".error").hide();
	     var hasError = false;
	     var todaydate=$('#today_date').val();
	   
	     if( todaydate=='')
	        {
	    	 	
	    	 	$("#today_date").after('<span style="color: red;white-space:nowrap;" class="error">Please Select Date.....</span>');
	    	
	    	 	hasError = true;
	        }
	    if(hasError == true){
	    	return false; 
	    }
	    
		    else
		    {
		    	
		    	
		    	$('#callback_reports_div').load("loadReports.action?report="+todaydate+"&type=all");
				$('#callback_reports_div').show();
		    	$('#callback_reports_div_bydate').hide();
		    	
		    	
		    
		    	return true; 
		    }
			
			
	
	
	
	}

</script>
<script type="text/javascript">
function problem_clicked(){
	
    var hasError = false;
	var subject=$("#subject").val();
	var description=$("#description").val();
	
	var user=$("#sessionuser").val();
	$(".error").hide();
	if(subject=="")
		{
		  $("#subject").after('<span style="color: red" class="error"> Please Enter  Subject.</span>');
		  hasError = true;
		}
	if(description=="")
		{
		 $("#description").after('<span style="color: red" class="error"> Please Enter Description.</span>');
		  hasError = true;
		}
	if(hasError == true)
    { 
    	return false; 
    }
    else
    {
    	description=encodeURIComponent(description);
    	subject=encodeURIComponent(subject);

  $.post("ProblemsAction.action?username="+user+"&subject="+subject+"&description="+description);
  $("#problems_data_div").hide();  
  $("#subject").val("");
  $("#description").val("");
    return true; 
    }
	
	
	
	
}

</script>
<script>

function submit_button_clicked1()
{
	
		 $(".error").hide();
	     var hasError = false;
	     var savedtodaydate=$('#saved_today_date').val();
	   
	     if( savedtodaydate=='')
	        {
	    	 	
	    	 	$("#saved_today_date").after('<span style="color: red;white-space:nowrap;" class="error">Please Select Date.....</span>');
	    	
	    	 	hasError = true;
	        }
	    if(hasError == true){
	    	return false; 
	    }
	    
		    else
		    {
		    	
		    	
		    	$('#saveddata_div').load("loadSavedData.action?todaydate="+savedtodaydate);
		    	
				$('#saveddata_div').show();
		    	$('#saveddata_div_bydate').hide();
		    	
		    	
		    
		    	return true; 
		    }
			
			
	
	
	
	}
	
	


</script>
<script>




</script>
<script type="text/javascript">
function date_time(id)
{
        date = new Date;
        year = date.getFullYear();
        month = date.getMonth();
        months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'Jully', 'August', 'September', 'October', 'November', 'December');
        d = date.getDate();
        day = date.getDay();
        days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
        h = date.getHours();
        if(h<10)
        {
                h = "0"+h;
        }
        m = date.getMinutes();
        if(m<10)
        {
                m = "0"+m;
        }
        s = date.getSeconds();
        if(s<10)
        {
                s = "0"+s;
        }
        result =  ''+d+' '  +months[month]+'  '+year+' -  '+h+':'+m+':'+s;
        document.getElementById(id).innerHTML = result;
        setTimeout('date_time("'+id+'");','1000');
        return true;
}


$(document).ready(function()
{
	$('#success_msg').fadeOut(8000);
	$('#appoint_msg').fadeOut(10000);
	$('#reports_div').hide();
	$('#callstatus_div').hide();
	$('#appoint_div').hide();
	$('#fixappointment_div').hide();
	$('#banner_div').show();
	$('#dailyreport_div').hide();
	$('#weeklyreport_div').hide();
	$('#saveddata_div').hide();
	$('#callback_reports_div').hide();
	$('#monthlyreport_div').hide();
	$('#callback_reports_div_bydate').hide();
	$('#saveddata_div_bydate').hide();
	$("#dailyplan_div").hide();
	 $("#dialycallplan_inner_div").hide();
	$('#renewaldata').hide();
	$('#callplan_div').hide();
	$("#sms_div").hide();
	$("#leads_div").hide();
	
	$("#city_name").autocomplete("DialyCallPlanCityAutoComleteAction.action");
	
	
	$("#appointments_link").click(function()
	{
		
		
		$("#appointments_link").css("background-color","yellow");
		$("#reports_link").css("background-color","");
		$("#callback_link").css("background-color","");
		$("#saveddata_link").css("background-color","");
		$(".targets_link").css("background-color","");
		$("#dailyplan_link").css("background-color","");
		$("#callplan_on_leads").css("background-color","");
		$("#renewaldata_link").css("background-color","");
		$("#business_link").css("background-color","");
		$("#sms_link").css("background-color","");
		$("#leads_link").css("background-color","");
		$(".problems_link").css("background-color","");
		 $("#business_div").hide();
		$('#banner_div').hide();	
		$('#reports_div').hide();
		$('#callstatus_div').hide();
		$('#fixappointment_div').show();
		$('#appoint_div').hide();
		$('#dailyreport_div').hide();
		$('#saveddata_div').hide();
		$('#weeklyreport_div').hide();
		$('#callback_reports_div').hide();
		$('#callback_reports_div_bydate').hide();
		$('#monthlyreport_div').hide();
		$('#saveddata_div_bydate').hide();
		$("#dailyplan_div").hide();
		 $("#dialycallplan_inner_div").hide();
		$('#renewaldata').hide();
		$("#target_div").hide();
		$('#callplan_div').hide();
		$("#sms_div").hide();
		$("#leads_div").hide();
		$("#problems_data_div").hide();
		$("#problems_div").hide();
	});
	
	
	$("#reports_link").click(function()
			{
		
		
		$("#appointments_link").css("background-color","");
		$("#reports_link").css("background-color","yellow");
		$("#callback_link").css("background-color","");
		$("#saveddata_link").css("background-color","");
		$(".targets_link").css("background-color","");
		$("#dailyplan_link").css("background-color","");
		$("#callplan_on_leads").css("background-color","");
		$("#renewaldata_link").css("background-color","");
		$("#business_link").css("background-color","");
		$("#sms_link").css("background-color","");
		$("#leads_link").css("background-color","");
		$(".problems_link").css("background-color","");
		
		
		$("#dailyplan_div").hide();
		 $("#dialycallplan_inner_div").hide();
		        $("#business_div").hide();
		        $('#banner_div').hide();
				$('#callstatus_div').hide();
				$('#fixappointment_div').hide();
				$('#dailyreport_div').hide();
				$('#weeklyreport_div').hide();
				$('#monthlyreport_div').hide();
				$('#appoint_div').hide();
				$('#saveddata_div').hide();
				$('#callback_reports_div').hide();
				$('#callback_reports_div_bydate').hide();
				$('#saveddata_div_bydate').hide();
				$('#renewaldata').hide();
				$('#callplan_div').hide();
				$('#reports_div').show();
				$("#target_div").hide();
				$("#sms_div").hide();
				$("#leads_div").hide();
				$("#problems_data_div").hide();
				$("#problems_div").hide();
				
			});
	
	
	$("#business_link").click(function()
			{
		
		
		
		$("#appointments_link").css("background-color","");
		$("#reports_link").css("background-color","");
		$("#callback_link").css("background-color","");
		$("#saveddata_link").css("background-color","");
		$(".targets_link").css("background-color","");
		$("#dailyplan_link").css("background-color","");
		$("#callplan_on_leads").css("background-color","");
		$("#renewaldata_link").css("background-color","");
		$("#business_link").css("background-color","yellow");
		$("#sms_link").css("background-color","");
		$("#leads_link").css("background-color","");
		$(".problems_link").css("background-color","");
		
		
				
		$('#banner_div').hide();
		$('#callstatus_div').hide();
		$('#fixappointment_div').hide();
		$('#dailyreport_div').hide();
		$('#weeklyreport_div').hide();
		$('#monthlyreport_div').hide();
		$('#appoint_div').hide();
		$('#saveddata_div').hide();
		$('#callback_reports_div').hide();
		$('#callback_reports_div_bydate').hide();
		$('#saveddata_div_bydate').hide();
		$('#reports_div').hide();
		
		$("#loading_img2").show();
		$("#business_div").load("BusinessDetailAction.action");
		$("#business_div").show();
		$("#dailyplan_div").hide();
		 $("#dialycallplan_inner_div").hide();
		$('#renewaldata').hide();
		$("#target_div").hide();
		$('#callplan_div').hide();
		$("#sms_div").hide();
		$("#leads_div").hide();
		$("#problems_data_div").hide();
		$("#problems_div").hide();
			});
	
		
	$("#callback_link").click(function()
			{
		
		
		$("#appointments_link").css("background-color","");
		$("#reports_link").css("background-color","");
		$("#callback_link").css("background-color","yellow");
		$("#saveddata_link").css("background-color","");
		$(".targets_link").css("background-color","");
		$("#dailyplan_link").css("background-color","");
		$("#callplan_on_leads").css("background-color","");
		$("#renewaldata_link").css("background-color","");
		$("#business_link").css("background-color","");
		$("#sms_link").css("background-color","");
		$("#leads_link").css("background-color","");
		$(".problems_link").css("background-color","");    
		        $("#business_div").hide();
		    
		    	$("#dailyplan_div").hide();
				 $("#dialycallplan_inner_div").hide();
				$('#banner_div').hide();
				$('#callstatus_div').hide();
				$('#fixappointment_div').hide();
				$('#dailyreport_div').hide();
				$('#weeklyreport_div').hide();
				$('#monthlyreport_div').hide();
				$('#appoint_div').hide();
				$('#reports_div').hide();
				$('#saveddata_div').hide();
				$('#callback_reports_div').hide(); 
				$('#saveddata_div_bydate').hide();
				$('#renewaldata').hide();
				$("#target_div").hide();
				$('#callplan_div').hide();
				$('#callback_reports_div_bydate').show();
				$("#sms_div").hide();
				$('#callback_inner').load("LoadTodayAppointmentsAction.action");
			/*   $('#callback_reports_div').load("loadReports.action?report=callback&type=all");*/
				$("#leads_div").hide();
				$("#problems_data_div").hide();
				$("#problems_div").hide();
				
			});
	
	
	$("#dailyplan_link").click(function()
			{
		
		
		
		$("#appointments_link").css("background-color","");
		$("#reports_link").css("background-color","");
		$("#callback_link").css("background-color","");
		$("#saveddata_link").css("background-color","");
		$(".targets_link").css("background-color","");
		$("#dailyplan_link").css("background-color","yellow");
		$("#callplan_on_leads").css("background-color","");
		$("#renewaldata_link").css("background-color","");
		$("#business_link").css("background-color","");
		$("#sms_link").css("background-color","");
		$("#leads_link").css("background-color","");
		$(".problems_link").css("background-color","");
		 $("#dialycallplan_inner_div").hide();
		
		
		        $("#dailyplan_div").show();
		       /*  $("#dailyplan_div").load("DailyCallPlanAction.action");
		        $("#dailyplan_div").show(); */
		        $("#business_div").hide();
		        $('#banner_div').hide();
				$('#callstatus_div').hide();
				$('#fixappointment_div').hide();
				$('#dailyreport_div').hide();
				$('#weeklyreport_div').hide();
				$('#monthlyreport_div').hide();
				$('#appoint_div').hide();
				$('#reports_div').hide();
				$('#callback_reports_div').hide();
				$('#callback_reports_div_bydate').hide();
				$('#saveddata_div_bydate').hide();
				$('#saveddata_div').hide();
				$('#renewaldata').hide();
				$("#target_div").hide();
				$('#callplan_div').hide();
				$("#sms_div").hide();
				$("#leads_div").hide();
				$("#problems_data_div").hide();
				$("#problems_div").hide();
				$('#city_name').val("");
			});
	$("#dialycallplan_submit").click(function()
			{ 
		
		 $(".error").hide();
		 var hasError = false;
		var city= encodeURIComponent($('#city_name').val());
	     
		 if(city=='')
			 {
			 $("#city_name").after('<span style="color: red" class="error"> Please Enter City.</span>');
               hasError = true;
			 }
		     if(hasError == true)
	        { 
	        	
	      
	        	return false; 
	        }
		
	

		$("#dailyplan_link").css("background-color","yellow");
		$("#all_appointments_link").css("background-color","");
		$("#allocated_appointments_link").css("background-color","");
		$("#trackfield_link").css("background-color","");
		$("#submit_allocated").css("background-color","");
		$("#edituser_link").css("background-color","");
		$("#deleteuser_link").css("background-color","");
		$("#createuser_link").css("background-color","");
		$("#datewisereport_link").css("background-color","");
		
		$("#addtarget_link").css("background-color","");
		$("#appointments_link").css("background-color","");
		$("#callplan_on_leads").css("background-color","");
		$("#renewaldata_link").css("background-color","");
		$("#business_link").css("background-color","");
		$("#sms_link").css("background-color","");
		$("#leads_link").css("background-color","");
		$(".problems_link").css("background-color","");
		$("#mysms").hide();
        $("#show_sms_div").hide();
		$("#dailyplan_div").hide();
		$('#deletetelecallers_div').hide();
		$('#deletesalespersons_div').hide();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').hide();
		$('#editsalespersonsdetails_div').hide();
		$('#edittelecallersdetails_div').hide();
		$('#telecaller_div').hide();
		$('#salesperson_div').hide();
		$("#calllist_div").hide();
		$("#saleslist_div").hide();
		$("#datewisereport_div").hide();
		$("#datewiseReportsResults_div").hide();
		$("#business_div").hide();
		$('#pending_appointments_div,#rejected_appointments_div,#banner_div,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();  	
	
		$('#appointments').hide();
		$("#sms_div").hide();
		$('#appointments_by_name').hide();
		$('#allocated_div').hide();
		$('#allocated_load_div').hide();
		$('#renewaldata').hide();
		$('#trackfield_div').hide();
		$('#targets_div').hide();
		$('#callplan_div').hide();
		$('#teamleader_div').hide();
		$('#editteamleader_div').hide();
		$('#editteamleaderdetails_div').hide();
		
		$("#dialycallplan_inner_div").html("");
		$("#dialycallplan_inner_div").load("DailyCallPlanAction.action?city="+city);
		$("#dialycallplan_inner_div").show();
		$("#problems_data_div").hide();
		$("#problems_div").hide();
		
		
		
			});
			
		
	
	
	
	$("#saveddata_link").click(function()
			{
		
		
		$("#appointments_link").css("background-color","");
		$("#reports_link").css("background-color","");
		$("#callback_link").css("background-color","");
		$("#saveddata_link").css("background-color","yellow");
		$(".targets_link").css("background-color","");
		$("#dailyplan_link").css("background-color","");
		$("#callplan_on_leads").css("background-color","");
		$("#renewaldata_link").css("background-color","");
		$("#business_link").css("background-color","");
		$("#sms_link").css("background-color","");
		$("#leads_link").css("background-color","");
		$(".problems_link").css("background-color","");
		
		
		$("#dailyplan_div").hide();
		 $("#dialycallplan_inner_div").hide();
		        $("#business_div").hide();
		        $('#banner_div').hide();
				$('#callstatus_div').hide();
				$('#fixappointment_div').hide();
				$('#dailyreport_div').hide();
				$('#weeklyreport_div').hide();
				$('#monthlyreport_div').hide();
				$('#appoint_div').hide();
				$('#reports_div').hide();
				$('#callback_reports_div').hide();
				$('#callback_reports_div_bydate').hide();
				$('#saveddata_div_bydate').show();
				/* $('#saveddata_div').load("loadSavedData.action"); */
				$('#saveddata_div').hide();
				$('#renewaldata').hide();
				$("#target_div").hide();
				$('#callplan_div').hide();
				$("#sms_div").hide();
				$("#leads_div").hide();
				$("#problems_data_div").hide();
				$("#problems_div").hide();
			});
	
	
	$("#renewaldata_link").click(function()
			{
		
		
		
		$("#appointments_link").css("background-color","");
		$("#reports_link").css("background-color","");
		$("#callback_link").css("background-color","");
		$("#saveddata_link").css("background-color","");
		$(".targets_link").css("background-color","");
		$("#dailyplan_link").css("background-color","");
		$("#callplan_on_leads").css("background-color","");
		$("#renewaldata_link").css("background-color","yellow");
		$("#business_link").css("background-color","");
		$("#sms_link").css("background-color","");
		$("#leads_link").css("background-color","");
		$(".problems_link").css("background-color","");		
		        $("#business_div").hide();
		        $('#banner_div').hide();
				$('#callstatus_div').hide();
				$('#fixappointment_div').hide();
				$('#dailyreport_div').hide();
				$('#weeklyreport_div').hide();
				$('#monthlyreport_div').hide();
				$('#appoint_div').hide();
				$('#reports_div').hide();
				$('#callback_reports_div').hide();
				$('#callback_reports_div_bydate').hide();
				$("#dailyplan_div").hide();
				 $("#dialycallplan_inner_div").hide();
				 $("#loading_img111").show();
				$('#renewaldata').load("RenewalDataAction.action");
				$('#renewaldata').show();
				
				$('#saveddata_div_bydate').hide();
				/* $('#saveddata_div').load("loadSavedData.action"); */
				$('#saveddata_div').hide();
				$("#target_div").hide();
				$('#callplan_div').hide();
				$("#sms_div").hide();
				$("#leads_div").hide();
				$("#problems_data_div").hide();
				$("#problems_div").hide();
			});
	
	$("#callplan_on_leads").click(function()
			{
		
		$("#appointments_link").css("background-color","");
		$("#reports_link").css("background-color","");
		$("#callback_link").css("background-color","");
		$("#saveddata_link").css("background-color","");
		$(".targets_link").css("background-color","");
		$("#dailyplan_link").css("background-color","");
		$("#callplan_on_leads").css("background-color","yellow");
		$("#renewaldata_link").css("background-color","");
		$("#business_link").css("background-color","");
		$("#sms_link").css("background-color","");
		$("#leads_link").css("background-color","");
		$(".problems_link").css("background-color","");
		
		        $('#renewaldata').hide();
		        $("#business_div").hide();
		        $('#banner_div').hide();
				$('#callstatus_div').hide();
				$('#fixappointment_div').hide();
				$('#dailyreport_div').hide();
				$('#weeklyreport_div').hide();
				$('#monthlyreport_div').hide();
				$('#appoint_div').hide();
				$('#reports_div').hide();
				$('#callback_reports_div').hide();
				$('#callback_reports_div_bydate').hide();
				$("#dailyplan_div").hide();
				$("#dialycallplan_inner_div").hide();
				$("#loading_img11").show();
				$('#callplan_div').load("CallPlanDataAction.action");
				$('#callplan_div').show();
				
				$('#saveddata_div_bydate').hide();
				/* $('#saveddata_div').load("loadSavedData.action"); */
				$('#saveddata_div').hide();
				$("#target_div").hide();
				$("#sms_div").hide();
				$("#leads_div").hide();
				$("#problems_data_div").hide();
				$("#problems_div").hide();
			});
	
	
	
	
	
	 $(".targets_link").click(function()
				{
		
		 
		 
		 $("#appointments_link").css("background-color","");
			$("#reports_link").css("background-color","");
			$("#callback_link").css("background-color","");
			$("#saveddata_link").css("background-color","");
			$(".targets_link").css("background-color","yellow");
			$("#dailyplan_link").css("background-color","");
			$("#callplan_on_leads").css("background-color","");
			$("#renewaldata_link").css("background-color","");
			$("#business_link").css("background-color","");
			$("#sms_link").css("background-color","");
			$("#leads_link").css("background-color","");
			$(".problems_link").css("background-color","");
		 
		 
		 
		 
		 
		 
		 
		 
		 
		           var user=$("#sessionuser").val();
		           
						$("#target_div").load("LoadTargetAction.action?username="+user);
						$("#target_div").show();
						
						 $("#business_div").hide();
					        $('#banner_div').hide();
							$('#callstatus_div').hide();
							$('#fixappointment_div').hide();
							$('#dailyreport_div').hide();
							$('#weeklyreport_div').hide();
							$('#monthlyreport_div').hide();
							$('#appoint_div').hide();
							$('#reports_div').hide();
							$('#callback_reports_div').hide();
							$('#callback_reports_div_bydate').hide();
							$("#dailyplan_div").hide();
							$("#dialycallplan_inner_div").hide();
							
							 $('#callplan_div').hide();
							$('#renewaldata').hide();
							
							$('#saveddata_div_bydate').hide();
							/* $('#saveddata_div').load("loadSavedData.action"); */
							$('#saveddata_div').hide();
							$("#sms_div").hide();
							$("#leads_div").hide();
							$("#problems_data_div").hide();
							$("#problems_div").hide();
						
				}); 
	 
	 
	 

		$("#sms_link").click(function()
				{
			
			
			$("#appointments_link").css("background-color","");
			$("#reports_link").css("background-color","");
			$("#callback_link").css("background-color","");
			$("#saveddata_link").css("background-color","");
			$(".targets_link").css("background-color","");
			$("#dailyplan_link").css("background-color","");
			$("#callplan_on_leads").css("background-color","");
			$("#renewaldata_link").css("background-color","");
			$("#business_link").css("background-color","");
			$("#sms_link").css("background-color","yellow");
			$("#leads_link").css("background-color","");
			$(".problems_link").css("background-color","");
		
			
			      $("#loading_img3").show();
					$("#sms_div").load("SmsDetailAction.action");
					$("#sms_div").show();
				
					$("#target_div").hide();
					
					 $("#business_div").hide();
				        $('#banner_div').hide();
						$('#callstatus_div').hide();
						$('#fixappointment_div').hide();
						$('#dailyreport_div').hide();
						$('#weeklyreport_div').hide();
						$('#monthlyreport_div').hide();
						$('#appoint_div').hide();
						$('#reports_div').hide();
						$('#callback_reports_div').hide();
						$('#callback_reports_div_bydate').hide();
						$("#dailyplan_div").hide();
						 $("#dialycallplan_inner_div").hide();
						
						 $('#callplan_div').hide();
						$('#renewaldata').hide();
						
						$('#saveddata_div_bydate').hide();
					
						$('#saveddata_div').hide();
						$("#leads_div").hide();
						$("#problems_data_div").hide();
						$("#problems_div").hide();
				});
		
		
		$("#leads_link").click(function()
				{
			
			
			
			
			$("#appointments_link").css("background-color","");
			$("#reports_link").css("background-color","");
			$("#callback_link").css("background-color","");
			$("#saveddata_link").css("background-color","");
			$(".targets_link").css("background-color","");
			$("#dailyplan_link").css("background-color","");
			$("#callplan_on_leads").css("background-color","");
			$("#renewaldata_link").css("background-color","");
			$("#business_link").css("background-color","");
			$("#sms_link").css("background-color","");
			$("#leads_link").css("background-color","yellow");
			$(".problems_link").css("background-color","");
			
			
			
			
			      $("#loading_img4").show();
					$("#leads_div").load("GetLeadsAction.action");
					$("#leads_div").show();
				
					$("#sms_div").hide();
				
					$("#target_div").hide();
					
					 $("#business_div").hide();
				        $('#banner_div').hide();
						$('#callstatus_div').hide();
						$('#fixappointment_div').hide();
						$('#dailyreport_div').hide();
						$('#weeklyreport_div').hide();
						$('#monthlyreport_div').hide();
						$('#appoint_div').hide();
						$('#reports_div').hide();
						$('#callback_reports_div').hide();
						$('#callback_reports_div_bydate').hide();
						$("#dailyplan_div").hide();
						 $("#dialycallplan_inner_div").hide();
						
						 $('#callplan_div').hide();
						$('#renewaldata').hide();
						
						$('#saveddata_div_bydate').hide();
					
						$('#saveddata_div').hide();
						 $("#problems_data_div").hide();
						 $("#problems_div").hide();
					
				});
		
		 $(".problems_link").click(function()
					{
			 
			 
			   $("#appointments_link").css("background-color","");
				$("#reports_link").css("background-color","");
				$("#callback_link").css("background-color","");
				$("#saveddata_link").css("background-color","");
				$(".targets_link").css("background-color","");
				$("#dailyplan_link").css("background-color","");
				$("#callplan_on_leads").css("background-color","");
				$("#renewaldata_link").css("background-color","");
				$("#business_link").css("background-color","");
				$("#sms_link").css("background-color","");
				$("#leads_link").css("background-color","");
				$(".problems_link").css("background-color","yellow");
				
			
			              var user=$("#sessionuser").val();
			               
						    $("#problems_data_div").show();
							$("#problems_div").load("ProblemsAction.action?username="+user);
							$("#problems_div").show();
							$("#leads_div").hide();
							
							$("#sms_div").hide();
						
							$("#target_div").hide();
							
							 $("#business_div").hide();
						        $('#banner_div').hide();
								$('#callstatus_div').hide();
								$('#fixappointment_div').hide();
								$('#dailyreport_div').hide();
								$('#weeklyreport_div').hide();
								$('#monthlyreport_div').hide();
								$('#appoint_div').hide();
								$('#reports_div').hide();
								$('#callback_reports_div').hide();
								$('#callback_reports_div_bydate').hide();
								$("#dailyplan_div").hide();
								 $("#dialycallplan_inner_div").hide();
								
								 $('#callplan_div').hide();
								$('#renewaldata').hide();
								
								$('#saveddata_div_bydate').hide();
							
								$('#saveddata_div').hide();
							
					}); 
		
		
		
		
	
	
	
});
/* function getContactNo()
{
	var xmlhttp1;    
	var salesPersonName=$('#salesPersonName').val();
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp1=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp1=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp1.onreadystatechange=function()
	  {
	  if (xmlhttp1.readyState==4 && xmlhttp1.status==200)
	    {
	    	document.getElementById("salesPersonContactNo").value = xmlhttp1.responseText;
	    }
	  };
	  
	xmlhttp1.open("POST","getsalesPersonContactNo.action?salesPersonName="+salesPersonName,true);
	xmlhttp1.send();
} */
</script>
<s:if test="%{#request.operation!=null}">
	<script type="text/javascript">
		$(document).ready(function()
		{
			$('#fixappointment_div').hide();
			$('#reports_div').hide();
			$('#callstatus_div').hide();
			$('#appoint_div').hide();
			$('#appointmentfrom_div').hide();
			$('#callback_reports_div').hide();
			$('#appointment_status').fadeOut(1000);
			$('.white_content').fadeOut(1000);
			$('.black_overlay').fadeOut(1000);
			$('#banner_div').hide();
			$('#saveddata_div').hide();
			$('#fixappointment_div').show();
			$('#appoint_div').hide();
			$('#renewaldata').hide();
			$("#target_div").hide();
			$('#callplan_div').hide();
		});
	</script>
</s:if>
<script>
function selectReport()
{
	
	var report=$('#reporttype').val();
	if(report=='daily')
	{
		
		$('#banner_div').hide();	
		$('#reports_div').hide();
		$('#callstatus_div').hide();
		$('#fixappointment_div').hide();
		$('#weeklyreport_div').hide();
		$('#monthlyreport_div').hide();
		$('#saveddata_div').hide();
		$('#callback_reports_div').hide();
		$('#appoint_div').hide();
		$('#callback_reports_div_bydate').hide();
		$('#saveddata_div_bydate').hide();
		$('#renewaldata').hide();
		$("#target_div").hide();
		$('#callplan_div').hide();
		$('#dailyreport_div').load("loadReports.action?type=daily&report=all");
		$('#dailyreport_div').show();
	
	}else if(report=='weekly')
	{
		$('#banner_div').hide();	
		$('#reports_div').hide();
		$('#callstatus_div').hide();
		$('#fixappointment_div').hide();
		$('#dailyreport_div').hide();
		$('#saveddata_div').hide();
		$('#monthlyreport_div').hide();
		$('#callback_reports_div').hide();
		$('#appoint_div').hide();
		$('#callback_reports_div_bydate').hide();
		$('#saveddata_div_bydate').hide();
		$('#renewaldata').hide();
		$("#target_div").hide();
		$('#callplan_div').hide();
		$('#weeklyreport_div').load("loadReports.action?type=weekly&report=all");
		$('#weeklyreport_div').show();
		
	}
	else if(report=='monthly')
	{
		$('#banner_div').hide();	
		$('#reports_div').hide();
		$('#callstatus_div').hide();
		$('#fixappointment_div').hide();
		$('#dailyreport_div').hide();
		$('#callback_reports_div_bydate').hide();
		$('#saveddata_div').hide();
		$('#weeklyreport_div').hide();
		$('#callback_reports_div').hide();
		$('#saveddata_div_bydate').hide();
		$('#appoint_div').hide();
		$('#renewaldata').hide();
		$("#target_div").hide();
		$('#callplan_div').hide();
		$('#monthlyreport_div').load("loadReports.action?type=monthly&report=all");
		$('#monthlyreport_div').show();
		
	}
	else if(report=='123')
	{
		$('#banner_div').hide();	
		$('#callstatus_div').hide();
		$('#fixappointment_div').hide();
		$('#saveddata_div').hide();
		$('#appoint_div').hide();
		$('#dailyreport_div').hide();
		$('#callback_reports_div').hide();
		$('#weeklyreport_div').hide();
		$('#monthlyreport_div').hide();
		$('#saveddata_div_bydate').hide();
		$('#callback_reports_div_bydate').hide();
		$('#renewaldata').hide();
		$('#callplan_div').hide();
		$("#target_div").hide();
		$('#reports_div').show();
		$('#reporttype').after('<span style="color: red" class="error">Select Any One Report Type..... .</span>');
	
	}
}
</script>
</head>

<body>

<input type="hidden" name="username"  id="sessionuser"  value="<s:property value="#session.username"/>"/>
<div class="top_bg">
	<div class="topbg_inner">
   	  <div class="topbg_inner_left">
        	<div class="topbg_inner_left_top">
            	SALES TRACKING
            </div>
            <div class="topbg_inner_left_bottom">
             <span id="date_time"></span>
            <script type="text/javascript">window.onload = date_time('date_time');</script>
          </div>
      </div>
      <div class="topbg_inner_right">
        	<div class="topbg_inner_right_left">
       	  <img src="images/dialus_small.png" width="28" height="27" /> </div>
            <div class="topbg_inner_right_right">
            9666 888 888
            </div>
      </div>
    </div>
</div>
<div style="clear:both"></div>
<div class="wrapper">
<div class="logopart">
	<div class="logopart_left">
    <img src="images/logo.jpg" width="225" height="90" /> 
    </div>
    <div class="logopart_right">
        
    </div>
</div>
<s:if test="%{#request.success_msg!=null && #request.success_msg!=''}">
		<div style="text-align: center;width:980px;" id="success_msg"> <font style="color: red;font-family: georgia;font-weight: bold;"><s:property value="%{#request.success_msg}"/></font></div>
	</s:if>
<div class="contentpart">
	<div class="content_left">
    	
        <div id="login_links">
	        <div class="content_left_headding">
		  <div class="content_left_text">
            Menu
          </div>
      </div>
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="appointments_link" >Search/Add Data</a>
	        	</div>
	       </div>
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="reports_link" >Send Reports</a>
	        	</div>
	       </div>
	       
	        
	    
	       
	       
	       
	       
	       
	       
	        <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="callback_link" >CallBack Appointments</a>
	        	</div>
	       </div>
	       
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="saveddata_link" >Saved Data</a>
	        	</div>
	       </div>
	     
	       
	       
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="dailyplan_link" >Daily Call Plan</a>
	        	</div>
	       </div>
	       
	       
	       
	       
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" class="targets_link" >Targets</a>
	        	</div>
	       </div> 
	       
	      
	        
	     <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="renewaldata_link" >Renewal Data</a>
	        	</div>
	       </div>
	        
	     
	       
	       
	      <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="callplan_on_leads" >Call Plan On Leads</a>
	        	</div>
	       </div>
	       
	       
	         <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="business_link" >Business Details</a>
	        	</div>
	       </div> 
	       
	       
	       
	       
	       
	       
	         <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="sms_link" >SMS Details</a>
	        	</div>
	       </div>
	       
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="leads_link" >Leads  Sent</a>
	        	</div>
	     </div>
	     
	     
	     
	     
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" class="problems_link" >Complaints</a>
	        	</div>
	     </div>
	     
	     
	       
	        
	        
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a href="LogOut.action"style="cursor: pointer;text-decoration: none;color:#f30909;" id="logout_link">Log Out</a>
	        	</div>
	       </div>
	       
	       
	       
      </div>
      
    </div>
    
    <div id="renewaldata">
    
    <img src="http://dialusimages.dialus.com/images/loader32.gif" id="loading_img111"  style="display:none; margin:30px 0 0 75px; "/>
    </div>
    <div id="callplan_div">
    <img src="http://dialusimages.dialus.com/images/loader32.gif" id="loading_img11"  style="display:none; margin:30px 0 0 75px; "/>
    </div>
    
    <div id="sms_div" style="display: block; float: right; width: 741px;">
    
    <img src="http://dialusimages.dialus.com/images/loader32.gif" id="loading_img3"  style="display:none; margin:30px 0 0 175px; "/>
    
    </div>
    <div id="leads_div" style="display: block; float: right; width: 741px;">
    
    
     <img src="http://dialusimages.dialus.com/images/loader32.gif" id="loading_img4"  style="display:none; margin:30px 0 0 175px; "/>
    
    
    
     </div>
	     
	     
    <div class="content_right">
    
	   	  <div class="banner_part" id="banner_div">
	   	 
	        	
	            
	            <div class="banner_part">
	             </div>
	            
	      </div>
	      
	       <div class="main_div">
	       
	      <div id="fixappointment_div">
			      	<s:form action="checkAppointment" theme="simple">
					    	<s:hidden name="usertype" value="telecaller"></s:hidden>
					        <div class="main_div_middle">
					         	<div class="main_div_middle_textfields_main" style="text-align:center;background: #008ABB;width: 100%;color:#fff;padding: 8px 0px;margin-top:-10px;border-top-left-radius:10px;border-top-right-radius:10px;">
								         Search (or) Add  Data
								</div>
						        <div class="main_div_middle_textfields" style="float:left; width:600px;">
								       
							           <div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:180px; text-align:right; margin-left:60px;">
								       	   		<b style="color:red">&lowast;</b>Enter Mobile Number :
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								           		  <s:textfield  name="objCheckAppointmentDTO.businessMobile" maxlength="10"  id="mobileno"  cssStyle="width:280px; border:1px solid #008abb; padding:11px 0 10px 3px" ></s:textfield>
								            </div>
							           </div>
							               <div class="main_div_middle_textfields_main" style="width:600px; text-align:center; margin:0; font-size:13px">
                                           (Or)
                                           </div>
							           <div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:180px; text-align:right; margin-left:60px;">
								       	    	<b style="color:red">&lowast;</b>Enter Phone Number :
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								            	<s:textfield  name="objCheckAppointmentDTO.businessPhone"  maxlength="15"  id="phoneno"  cssStyle="width:280px; border:1px solid #008abb; padding:11px 0 10px 3px"  ></s:textfield>
								            </div>
							           </div>
							           <div class="main_div_middle_textfields_main" style="width:600px; text-align:center; margin:0; font-size:13px">
                                           (Or)
                                           </div>
							             <div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:180px; text-align:right; margin-left:60px;">
								       	    	<b style="color:red">&lowast;</b>Enter Email Id :
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								            	<s:textfield  name="objCheckAppointmentDTO.businessMail"  maxlength="60"  id="mailid"  cssStyle="width:280px; border:1px solid #008abb; padding:11px 0 10px 3px"  ></s:textfield>
								            </div>
							           </div>
							          
							           <div class="main_div_middle_textfields_main" style="width:340px; margin-left:230px;">
							          			 <input type="button" id="retrieveAppointment"  value="Retrive Data" style="color:#fff; font-weight:bold; background-color:#008abb; text-align:center; padding:5px 0 5px 0; width:328px; border:none" />
							           </div>
							           
							           <div style="clear:both"></div>
						            </div>
						            
						            
						       </div>
					     
					     </s:form>
				</div>
				
				<div id="reports_div">
				
				
	      				<div class="main_div_middle">
		         
			                       	<div class="appointmentform_left_left">
			                         		   Select Report Type:
			                        </div>
			                            
			                   		<div class="appointmentform_left_right">
			                     		<select id="reporttype" onchange="selectReport()">
						      					<option value="123">-----Select-------</option>
							      				<option value="daily">Daily Report</option>
							      				<option value="weekly">Weekly Report</option>
							      				<option value="monthly">Monthly Report</option>
			      						</select>
			                   		</div>
		         
	          		 </div>
				
				</div>
				
				<div id="callback_reports_div_bydate">
				  <div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:150px; text-align:right; margin-left:30px;">
								       	   		<b style="color:red">&lowast;</b>Select Date :
								            </div>
                                            
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px; width:219px;">
								           		<%--  <s:textfield name="reportTodate" id="reportTodate"  cssClass="reportTodate"></s:textfield> --%>
								           		 <s:textfield name="todaydate" id="today_date"  cssClass="reportTodate123" onfocus="show_date()"></s:textfield>
								            </div>
								            
								            <div class="main_div_middle_textfields_main_right" style="margin-left:5px; width:80px;">
								           		<%--  <s:textfield name="reportTodate" id="reportTodate"  cssClass="reportTodate"></s:textfield> --%>
								           		 <s:submit id="submit_date"  onclick="submit_button_clicked()"></s:submit>
								            </div>
							</div>
				<div class="main_div_middle_textfields_main" style="width:600px;">
				<div id="callback_inner"></div>
				</div>
				</div>
				<div id="business_div">
				
				<img src="http://dialusimages.dialus.com/images/loader32.gif" id="loading_img2"  style="display:none; margin:30px 0 0 175px; "/>
				
				</div>
				<div id="saveddata_div_bydate">
				
				  <div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:150px; text-align:right; margin-left:30px;">
								       	   		<b style="color:red">&lowast;</b>Select Date :
								            </div>
                                            
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px; width:219px;">
								           		<%--  <s:textfield name="reportTodate" id="reportTodate"  cssClass="reportTodate"></s:textfield> --%>
								           		 <s:textfield name="todaydate" id="saved_today_date"  cssClass="reportTodate123" onfocus="show_date()"></s:textfield>
								            </div>
								            
								            <div class="main_div_middle_textfields_main_right" style="margin-left:5px; width:80px;">
								           		<%--  <s:textfield name="reportTodate" id="reportTodate"  cssClass="reportTodate"></s:textfield> --%>
								           		 <s:submit id="submit_saved_date"  onclick="submit_button_clicked1()"></s:submit>
								            </div>
							</div>
				
				
				</div>
	
				
				<div id="dailyreport_div"></div>
				<div id="weeklyreport_div"></div>
				<div id="monthlyreport_div"></div>
				<div id="appoint_div"></div>
				<div id="callback_reports_div"></div>
	      		<div id="saveddata_div"></div>
	      		<div id="business_div"></div>
	      		 <div id="dailyplan_div">
	     
	     <p style="color: red;font-size: 18px;text-align: center;">Dialy Call Plan</p>
     	          <div class="new_changepassword_main_middle_textfields">
                	<div class="new_changepassword_main_middle_textfields_left">
                    <b style="color:red">&lowast;</b>Select City: 
                    </div>
                    
                    <div class="new_changepassword_main_middle_textfields_right">
                    
                    <s:textfield name="city_name"  id="city_name" theme="simple" cssStyle=" width:200px ; height:27px"></s:textfield>
                     
                    </div>
                    
                    
                    <div class="new_changepassword_main_middle_textfields">
               	  <div class="new_changepassword_main_middle_textfields_left" style="color:#fff">
                    :
                  </div>
                    
                    <div class="new_changepassword_main_middle_textfields_right">
                    
                   <input    type="button"  value="Submit" id="dialycallplan_submit" style="background-color: #008abb; border: none; color: #fff; height: 26px; float: left; cursor: pointer;" />
                    </div>
                
                </div>          
                </div>
	     
	     <!-- <img src="http://images.dialus.com/english/images/india/images/loader32.gif" id="loading_img"  style="display:none; margin:30px 0 0 175px; "/> -->
	  	     </div>
	  	     
	  	       <div id="dialycallplan_inner_div"></div>   
	      		<div id="target_div"></div>
	      		
	      		<div id="problems_data_div" style="display: none;">
	      		
	      		
	      
								       
							           <div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:180px; text-align:right; margin-left:60px;">
								       	   		<b style="color:red">&lowast;</b>Enter Subject :
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								            <s:textfield name="subject"  id="subject" maxlength="100"    cssStyle="width:280px; border:1px solid #008abb; padding:11px 0 10px 3px"/>
								           		  <%--  <s:textfield  name="objCheckAppointmentDTO.businessMobile" maxlength="10"  id="mobileno"  cssStyle="width:280px; border:1px solid #008abb; padding:11px 0 10px 3px" ></s:textfield>--%>
								            </div>
							           </div>
							              
							           <div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:180px; text-align:right; margin-left:60px;">
								       	    	<b style="color:red">&lowast;</b>Enter Description :
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								            	
								            	<s:textarea name="description" id="description"  cssStyle="width:280px; border:1px solid #008abb; padding:11px 0 10px 3px" />
								            </div>
							           </div>
							     
							          
							           <div class="main_div_middle_textfields_main" style="width:340px; margin-left:230px;">
							          			 <input type="button"  id="problem_submit"  value="Submit" onclick="problem_clicked()" style="color:#fff; font-weight:bold; background-color:#008abb; text-align:center; padding:5px 0 5px 0; width:328px; border:none" />
							          			
							           </div>
							           
							           <div style="clear:both"></div>
						            </div>	
	      		
	      		
	      		
	      		
     
          
                 
          <div id="problems_div"></div>
          
   </div>  
	      		
	      		
	      		
		</div>
	     
  </div>
</div>
</div>
<div class="fotter">
	<div class="fotter_inner">
     All Rights Reserved - Copyright ｩ Dialus.com 2010-2013
    </div>
</div>
</body>
</html>
