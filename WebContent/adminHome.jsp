<%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Sales Tracking</title>
 <style type="text/css">
#map_div {
width:500px;
height:380px;
}
.white_content {		
	display: none;	
	position: fixed;
		top: 26%;
			left: 50%; margin-left:-250px;
			width: 40%;
			height: auto;
			padding: 16px;
			border: 5px solid #008abb;
			background-color:#fff;
				z-index: 99999;
				box-shadow: 0px 0px 20px #008abb;
				-moz-box-shadow: 0px 0px 20px #999; /* Firefox */
			    -webkit-box-shadow: 0px 0px 20px #999; /* Safari, Chrome */
				border-radius:20px 20px 20px 20px ;
				-moz-border-radius:20px 20px 20px 20px ;
			  -webkit-border-radius:20px 20px 20px 20px ;
			   
			overflow: auto;
		}
		 .black_overlay{
			display: none;
			position:fixed;
			top: 0%;
			left: 0%;
			width: 100%;
			height: 100%;
			background-color:black;
			-moz-opacity: 0.8;
			opacity:.70;
			filter: alpha(opacity=30);
		}
</style> 
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />
<script src="js/jquery-min.js"></script>
<link rel="stylesheet" href="css/jquery-ui123.css" />	 
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/redmond/jquery-ui.css" />

<!-- Plugin files below -->
<link rel="stylesheet" type="text/css" href="css/jquery.ptTimeSelect.css" />
<script type="text/javascript" src="js/jquery.ptTimeSelect.js"></script>

 <script type="text/javascript" src="js/salesTrackingValid.js"></script> 
 
 <script type="text/javascript">
function myfun1234(e,e1)
{	
		$("#update_targets").show();
		$(".black_overlay").show();
		
		$("#changetelename").val(e);
		$("#changetargets").val(e1);
		$("#reviews_results").hide();
		$("#reviews_div").hide();
	
	 
}

</script>
  <script>
  $(function() {
    $( ".datepicker" ).datepicker();
    $(".datepicker").datepicker("setDate",new Date);
  });
  </script>
  <script>
  
  function problemSubmit()
			{
	  
	 
				var hasError = false;
				var fdate=$("#datepicker11").val();
				var tdate=$("#datepicker12").val();
				$(".error").hide();
				if(fdate==''&& tdate=='')
			     {
			        
			           $("#msg3").html("");
			      	   $("#msg3").after('<span style="color: red" class="error"> Please Enter  Details Properly.</span>');
			           hasError = true;
			      	
			     }
			    var date1=new Date(fdate);
			    var date2=new Date(tdate);
			   
			     if(fdate!=''&&tdate!='')
			    	 {
			    	 	
			    	 	if(date1>date2)
			    	 		{
			    	 		   $("#msg3").html("");
					      	   $("#msg3").after('<span style="color: red" class="error"> From Date Should Be Less Than To date.</span>');
					           hasError = true;
			    	 		}
			    	 }

			        if(hasError == true)
			        { 
			        	return false; 
			        }
			        else
			        {
			        $("#problem_div").load("ProblemsAction.action?fdate="+fdate+"&tdate="+tdate+"&param=venki");
			        $("#problem_div_show").show();
			        $("#loading_img144").show();
			      
			       
			        	return true; 
			        }
		}
  </script>

<script type="text/javascript">


function show_date()
{
	
	 $( ".reportTodate123" ).datepicker();
	
	}
function submit_button_clicked()
{
	

	
		 $(".error").hide();
	     var hasError = false;
	     var todaydate=$('#today_date').val();
	   
	     if( todaydate=='')
	        {
	    	 	
	    	 	$("#today_date").after('<span style="color: red;white-space:nowrap;" class="error">Please Select Date.....</span>');
	    	
	    	 	hasError = true;
	        }
	    if(hasError == true){
	    	return false; 
	    }
	    
		    else
		    {
		    	
		    	
		    	$('#appointments').load("loadAppointments.action?todaydate="+todaydate);
		    	$('#appointments').show();
		    	
		    	$('#appointments_by_date').hide();
		    	
		    	
		    
		    	return true; 
		    }
			
			
	
	
	
	}

function date_time(id)
{
        date = new Date;
        year = date.getFullYear();
        month = date.getMonth();
        months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'Jully', 'August', 'September', 'October', 'November', 'December');
        d = date.getDate();
        day = date.getDay();
        days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
        h = date.getHours();
        if(h<10)
        {
                h = "0"+h;
        }
        m = date.getMinutes();
        if(m<10)
        {
                m = "0"+m;
        }
        s = date.getSeconds();
        if(s<10)
        {
                s = "0"+s;
        }
        result =  ''+d+' '  +months[month]+'  '+year+' -  '+h+':'+m+':'+s;
        document.getElementById(id).innerHTML = result;
        setTimeout('date_time("'+id+'");','1000');
        return true;
}







$(document).ready(function()
{
	   $( ".reportTodate123" ).datepicker(); 
	    $("#dailyplan_div").hide();
	    $("#dialycallplan_inner_div").hide();
		$('#success_msg').fadeOut(5000);
		$('#banner_div').show();
		$('#pending_appointments_div,#rejected_appointments_div,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#salesperson_div').hide();
		$('#telecaller_div').hide();
		$('#deletetelecallers_div').hide();
		$('#deletesalespersons_div').hide();
		$('#editsalespersons_div').hide();
		$("#calllist_div").hide();
		$("#saleslist_div").hide();
		$("#datewisereport_div").hide();
		$("#datewiseReportsResults_div").hide();
		$('#edittelecallers_div').hide();
		$('#editsalespersonsdetails_div').hide();
		$('#edittelecallersdetails_div').hide();
		$('#appointments').hide();
		$('#appointments_by_date').hide();
		$('#allocated_div').hide();
		$('#allocated_load_div').hide();
		$('#renewaldata').hide();
		$('#trackfield_div').hide();
		$("#mysms").hide();
        $("#show_sms_div").hide();
		
		$('#targets_div').hide();
		$('#teamleader_div').hide();
		$('#callplan_div').hide();
		$('#editteamleader_div').hide();
		$('#editteamleaderdetails_div').hide();
		
		$("#dialycallplan_submit").click(function()
				{ 
			
			 $(".error").hide();
			 var hasError = false;
			var city= encodeURIComponent($('#city_name').val());
		     
			 if(city=='')
				 {
				 $("#city_name").after('<span style="color: red" class="error"> Please Enter City.</span>');
	               hasError = true;
				 }
			     if(hasError == true)
		        { 
		        	
		      
		        	return false; 
		        }
			
		

			$("#dailyplan_link").css("background-color","yellow");
			$("#all_appointments_link").css("background-color","");
			$("#allocated_appointments_link").css("background-color","");
			$("#trackfield_link").css("background-color","");
			$("#submit_allocated").css("background-color","");
			$("#edituser_link").css("background-color","");
			$("#deleteuser_link").css("background-color","");
			$("#createuser_link").css("background-color","");
			$("#datewisereport_link").css("background-color","");
			
			$("#addtarget_link").css("background-color","");
			$("#appointments_link").css("background-color","");
			$("#callplan_on_leads").css("background-color","");
			$("#renewaldata_link").css("background-color","");
			$("#business_link").css("background-color","");
			$("#sms_link").css("background-color","");
			$("#leads_link").css("background-color","");
			
			$("#mysms").hide();
            $("#show_sms_div").hide();
			$("#dailyplan_div").hide();
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$('#editsalespersonsdetails_div').hide();
			$('#edittelecallersdetails_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$("#calllist_div").hide();
			$("#saleslist_div").hide();
			$("#datewisereport_div").hide();
			$("#datewiseReportsResults_div").hide();
			$("#business_div").hide();
			$('#pending_appointments_div,#rejected_appointments_div,#banner_div,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();  	
		
			$('#appointments').hide();
			$("#sms_div").hide();
			$('#appointments_by_name').hide();
			$('#allocated_div').hide();
			$('#allocated_load_div').hide();
			$('#renewaldata').hide();
			$('#trackfield_div').hide();
			$('#targets_div').hide();
			$('#callplan_div').hide();
			$('#teamleader_div').hide();
			$('#editteamleader_div').hide();
			$('#editteamleaderdetails_div').hide();
			
			$("#dialycallplan_inner_div").html("");
			$("#dialycallplan_inner_div").load("DailyCallPlanAction.action?city="+city);
			$("#dialycallplan_inner_div").show();
			
			$("#problem_div").hide();
			$("#problem_div_show").hide();
			
			
				});
				
		
		$("#appointments_link").click(function()
		{
			
		
			$("#appointments_link").css("background-color","yellow");
			$("#all_appointments_link").css("background-color","");
			$("#allocated_appointments_link").css("background-color","");
			$("#trackfield_link").css("background-color","");
			$("#submit_allocated").css("background-color","");
			$("#edituser_link").css("background-color","");
			$("#deleteuser_link").css("background-color","");
			$("#createuser_link").css("background-color","");
			$("#datewisereport_link").css("background-color","");
			
			$("#addtarget_link").css("background-color","");
			$("#dailyplan_link").css("background-color","");
			$("#callplan_on_leads").css("background-color","");
			$("#renewaldata_link").css("background-color","");
			$("#business_link").css("background-color","");
			$("#sms_link").css("background-color","");
			$("#leads_link").css("background-color","");
			$("#problems_link").css("background-color","");
			$("#mysms").hide();
            $("#show_sms_div").hide();
			$("#dailyplan_div").hide();
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$('#editsalespersonsdetails_div').hide();
			$('#edittelecallersdetails_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$("#calllist_div").hide();
			$("#saleslist_div").hide();
			$("#datewisereport_div").hide();
			$("#datewiseReportsResults_div").hide();
			$("#business_div").hide();
			$('#pending_appointments_div,#rejected_appointments_div,#banner_div,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();  	
		
			$('#appointments').hide();
			$("#sms_div").hide();
			$('#appointments_by_date').show();
			$('#appointments_by_name').hide();
			$('#allocated_div').hide();
			$('#allocated_load_div').hide();
			$('#renewaldata').hide();
			$('#trackfield_div').hide();
			$('#targets_div').hide();
			$('#callplan_div').hide();
			$('#teamleader_div').hide();
			$('#editteamleader_div').hide();
			$('#editteamleaderdetails_div').hide();
			 $("#dialycallplan_inner_div").hide();
			
			 $("#problem_div_show").hide();
		});
		
		$("#all_appointments_link").click(function()
				{
			$("#appointments_link").css("background-color","");
			$("#all_appointments_link").css("background-color","yellow");
			$("#allocated_appointments_link").css("background-color","");
			$("#trackfield_link").css("background-color","");
			$("#submit_allocated").css("background-color","");
			$("#edituser_link").css("background-color","");
			$("#deleteuser_link").css("background-color","");
			$("#createuser_link").css("background-color","");
			$("#datewisereport_link").css("background-color","");
			$("#addtarget_link").css("background-color","");
			$("#dailyplan_link").css("background-color","");
			$("#callplan_on_leads").css("background-color","");
			$("#renewaldata_link").css("background-color","");
			$("#business_link").css("background-color","");
			$("#sms_link").css("background-color","");
			$("#leads_link").css("background-color","");
			$("#problems_link").css("background-color","");
		
			
			        $("#mysms").hide();
                    $("#show_sms_div").hide();
			        $("#dailyplan_div").hide();
			        $("#dialycallplan_inner_div").hide();
					$('#deletetelecallers_div').hide();
					$('#deletesalespersons_div').hide();
					$('#editsalespersons_div').hide();
					$('#edittelecallers_div').hide();
					$('#editsalespersonsdetails_div').hide();
					$('#edittelecallersdetails_div').hide();
					$('#telecaller_div').hide();
					$('#salesperson_div').hide();
					$("#calllist_div").hide();
					$("#saleslist_div").hide();
					$("#datewisereport_div").hide();
					$("#datewiseReportsResults_div").hide();
					$("#business_div").hide();
					$('#pending_appointments_div,#rejected_appointments_div,#banner_div,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();  	
				    $('#appointments_by_name').load("allAppointments.action"); 
					$('#appointments').hide();
					$("#sms_div").hide();
					$('#appointments_by_name').show();
					$('#appointments_by_date').hide();
					$('#allocated_div').hide();
					$('#allocated_load_div').hide();
					$('#renewaldata').hide();
					$('#trackfield_div').hide();
					$('#targets_div').hide();
					$('#callplan_div').hide();
					$('#teamleader_div').hide();
					$('#editteamleader_div').hide();
					$('#editteamleaderdetails_div').hide();
					
					$("#problem_div_show").hide();
				});
		$("#allocated_appointments_link").click(function()
				{ 
			
			
			$("#appointments_link").css("background-color","");
			$("#all_appointments_link").css("background-color","");
			$("#allocated_appointments_link").css("background-color","yellow");
			$("#trackfield_link").css("background-color","");
			$("#submit_allocated").css("background-color","");
			$("#edituser_link").css("background-color","");
			$("#deleteuser_link").css("background-color","");
			$("#createuser_link").css("background-color","");
			$("#datewisereport_link").css("background-color","");
			$("#addtarget_link").css("background-color","");
			$("#dailyplan_link").css("background-color","");
			$("#callplan_on_leads").css("background-color","");
			$("#renewaldata_link").css("background-color","");
			$("#business_link").css("background-color","");
			$("#sms_link").css("background-color","");
			$("#leads_link").css("background-color","");
			$("#problems_link").css("background-color","");
			
			
			
			
			
			        $("#dailyplan_div").hide();
			        $("#dialycallplan_inner_div").hide();
			    	$("#mysms").hide();
                    $("#show_sms_div").hide();
					$('#deletetelecallers_div').hide();
					$('#deletesalespersons_div').hide();
					$('#editsalespersons_div').hide();
					$('#edittelecallers_div').hide();
					$('#editsalespersonsdetails_div').hide();
					$('#edittelecallersdetails_div').hide();
					$('#telecaller_div').hide();
					$('#salesperson_div').hide();
					$("#calllist_div").hide();
					$("#saleslist_div").hide();
					$("#datewisereport_div").hide();
					$("#datewiseReportsResults_div").hide();
					$("#business_div").hide();
					$('#pending_appointments_div,#rejected_appointments_div,#banner_div,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();  	
				    $('#appointments_by_name').hide(); 
					$('#appointments').hide();
					$("#sms_div").hide();
					$('#appointments_by_name').hide();
					$('#appointments_by_date').hide();
					$('#allocated_load_div').hide();
					$('#renewaldata').hide();
					$('#allocated_div').show();
					$('#trackfield_div').hide();
					$('#targets_div').hide();
					$('#callplan_div').hide();
					$('#teamleader_div').hide();
					$('#deleteteamleader_div').hide();
					$('#editteamleader_div').hide();
					$('#editteamleaderdetails_div').hide();
				
					$("#problem_div_show").hide();
				});
		
		$("#trackfield_link").click(function()
				{  
			
			$("#trackfield_link").css("background-color","yellow");
		
			$("#appointments_link").css("background-color","");
			$("#all_appointments_link").css("background-color","");
			$("#allocated_appointments_link").css("background-color","");
			
			$("#submit_allocated").css("background-color","");
			$("#edituser_link").css("background-color","");
			$("#deleteuser_link").css("background-color","");
			$("#createuser_link").css("background-color","");
			$("#datewisereport_link").css("background-color","");
			$("#addtarget_link").css("background-color","");
			$("#dailyplan_link").css("background-color","");
			$("#callplan_on_leads").css("background-color","");
			$("#renewaldata_link").css("background-color","");
			$("#business_link").css("background-color","");
			$("#sms_link").css("background-color","");
			$("#leads_link").css("background-color","");
			$("#problems_link").css("background-color",""); 
			
			       $("#loading_img6").show();
			       $("#trackfield_div").load("TrackFieldAction.action");
			       
			       $('#trackfield_div').show();
			       $("#mysms").hide();
                   $("#show_sms_div").hide();
					$('#success_msg').fadeOut(10000);
					$('#banner_div').hide();
					$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
					$('#salesperson_div').hide();
					$('#telecaller_div').hide();
					$('#deletetelecallers_div').hide();
					$('#deletesalespersons_div').hide();
					$('#editsalespersons_div').hide();
					$("#calllist_div").hide();
					$("#saleslist_div").hide();
					$("#datewiseReportsResults_div").hide();
					$('#edittelecallers_div').hide();
					$('#editsalespersonsdetails_div').hide();
					$('#edittelecallersdetails_div').hide();
					$('#appointments_by_date').hide();
				
					$("#datewisereport_div").hide();
					$('#appointments_by_name').hide();
					$("#business_div").hide();
					$("#sms_div").hide();
					$('#allocated_div').hide();
					$('#allocated_load_div').hide();
					$('#targets_div').hide();
					$('#callplan_div').hide();
					$('#teamleader_div').hide();
					$('#deleteteamleader_div').hide();
					$('#editteamleader_div').hide();
					$('#editteamleaderdetails_div').hide();
					
					$("#problem_div_show").hide();
				});
		
		
		
		$("#submit_allocated").click(function()
				{
			
			
			
			
			$("#appointments_link").css("background-color","");
			$("#all_appointments_link").css("background-color","");
			$("#allocated_appointments_link").css("background-color","");
			$("#trackfield_link").css("background-color","");
			$("#submit_allocated").css("background-color","yellow");
			$("#edituser_link").css("background-color","");
			$("#deleteuser_link").css("background-color","");
			$("#createuser_link").css("background-color","");
			$("#datewisereport_link").css("background-color","");
		
			$("#addtarget_link").css("background-color","");
			$("#dailyplan_link").css("background-color","");
			$("#callplan_on_leads").css("background-color","");
			$("#renewaldata_link").css("background-color","");
			$("#business_link").css("background-color","");
			$("#sms_link").css("background-color","");
			$("#leads_link").css("background-color","");
			$("#problems_link").css("background-color","");
			
			       $("#mysms").hide();
                   $("#show_sms_div").hide();
			      $("#dailyplan_div").hide();
			      $("#dialycallplan_inner_div").hide();
			        var name=$("#allocated_name").val();
			        var date=$("#allocated_date").val();
			        name=encodeURIComponent(name);
			        date=encodeURIComponent(date);
			        $('#trackfield_div').hide();
					$('#deletetelecallers_div').hide();
					$('#deletesalespersons_div').hide();
					$('#editsalespersons_div').hide();
					$('#edittelecallers_div').hide();
					$('#editsalespersonsdetails_div').hide();
					$('#edittelecallersdetails_div').hide();
					$('#telecaller_div').hide();
					$('#salesperson_div').hide();
					$("#calllist_div").hide();
					$("#saleslist_div").hide();
					$("#datewisereport_div").hide();
					$("#datewiseReportsResults_div").hide();
					$("#business_div").hide();
					$('#pending_appointments_div,#rejected_appointments_div,#banner_div,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();  	
				    $('#appointments_by_name').hide(); 
					$('#appointments').hide();
					$("#sms_div").hide();
					$('#appointments_by_name').hide();
					$('#appointments_by_date').hide();
					$('#allocated_div').hide();
					$('#allocated_load_div').load("AllocatedDetails.action?name="+name+"&date="+date);
					$('#allocated_load_div').show();
				   $("#allocated_name").val("");
			       $("#allocated_date").val("");
			       $('#renewaldata').hide();
			       $('#targets_div').hide();
			       $('#callplan_div').hide();
			   	   $('#teamleader_div').hide();
			    	$('#deleteteamleader_div').hide();
			    	$('#editteamleader_div').hide();
			    	$('#editteamleaderdetails_div').hide();
			    	
			    	$("#problem_div_show").hide();
				});
		
		
		
		$("#rejected_appointments_link").click(function()
		{	 $("#mysms").hide();
       		 $("#show_sms_div").hide();
			$("#dailyplan_div").hide();
			 $("#dialycallplan_inner_div").hide();
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#editsalespersonsdetails_div').hide();
			$('#edittelecallersdetails_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$("#calllist_div").hide();
			$('#telecaller_div').hide();
			$("#datewiseReportsResults_div").hide();
			$("#datewisereport_div").hide();
			$("#saleslist_div").hide();
			$('#salesperson_div').hide();
			$("#business_div").hide();
			$('#appointments_by_date').hide();
			$('#pending_appointments_div,#appointments,#banner_div,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();  	
			
			$('#rejected_appointments_div').load("rejectAppointments.action?type=reject");
			$('#rejected_appointments_div').show();
			$('#appointments_by_name').hide();
			$("#sms_div").hide();
			$('#allocated_load_div').hide();
			$('#renewaldata').hide();
			$('#trackfield_div').hide();
			$('#targets_div').hide();
			$('#callplan_div').hide();
			$('#teamleader_div').hide();
			$('#deleteteamleader_div').hide();
			$('#editteamleader_div').hide();
			$('#editteamleaderdetails_div').hide();
			$("#problem_div_show").hide();
		});
		
		$("#pending_appointments_link").click(function()
				{
					$("#mysms").hide();
            		$("#show_sms_div").hide();
            		
			        $("#dailyplan_div").hide();
			        $("#dialycallplan_inner_div").hide();
					$('#deletetelecallers_div').hide();
					$('#deletesalespersons_div').hide();
					$('#editsalespersonsdetails_div').hide();
					$('#edittelecallersdetails_div').hide();
					$('#editsalespersons_div').hide();
					$('#edittelecallers_div').hide();
					$("#datewiseReportsResults_div").hide();
					$("#datewisereport_div").hide();
					$('#appointments_by_date').hide();
					$('#telecaller_div').hide();
					$('#salesperson_div').hide();
					$("#calllist_div").hide();
					$('#rejected_appointments_div,#pending_appointments_div,#appointments,#banner_div,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();  	
					
					$('#pending_appointments_div').load("pendingAppointments.action?type=pending");
					$('#pending_appointments_div').show();
					$("#saleslist_div").hide();
					$("#business_div").hide();
					$('#appointments_by_date').hide();
					$('#appointments_by_name').hide();
					$("#sms_div").hide();
					$('#allocated_load_div').hide();
					$('#renewaldata').hide();
					$('#trackfield_div').hide();
					$('#targets_div').hide();
					$('#callplan_div').hide();
					$('#teamleader_div').hide();
					$('#deleteteamleader_div').hide();
					$('#editteamleader_div').hide();
					$('#editteamleaderdetails_div').hide();
				});
		
		
		$("#edituser_link").click(function()
		{
			
			
			
			
			
			$("#appointments_link").css("background-color","");
			$("#all_appointments_link").css("background-color","");
			$("#allocated_appointments_link").css("background-color","");
			$("#trackfield_link").css("background-color","");
			$("#submit_allocated").css("background-color","");
			$("#edituser_link").css("background-color","yellow");
			$("#deleteuser_link").css("background-color","");
			$("#createuser_link").css("background-color","");
			$("#datewisereport_link").css("background-color","");
		
			$("#addtarget_link").css("background-color","");
			$("#dailyplan_link").css("background-color","");
			$("#callplan_on_leads").css("background-color","");
			$("#renewaldata_link").css("background-color","");
			$("#business_link").css("background-color","");
			$("#sms_link").css("background-color","");
			$("#leads_link").css("background-color","");
			$("#problems_link").css("background-color","");
			
			$("#mysms").hide();
            $("#show_sms_div").hide();
            
			$("#dailyplan_div").hide();
			 $("#dialycallplan_inner_div").hide();
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$('#editsalespersonsdetails_div').hide();
			$('#edittelecallersdetails_div').hide();
			$("#datewisereport_div").hide();
			$("#saleslist_div").hide();
			$('#pending_appointments_div,#banner_div,#rejected_appointments_div,#appointments,#appointmentform_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
			
			$('#edituser_div').show();
			$("#datewiseReportsResults_div").hide();
			$('#appointments_by_date').hide();
			$("#calllist_div").hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$("#saleslist_div").hide();
			$("#business_div").hide();
			$("#sms_div").hide();
			$('#appointments_by_date').hide();
			$('#appointments_by_name').hide();
			$('#allocated_div').hide();
			$('#allocated_load_div').hide();
			$('#renewaldata').hide();
			$('#trackfield_div').hide();
			$('#targets_div').hide();
			$('#callplan_div').hide();
			$('#teamleader_div').hide();
			$('#deleteteamleader_div').hide();
			$('#editteamleader_div').hide();
			$('#editteamleaderdetails_div').hide();
			
			$("#problem_div_show").hide();
		});
		$("#deleteuser_link").click(function()
		{
			
			$("#appointments_link").css("background-color","");
			$("#all_appointments_link").css("background-color","");
			$("#allocated_appointments_link").css("background-color","");
			$("#trackfield_link").css("background-color","");
			$("#submit_allocated").css("background-color","");
			$("#edituser_link").css("background-color","");
			$("#deleteuser_link").css("background-color","yellow");
			$("#createuser_link").css("background-color","");
			$("#datewisereport_link").css("background-color","");
			
			$("#addtarget_link").css("background-color","");
			$("#dailyplan_link").css("background-color","");
			$("#callplan_on_leads").css("background-color","");
			$("#renewaldata_link").css("background-color","");
			$("#business_link").css("background-color","");
			$("#sms_link").css("background-color","");
			$("#leads_link").css("background-color","");
			$("#problems_link").css("background-color","");
			
			
			$("#mysms").hide();
            $("#show_sms_div").hide();
            
			$("#dailyplan_div").hide();
			 $("#dialycallplan_inner_div").hide();
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$('#editsalespersons_div').hide();
			$("#datewisereport_div").hide();
			$('#edittelecallers_div').hide();
			$("#datewiseReportsResults_div").hide();
			$("#calllist_div").hide();
			$("#saleslist_div").hide();
			$('#editsalespersonsdetails_div').hide();
			$('#appointments_by_date').hide();
			$('#edittelecallersdetails_div').hide();
			$("#business_div").hide();
			$('#pending_appointments_div,#edituser_div,#banner_div,#rejected_appointments_div,#appointments,#appointmentform_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
	
			$('#deleteuser_div').show();
			$('#appointments_by_name').hide();
			$("#sms_div").hide();
			$('#allocated_div').hide();
			$('#allocated_load_div').hide();
			$('#renewaldata').hide();
			$('#trackfield_div').hide();
			$('#targets_div').hide();
			$('#callplan_div').hide();
			$('#teamleader_div').hide();
			$('#deleteteamleader_div').hide();
			$('#editteamleader_div').hide();
			$('#editteamleaderdetails_div').hide();
	
			$("#problem_div_show").hide();
		});
		$("#createuser_link").click(function()
		{
			
			
			$("#appointments_link").css("background-color","");
			$("#all_appointments_link").css("background-color","");
			$("#allocated_appointments_link").css("background-color","");
			$("#trackfield_link").css("background-color","");
			$("#submit_allocated").css("background-color","");
			$("#edituser_link").css("background-color","");
			$("#deleteuser_link").css("background-color","");
			$("#createuser_link").css("background-color","yellow");
			$("#datewisereport_link").css("background-color","");
			
			$("#addtarget_link").css("background-color","");
			$("#dailyplan_link").css("background-color","");
			$("#callplan_on_leads").css("background-color","");
			$("#renewaldata_link").css("background-color","");
			$("#business_link").css("background-color","");
			$("#sms_link").css("background-color","");
			$("#leads_link").css("background-color","");
			$("#problems_link").css("background-color","");
			
			
			$("#mysms").hide();
            $("#show_sms_div").hide();
            
			$("#dailyplan_div").hide();
			 $("#dialycallplan_inner_div").hide();
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$('#editsalespersonsdetails_div').hide();
			$('#edittelecallersdetails_div').hide();
			$("#datewisereport_div").hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$("#calllist_div").hide();
			$("#datewiseReportsResults_div").hide();
			$('#salesperson_div').hide();
			$("#saleslist_div").hide();
			$('#telecaller_div').hide();
			$("#business_div").hide();
			$('#appointments_by_date').hide();
			$('#appointments_by_name').hide();
			$('#pending_appointments_div,#deleteuser_div,#edituser_div,#banner_div,#rejected_appointments_div,#appointments,#appointmentform_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
	
			$('#createuser_div').show();
			$("#sms_div").hide();
			$('#allocated_div').hide();
			$('#allocated_load_div').hide();
			$('#renewaldata').hide();
			$('#trackfield_div').hide();
			$('#targets_div').hide();
			$('#callplan_div').hide();
			$('#teamleader_div').hide();
			$('#deleteteamleader_div').hide();
			$('#editteamleader_div').hide();
			$('#editteamleaderdetails_div').hide();
		
			$("#problem_div_show").hide();
		});
		
		$("#telecallerreport_link").click(function()
				{
			
			
	
			
			
			        $("#mysms").hide();
                    $("#show_sms_div").hide();
                    
			        $("#dailyplan_div").hide();
			        $("#dialycallplan_inner_div").hide();
					$('#deletetelecallers_div').hide();
					$('#deletesalespersons_div').hide();
					$('#editsalespersons_div').hide();
					$('#edittelecallers_div').hide();
					$('#editsalespersonsdetails_div').hide();
					$('#edittelecallersdetails_div').hide();
					$("#datewisereport_div").hide();
					$('#telecaller_div').hide();
					$("#datewiseReportsResults_div").hide();
					$('#appointments_by_date').hide();
					$("#calllist_div").hide();
					$('#salesperson_div').hide();
					$("#saleslist_div").hide();
					$("#business_div").hide();
					$('#pending_appointments_div,#createuser_div,#deleteuser_div,#edituser_div,#banner_div,#rejected_appointments_div,#appointments,#appointmentform_div,#calllist_div,#salesreport_div').hide();
					$('#telecallername').load("ReportsAction.action?param=tele");
					$('#telecallerreport_div').show();
					$("#sms_div").hide();
					$('#appointments_by_name').hide();
					$('#allocated_div').hide();
					$('#allocated_load_div').hide();
					$('#renewaldata').hide();
					$('#trackfield_div').hide();
					$('#targets_div').hide();
					$('#callplan_div').hide();
					$('#teamleader_div').hide();
					$('#deleteteamleader_div').hide();
					$('#editteamleader_div').hide();
					$('#editteamleaderdetails_div').hide();
				
					$("#problem_div_show").hide();
				});
		
		
		
		$("#salesreport_link").click(function()
		{
			$("#mysms").hide();
            $("#show_sms_div").hide();
            
			$("#dailyplan_div").hide();
			 $("#dialycallplan_inner_div").hide();
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#editsalespersons_div').hide();
			$("#datewisereport_div").hide();
			$('#edittelecallers_div').hide();
			$("#calllist_div").hide();
			$('#editsalespersonsdetails_div').hide();
			$('#edittelecallersdetails_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$("#datewiseReportsResults_div").hide();
			$("#saleslist_div").hide();
			$('#appointments_by_date').hide();
			$("#business_div").hide();
			$("#sms_div").hide();
			$('#pending_appointments_div,#calllist_div,#telecallerreport_div,#createuser_div,#deleteuser_div,#edituser_div,#banner_div,#rejected_appointments_div,#appointments,#appointmentform_div').hide();
			$('#salespersonnamereport').load("ReportsAction.action?param=raj");
			$('#renewaldata').hide();
			$('#editteamleader_div').hide();
			$('#editteamleaderdetails_div').hide();
			
			$('#salesreport_div').show();
			$('#appointments_by_name').hide();
			$('#allocated_div').hide();
			$('#allocated_load_div').hide();
			$('#trackfield_div').hide();
			$('#targets_div').hide();
			$('#callplan_div').hide();
			$('#teamleader_div').hide();
			$('#deleteteamleader_div').hide();
	
			$("#problem_div_show").hide();
		});
		
		
		$("#salesperson_mobile").blur(function()
				{
					var businessMob1=$("#salesperson_mobile").val();	
					if(businessMob1!="")
					{
						$("#mbnoExist").load("mobStatus.action?salesperson_mobile="+businessMob1+"&type=sales");
						$("#mbnoExist").show();
						$("#mbnoExist").fadeOut(10000);
					}
				});
		
		$("#telecaller_mobile").blur(function()
				{
					var businessMob1=$("#telecaller_mobile").val();	
					if(businessMob1!="")
					{
						$("#telecallermbnoExist").load("telecallermobStatus.action?salesperson_mobile="+businessMob1+"&type=telecaller");
						$("#telecallermbnoExist").show();
						$("#telecallermbnoExist").fadeOut(10000);
					}
				});

		$("#target_submit").click(function()
				{
							 $(".error").hide();
						     var hasError = false;
						     var targetuser=$("#targetuser").val();
						     var targets= $("#targets").val();
						     if(targetuser=='')
						        {
						    	 	$("#targetuser").after('<span style="color: red;white-space:nowrap;" class="error">Please Enter TargetUser.....</span>');
						            hasError = true;
						        }
						     if(targets=='')
						     {
						    	 	$("#targets").after('<span style="color: red;white-space:nowrap;" class="error">Please Enter Targets.....</span>');
						            hasError = true;
						     }
						    
						    if(hasError == true)
						    { 
						    	
						    	return false; 
						    }
						    
				});
				

				$("#getTelecallerReport").click(function()
				{
							 $(".error").hide();
						     var hasError = false;
						     var report=$("#reporttype").val();
						     var person= $("#telecallername").val();
						     if(report=='123')
						        {
						    	 	$("#reporttype").after('<span style="color: red;white-space:nowrap;" class="error">Please Select Report Type.....</span>');
						            hasError = true;
						        }
						     if(person=='123')
						     {
						    	 	$("#telecallername").after('<span style="color: red;white-space:nowrap;" class="error">Please Select Telecaller Name.....</span>');
						            hasError = true;
						     }
						    
						    if(hasError == true)
						    { 
						    	
						    	return false; 
						    }
						    else
						    {
						    	$("#mysms").hide();
			                    $("#show_sms_div").hide();
						    	$('#deletetelecallers_div').hide();
								$('#deletesalespersons_div').hide();
								$('#editsalespersons_div').hide();
								$('#edittelecallers_div').hide();
								$('#editsalespersonsdetails_div').hide();
								$('#edittelecallersdetails_div').hide();
								$("#datewisereport_div").hide();
								$('#telecaller_div').hide();
								$('#salesperson_div').hide();
								$("#datewiseReportsResults_div").hide();
								$('#pending_appointments_div,#createuser_div,#deleteuser_div,#edituser_div,#banner_div,#rejected_appointments_div,#appointments,#appointmentform_div,#calllist_div,#salesreport_div').hide();
							
								$('#telecallerreport_div').hide();
								$("#saleslist_div").hide();
								$('#appointments_by_date').hide();
								$("#business_div").hide();
								$("#sms_div").hide();
						    	$("#calllist_div").load("getAllReports.action?report="+report+"&person="+person+"&type=caller");
						    	$("#calllist_div").show();
						    	$('#appointments_by_name').hide();
						    	$('#allocated_div').hide();
						    	$('#allocated_load_div').hide();
						    	$('#renewaldata').hide();
						    	$('#trackfield_div').hide();
						    	$('#targets_div').hide();
						    	$('#callplan_div').hide();
						    	$('#deleteteamleader_div').hide();
						    	$('#editteamleader_div').hide();
						    	$('#editteamleaderdetails_div').hide();
						    
						    	$("#problem_div_show").hide();
						    }
				});
			
				$("#getSalesPersonReport_but").click(function()
						{
									 $(".error").hide();
								     var hasError = false;
								     var report=$("#salesreporttype").val();
								     var person= $("#salespersonnamereport").val();
								     if(report=='123')
								        {
								    	 	$("#salesreporttype").after('<span style="color: red;white-space:nowrap;" class="error">Please Select Report Type.....</span>');
								            hasError = true;
								        }
								     if(person=='123')
								     {
								    	 	$("#salespersonnamereport").after('<span style="color: red;white-space:nowrap;" class="error">Please Select Telecaller Name.....</span>');
								            hasError = true;
								     }
								    
								    if(hasError == true)
								    { 
								    	
								    	return false; 
								    }
								    else
								    {
								    	$("#mysms").hide();
					                    $("#show_sms_div").hide();
					                    
								    	$('#deletetelecallers_div').hide();
										$('#deletesalespersons_div').hide();
										$('#editsalespersons_div').hide();
										$('#edittelecallers_div').hide();
										$('#editsalespersonsdetails_div').hide();
										$('#edittelecallersdetails_div').hide();
										$("#datewisereport_div").hide();
										$('#telecaller_div').hide();
										$("#datewiseReportsResults_div").hide();
										$('#salesperson_div').hide();
										$('#pending_appointments_div,#createuser_div,#deleteuser_div,#edituser_div,#banner_div,#rejected_appointments_div,#appointments,#appointmentform_div,#calllist_div,#salesreport_div').hide();
									
										$('#telecallerreport_div').hide();
								    	$("#calllist_div").hide();
								    	$("#saleslist_div").load("getAllReports.action?report="+report+"&person="+person+"&type=sales");
								    	$("#saleslist_div").show();
								    	$("#business_div").hide();
								    	$("#sms_div").hide();
								    	$('#appointments_by_date').hide();
								    	$('#appointments_by_name').hide();
								    	$('#allocated_div').hide();
								    	$('#allocated_load_div').hide();
								    	$('#renewaldata').hide();
								    	$('#trackfield_div').hide();
								    	$('#targets_div').hide();
								    	$('#callplan_div').hide();
								    	$('#teamleader_div').hide();
								    	$('#deleteteamleader_div').hide();
								    	$('#editteamleader_div').hide();
								    	$('#editteamleaderdetails_div').hide();
								    	$("#problem_div").hide();
								    	$("#problem_div_show").hide();
								    }
						});
		
		
				
				$("#datewisereport_link").click(function()
						{  
					
					
					
					
					
					$("#appointments_link").css("background-color","");
					$("#all_appointments_link").css("background-color","");
					$("#allocated_appointments_link").css("background-color","");
					$("#trackfield_link").css("background-color","");
					$("#submit_allocated").css("background-color","");
					$("#edituser_link").css("background-color","");
					$("#deleteuser_link").css("background-color","");
					$("#createuser_link").css("background-color","");
					$("#datewisereport_link").css("background-color","yellow");
			
					$("#addtarget_link").css("background-color","");
					$("#dailyplan_link").css("background-color","");
					$("#callplan_on_leads").css("background-color","");
					$("#renewaldata_link").css("background-color","");
					$("#business_link").css("background-color","");
					$("#sms_link").css("background-color","");
					$("#leads_link").css("background-color","");
					$("#problems_link").css("background-color","");
					
					
					
					
					
					
					
					         $("#mysms").hide();
                             $("#show_sms_div").hide();
					        $("#dailyplan_div").hide();
					        $("#dialycallplan_inner_div").hide();
							$('#success_msg').fadeOut(10000);
							$('#banner_div').show();
							$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
							$('#salesperson_div').hide();
							$('#telecaller_div').hide();
							$('#deletetelecallers_div').hide();
							$('#deletesalespersons_div').hide();
							$('#editsalespersons_div').hide();
							$("#calllist_div").hide();
							$("#saleslist_div").hide();
							$("#datewiseReportsResults_div").hide();
							$('#edittelecallers_div').hide();
							$('#editsalespersonsdetails_div').hide();
							$('#appointments_by_date').hide();
							$('#edittelecallersdetails_div').hide();

							$("#datewisereport_div").show();
							$("#business_div").hide();
							$("#sms_div").hide();
							$('#appointments_by_name').hide();
							$('#allocated_div').hide();
							$('#allocated_load_div').hide();
							$('#renewaldata').hide();
							$('#trackfield_div').hide();
							$('#targets_div').hide();
							$('#callplan_div').hide();
							$('#teamleader_div').hide();
							$('#deleteteamleader_div').hide();
							$('#editteamleader_div').hide();
							$('#editteamleaderdetails_div').hide();
							
							$("#problem_div_show").hide();
						});
		
				$("#dailyplan_link").click(function()
						{
					
					
					
					$("#appointments_link").css("background-color","");
					$("#all_appointments_link").css("background-color","");
					$("#allocated_appointments_link").css("background-color","");
					$("#trackfield_link").css("background-color","");
					$("#submit_allocated").css("background-color","");
					$("#edituser_link").css("background-color","");
					$("#deleteuser_link").css("background-color","");
					$("#createuser_link").css("background-color","");
					$("#datewisereport_link").css("background-color","");
				
					$("#addtarget_link").css("background-color","");
					$("#dailyplan_link").css("background-color","yellow");
					$("#callplan_on_leads").css("background-color","");
					$("#renewaldata_link").css("background-color","");
					$("#business_link").css("background-color","");
					$("#sms_link").css("background-color","");
					$("#leads_link").css("background-color","");
					$("#problems_link").css("background-color","");
					
					
					
					
					
					
					
					
					       $("#mysms").hide();
                           $("#show_sms_div").hide();
                           $("#loading_img").show();
					     //  $("#dailyplan_div").load("DailyCallPlanAction.action");
					       $("#dailyplan_div").show();
					       
							$('#success_msg').fadeOut(10000);
							
							$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
							$('#salesperson_div').hide();
							$('#telecaller_div').hide();
							$('#deletetelecallers_div').hide();
							$('#deletesalespersons_div').hide();
							$('#editsalespersons_div').hide();
							$("#calllist_div").hide();
							$("#saleslist_div").hide();
							$("#datewiseReportsResults_div").hide();
							$('#edittelecallers_div').hide();
							$('#editsalespersonsdetails_div').hide();
							$('#appointments_by_date').hide();
							$('#edittelecallersdetails_div').hide();
							$("#datewisereport_div").hide();
							$("#datewiseReportsResults_div").hide();
							$("#business_div").hide();
							$("#sms_div").hide();
							$('#appointments_by_name').hide();
							$('#allocated_div').hide();
							$('#allocated_load_div').hide(); 
							$('#renewaldata').hide();
							$('#trackfield_div').hide();
							$('#targets_div').hide();
							$('#callplan_div').hide();
							$('#teamleader_div').hide();
							$('#deleteteamleader_div').hide();
							$('#editteamleader_div').hide();
							$('#editteamleaderdetails_div').hide();
							 $("#dialycallplan_inner_div").hide();
						
							 $("#problem_div_show").hide();
							 $('#city_name').val("");
						});
				
				$("#business_link").click(function()
						{
					
					
					$("#appointments_link").css("background-color","");
					$("#all_appointments_link").css("background-color","");
					$("#allocated_appointments_link").css("background-color","");
					$("#trackfield_link").css("background-color","");
					$("#submit_allocated").css("background-color","");
					$("#edituser_link").css("background-color","");
					$("#deleteuser_link").css("background-color","");
					$("#createuser_link").css("background-color","");
					$("#datewisereport_link").css("background-color","");
				
					$("#addtarget_link").css("background-color","");
					$("#dailyplan_link").css("background-color","");
					$("#callplan_on_leads").css("background-color","");
					$("#renewaldata_link").css("background-color","");
					$("#business_link").css("background-color","yellow");
					$("#sms_link").css("background-color","");
					$("#leads_link").css("background-color","");
					$("#problems_link").css("background-color","");
					
							
					        
					        $("#dailyplan_div").hide();
					        $("#dialycallplan_inner_div").hide();
					       $('#success_msg').fadeOut(10000);
							$('#banner_div').show();
							$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
							$('#salesperson_div').hide();
							$('#telecaller_div').hide();
							$('#deletetelecallers_div').hide();
							$('#deletesalespersons_div').hide();
							$('#editsalespersons_div').hide();
							$("#calllist_div").hide();
							$("#saleslist_div").hide();
							$("#datewiseReportsResults_div").hide();
							$('#edittelecallers_div').hide();
							$('#editsalespersonsdetails_div').hide();
							$('#edittelecallersdetails_div').hide();
							$('#appointments_by_date').hide();
							 $("#mysms").hide();
                            $("#show_sms_div").hide();
							$("#datewisereport_div").hide();
							
							$("#loading_img2").show();
							$("#business_div").load("BusinessDetailAction.action");
							$("#business_div").show();
							$("#sms_div").hide();
							$('#appointments_by_name').hide();
							$('#allocated_div').hide();
							$('#allocated_load_div').hide();
							$('#renewaldata').hide();
							$('#trackfield_div').hide();
							$('#targets_div').hide();
							$('#callplan_div').hide();
							$('#teamleader_div').hide();
							$('#deleteteamleader_div').hide();
							$('#editteamleader_div').hide();
							$('#editteamleaderdetails_div').hide();
						
							$("#problem_div_show").hide();
						});
				
				
				$("#sms_link").click(function()
						{
					$("#appointments_link").css("background-color","");
					$("#all_appointments_link").css("background-color","");
					$("#allocated_appointments_link").css("background-color","");
					$("#trackfield_link").css("background-color","");
					$("#submit_allocated").css("background-color","");
					$("#edituser_link").css("background-color","");
					$("#deleteuser_link").css("background-color","");
					$("#createuser_link").css("background-color","");
					$("#datewisereport_link").css("background-color","");
					
					$("#addtarget_link").css("background-color","");
					$("#dailyplan_link").css("background-color","");
					$("#callplan_on_leads").css("background-color","");
					$("#renewaldata_link").css("background-color","");
					$("#business_link").css("background-color","");
					$("#sms_link").css("background-color","yellow");
					$("#leads_link").css("background-color","");
					$("#problems_link").css("background-color","");
					
					
					
					
					
					
					
					$("#mysms").hide();
                    $("#show_sms_div").hide();
							$('#success_msg').fadeOut(10000);
							$('#banner_div').hide();
							$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
							$('#salesperson_div').hide();
							$('#telecaller_div').hide();
							$('#deletetelecallers_div').hide();
							$('#deletesalespersons_div').hide();
							$('#editsalespersons_div').hide();
							$("#calllist_div").hide();
							$("#saleslist_div").hide();
							$("#datewiseReportsResults_div").hide();
							$('#edittelecallers_div').hide();
							$('#editsalespersonsdetails_div').hide();
							$('#edittelecallersdetails_div').hide();
							$('#appointments_by_date').hide();
						
							$("#datewisereport_div").hide();
							$('#appointments_by_name').hide();
							$("#business_div").hide();
							$("#loading_img3").show();
							$("#sms_div").load("SmsDetailAction.action");
							$("#sms_div").show();
							$('#allocated_div').hide();
							$('#allocated_load_div').hide();
							$('#renewaldata').hide();
							$('#trackfield_div').hide();
							$('#targets_div').hide();
							$('#callplan_div').hide();
							$('#teamleader_div').hide();
							$('#deleteteamleader_div').hide();
							 $("#dailyplan_div").hide();
							 $("#dialycallplan_inner_div").hide();
							 $('#editteamleader_div').hide();
							 $('#editteamleaderdetails_div').hide();
							
							 $("#problem_div_show").hide();
						});
							
				$("#leads_link").click(function()
						{
					
					
					$("#appointments_link").css("background-color","");
					$("#all_appointments_link").css("background-color","");
					$("#allocated_appointments_link").css("background-color","");
					$("#trackfield_link").css("background-color","");
					$("#submit_allocated").css("background-color","");
					$("#edituser_link").css("background-color","");
					$("#deleteuser_link").css("background-color","");
					$("#createuser_link").css("background-color","");
					$("#datewisereport_link").css("background-color","");
				
					$("#addtarget_link").css("background-color","");
					$("#dailyplan_link").css("background-color","");
					$("#callplan_on_leads").css("background-color","");
					$("#renewaldata_link").css("background-color","");
					$("#business_link").css("background-color","");
					$("#sms_link").css("background-color","");
					$("#leads_link").css("background-color","yellow");
					$("#problems_link").css("background-color","");
					
					
					
					
					
					
					
					
					
					
					
							$('#success_msg').fadeOut(10000);
							$('#banner_div').hide();
							$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
							$('#salesperson_div').hide();
							$('#telecaller_div').hide();
							$('#deletetelecallers_div').hide();
							$('#deletesalespersons_div').hide();
							$('#editsalespersons_div').hide();
							$("#calllist_div").hide();
							$("#saleslist_div").hide();
							$("#datewiseReportsResults_div").hide();
							$('#edittelecallers_div').hide();
							$('#editsalespersonsdetails_div').hide();
							$('#edittelecallersdetails_div').hide();
							$('#appointments_by_date').hide();
						    $("#datewisereport_div").hide();
							$('#appointments_by_name').hide();
							$("#business_div").hide();
							$("#loading_img4").show();
							$("#leads_div").load("GetLeadsAction.action");
							$("#leads_div").show();
							$("#sms_div").hide();
							$('#allocated_div').hide();
							$('#allocated_load_div').hide();
							$('#renewaldata').hide();
							$('#trackfield_div').hide();
							$('#targets_div').hide();
							$('#callplan_div').hide();
							$('#teamleader_div').hide();
							$('#deleteteamleader_div').hide();
							$("#dailyplan_div").hide();
							 $("#dialycallplan_inner_div").hide();
							$('#editteamleader_div').hide();
							$('#editteamleaderdetails_div').hide();
					
							$("#problem_div_show").hide();
						});
				
				
				
				
				
				$("#callplan_on_leads").click(function()
						{
					
					

					$("#appointments_link").css("background-color","");
					$("#all_appointments_link").css("background-color","");
					$("#allocated_appointments_link").css("background-color","");
					$("#trackfield_link").css("background-color","");
					$("#submit_allocated").css("background-color","");
					$("#edituser_link").css("background-color","");
					$("#deleteuser_link").css("background-color","");
					$("#createuser_link").css("background-color","");
					$("#datewisereport_link").css("background-color","");
				
					$("#addtarget_link").css("background-color","");
					$("#dailyplan_link").css("background-color","");
					$("#callplan_on_leads").css("background-color","yellow");
					$("#renewaldata_link").css("background-color","");
					$("#business_link").css("background-color","");
					$("#sms_link").css("background-color","");
					$("#leads_link").css("background-color","");
					$("#problems_link").css("background-color","");
					
					$("#mysms").hide();
                    $("#show_sms_div").hide();
					$('#banner_div').hide();
					$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
					$('#salesperson_div').hide();
					$('#telecaller_div').hide();
					$('#deletetelecallers_div').hide();
					$('#deletesalespersons_div').hide();
					$('#editsalespersons_div').hide();
					$("#calllist_div").hide();
					$("#saleslist_div").hide();
					$("#datewiseReportsResults_div").hide();
					$('#edittelecallers_div').hide();
					$('#editsalespersonsdetails_div').hide();
					$('#edittelecallersdetails_div').hide();
					$('#appointments_by_date').hide();

					$("#datewisereport_div").hide();
					$('#appointments_by_name').hide();
					$("#business_div").hide();
				
					$("#sms_div").hide();
					$('#allocated_div').hide();
					$('#allocated_load_div').hide();
					$('#renewaldata').hide();
					$('#trackfield_div').hide();
					$('#targets_div').hide();
					
					$("#loading_img11").show();
					$('#callplan_div').load("CallPlanDataAction.action");
						
					
					$('#callplan_div').show();
					$('#teamleader_div').hide();
					$('#deleteteamleader_div').hide();
					$("#dailyplan_div").hide();	
					 $("#dialycallplan_inner_div").hide();
					$('#editteamleader_div').hide();
					$('#editteamleaderdetails_div').hide();
					
					$("#problem_div_show").hide();
						});
				
				
				
				
				
				
				
				
				$("#problems_link").click(function(){
					
					$("#appointments_link").css("background-color","");
					$("#all_appointments_link").css("background-color","");
					$("#allocated_appointments_link").css("background-color","");
					$("#submit_allocated").css("background-color","");
					$("#edituser_link").css("background-color","");
					$("#deleteuser_link").css("background-color","");
					$("#createuser_link").css("background-color","");
					$("#datewisereport_link").css("background-color","");
					$("#trackfield_link").css("background-color","");
					$("#addtarget_link").css("background-color","");
					$("#dailyplan_link").css("background-color","");
					$("#callplan_on_leads").css("background-color","");
					$("#renewaldata_link").css("background-color","");
					$("#business_link").css("background-color","");
					$("#sms_link").css("background-color","");
					$("#leads_link").css("background-color","");
					$("#problems_link").css("background-color","yellow");
				   
				    $("#problem_div").load("ProblemsAction.action");
				    $("#problem_div_show").show();
				    
				    $("#mysms").hide();
                    $("#show_sms_div").hide();
					        $("#dailyplan_div").hide();
					        $("#dialycallplan_inner_div").hide();
					        
					
							
							$('#renewaldata').hide();
							
							$('#banner_div').hide();
							$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
							$('#salesperson_div').hide();
							$('#telecaller_div').hide();
							$('#deletetelecallers_div').hide();
							$('#deletesalespersons_div').hide();
							$('#editsalespersons_div').hide();
							$("#calllist_div").hide();
							$("#saleslist_div").hide();
							$("#datewiseReportsResults_div").hide();
							$('#edittelecallers_div').hide();
							$('#editsalespersonsdetails_div').hide();
							$('#edittelecallersdetails_div').hide();
							$('#appointments_by_date').hide();
					     	$("#datewisereport_div").hide();
							$('#appointments_by_name').hide();
							$("#business_div").hide();
							$('#callplan_div').hide();
							$("#sms_div").hide();
							$('#allocated_div').hide();
							$('#allocated_load_div').hide();
							$('#trackfield_div').hide();
							$('#targets_div').hide();
							$('#teamleader_div').hide();
							$('#deleteteamleader_div').hide();
							$('#editteamleader_div').hide();
							$('#editteamleaderdetails_div').hide();
				   
					
					});	
				
				
				

				$("#renewaldata_link").click(function()
						{
					
					$("#appointments_link").css("background-color","");
					$("#all_appointments_link").css("background-color","");
					$("#allocated_appointments_link").css("background-color","");
					$("#submit_allocated").css("background-color","");
					$("#edituser_link").css("background-color","");
					$("#deleteuser_link").css("background-color","");
					$("#createuser_link").css("background-color","");
					$("#datewisereport_link").css("background-color","");
					$("#trackfield_link").css("background-color","");
					$("#addtarget_link").css("background-color","");
					$("#dailyplan_link").css("background-color","");
					$("#callplan_on_leads").css("background-color","");
					$("#renewaldata_link").css("background-color","yellow");
					$("#business_link").css("background-color","");
					$("#sms_link").css("background-color","");
					$("#leads_link").css("background-color","");
					$("#problems_link").css("background-color","");
					$("#mysms").hide();
                    $("#show_sms_div").hide();
					        $("#dailyplan_div").hide();
					        $("#dialycallplan_inner_div").hide();
					        
					        $("#loading_img111").show();
							$('#renewaldata').load("RenewalDataAction.action");
							$('#renewaldata').show();
							$('#success_msg').fadeOut(10000);
							$('#banner_div').hide();
							$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
							$('#salesperson_div').hide();
							$('#telecaller_div').hide();
							$('#deletetelecallers_div').hide();
							$('#deletesalespersons_div').hide();
							$('#editsalespersons_div').hide();
							$("#calllist_div").hide();
							$("#saleslist_div").hide();
							$("#datewiseReportsResults_div").hide();
							$('#edittelecallers_div').hide();
							$('#editsalespersonsdetails_div').hide();
							$('#edittelecallersdetails_div').hide();
							$('#appointments_by_date').hide();
					     	$("#datewisereport_div").hide();
							$('#appointments_by_name').hide();
							$("#business_div").hide();
							$('#callplan_div').hide();
							$("#sms_div").hide();
							$('#allocated_div').hide();
							$('#allocated_load_div').hide();
							$('#trackfield_div').hide();
							$('#targets_div').hide();
							$('#teamleader_div').hide();
							$('#deleteteamleader_div').hide();
							$('#editteamleader_div').hide();
							$('#editteamleaderdetails_div').hide();
							$("#problem_div_show").hide();
						});
				

				 $("#addtarget_link").click(function()
						{
					 $("#appointments_link").css("background-color","");
						$("#all_appointments_link").css("background-color","");
						$("#allocated_appointments_link").css("background-color","");
						$("#submit_allocated").css("background-color","");
						$("#edituser_link").css("background-color","");
						$("#deleteuser_link").css("background-color","");
						$("#createuser_link").css("background-color","");
						$("#datewisereport_link").css("background-color","");
						$("#trackfield_link").css("background-color","");
						$("#addtarget_link").css("background-color","yellow");
						$("#dailyplan_link").css("background-color","");
						$("#callplan_on_leads").css("background-color","");
						$("#renewaldata_link").css("background-color","");
						$("#business_link").css("background-color","");
						$("#sms_link").css("background-color","");
						$("#leads_link").css("background-color","");
						$("#problems_link").css("background-color","");
					 
					 
							
					   $("#targetuser").val("");
			     	   $("#targets").val(""); 
					             
							
					 var userName=$("#sessionuser").val();
					 
							$('#targets_div').show();
							 $("#targets_div_inner").load("GetTargetAction.action?user="+userName);
							$('#success_msg').fadeOut(10000);

							$("#mysms").hide();
		                    $("#show_sms_div").hide();
							$('#renewaldata').hide();
							$('#success_msg').fadeOut(10000);
							$('#banner_div').hide();
							$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
							$('#salesperson_div').hide();
							$('#telecaller_div').hide();
							$('#deletetelecallers_div').hide();
							$('#deletesalespersons_div').hide();
							$('#editsalespersons_div').hide();
							$("#calllist_div").hide();
							$("#saleslist_div").hide();
							$("#datewiseReportsResults_div").hide();
							$('#edittelecallers_div').hide();
							$('#editsalespersonsdetails_div').hide();
							$('#edittelecallersdetails_div').hide();
							$('#appointments_by_date').hide();
			
							$("#datewisereport_div").hide();
							$('#appointments_by_name').hide();
							$("#business_div").hide();
							
							$("#sms_div").hide();
							$('#allocated_div').hide();
							$('#allocated_load_div').hide();
							$('#trackfield_div').hide();
							$('#callplan_div').hide();
							$('#teamleader_div').hide();
							$('#deleteteamleader_div').hide();
							$("#dailyplan_div").hide();
							 $("#dialycallplan_inner_div").hide();
							$('#editteamleader_div').hide();
							$('#editteamleaderdetails_div').hide();
							$("#problem_div_show").hide();
						});
				 
				 $(".closeRatingBox").click(function(){
						$('#update_targets').hide();
						$('.white_content').fadeOut(1000);
						$('.black_overlay').fadeOut(1000);
						
						
					
				 });
				
				
				
		
});


function loadMap()
{
	function initialize()
	{
		var mapProp =
		{
		  center:new google.maps.LatLng(51.508742,-0.120850),
		  zoom:5,
		  mapTypeId:google.maps.MapTypeId.ROADMAP
		};
		var map=new google.maps.Map(mapProp);
		return map;
	}
	$('#map_div').html(google.maps.event.addDomListener(window, 'load', initialize));
	
}

function selectUser()
{
	
	var person=$('#persontype').val();
	if(person=='salesperson')
	{
		$("#mysms").hide();
         $("#show_sms_div").hide();
		$('#editsalespersonsdetails_div').hide();
		$('#edittelecallersdetails_div').hide();
		$('#banner_div').hide();
		$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$("#datewisereport_div").hide();
		$("#calllist_div").hide();
		$('#deletetelecallers_div').hide();
		$('#deletesalespersons_div').hide();
		$("#saleslist_div").hide();
		$('#editsalespersons_div').hide();
		$("#datewiseReportsResults_div").hide();
		$('#edittelecallers_div').hide();
		$("#sms_div").hide();
		$('#salesperson_div').show();
		$('#appointments_by_name').hide();
		$('#appointments_by_date').hide();
		$('#allocated_div').hide();
		$('#allocated_load_div').hide();
		$('#trackfield_div').hide();
		$('#teamleader_div').hide();
		$('#deleteteamleader_div').hide();
		$("#dailyplan_div").hide();
		 $("#dialycallplan_inner_div").hide();
		$('#editteamleader_div').hide();
		$('#editteamleaderdetails_div').hide();
	}else if(person=='telecaller')
	{
		$("#mysms").hide();
        $("#show_sms_div").hide();
		$('#pending_appointments_div,#editsalespersonsdetails_div').hide();
		$('#edittelecallersdetails_div').hide();
		$('#banner_div').hide();
		$('#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').show();
		$("#calllist_div").hide();
		$('#salesperson_div').hide();
		$('#deletetelecallers_div').hide();
		$("#datewisereport_div").hide();
		$('#deletesalespersons_div').hide();
		$("#datewiseReportsResults_div").hide();
		$("#saleslist_div").hide();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').hide();
		$("#sms_div").hide();
		$('#appointments_by_name').hide();
		$('#appointments_by_date').hide();
		$('#allocated_div').hide();
		$('#allocated_load_div').hide();
		$('#trackfield_div').hide();
		$('#teamleader_div').hide();
		$('#deleteteamleader_div').hide();
		$("#dailyplan_div").hide();
		 $("#dialycallplan_inner_div").hide();
		$('#editteamleader_div').hide();
		$('#editteamleaderdetails_div').hide();
	}
	else if(person=='123')
	{
		$("#mysms").hide();
        $("#show_sms_div").hide();
		$('#pending_appointments_div,#editsalespersonsdetails_div').hide();
		$('#edittelecallersdetails_div').hide();
		$('#banner_div').hide();
		$("#calllist_div").hide();
		$('#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$('#salesperson_div').hide();
		$('#deletetelecallers_div').hide();
		$('#deletesalespersons_div').hide();
		$('#editsalespersons_div').hide();
		$("#datewiseReportsResults_div").hide();
		$("#datewisereport_div").hide();
		
		$('#edittelecallers_div').hide();
		$("#saleslist_div").hide();
		$('#appointments_by_date').hide();
		$('#createuser_div').show();
		$('#appointments_by_name').hide();
		$('#allocated_div').hide();
		$('#persontype').after('<span style="color: red" class="error">Select Any One User Type..... .</span>');
		$("#sms_div").hide();
		$('#allocated_load_div').hide();
		$('#trackfield_div').hide();
		$('#teamleader_div').hide();
		$('#deleteteamleader_div').hide();
		$("#dailyplan_div").hide();
		 $("#dialycallplan_inner_div").hide();
		$('#editteamleader_div').hide();
		$('#editteamleaderdetails_div').hide();
	}
	if(person=='teamleader')
	{
		$("#mysms").hide();
        $("#show_sms_div").hide();
		$('#editsalespersonsdetails_div').hide();
		$('#edittelecallersdetails_div').hide();
		$('#banner_div').hide();
		$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$("#datewisereport_div").hide();
		$("#calllist_div").hide();
		$('#deletetelecallers_div').hide();
		$('#deletesalespersons_div').hide();
		$("#saleslist_div").hide();
		$('#editsalespersons_div').hide();
		$("#datewiseReportsResults_div").hide();
		$('#edittelecallers_div').hide();
		$("#sms_div").hide();
		$('#salesperson_div').hide();
		$('#teamleader_div').show();
		$('#appointments_by_name').hide();
		$('#appointments_by_date').hide();
		$('#allocated_div').hide();
		$('#allocated_load_div').hide();
		$('#deleteteamleader_div').hide();
		$('#trackfield_div').hide();
		$('#deleteteamleader_div').hide();
		$("#dailyplan_div").hide();
		 $("#dialycallplan_inner_div").hide();
		$('#editteamleader_div').hide();
		$('#editteamleaderdetails_div').hide();
		
	}
	
	
	
	
}
function selectDeleteUser()
{
	var person=$('#deletepersontype').val();
	if(person=='salesperson')
	{   
		$("#mysms").hide();
        $("#show_sms_div").hide();
		$("#sms_div").hide();
		$('#pending_appointments_div,#banner_div').hide();
		$('#editsalespersons_div').hide();
		$("#calllist_div").hide();
		$('#edittelecallers_div').hide();
		$('#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$('#salesperson_div').hide();
		$('#deleteuser_div').hide();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').hide();
		$("#datewisereport_div").hide();
		$('#appointments_by_date').hide();
		$("#datewiseReportsResults_div").hide();
		$("#saleslist_div").hide();
		$('#deletetelecallers_div').hide();
		$('#deletesalespersons_div').load("loadUsers.action?type=salesperson");
		$('#deletesalespersons_div').show();
		$('#appointments_by_name').hide();
		$('#allocated_div').hide();
		$('#allocated_load_div').hide();
		$('#trackfield_div').hide();
		$('#teamleader_div').hide();
		$('#deleteteamleader_div').hide();
		$("#dailyplan_div").hide();
		 $("#dialycallplan_inner_div").hide();
		$('#editteamleader_div').hide();
		$('#editteamleaderdetails_div').hide();
	}else if(person=='telecaller')
	{
		$("#mysms").hide();
        $("#show_sms_div").hide();
		$('#pending_appointments_div,#banner_div').hide();
		$('#editsalespersons_div').hide();
		$("#calllist_div").hide();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').hide();
		$('#edittelecallers_div').hide();
		$('#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$('#salesperson_div').hide();
		$("#datewisereport_div").hide();
		$('#deleteuser_div').hide();
		$("#datewiseReportsResults_div").hide();
		$('#deletesalespersons_div').hide();
		$('#appointments_by_date').hide();
		$('#deletetelecallers_div').load("loadUsers.action?type=telecaller");
		$('#deletetelecallers_div').show();
		$('#appointments_by_name').hide();
		$("#sms_div").hide();
		$('#allocated_div').hide();
		$('#trackfield_div').hide();
		$('#teamleader_div').hide();
		$('#deleteteamleader_div').hide();
		$("#dailyplan_div").hide();
		 $("#dialycallplan_inner_div").hide();
		$('#editteamleader_div').hide();
		$('#editteamleaderdetails_div').hide();
	}
	else if(person=='123')
	{
		$("#mysms").hide();
        $("#show_sms_div").hide();
		$('#banner_div').hide();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').hide();
		$("#calllist_div").hide();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').hide();
		$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$('#salesperson_div').hide();
		$("#datewisereport_div").hide();
		$('#createuser_div').hide();
		$('#deleteuser_div').show();
		$('#deletetelecallers_div').hide();
		$("#datewiseReportsResults_div").hide();
		$('#appointments_by_date').hide();
		$('#deletesalespersons_div').hide();
		$("#saleslist_div").hide();
		$('#deletepersontype').after('<span style="color: red" class="error">Select Any One User Type..... .</span>');
		$("#sms_div").hide();
		$('#appointments_by_name').hide();
		$('#allocated_div').hide();
		$('#allocated_load_div').hide();
		$('#trackfield_div').hide();
		$('#teamleader_div').hide();
		$('#deleteteamleader_div').hide();
		$("#dailyplan_div").hide();
		 $("#dialycallplan_inner_div").hide();
		$('#editteamleader_div').hide();
		$('#editteamleaderdetails_div').hide();
	}else
		{
			
		if(person=='teamleader')
		{  
			$("#mysms").hide();
            $("#show_sms_div").hide();
			$("#sms_div").hide();
			$('#pending_appointments_div,#banner_div').hide();
			$('#editsalespersons_div').hide();
			$("#calllist_div").hide();
			$('#edittelecallers_div').hide();
			$('#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$('#deleteuser_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$("#datewisereport_div").hide();
			$('#appointments_by_date').hide();
			$("#datewiseReportsResults_div").hide();
			$("#saleslist_div").hide();
			$('#deletetelecallers_div').hide();
			$('#editteamleader_div').hide();
			$('#deletesalespersons_div').hide();
			$('#deleteteamleader_div').load("loadUsers.action?type=teamleader");
			$('#deleteteamleader_div').show();
			$('#appointments_by_name').hide();
			$('#allocated_div').hide();
			$('#allocated_load_div').hide();
			$('#trackfield_div').hide();
			$('#teamleader_div').hide();
			$("#dailyplan_div").hide();
			 $("#dialycallplan_inner_div").hide();
			$('#editteamleaderdetails_div').hide();
			
		  }
		}
		
}
function selectEditUser()
{
	
	var person=$('#editpersontype').val();
	
	if(person=='salesperson')
	{
		$("#mysms").hide();
        $("#show_sms_div").hide();
		$('#banner_div').hide();
		$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$('#editsalespersons_div').hide();
		$("#calllist_div").hide();
		$('#edittelecallers_div').hide();
		$('#salesperson_div').hide();
		$('#deleteuser_div').hide();
		$('#deletetelecallers_div').hide();
		$('#deletesalespersons_div').hide();
		$("#datewiseReportsResults_div").hide();
		$("#datewisereport_div").hide();
		$('#edittelecallers_div').hide();
		$("#saleslist_div").hide();
		$('#appointments_by_date').hide();
		$('#editsalespersons_div').load("loadEditUsers.action?type=salesperson");
		$('#editsalespersons_div').show();
		$("#sms_div").hide();
		$('#appointments_by_name').hide();
		$('#allocated_div').hide();
		$('#allocated_load_div').hide();
		$('#trackfield_div').hide();
		$('#deleteteamleader_div').hide();
		$("#dailyplan_div").hide();
		 $("#dialycallplan_inner_div").hide();
		$('#editteamleader_div').hide();
		$('#editteamleaderdetails_div').hide();
	}
	else if(person=='telecaller')
	{
		$('#banner_div').hide();
		$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$('#salesperson_div').hide();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').hide();
		$('#deleteuser_div').hide();
		$('#deletesalespersons_div').hide();
		$("#calllist_div").hide();
		$('#appointments_by_date').hide();
		$('#deletetelecallers_div').hide();
		$("#datewisereport_div").hide();
		$("#datewiseReportsResults_div").hide();
		$("#saleslist_div").hide();
		$('#appointments_by_date').hide();
		$('#edittelecallers_div').show();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').load("loadEditUsers.action?type=telecaller");
		$('#edittelecallers_div').show();
		$("#sms_div").hide();
		$('#appointments_by_name').hide();
		$('#allocated_div').hide();
		$('#allocated_load_div').hide();
		$('#trackfield_div').hide();
		$('#deleteteamleader_div').hide();
		$("#dailyplan_div").hide();
		 $("#dialycallplan_inner_div").hide();
		$('#editteamleader_div').hide();
		$('#editteamleaderdetails_div').hide();
	}
	else if(person=='123')
	{
		
		$("#mysms").hide();
        $("#show_sms_div").hide();
		$('#banner_div').hide();
		$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$('#salesperson_div').hide();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').hide();
		$("#calllist_div").hide();
		$('#createuser_div').hide();
		$('#deleteuser_div').hide();
		$('#deletetelecallers_div').hide();
		$("#datewisereport_div").hide();
		$('#deletesalespersons_div').hide();
		$('#editsalespersons_div').hide();
		$("#datewiseReportsResults_div").hide();
		$('#edittelecallers_div').hide();
		$('#appointments_by_date').hide();
		$("#saleslist_div").hide();
		$('#edituser_div').show();
		$('#editpersontype').after('<span style="color: red" class="error">Select Any One User Type..... .</span>');
		$("#sms_div").hide();
		$('#appointments_by_name').hide();
		$('#allocated_div').hide();
		$('#allocated_load_div').hide();
		$('#trackfield_div').hide();
		$('#deleteteamleader_div').hide();
		$("#dailyplan_div").hide();
		 $("#dialycallplan_inner_div").hide();
		$('#editteamleader_div').hide();
		$('#editteamleaderdetails_div').hide();
	}
	else if(person=='teamleader')
	{
		$("#mysms").hide();
        $("#show_sms_div").hide();
		$('#banner_div').hide();
		$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#telecaller_div').hide();
		$('#salesperson_div').hide();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').hide();
		$('#deleteuser_div').hide();
		$('#deletesalespersons_div').hide();
		$("#calllist_div").hide();
		$('#appointments_by_date').hide();
		$('#deletetelecallers_div').hide();
		$("#datewisereport_div").hide();
		$("#datewiseReportsResults_div").hide();
		$("#saleslist_div").hide();
		$('#appointments_by_date').hide();
		$('#editsalespersons_div').hide();
		$('#edittelecallers_div').hide();
		$('#editteamleader_div').load("loadEditUsers.action?type=teamleader");
		$('#editteamleader_div').show();
		$("#sms_div").hide();
		$('#appointments_by_name').hide();
		$('#allocated_div').hide();
		$('#allocated_load_div').hide();
		$('#trackfield_div').hide();
		$('#editteamleaderdetails_div').hide();
		$('#editteamleaderdetails_div').hide();
	}
	
	
	
	
	
	
	
}


function editTelecallers(type,userId)
{
	
	$("#mysms").hide();
    $("#show_sms_div").hide();
	$('#banner_div').hide();
	$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
	$('#salesperson_div').hide();
	$('#telecaller_div').hide();
	$("#calllist_div").hide();
	$('#deletetelecallers_div').hide();
	$("#datewisereport_div").hide();
	$('#deletesalespersons_div').hide();
	$('#editsalespersons_div').hide();
	$('#edittelecallers_div').hide();
	$('#editsalespersonsdetails_div').hide();
	$("#datewiseReportsResults_div").hide();
	$("#saleslist_div").hide();
	$('#appointments_by_date').hide();
	$('#edittelecallersdetails_div').load("editUser.action?type="+type+"&userId="+userId);
	$('#edittelecallersdetails_div').show();
	$('#appointments_by_name').hide();
	$("#sms_div").hide();
	$('#allocated_div').hide();
	$('#allocated_load_div').hide();
	$('#trackfield_div').hide();
	$('#deleteteamleader_div').hide();
	$("#dailyplan_div").hide();
	 $("#dialycallplan_inner_div").hide();
	$('#editteamleader_div').hide();
	$('#editteamleaderdetails_div').hide();
}


function editSalesPersons(type,userId)
{
	$('#banner_div').hide();
	$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
	$('#salesperson_div').hide();
	$('#telecaller_div').hide();
	$("#calllist_div").hide();
	$('#deletetelecallers_div').hide();
	$('#deletesalespersons_div').hide();
	$('#editsalespersons_div').hide();
	$("#datewisereport_div").hide();
	$('#edittelecallers_div').hide();
	$("#datewiseReportsResults_div").hide();
	$("#saleslist_div").hide();
	$('#edittelecallersdetails_div').hide();
	$('#appointments_by_date').hide();
	$('#editsalespersonsdetails_div').load("editUser.action?type="+type+"&userId="+userId);
	$('#editsalespersonsdetails_div').show();
	$('#appointments_by_name').hide();
	$("#sms_div").hide();
	$('#allocated_div').hide();
	$('#allocated_load_div').hide();
	$('#trackfield_div').hide();
	$('#deleteteamleader_div').hide();
	$("#dailyplan_div").hide();
	 $("#dialycallplan_inner_div").hide();
	$('#editteamleader_div').hide();
	$('#editteamleaderdetails_div').hide();
}
function editTeamLeader(type,userId)
{
	$("#mysms").hide();
    $("#show_sms_div").hide();	
	$('#banner_div').hide();
	$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
	$('#salesperson_div').hide();
	$('#telecaller_div').hide();
	$("#calllist_div").hide();
	$('#deletetelecallers_div').hide();
	$("#datewisereport_div").hide();
	$('#deletesalespersons_div').hide();
	$('#editsalespersons_div').hide();
	$('#edittelecallers_div').hide();
	$('#editsalespersonsdetails_div').hide();
	$("#datewiseReportsResults_div").hide();
	$("#saleslist_div").hide();
	$('#appointments_by_date').hide();
	$('#editteamleader_div').hide();
	$('#edittelecallersdetails_div').hide();
	$('#editteamleaderdetails_div').load("editUser.action?type="+type+"&userId="+userId);
	$('#editteamleaderdetails_div').show();
	$('#appointments_by_name').hide();
	$("#sms_div").hide();
	$('#allocated_div').hide();
	$('#allocated_load_div').hide();
	$('#trackfield_div').hide();
	$('#editteamleader_div').hide();
	
}







</script>

<s:if test="%{#request.operation=='allot'}">
	<script type="text/javascript">
		$(document).ready(function()
		{
			$("#mysms").hide();
            $("#show_sms_div").hide();
			$('#deleteteamleader_div').hide();
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$('#editsalespersonsdetails_div').hide();
			$("#calllist_div").hide();
			$('#edittelecallersdetails_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$("#datewisereport_div").hide();
			$("#datewiseReportsResults_div").hide();
			$("#saleslist_div").hide();
			$('#pending_appointments_div,#rejected_appointments_div,#banner_div,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();  	
			$('#appointments').load("loadAppointments.action?todaydate="+todaydate); 
		  /*   $('#appointments').load("loadAppointments.action?type=all"); */
			$('#appointments').show();
			$('#appointments_by_name').hide();
			$('#sms_div').hide();
			$('#appointments_by_date').hide();
			$('#allocated_div').hide();
			$('#allocated_load_div').hide();
			$('#trackfield_div').hide();
			$("#dailyplan_div").hide();
			 $("#dialycallplan_inner_div").hide();
			$('#editteamleader_div').hide();
			$('#editteamleaderdetails_div').hide();
		});
	</script>
</s:if>
<s:if test="%{#request.update_status!=null}">
	<script type="text/javascript">
		$(document).ready(function()
		{  
			
			$("#mysms").hide();
            $("#show_sms_div").hide();
			$('#deleteteamleader_div').hide();
			$('#banner_div').hide();	
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$("#datewisereport_div").hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$("#calllist_div").hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$("#datewiseReportsResults_div").hide();
			$("#saleslist_div").hide();
			$('#pending_appointments_div,#rejected_appointments_div,#banner_div,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();  
			
			$('#appointments').load("loadAppointments.action?todaydate="+todaydate);
			$('#appointments').show();
			$('#appointments_by_name').hide();
			$('#allocated_div').hide();
			$('#allocated_load_div').hide();
			$("#dailyplan_div").hide();
			 $("#dialycallplan_inner_div").hide();
			$('#editteamleader_div').hide();
			$('#editteamleaderdetails_div').hide();
		});
	</script>
</s:if>
<s:if test="%{#request.operation=='deletesalesperson'}">
	<script type="text/javascript">
		$(document).ready(function()
		{  
			$("#mysms").hide();
            $("#show_sms_div").hide();
			$('#deleteteamleader_div').hide();
			$('#banner_div').hide();
			$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$('#deleteuser_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$("#datewisereport_div").hide();
			$("#calllist_div").hide();
			$('#deletetelecallers_div').hide();
			$("#datewiseReportsResults_div").hide();
			$("#saleslist_div").hide();
			$('#deletesalespersons_div').load("loadUsers.action?type=salesperson");
			$('#deletesalespersons_div').show();
			$('#allocated_div').hide();
			$('#allocated_load_div').hide();
			$('#trackfield_div').hide();
			$("#dailyplan_div").hide();
			 $("#dialycallplan_inner_div").hide();
			$('#editteamleader_div').hide();
			$('#editteamleaderdetails_div').hide();
		});
	</script>
</s:if>
<s:if test="%{#request.operation=='deletetelecaller'}">
	<script type="text/javascript">
		$(document).ready(function()
		{ 
			$("#mysms").hide();
            $("#show_sms_div").hide();
			$('#deleteteamleader_div').hide();
			$('#banner_div').hide();
			$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$('#deleteuser_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$("#calllist_div").hide();
			$("#saleslist_div").hide();
			$('#deletesalespersons_div').hide();
			$("#datewiseReportsResults_div").hide();
			$('#deletetelecallers_div').load("loadUsers.action?type=telecaller");
			$('#deletetelecallers_div').show();
			$('#allocated_div').hide();
			$('#allocated_load_div').hide();
			$('#trackfield_div').hide();
			$("#dailyplan_div").hide();
			 $("#dialycallplan_inner_div").hide();
			$('#editteamleader_div').hide();
			$('#editteamleaderdetails_div').hide();
			$('#editteamleaderdetails_div').hide();
		});
	</script>
</s:if>
<s:if test="%{#request.editoperation=='telecaller'}">
	<script type="text/javascript">
		$(document).ready(function()
		{
			$("#mysms").hide();
            $("#show_sms_div").hide();
			$('#deleteteamleader_div').hide();
			$('#banner_div').hide();
			$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$('#deleteuser_div').hide();
			$('#deletesalespersons_div').hide();
			$('#deletetelecallers_div').hide();
			$('#edittelecallers_div').show();
			$('#editsalespersons_div').hide();
			$("#datewisereport_div").hide();
			$("#datewiseReportsResults_div").hide();
			$("#calllist_div").hide();
			$("#saleslist_div").hide();
			$('#edittelecallers_div').load("loadEditUsers.action?type=telecaller");
			$('#edittelecallers_div').show();
			$('#allocated_div').hide();
			$('#allocated_load_div').hide();
			$('#trackfield_div').hide();
			$("#dailyplan_div").hide();
			 $("#dialycallplan_inner_div").hide();
			$('#editteamleader_div').hide();
			$('#editteamleaderdetails_div').hide();
			
		});
	</script>
</s:if>

<s:if test="%{#request.editoperation=='salesperson'}">
	<script type="text/javascript">
		$(document).ready(function()
		{  
			$("#mysms").hide();
            $("#show_sms_div").hide();
			$('#deleteteamleader_div').hide();
			$('#banner_div').hide();
			$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
			$('#telecaller_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$('#salesperson_div').hide();
			$('#deleteuser_div').hide();
			$('#deletetelecallers_div').hide();
			$('#deletesalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$("#calllist_div").hide();
			$("#datewisereport_div").hide();
			$("#datewiseReportsResults_div").hide();
			$("#saleslist_div").hide();
			$('#editsalespersons_div').load("loadEditUsers.action?type=salesperson");
			$('#editsalespersons_div').show();
			$('#allocated_div').hide();
			$('#allocated_load_div').hide();
			$("#dailyplan_div").hide();
			 $("#dialycallplan_inner_div").hide();
			$('#editteamleader_div').hide();
			$('#editteamleaderdetails_div').hide();
		});
	</script>
</s:if>

<s:if test="%{#request.editoperation=='teamleader'}">
	<script type="text/javascript">
		$(document).ready(function()
		{
			$("#mysms").hide();
            $("#show_sms_div").hide();
			$('#banner_div').hide();
			$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
			$('#telecaller_div').hide();
			$('#salesperson_div').hide();
			$('#editsalespersons_div').hide();
			$('#edittelecallers_div').hide();
			$('#deleteuser_div').hide();
			$('#deletesalespersons_div').hide();
			$('#deletetelecallers_div').hide();
			$('#edittelecallers_div').show();
			$('#editsalespersons_div').hide();
			$("#datewisereport_div").hide();
			$("#datewiseReportsResults_div").hide();
			$("#calllist_div").hide();
			$("#saleslist_div").hide();
			$('#editteamleader_div').load("loadEditUsers.action?type=teamleader");
			$('#edittelecallers_div').hide();
			$('#allocated_div').hide();
			$('#allocated_load_div').hide();
			$('#trackfield_div').hide();
			$('#editteamleader_div').show();
			
		});
	</script>
</s:if>


<script type="text/javascript" src="http://dialusbanners.dialus.com/jquery.autocomplete.js"></script>
<link href="http://dialuscss.dialus.com/dialus.css" rel="stylesheet" type="text/css" media="all"/>
<script>
function changeTextVal(typeValue)
{
	$("#reportUserType").val(typeValue);
}
function changeTextVal1(typeValue)
{
	$("#reportUserType").val(typeValue);
}


$(document).ready(function()
{
	
	
	$("#reportUserName").focus(function()
	{
		var reportUserName=$('#reportUserName').val();
		var reportUserTypeCheck=$('#reportUserType').val();
		$('#reportUserName').autocomplete("getUsers.action?type="+reportUserTypeCheck+"&userName="+reportUserName);
	});
	$("#city_name").autocomplete("DialyCallPlanCityAutoComleteAction.action");
	$("#targetuser").autocomplete("TargetUserAutoCompletAction.action");
	$("#success_message_div").show();
	$("#success_message_div").fadeOut(9000);
	
	$("#getReportBut").click(function()
	{
		var reportUserName=$('#reportUserName').val();
		var reportUserTypeCheck=$('#reportUserType').val();
		var reportTodate=$('#reportTodate').val();
		var reportFromdate=$('#reportFromdate').val();
		$("#datewiseReportsResults_div").load("getUsersDatewiseReports.action?type="+reportUserTypeCheck+"&userName="+reportUserName+"&fromDate="+reportFromdate+"&toDate="+reportTodate);
		$('#banner_div').hide();
		$('#pending_appointments_div,#rejected_appointments_div,#appointments,#appointmentform_div,#edituser_div,#deleteuser_div,#createuser_div,#telecallerreport_div,#calllist_div,#salesreport_div').hide();
		$('#salesperson_div').hide();
		$('#telecaller_div').hide();
		$('#deletetelecallers_div').hide();
		$('#deletesalespersons_div').hide();
		$('#editsalespersons_div').hide();
		$("#calllist_div").hide();
		$("#saleslist_div").hide();
		$("#datewisereport_div").hide();
		$('#edittelecallers_div').hide();
		$('#editsalespersonsdetails_div').hide();
		$('#edittelecallersdetails_div').hide();
		$('#appointments_by_date').hide();
		$("#datewiseReportsResults_div").show();
		$('#allocated_div').hide();
		$('#allocated_load_div').hide();
		$('#trackfield_div').hide();
		$('#teamleader_div').hide();
		$("#dailyplan_div").hide();
		 $("#dialycallplan_inner_div").hide();
		$('#editteamleaderdetails_div').hide();
		$("#mysms").hide();
        $("#show_sms_div").hide();
	});
	

});
</script>
<script>
	
	
	jQuery(function(){
		 
		$('#allocated_name').autocomplete('AllocatedautoCompleteAction.action');
		 $('#salesperson_state').autocomplete('CityautoCompleteAction.action?variable=state');
			$('#salesperson_city').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businessState: function () {
				                return $("#salesperson_state").val();
				            },
				            
				            
				            variable: function () {
				                return "city";
				            }
				
				        }
					}
			
			
			);
			
			/*area starts here  */
			
			
			
			/*Start of area auto complete  */
		$('#salesperson_area').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businesCity: function () {
				                return $("#salesperson_city").val();
				            },
				            
				            
				            variable: function () {
				                return "area";
				            }

				        }
					}
			
			
			);
	});
			</script>
	<script>
	jQuery(function(){
		 
		
	
		 $('#telecaller_state').autocomplete('CityautoCompleteAction.action?variable=state');
			$('#telecaller_city').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businessState: function () {
				                return $("#telecaller_state").val();
				            },
				            
				            
				            variable: function () {
				                return "city";
				            }
				
				        }
					}
			
			
			);
			
			/*area starts here  */
			
			
			
			/*Start of area auto complete  */
		$('#telecaller_area').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businesCity: function () {
				                return $("#telecaller_city").val();
				            },
				            
				            
				            variable: function () {
				                return "area";
				            }

				        }
					}
			
			
			);
	});
</script>
<script>
$(document).ready(function()
{
		
		$(function()
		{
		  $( ".reportTodate" ).datepicker();
		});
		
		$(function()
		{
		  $( ".reportFromdate" ).datepicker();
		});
});
</script>

</head>

<body>
<div id="appointmentform_div"></div>
<div id="salesPersonContactDetails"></div>
<div class="top_bg">
	<div class="topbg_inner">
   	  <div class="topbg_inner_left">
        	<div class="topbg_inner_left_top">
            	SALES TRACKING
            </div>
            <div class="topbg_inner_left_bottom">
             <span id="date_time"></span>
            <script type="text/javascript">window.onload = date_time('date_time');</script>
          </div>
      </div>
      <div class="topbg_inner_right">
        	<div class="topbg_inner_right_left">
       	  <img src="images/dialus_small.png" width="28" height="27" /> </div>
            <div class="topbg_inner_right_right">
            9666 888 888
            </div>
      </div>
    </div>
</div>
<div style="clear:both"></div>
<div class="wrapper">
<div class="logopart">
	<div class="logopart_left">
    <img src="images/logo.jpg" width="225" height="90" /> 
    </div>
   
</div>
<s:if test="%{#request.success_msg!=null && #request.success_msg!=''}">
		<div style="text-align: center;width:980px;" id="success_msg"> <font style="color: red;font-family: georgia;font-weight: bold;"><s:property value="%{#request.success_msg}"/></font></div>
	</s:if>
<div class="contentpart">
	<div class="content_left">
    	
        <div id="login_links">
	        <div class="content_left_headding">
		  <div class="content_left_text">
            Menu
          </div>
      </div>
      
      
      
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="appointments_link" >Fix Appointments</a>
	        	</div>
	       </div>
	       
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="all_appointments_link" >All Appointments</a>
	        	</div>
	       </div>
	       
	     <!--   Allocated Appointments start -->
	       
	         <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="allocated_appointments_link" >Allocated Appointments</a>
	        	</div>
	       </div>
	       
	       
	       
	     
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="createuser_link" >Create Users</a>
	        	</div>
	       </div>
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="deleteuser_link" >Delete Users</a>
	        	</div>
	       </div>
	        <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="edituser_link" >Edit Users</a>
	        	</div>
	       </div>
	         
	         
	       
	       
	        <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="datewisereport_link" >Date Wise Reports</a>
	        	</div>
	       </div>
	       
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="trackfield_link" >Track Field Sales</a>
	        	</div>
	       </div>
	       
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="addtarget_link" >Add Targets</a>
	        	</div>
	       </div>
	     	     
	     
	     <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="dailyplan_link" >Daily Call Plan</a>
	        	</div>
	       </div>
	     
	     
	     
	     
	     
	     
	      
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="callplan_on_leads" >Call Plan On Leads</a>
	        	</div>
	       </div>
	       
	       
	       	   <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="renewaldata_link" >Renewal Data</a>
	        	</div>
	       </div>
	       
	       
	       
	      <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="business_link" >Business Details</a>
	        	</div>
	       </div> 
	     
	   
	     <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="sms_link" >SMS Details</a>
	        	</div>
	     </div>
	       
	    <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="leads_link" >Leads  Sent</a>
	        	</div>
	     </div>
	     
	     
	     <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a style="cursor: pointer;color:#008abb;" id="problems_link" >Complaints</a>
	        	</div>
	     </div>
	     	     
	     	     
	     	     
	     	     
	     	     
	     	     
	       <div class="smallheadding_conleft_inner">
	       		<div class="smallheadding_conleft_inner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_inner_right">
	        		 <a href="LogOut.action"style="cursor: pointer;text-decoration: none;color:#f30909;" id="logout_link">Log Out</a>
	        	</div>
	       </div>
	       
	       
	       
	       
	       
      </div>
      
    </div>
    <div class="content_right">
    		 <!-- <div id="appointmentform_div"></div> -->
    		  <div id="appointments"></div>
    		  
    		  <div id="appointments_by_date">
    		  <p style="color: red;font-size: 18px;text-align: center;">Fix Appointments</p>
    		  <div class="main_div_middle_textfields_main" style="width:600px;">
								 
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:150px; text-align:right; margin-left:30px;">
								       	   		<b style="color:red">&lowast;</b>Select Date :
								            </div>
                                            
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px; width:219px;">
								           		<%--  <s:textfield name="reportTodate" id="reportTodate"  cssClass="reportTodate"></s:textfield> --%>
								           		 <s:textfield name="todaydate" id="today_date"  cssClass="reportTodate123" onfocus="show_date()"></s:textfield>
								            </div>
								            
								            <div class="main_div_middle_textfields_main_right" style="margin-left:5px; width:80px;">
								           		<%--  <s:textfield name="reportTodate" id="reportTodate"  cssClass="reportTodate"></s:textfield> --%>
								           		 <s:submit id="submit_date"  onclick="submit_button_clicked()"></s:submit>
								            </div>
								 
	        </div>
    		  
    		  
    		  
    		  
    		  </div>
    		  <div id="allocated_div" style="width: 595px;">
    		  
    		  <p style="color: red;font-size: 18px;text-align: center;">Allocated Appointments</p>
    		  <div class="main_div_middle_textfields_main" style="width:600px; margin-bottom:5px">
								 
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:150px; text-align:right; margin-left:30px;">
								       	   		<b style="color:red">&lowast;</b>Select Date :
								            </div>
                                            
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px; width:219px;">
								           		<%--  <s:textfield name="reportTodate" id="reportTodate"  cssClass="reportTodate"></s:textfield> --%>
								           		 <s:textfield name="allocated_date" id="allocated_date"  cssClass="reportTodate123" onfocus="show_date()"></s:textfield><br/><br/>
								            </div>
    		  </div>
    		  
    		  
              
              
              <div class="main_div_middle_textfields_main" style="width:600px; margin:5px 05px 0">
                
	             
	            <div class="main_div_middle_form_textfields_main_label" style=" width:150px; text-align:right; margin-left:30px;">
	       	   		<b style="color:#fff">.</b>
								            </div>
                                            
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px; width:219px;">
								           (OR)
								            </div>
              </div>
            
            
            <div class="main_div_middle_textfields_main" style="width:600px; margin-bottom:3px">
								 
								           	
                                            
								            
								           
								            <div class="main_div_middle_form_textfields_main_label" style=" width:150px; text-align:right; margin-left:30px;">
								       	   		<b style="color:red">&lowast;</b>Select Name :
								            </div>
                                            
              <div class="main_div_middle_textfields_main_right" style="margin-left:10px; width:219px;">
								           		<%--  <s:textfield name="reportTodate" id="reportTodate"  cssClass="reportTodate"></s:textfield> --%>
								           		 <s:textfield name="allocated_name" id="allocated_name" ></s:textfield><br/><br/>
								            </div>
            </div>
            
            <div class="main_div_middle_textfields_main" style="width:600px; margin:0">
								 
								           	
                                            
								            
								           
              <div class="main_div_middle_form_textfields_main_label" style=" width:150px; text-align:right; margin-left:30px;">
								       	   		<b style="color:#fff">.</b>
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:0px; width:80px;">
								           		<%--  <s:textfield name="reportTodate" id="reportTodate"  cssClass="reportTodate"></s:textfield> --%>
							           		  <s:submit id="submit_allocated"  onclick="submit_allocated()"></s:submit>
              </div>
								 
	        </div>
    		  
    		  
	  </div>
    		  
    		  
    		  
    		    <div id="trackfield_div">
    		    
    		    
    		   <img src="http://dialusimages.dialus.com/images/india/images/loader32.gif" id="loading_img6"  style="display:none; margin:30px 0 0 175px; "/> 
    		    
    		    </div>
    		  
    		   <div id="appointments_by_name"></div>
    		
    		 
  
    		  
    		
    		   <div id="allocated_load_div"></div>
    		  
    		  <div id="renewaldata">
    		   <img src="http://dialusimages.dialus.com/images/india/images/loader32.gif" id="loading_img111"  style="display:none; margin:30px 0 0 75px; "/>
    		  </div>
    		  
    		  <div id="callplan_div">
       <img src="http://dialusimages.dialus.com/images/india/images/loader32.gif" id="loading_img11"  style="display:none; margin:30px 0 0 75px; "/>
    		  </div>
    		  
    		
    		  
    		  
    		  <div id="targets_div">
    		  <p style="color: red;font-size: 18px;text-align: center;">Add Targets</p>
            <s:form action="AddTargetAction" method="post" id="loginForm1" theme="simple">
          
          <input type="hidden" name="tlname"  id="sessionuser"  value="<s:property value="#session.username"/>"/>
          
            	<div class="new_changepassword_main_middle_textfields">
                	<div class="new_changepassword_main_middle_textfields_left">
                    <b style="color:red">&lowast;</b>TelecallerName:
                    </div>
                    
                    <div class="new_changepassword_main_middle_textfields_right">
                    
                    <s:textfield name="targetuser"   maxlength="30"  id="targetuser" theme="simple" cssStyle=" width:200px ; height:27px"></s:textfield>
                     
                    </div>
                
                </div>
                
                <div class="new_changepassword_main_middle_textfields">
                	<div class="new_changepassword_main_middle_textfields_left">
                     <b style="color:red">&lowast;</b>No Of Targets:
                    </div>
                    
                    <div class="new_changepassword_main_middle_textfields_right">
                    
                     <s:textfield name="targets" maxlength="30" id="targets" theme="simple" cssStyle=" width:200px; height:27px"/>
                    </div>
                
                </div>
              
                <div class="new_changepassword_main_middle_textfields">
               	  <div class="new_changepassword_main_middle_textfields_left" style="color:#fff">
                    :
                  </div>
                    
                    <div class="new_changepassword_main_middle_textfields_right">
                    
                   <s:submit id="target_submit" cssStyle="background-color: #008abb; border: none; color: #fff; height: 26px; float: left; cursor: pointer;" />
                    </div>
                
                </div>
            </s:form>
    
    <div id="targets_div_inner"></div>
    
    
    </div> 
    
    		  
	     <!--  <div id="appointmentform_div"></div> -->
	       <div id="rejected_appointments_div"></div>
	        <div id="pending_appointments_div"></div>
	        
	   	  <div class="banner_part" id="banner_div">
	   	  WelCome To Sales Tracking Admin Panel
	        	
	            
	            <div class="banner_part">
	             </div>
	            
	      </div>
	     
	        
	      <div id="createuser_div">
	      
		       <div class="main_div_middle">
			         
		                       	<div class="appointmentform_left_left">
		                         		  Select User Type:
		                        </div>
		                            
		                   		<div class="appointmentform_left_right">
		                     			<select id="persontype" onchange="selectUser()">
					      					<option value="123">-----Select-------</option>
						      				<option value="salesperson">Business Dev Officer</option>
						      				<option value="telecaller">Tele Sales</option>
	      								    <option value="teamleader">Team Leader</option>
	      								
	      								</select>
		                   		</div>
			         
		           </div>
	      
	      				
	        </div>
	         <div id="teamleader_div" >
						      	<s:form action="createUsersAction" theme="simple">
								    	<s:hidden name="objUsersOperationsDTO.usertype" value="teamleader"></s:hidden>
								        <div class="main_div_middle">
								         <div class="main_div_middle_textfields_main" style="text-align:center;background: #008ABB;width: 100%;color:#fff;padding: 8px 0px;margin-top:-10px;border-top-left-radius:10px;border-top-right-radius:10px;font-family: georgia;">
											           Please Enter Team lead   Details
											        </div>
									        <div class="appointmentform_left1" style="margin-left:200px;">
                                	<div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Name :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userName" maxlength="90" id="teamlead_name"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                   
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b> Mobile :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userMobile" maxlength="10"    id="teamlead_mobile"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                       <div id="telecallermbnoExist"></div>
                                        </div>
                                    </div>
                                  
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                      Mail Id :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userMailId" maxlength="40"    id="teamlead_mailid"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Password :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:password  name="objUsersOperationsDTO.userPassword"  maxlength="8"    id="teamlead_password"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:password>
                                        </div>
                                    </div>
                                      <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Confirm Password
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:password   maxlength="8"    id="teamlead_confirmpassword"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:password>
                                        </div>
                                    </div>
                                      <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                    
                                        </div>
                                       
                                        <div class="appointmentform_left_right" style="float: right;">
                                      <s:submit value="Create User" id="createTeamlead_but"></s:submit>
                                        </div>
                                    </div>
                                    </div>
									            
									            
									          
									        </div>
								     
								     </s:form>
							
				      </div>
	        
	        
	        
				       <div id="salesperson_div">
						      	<s:form action="createUsersAction" theme="simple">
								    	<s:hidden name="objUsersOperationsDTO.usertype" value="salesperson"></s:hidden>
								        <div class="main_div_middle">
								         <div class="main_div_middle_textfields_main" style="text-align:center;background: #008ABB;width: 100%;color:#fff;padding: 8px 0px;margin-top:-10px;border-top-left-radius:10px;border-top-right-radius:10px;font-family: georgia;">
											           Please Enter Business Dev Officer  Details
											        </div>
									        <div class="appointmentform_left1" style="margin-left:200px;">
                                	<div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                         <b style="color:red">&lowast;</b>Name :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userName" maxlength="45" id="salesperson_name"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                         <b style="color:red">&lowast;</b>State :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userState" maxlength="40"    id="salesperson_state"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b> City :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userCity" maxlength="40"   id="salesperson_city"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Area :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userArea" maxlength="40"    id="salesperson_area"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Mobile :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userMobile" maxlength="10"    id="salesperson_mobile"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                       <div id="mbnoExist"></div>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        Mobile2 :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userMobile1" maxlength="10"     id="salesperson_mobile1"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                     Mail Id :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userMailId" maxlength="40"    id="salesperson_mailid"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Password :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:password  name="objUsersOperationsDTO.userPassword" maxlength="8"    id="salesperson_password"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:password>
                                        </div>
                                    </div>
                                      <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Confirm Password
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:password    maxlength="8"    id="salesperson_confirmpassword"   cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:password>
                                        </div>
                                    </div>
                                      <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                    
                                        </div>
                                        
                                        <div class="appointmentform_left_right" style="float: right;">
                                      <s:submit value="Create User" id="createSalesperson_but"></s:submit>
                                        </div>
                                    </div>
                                    </div>
									            
									            
									          
									        </div>
								     
								     </s:form>
							
				      </div>
				  
				    <div id="deleteteamleader_div"></div>
				      
				      <div id="telecaller_div">
						      	<s:form action="createUsersAction" theme="simple">
								    	<s:hidden name="objUsersOperationsDTO.usertype" value="telecaller"></s:hidden>
								        <div class="main_div_middle">
								         <div class="main_div_middle_textfields_main" style="text-align:center;background: #008ABB;width: 100%;color:#fff;padding: 8px 0px;margin-top:-10px;border-top-left-radius:10px;border-top-right-radius:10px;font-family: georgia;">
											           Please Enter Tele Sales Person  Details
											        </div>
									        <div class="appointmentform_left1" style="margin-left:200px;">
                                	<div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Name :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userName" maxlength="90" id="telecaller_name"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                  
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b> Mobile :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userMobile" maxlength="10"    id="telecaller_mobile"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                       <div id="telecallermbnoExist"></div>
                                        </div>
                                    </div>
                                   
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                      Mail Id :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userMailId" maxlength="40"    id="telecaller_mailid"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>Password :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:password  name="objUsersOperationsDTO.userPassword"  maxlength="8"    id="telecaller_password"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:password>
                                        </div>
                                    </div>
                                      <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>Confirm Password
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:password   maxlength="8"    id="telecaller_confirmpassword"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:password>
                                        </div>
                                    </div>
                                      <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                    
                                        </div>
                                        
                                        <div class="appointmentform_left_right" style="float: right;">
                                      <s:submit value="Create User" id="createTelecaller_but"></s:submit>
                                        </div>
                                    </div>
                                    </div>
									            
									            
									          
									        </div>
								     
								     </s:form>
							
				      </div>
				      
	      
	    
	      <div id="deleteuser_div">
	           <div class="main_div_middle">
		         
	                       	<div class="appointmentform_left_left">
	                         		 Select User Type:
	                        </div>
	                            
	                   		<div class="appointmentform_left_right">
	                     		<select id="deletepersontype" onchange="selectDeleteUser()">
									<option value="123">-----Select-------</option>
									<option value="salesperson">Business Dev Officer</option>
									<option value="telecaller">Tele Sales</option>
							    	<option value="teamleader">Team Leader</option>
								
								</select>
	                   		</div>
		         
	           </div>
          
	      </div>
	      <div id="deletesalespersons_div"></div>
	      <div id="deletetelecallers_div"></div>
	      <div id="business_div">
	      <img src="http://dialusimages.dialus.com/images/india/images/loader32.gif" id="loading_img2"  style="display:none; margin:30px 0 0 175px; "/>
	      </div>
	      <div id="sms_div">
	      	  <img src="http://dialusimages.dialus.com/images/india/images/loader32.gif" id="loading_img3"  style="display:none; margin:30px 0 0 175px; "/>      
	      </div>
	       <div id="leads_div">
	      <img src="http://dialusimages.dialus.com/images/india/images/loader32.gif" id="loading_img4"  style="display:none; margin:30px 0 0 175px; "/>      
	      </div>
	      <div id="edituser_div">
	      
	      <div class="main_div_middle">
		         
	                       	<div class="appointmentform_left_left">
	                         		   Select User Type:
	                        </div>
	                            
	                   		<div class="appointmentform_left_right">
	                     		<select id="editpersontype" onchange="selectEditUser()">
				      					<option value="123">-----Select-------</option>
					      				<option value="salesperson">Business Dev Officer</option>
					      				<option value="telecaller">Tele Sales</option>
					      			    <option value="teamleader">Team Leader</option>
					      				
	      						</select>
	                   		</div>
		         
	           </div>
	    </div>
	      <div id="editsalespersons_div"></div>
	      <div id="edittelecallers_div"></div>
	       <div id="editteamleader_div"></div>
         <div id="editteamleaderdetails_div"></div>      
	       <div id="editsalespersonsdetails_div"></div>
	      <div id="edittelecallersdetails_div"></div>
	       <div id="telecallerreport_div">

			       <div class="main_div_middle" style="width:610px;">
				         
					                       	
					                            
					                   		<div class="appointmentform_left_right" style="width:40%;">
					                   			<div class="appointmentform_left_left" style="margin-bottom: 10px;">
					                         		   Select Report Type:
					                        	</div>
					                        	
					                     		<select id="reporttype">
								      					<option value="123">-----Select-------</option>
									      				<option value="daily">Daily Report</option>
									      				<option value="weekly">Weekly Report</option>
									      				<option value="monthly">Monthly Report</option>
					      						</select>
					      						
					      						
					                   		</div>
					                   		
					                            
					                   		<div class="appointmentform_left_right" style="width:50%;">
						                   		<div class="appointmentform_left_left" style="width:180px;margin-bottom: 10px;">
						                         		   Select TeleSales Person Name:
						                        </div>
					                     		<select id="telecallername">
								      					<option value="123">-----Select-------</option>
								      					
								      					<s:if test="%{telecallerNames.size!=0}">
										      				<s:iterator value="telecallerNames" id="a">
										      				<option value="<s:property/>"><s:property/></option>
										      				</s:iterator>
									      				</s:if>
									      				<s:else><font color="red" face="georgia"><b>Please Create TeleSales Persons</b></font></s:else>
									      				
					      						</select>
					                   		</div>
					                   		
					                   		
					                   		<div class="appointmentform_left_left" style="margin-left: 130px;margin-top: 20px;">
						                         		   <input type="button" id="getTelecallerReport"  value="Get Report"/>
						                        </div>
				         
			          		 </div>
	       
	       
	       
	       
	       
	       </div>
	        <div id="calllist_div"></div>
	        <div id="saleslist_div"></div>
	        
	         <div id="salesreport_div">
	         
	             <div class="main_div_middle" style="width:610px;">
		         
			                       	
			                            
			                   		<div class="appointmentform_left_right" style="width:40%;">
			                   			<div class="appointmentform_left_left" style="margin-bottom: 10px;">
			                         		   Select Report Type:
			                        	</div>
			                     		<select id="salesreporttype">
						      					<option value="123">-----Select-------</option>
							      				<option value="daily">Daily Report</option>
							      				<option value="weekly">Weekly Report</option>
							      				<option value="monthly">Monthly Report</option>
			      						</select>
			                   		</div>
			                   		
			                            
			                   		<div class="appointmentform_left_right" style="width:50%;">
				                   		<div class="appointmentform_left_left" style="width:180px;margin-bottom: 10px;white-space: nowrap;">
				                         		   Select Business Dev Officer Name:
				                        </div>
			                     		<select id="salespersonnamereport">
						      					<option value="123">-----Select-------</option>
						      					<s:if test="%{salesPersons.size!=0}">
								      				<s:iterator value="salesPersons" id="a">
								      				<option value="<s:property value="#a.salesPersonContactNo"/>"><s:property value="#a.salesPersonName"/></option>
								      				</s:iterator>
							      				</s:if>
							      				<s:else><font color="red" face="georgia"><b>Please Create Business Dev Officers</b></font></s:else>
			      						</select>
			                   		</div>
			                   		
			                   		
			                   		<div class="appointmentform_left_left" style="margin-left: 130px;margin-top: 20px;">
				                         		   <input type="button" id="getSalesPersonReport_but"  value="Get Report"/>
				                        </div>
		         
	          		 </div>
	         
	         
	         
	         
	         </div>
	       
	       
	       <div id="datewisereport_div">
	       			<div class="main_div_middle">
	       				<div class="main_div_middle_textfields_main" style="text-align:center;background: #008ABB;width: 100%;color:#fff;padding: 8px 0px;margin-top:-10px;border-top-left-radius:10px;border-top-right-radius:10px;">
								        Date Wise Reports
						</div>
  						
  							<s:hidden name="reportUserType"  id="reportUserType"></s:hidden>
  							<div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:180px; text-align:right; margin-left:60px;">
								       	   		<b style="color:red">&lowast;</b>From Date :
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								           		 <s:textfield name="reportFromdate" id="reportFromdate"   cssClass="reportFromdate"></s:textfield>
								            </div>
							</div>
							<div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:180px; text-align:right; margin-left:60px;">
								       	   		<b style="color:red">&lowast;</b>To Date :
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								           		 <s:textfield name="reportTodate" id="reportTodate"  cssClass="reportTodate"></s:textfield>
								            </div>
							</div>
							<div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:465px; text-align:right;float: left;">
								       	   		Business Dev Officer: <input type="radio" onclick="changeTextVal('sales')" name="reportUserTypeCheck"  id="reportUserType1" value="sales" />
								       	   		Tele Sales Person:	<input type="radio"  onclick="changeTextVal1('telecaller')"  name="reportUserTypeCheck"   id="reportUserType2" value="telecaller" />
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								           		
								            </div>
							</div>
							<div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:180px; text-align:right; margin-left:60px;">
								       	   		<b style="color:red">&lowast;</b>Enter User Name :
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								           		<s:textfield name="reportUserName"   id="reportUserName"> </s:textfield>
								            </div>
							</div>
	  						<div class="main_div_middle_textfields_main" style="width:600px;">
								           	<div class="main_div_middle_form_textfields_main_label" style=" width:180px; text-align:right; margin-left:60px;color: white;">
								            .
								            </div>
								            <div class="main_div_middle_textfields_main_right" style="margin-left:10px;">
								           		<input type="button" value="get Report" id="getReportBut"/>
								            </div>
							</div>
	  						
	  					
	  						
	  						
  						
	       				
	       </div>
	   </div>
	     <div id="datewiseReportsResults_div"></div>
	     <div id="dailyplan_div" style="width: 660px;">
	     
	     <p style="color: red;font-size: 18px;text-align: center;">Dialy Call Plan</p>
     	          <div class="new_changepassword_main_middle_textfields">
                	<div class="new_changepassword_main_middle_textfields_left">
                    <b style="color:red">&lowast;</b>Select City: 
                    </div>
                    
                    <div class="new_changepassword_main_middle_textfields_right">
                    
                    <s:textfield name="city_name"  id="city_name" theme="simple" cssStyle=" width:200px ; height:27px"></s:textfield>
                     
                    </div>
                    
                    
                    <div class="new_changepassword_main_middle_textfields">
               	  <div class="new_changepassword_main_middle_textfields_left" style="color:#fff">
                    :
                  </div>
                    
                    <div class="new_changepassword_main_middle_textfields_right">
                    
                   <input    type="button"  value="Submit" id="dialycallplan_submit" style="background-color: #008abb; border: none; color: #fff; height: 26px; float: left; cursor: pointer;" />
                    </div>
                
                </div>          
                </div>
	     
	     <!-- <img src="http://images.dialus.com/english/images/india/images/loader32.gif" id="loading_img"  style="display:none; margin:30px 0 0 175px; "/> -->
	  	     </div>
	  	     
	  	       <div id="dialycallplan_inner_div"></div>      
	     
	  <div id="problem_div_show" style="display: none">
	  

                    <table>
                    <tr>
                 		 <td><s:textfield   cssClass="datepicker"  name="fdate" id="datepicker11"  label="From Date"/></td>
                 		 <td> <s:textfield  name="tdate" id="datepicker12"   cssClass="datepicker" label="To Date"/></td>
                 	</tr>
                 	<tr>
                 	<td></td>
                 	<td>
                 	<input type="button" value="Submit" id="problem_submit" onclick="problemSubmit()"  />
                 	</td>
                 	</tr>
                 	</table>

	  
	  
	  
                   
            
  <div id="problem_div">
  <img src="http://dialusimages.dialus.com/images/loader32.gif" id="loading_img144"  style="display:none; margin:30px 0 0 75px; "/>
  </div>
                     
       
  
  </div>  
   <!-- <div id="problem_div">
  </div>  -->
	     
	     
	     
	     
	  <div id="update_targets" class="white_content" style="display: none;">
           <a href = "#" class = "closeRatingBox" style="float:right;">
	<img src="http://dialusimages.dialus.com/images/close_4.png" alt="" style="margin-right:-15px; margin-top:-15px;" />\
	       </a> 
           
		<s:form action="UpdateTargetAction" method="post" theme="simple" >
		 
            	<div class="new_changepassword_main_middle_textfields">
                	<div class="new_changepassword_main_middle_textfields_left">
                    TeleCaller Name<b style="color:red">&lowast;</b> :
                    </div>
                    
                    <div class="new_changepassword_main_middle_textfields_right">
                    
                    <s:textfield name="changetelename"   maxlength="30"  id="changetelename" theme="simple" cssStyle=" width:200px ; height:27px" readonly="true"></s:textfield>
                      <span id="disstatus"></span>
                    </div>
                
                </div>
                
                <div class="new_changepassword_main_middle_textfields">
                	<div class="new_changepassword_main_middle_textfields_left">
                    Targets<b style="color:red">&lowast;</b>  :
                    </div>
                    
                    <div class="new_changepassword_main_middle_textfields_right">
                    
                     <s:textfield name="changetargets" maxlength="30" id="changetargets" theme="simple" cssStyle=" width:200px; height:27px"/>
                    </div>
                
                </div>
                
                
                <div class="new_changepassword_main_middle_textfields">
               	  <div class="new_changepassword_main_middle_textfields_left" style="color:#fff">
                    :
                  </div>
                    
                    <div class="new_changepassword_main_middle_textfields_right">
                    
                   <s:submit id="changeuser_submit" cssStyle="background-color: #008abb; border: none; color: #fff; height: 26px; float: left; cursor: pointer;" />
                    </div>
                
                </div>
            
        </s:form>
            
            </div> 
  </div>
</div>
</div>
<div class="fotter">
	<div class="fotter_inner">
     All Rights Reserved - Copyright ｩ Dialus.com 2010-2013
    </div>
</div>
</body>
</html>
