package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.BusinessDetailDao;
import com.opensymphony.xwork2.ActionSupport;

public class BusinessDetailAction extends ActionSupport {
	
	ArrayList obArrayList;
	
	public ArrayList getObArrayList() {
		return obArrayList;
	}

	public void setObArrayList(ArrayList obArrayList) {
		this.obArrayList = obArrayList;
	}

	public String execute(){
		
		
		BusinessDetailDao objBusinessDetailDao=new BusinessDetailDao();
		
		obArrayList=objBusinessDetailDao.getBusinessDetail();
		
		return "success";
	}

}
