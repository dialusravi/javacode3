package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.dialuz.salesTracking.DAO.LoginDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.dialuz.salesTracking.DTO.LoginDTO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7895900140916347901L;
	private LoginDTO objLoginDTO=new LoginDTO();
	private String success_msg;
	private boolean login_status;
	private String resultPage;
	private String usertype;
	private String user;
	private ArrayList<String> telecallerNames;
	private ArrayList<AppointmentDTO> salesPersons;
	public String getSuccess_msg() {
		return success_msg;
	}
	public void setSuccess_msg(String success_msg) {
		this.success_msg = success_msg;
	}
	
	public LoginDTO getObjLoginDTO() {
		return objLoginDTO;
	}
	public void setObjLoginDTO(LoginDTO objLoginDTO) {
		this.objLoginDTO = objLoginDTO;
	}
	public boolean getLogin_status() {
		return login_status;
	}
	public void setLogin_status(boolean login_status) {
		this.login_status = login_status;
	}
	public String getResultPage() {
		return resultPage;
	}
	public void setResultPage(String resultPage) {
		this.resultPage = resultPage;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public ArrayList<String> getTelecallerNames() {
		return telecallerNames;
	}
	public void setTelecallerNames(ArrayList<String> telecallerNames) {
		this.telecallerNames = telecallerNames;
	}
	
	public ArrayList<AppointmentDTO> getSalesPersons() {
		return salesPersons;
	}
	public void setSalesPersons(ArrayList<AppointmentDTO> salesPersons) {
		this.salesPersons = salesPersons;
	}
	
	
	public String execute()
	{
		HttpServletRequest request  = (HttpServletRequest)ActionContext.getContext().get(org.apache.struts2.StrutsStatics.HTTP_REQUEST);
		HttpSession session=request.getSession(true);
		LoginDAO objLoginDAO=new LoginDAO();
		objLoginDTO.setUsertype(usertype);
		objLoginDTO=objLoginDAO.checkLoginDetails(objLoginDTO);
		
		
		
		telecallerNames=objLoginDAO.getTelecallerNames();
		salesPersons=objLoginDAO.getSalesPersonNames();
		
		
		if(usertype!=null){
		if(usertype.equals("admin"))
		{
			user="admin";
		}else if(usertype.equals("sales"))
		{
			user="salesperson";
		}else{
			
			user="telecaller";}
		
		}
		
		if(objLoginDTO.isLoginStatus())
		{
			
			try
			{
				if(usertype.equals(objLoginDTO.getDbUserType()))
				{
					if(objLoginDTO.getDbUserType().equals("admin"))
					{
						
						success_msg="Hai Dear You Are  Succesfully Logged in Sales Tracking";
						session.setAttribute("username", objLoginDTO.getUsername());
						session.setAttribute("usertype", objLoginDTO.getDbUserType());
						resultPage= "adminpage";
						
					}else if(objLoginDTO.getDbUserType().equals("sales"))
					{
						success_msg="Hai Dear You Are  Succesfully Logged in Sales Tracking";
						session.setAttribute("username", objLoginDTO.getUsername());
						session.setAttribute("usertype", objLoginDTO.getDbUserType());
						resultPage= "userpage";
						
					}
					else if(objLoginDTO.getDbUserType().equals("telecaller"))
					{
						success_msg="Hai Dear You Are  Succesfully Logged in Sales Tracking";
						session.setAttribute("username", objLoginDTO.getUsername());
						session.setAttribute("usertype", objLoginDTO.getDbUserType());
						session.setAttribute("usermobile", objLoginDTO.getUserMobile());
						resultPage= "telecallerpage";
						
					}
					
						
				}
				else
				{
					success_msg="Please Enter Correct Login Details";
					return "indexpage";
				}
			}catch(Exception e)
			{e.printStackTrace();
			return "indexpage";
			}
			return resultPage;
			
		}
		else
		{
			success_msg="Please Enter Correct Login Details";
			return "indexpage";
		}
		
	}
	
	

}
