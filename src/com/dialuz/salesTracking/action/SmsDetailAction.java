package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.BusinessDetailDao;
import com.opensymphony.xwork2.ActionSupport;

public class SmsDetailAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3491380003377567150L;
	
	private ArrayList objArrayList=new ArrayList();
	private int today_smscount; 

	public ArrayList getObjArrayList() {
		return objArrayList;
	}

	public void setObjArrayList(ArrayList objArrayList) {
		this.objArrayList = objArrayList;
	}
	public String execute(){
	BusinessDetailDao objBusinessDetailDao=new BusinessDetailDao();
	objArrayList=objBusinessDetailDao.getAllSms();
	today_smscount=objBusinessDetailDao.getTodaySmsCount();
	return "success";
}

	public int getToday_smscount() {
		return today_smscount;
	}

	public void setToday_smscount(int today_smscount) {
		this.today_smscount = today_smscount;
	}
}