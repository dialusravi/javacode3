package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionSupport;

public class LoadTodayAppointmentsAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<AppointmentDTO> objTodayAppointmentlist=new ArrayList<AppointmentDTO>();
	public String execute()
	{
		AppointmentDAO objTodayAppointmentDAO=new AppointmentDAO();
	
		objTodayAppointmentlist=objTodayAppointmentDAO.getTodayAppointmentDetails();
		return SUCCESS;
	}
	public ArrayList<AppointmentDTO> getObjTodayAppointmentlist() {
		return objTodayAppointmentlist;
	}
	public void setObjTodayAppointmentlist(
			ArrayList<AppointmentDTO> objTodayAppointmentlist) {
		this.objTodayAppointmentlist = objTodayAppointmentlist;
	}
	
	
	
}
