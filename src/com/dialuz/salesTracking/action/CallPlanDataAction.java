package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.BusinessDetailDao;
import com.dialuz.salesTracking.DTO.BusinessDetailDTO;
import com.opensymphony.xwork2.ActionSupport;

public class CallPlanDataAction extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5530590262334030488L;
	ArrayList<BusinessDetailDTO> list=new ArrayList<BusinessDetailDTO>();
	public ArrayList<BusinessDetailDTO> getList()
	{
		return list;
	}
	public void setList(ArrayList<BusinessDetailDTO> list)
	{
		this.list = list;
	}
	
	public String execute()
	{
		BusinessDetailDao objBusinessDetailDao=new BusinessDetailDao();
		list=objBusinessDetailDao.getCallPlanLeads();
		return "success";
	}

}
