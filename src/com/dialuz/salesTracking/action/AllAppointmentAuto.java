package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;


import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class AllAppointmentAuto extends ActionSupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9016997175794148083L;
	private String name;
	ArrayList<String> list=new ArrayList<String>();

	public ArrayList<String> getList() {
		return list;
	}

	public void setList(ArrayList<String> list) {
		this.list = list;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String execute()
	{
		HttpServletRequest request  = (HttpServletRequest)ActionContext.getContext().get(org.apache.struts2.StrutsStatics.HTTP_REQUEST);
		
	   name=request.getParameter("q");
	 
	AppointmentDAO objAppointmentDAO=new AppointmentDAO();
	list=objAppointmentDAO.getTelecallerName(name);
	
	return "success";
	
	}

}
