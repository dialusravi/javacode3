package com.dialuz.salesTracking.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import javax.servlet.ServletContext;

import org.apache.struts2.ServletActionContext;

import com.dialuz.salesTracking.DAO.SendEodDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.dialuz.salesTracking.commonUtilities.SendMail;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class SendEodAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4067241668454153296L;
	private String type;
	private String success_msg;
	private String personType; 
	private AppointmentDTO objCheckAppointmentDTO=new AppointmentDTO();
	public String getSuccess_msg() {
		return success_msg;
	}
	public void setSuccess_msg(String success_msg) {
		this.success_msg = success_msg;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPersonType() {
		return personType;
	}
	public void setPersonType(String personType) {
		this.personType = personType;
	}
	
	public String execute()
	{
		String resultpage="";
		SendEodDAO objSendEodDAO=new SendEodDAO();
		HttpServletRequest request  = (HttpServletRequest)ActionContext.getContext().get(org.apache.struts2.StrutsStatics.HTTP_REQUEST);
		HttpSession session=request.getSession(true);
		objCheckAppointmentDTO.setTelecallerName((String)session.getAttribute("username"));
		
		objCheckAppointmentDTO.setAppointmentType(type);
		 ServletContext context = ServletActionContext.getServletContext();
		String filepath=context.getInitParameter("eodpath");
		objCheckAppointmentDTO.setFilepath(filepath);
		
		String filename="";
		if(personType.equals("sales"))
		{
			filename=objSendEodDAO.sendDailyEodDetails(objCheckAppointmentDTO);
			resultpage="sales";
		}else
		{
			filename=objSendEodDAO.sendDailyTelecallerEodDetails(objCheckAppointmentDTO);
			resultpage="caller";
		}
		String content="End Of The Day Report,Dear Sir/Madam Please Find The Attachment,";
		if(type.equals("daily"))
		{
			try{
			SendMail.sendMail(filename, "Greetings Of The Day", content);
			}catch (Exception e) {
			}
		}else if(type.equals("weekly"))
		{
			try{
				SendMail.sendMail(filename, "End Of The Week Report", content);
				}catch (Exception e) {
				}
		}else
		{
			try{
				SendMail.sendMail(filename, "End Of The Monthly Report",content);
				}catch (Exception e) {
				}
		}
		
		
		return resultpage;
	}
	
	

}
