package com.dialuz.salesTracking.action;





import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.BusinessDetailDao;


public class DeleteBusinessDetailAction {/**
	 * 
	 */
	  private String  mobile_no;
	  private int  business_id;
	  private int id;
	  public int getId()
	  {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	ArrayList obArrayList;
		
		public ArrayList getObArrayList() {
			return obArrayList;
		}

		public void setObArrayList(ArrayList obArrayList) {
			this.obArrayList = obArrayList;
		}
	

	public String getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}

	

	public String execute(){
		
						
		BusinessDetailDao objBusinessDetailDao=new BusinessDetailDao();
				
		int status=objBusinessDetailDao.deleteBusinessDetail(business_id,id);
		
				
		if(status>0)
		{			
			
			obArrayList=objBusinessDetailDao.getBusinessDetail();
			
			
		     return "success";
		}
		else{
			return "error";	
		}
		
	}

	public int getBusiness_id() {
		return business_id;
	}

	public void setBusiness_id(int business_id) {
		this.business_id = business_id;
	}

	
	

}
