package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class LoadUserAppointmentsAction extends ActionSupport
{

	/**
	 * 
	 */
	/**
	 * 
	 */
	private static final long serialVersionUID = -7029884813089261923L;
	private AppointmentDTO objCheckAppointmentDTO=new AppointmentDTO();
	private ArrayList<String> objSalesPersonsList=new ArrayList<String>();
	private String type;
	private String usertype;
	private String appointmentId;
	private String appointmentPlace;
	private String appointmentCity;
	String resultpage;
	public String getAppointmentId() {
		return appointmentId;
	}
	public String getAppointmentPlace() {
		return appointmentPlace;
	}
	public void setAppointmentPlace(String appointmentPlace) {
		this.appointmentPlace = appointmentPlace;
	}
	public void setAppointmentId(String appointmentId) {
		this.appointmentId = appointmentId;
	}
	private ArrayList<AppointmentDTO> objAppointmentDTOList=new ArrayList<AppointmentDTO>();
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public AppointmentDTO getObjCheckAppointmentDTO() {
		return objCheckAppointmentDTO;
	}
	public void setObjCheckAppointmentDTO(AppointmentDTO objCheckAppointmentDTO) {
		this.objCheckAppointmentDTO = objCheckAppointmentDTO;
	}
	public ArrayList<AppointmentDTO> getObjAppointmentDTOList()
	{
		return objAppointmentDTOList;
	}
	public void setObjAppointmentDTOList(ArrayList<AppointmentDTO> objAppointmentDTOList) {
		this.objAppointmentDTOList = objAppointmentDTOList;
	}
	public String getAppointmentCity() {
		return appointmentCity;
	}
	public void setAppointmentCity(String appointmentCity) {
		this.appointmentCity = appointmentCity;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public ArrayList<String> getObjSalesPersonsList() {
		return objSalesPersonsList;
	}
	public void setObjSalesPersonsList(ArrayList<String> objSalesPersonsList) {
		this.objSalesPersonsList = objSalesPersonsList;
	}
	
	public String execute()
	{
		HttpServletRequest request  = (HttpServletRequest)ActionContext.getContext().get(org.apache.struts2.StrutsStatics.HTTP_REQUEST);
		HttpSession session=request.getSession(true);
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		objCheckAppointmentDTO.setUsername((String)session.getAttribute("username"));
		objCheckAppointmentDTO.setAppointmentId(appointmentId);
		objCheckAppointmentDTO.setUsertype(usertype);
		usertype=(String)session.getAttribute("usertype");
		objCheckAppointmentDTO.setUsertype(usertype);
		objCheckAppointmentDTO.setAppointmentType(type);
		
			
		if(type.equals("one"))
		{
			objCheckAppointmentDTO=objAppointmentDAO.getAppointmentDetails(objCheckAppointmentDTO);
			resultpage="userappointmentDetailsPage";
		}else if(type.equals("fixed"))
		{
			objCheckAppointmentDTO.setAppointmentType(type);
			objAppointmentDTOList=objAppointmentDAO.loadAppointments(objCheckAppointmentDTO);

			resultpage="userappointmentsPage";
		}
		else if(type.equals("pending"))
		{
			objCheckAppointmentDTO.setAppointmentType(type);
			objAppointmentDTOList=objAppointmentDAO.loadAppointments(objCheckAppointmentDTO);

			resultpage="userpendingappointmentsPage";
		}
		
		
		
		return resultpage;
		
	}
}