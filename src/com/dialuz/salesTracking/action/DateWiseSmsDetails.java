package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.BusinessDetailDao;
import com.dialuz.salesTracking.DTO.BusinessDetailDTO;
import com.opensymphony.xwork2.ActionSupport;

public class DateWiseSmsDetails extends ActionSupport
{
private String fdate;
private String tdate;
private int today_smscount; 

private ArrayList<BusinessDetailDTO> objArrayList=new ArrayList<BusinessDetailDTO>();
public String getFdate() {
	return fdate;
}
public void setFdate(String fdate) {
	this.fdate = fdate;
}
public String getTdate() {
	return tdate;
}
public void setTdate(String tdate) {
	this.tdate = tdate;
}
public ArrayList<BusinessDetailDTO> getObjArrayList() {
	return objArrayList;
}
public void setObjArrayList(ArrayList<BusinessDetailDTO> objArrayList) {
	this.objArrayList = objArrayList;
}
public String execute()
{
	BusinessDetailDao objBusinessDetailDao=new BusinessDetailDao();
	objArrayList=objBusinessDetailDao.getDateWiseAllSms(fdate,tdate);
	today_smscount=objBusinessDetailDao.getTodaySmsCount();
	return "success";
}
public int getToday_smscount() {
	return today_smscount;
}
public void setToday_smscount(int today_smscount) {
	this.today_smscount = today_smscount;
}
}
