package com.dialuz.salesTracking.action;

import com.opensymphony.xwork2.ActionSupport;

public class LogOutAction extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6474212150551579807L;
	private String success_msg;
	public String getSuccess_msg() {
		return success_msg;
	}
	public void setSuccess_msg(String success_msg) {
		this.success_msg = success_msg;
	}
	public String execute()
	{
		success_msg="Hai User You Are Successfully Logged Out...";
		return SUCCESS;
	}

}
