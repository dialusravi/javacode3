package com.dialuz.salesTracking.action;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.opensymphony.xwork2.ActionSupport;

public class RejectAppointmentAction extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -308718600114329251L;
	private String businessName;
	private String tellcallername;
	private String reason;
	private String appointmentId;
	public String getAppointmentId() {
		return appointmentId;
	}
	public void setAppointmentId(String appointmentId) {
		this.appointmentId = appointmentId;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getTellcallername() {
		return tellcallername;
	}
	public void setTellcallername(String tellcallername) {
		this.tellcallername = tellcallername;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}

	public String execute()
	{
		
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		boolean d=objAppointmentDAO.rejectAppointments(appointmentId,tellcallername,businessName,reason);
		System.out.println(d);
		return "success";
	}
}
