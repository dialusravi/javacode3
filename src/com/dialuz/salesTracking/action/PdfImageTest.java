package com.dialuz.salesTracking.action;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import com.dialuz.salesTracking.DTO.BusinessDetailDTO;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;



public class PdfImageTest {
	
//	private BusinessDetailDto objBusinessDetailDto=null;

	public PdfImageTest(ArrayList<BusinessDetailDTO> objArrayList){
		
	

		 try{
	        	
         FileOutputStream file = new FileOutputStream(new File("/var/opt/telesales/webapps/ROOT/Appointments/callplanonleads.pdf"));
         Document document = new Document();
         PdfWriter.getInstance(document, file);

      
            
            PdfPTable table=new PdfPTable(7);
           
            /*float[] columnWidths = {16f,16f,6f,6f,6f,6f,6f,6f};
            table.setWidths(columnWidths);*/
            
               
            
		       PdfPCell c1 = new PdfPCell(new Phrase("S.No"));
			    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			    table.addCell(c1);

			    c1 = new PdfPCell(new Phrase("Business Name"));
			    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			    table.addCell(c1);
			    
			    
			    c1 = new PdfPCell(new Phrase("Business Mobile"));
			    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			    table.addCell(c1);

			    c1 = new PdfPCell(new Phrase("Keyword"));
			    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			    table.addCell(c1);
			    
			    c1 = new PdfPCell(new Phrase("City"));
			    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			    table.addCell(c1);
			    
			    c1 = new PdfPCell(new Phrase("Area"));
			    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			    table.addCell(c1);
			    
			    c1 = new PdfPCell(new Phrase("No of Views"));
			    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			    table.addCell(c1);
			    int count=0;
			    table.setHeaderRows(1);
			    Iterator<BusinessDetailDTO> itr = objArrayList.iterator();
			    while(itr.hasNext())
			    {
			    	
			    	BusinessDetailDTO objuserregdto=itr.next();
			    	
			    	    table.addCell(String.valueOf(count));		    	    
					    table.addCell(objuserregdto.getBusiness_name());
					    table.addCell(objuserregdto.getMobile());
					    table.addCell(objuserregdto.getCat());
					    table.addCell(objuserregdto.getCity());
					    table.addCell(objuserregdto.getArea());
					    table.addCell(String.valueOf(objuserregdto.getView_count()));
		           
		              count++;
			    
			    }

			    
     
            //Now Insert Every Thing Into PDF Document
            document.open();//PDF document opened........                   
             document.add(new Paragraph("Document Generated On - "+new Date().toString()));  
             document.add(Chunk.NEWLINE); 

               document.add(table);

                  
                document.close();

                file.close();

     
   } catch (Exception e) {
       e.printStackTrace();
   }
   
	}
		
	
	public static void main(String[] args) {
		 

    }
	        }

