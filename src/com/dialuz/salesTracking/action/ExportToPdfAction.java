package com.dialuz.salesTracking.action;

import java.util.ArrayList;
import com.dialuz.salesTracking.DAO.BusinessDetailDao;
import com.dialuz.salesTracking.DTO.BusinessDetailDTO;
import com.opensymphony.xwork2.ActionSupport;

public class ExportToPdfAction extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2370212389489281338L;
	ArrayList<BusinessDetailDTO> list=new ArrayList<BusinessDetailDTO>();
	private String success_msg;
	public ArrayList<BusinessDetailDTO> getList()
	{
		return list;
	}
	public void setList(ArrayList<BusinessDetailDTO> list)
	{
		this.list = list;
	}
	
	public String execute()
	{
		BusinessDetailDao objBusinessDetailDao=new BusinessDetailDao();
		list=objBusinessDetailDao.getCallPlanLeads();
		if(!list.isEmpty())
		{
			success_msg="All Results Exported Successfully";
		
			new PdfImageTest(list);
		}
		
		
		return "success";
	}
	public String getSuccess_msg() {
		return success_msg;
	}
	public void setSuccess_msg(String success_msg) {
		this.success_msg = success_msg;
	}
}
