package com.dialuz.salesTracking.action;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;


public class LoadAppointmentsAction  extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1281231945981661100L;
	private AppointmentDTO objCheckAppointmentDTO=new AppointmentDTO();
	private ArrayList<String> objSalesPersonsList=new ArrayList<String>();
	private String type;
	private String usertype;
	private String appointmentId;
	private String appointmentPlace;
	private String success_msg;
	private String appointmentCity;
	private String tellcallername;
	private String todaydate;
	private String append_todaydate;
	
	
	public String getAppend_todaydate() {
		return append_todaydate;
	}
	public void setAppend_todaydate(String append_todaydate) {
		this.append_todaydate = append_todaydate;
	}
	public String getTodaydate() {
		return todaydate;
	}
	public void setTodaydate(String todaydate) {
		this.todaydate = todaydate;
	}
	public String getTellcallername() {
		return tellcallername;
	}
	public void setTellcallername(String tellcallername) {
		this.tellcallername = tellcallername;
	}
	String resultpage;
	public String getAppointmentId() {
		return appointmentId;
	}
	public String getAppointmentPlace() {
		return appointmentPlace;
	}
	public void setAppointmentPlace(String appointmentPlace) {
		this.appointmentPlace = appointmentPlace;
	}
	public void setAppointmentId(String appointmentId) {
		this.appointmentId = appointmentId;
	}
	private ArrayList<AppointmentDTO> objAppointmentDTOList=new ArrayList<AppointmentDTO>();
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public AppointmentDTO getObjCheckAppointmentDTO() {
		return objCheckAppointmentDTO;
	}
	public void setObjCheckAppointmentDTO(AppointmentDTO objCheckAppointmentDTO) {
		this.objCheckAppointmentDTO = objCheckAppointmentDTO;
	}
	public ArrayList<AppointmentDTO> getObjAppointmentDTOList()
	{
		return objAppointmentDTOList;
	}
	public void setObjAppointmentDTOList(ArrayList<AppointmentDTO> objAppointmentDTOList) {
		this.objAppointmentDTOList = objAppointmentDTOList;
	}
	public String getAppointmentCity() {
		return appointmentCity;
	}
	public void setAppointmentCity(String appointmentCity) {
		this.appointmentCity = appointmentCity;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public ArrayList<String> getObjSalesPersonsList() {
		return objSalesPersonsList;
	}
	public void setObjSalesPersonsList(ArrayList<String> objSalesPersonsList) {
		this.objSalesPersonsList = objSalesPersonsList;
	}
	
	public String execute()
	{
	
		
		HttpServletRequest request  = (HttpServletRequest)ActionContext.getContext().get(org.apache.struts2.StrutsStatics.HTTP_REQUEST);
		HttpSession session=request.getSession(true);
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		objCheckAppointmentDTO.setUsertype(usertype);
		objCheckAppointmentDTO.setTodaydate(todaydate);
		session.setAttribute("todaydate", todaydate);
		
		usertype=(String)session.getAttribute("usertype");
        
		
		/*telecallerNames=objLoginDAO.getTelecallerNames();
		salesPersons=objLoginDAO.getSalesPersonNames();*/
		
		
		
		
		objCheckAppointmentDTO.setUsertype(usertype);
		objCheckAppointmentDTO.setAppointmentType(type);
		if(objCheckAppointmentDTO.getTodaydate()!="" && objCheckAppointmentDTO.getTodaydate()!=null)
		{
			
			objAppointmentDTOList=objAppointmentDAO.loadAppointments(objCheckAppointmentDTO);
			
			resultpage="success";
			
		}
		
		else 
		{
			
			objCheckAppointmentDTO.setAppointmentPlace(appointmentPlace);
			objCheckAppointmentDTO.setAppointmentCity(appointmentCity);
			objCheckAppointmentDTO.setAppointmentId(appointmentId);
			objCheckAppointmentDTO=objAppointmentDAO.getAppointmentDetails(objCheckAppointmentDTO);
			
			objSalesPersonsList=objAppointmentDAO.getSalesPersons(appointmentPlace, appointmentCity);
			
			if(objSalesPersonsList.size()==0)
			{
				success_msg="Please Allot Sales Persons in "+appointmentCity;
			}
			
				resultpage="loadAppointmentForm";
				
			
		}
		return resultpage;
		
	}
	public String getSuccess_msg() {
		return success_msg;
	}
	public void setSuccess_msg(String success_msg) {
		this.success_msg = success_msg;
	}
	
	
	
	
}
