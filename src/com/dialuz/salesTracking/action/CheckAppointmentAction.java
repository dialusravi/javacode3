
package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.dialuz.salesTracking.DTO.BusinessDetailDTO;
import com.opensymphony.xwork2.ActionSupport;

public class CheckAppointmentAction extends ActionSupport
{
	private static final long serialVersionUID = 2336730759173520794L;
	private AppointmentDTO objCheckAppointmentDTO=new AppointmentDTO();
	private String mailid;
	private String mobile;
	private String phoneno;
	private String business_id;
	private ArrayList<String> objarraylist;
	BusinessDetailDTO objBusinessDetailDTO=new BusinessDetailDTO();
	public ArrayList<String> getObjarraylist() {
		return objarraylist;
	}


	public void setObjarraylist(ArrayList<String> objarraylist) {
		this.objarraylist = objarraylist;
	}
	public String getMailid() {
		return mailid;
	}
	public void setMailid(String mailid) {
		this.mailid = mailid;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
	private ArrayList<AppointmentDTO> objAppointmentDTOList=new ArrayList<AppointmentDTO>();
	
	public ArrayList<AppointmentDTO> getObjAppointmentDTOList() {
		return objAppointmentDTOList;
	}
	public void setObjAppointmentDTOList(ArrayList<AppointmentDTO> objAppointmentDTOList) {
		this.objAppointmentDTOList = objAppointmentDTOList;
	}
	
	public AppointmentDTO getObjCheckAppointmentDTO() {
		return objCheckAppointmentDTO;
	}
	public void setObjCheckAppointmentDTO(AppointmentDTO objCheckAppointmentDTO) {
		this.objCheckAppointmentDTO = objCheckAppointmentDTO;
	}

	
	public String execute()
	{
	
		
		objCheckAppointmentDTO.setBusinessMobile(mobile);
		objCheckAppointmentDTO.setBusinessPhone(phoneno);
		objCheckAppointmentDTO.setBusinessMail(mailid);
		
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
	
		
		
		if(business_id!=null && !business_id.isEmpty() && business_id!="")
		{
			
			mobile=objBusinessDetailDTO.getMobile();
			phoneno=objBusinessDetailDTO.getPh1();
			mailid=objBusinessDetailDTO.getEmail();
		objCheckAppointmentDTO.setBusinessMobile(objBusinessDetailDTO.getMobile());
		objCheckAppointmentDTO.setBusinessPhone(objBusinessDetailDTO.getPh1());
		//objCheckAppointmentDTO.setBusinessName(objBusinessDetailDTO.getBusiness_name());
		objCheckAppointmentDTO.setBusinessCity(objBusinessDetailDTO.getCity());
		objCheckAppointmentDTO.setBusinessArea(objBusinessDetailDTO.getArea());
		objCheckAppointmentDTO.setBusinessMail(objBusinessDetailDTO.getEmail());
		
		}
		else{
		objAppointmentDTOList=objAppointmentDAO.getAppointments(objCheckAppointmentDTO);
		}
		if(objAppointmentDTOList.size()==0)
		{
			return SUCCESS;
		}else
		{
			return "appointment";
		}
		
		
		
		
		
}
		
		
		
	


	public String getBusiness_id() {
		return business_id;
	}


	public void setBusiness_id(String business_id) {
		this.business_id = business_id;
	}


	public BusinessDetailDTO getObjBusinessDetailDTO() {
		return objBusinessDetailDTO;
	}


	public void setObjBusinessDetailDTO(BusinessDetailDTO objBusinessDetailDTO) {
		this.objBusinessDetailDTO = objBusinessDetailDTO;
	}
	
}




















