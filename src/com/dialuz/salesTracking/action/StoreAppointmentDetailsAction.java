package com.dialuz.salesTracking.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class StoreAppointmentDetailsAction extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1495972789632342756L;
	private AppointmentDTO objCheckAppointmentDTO=new AppointmentDTO();
	private String success_msg;
	private String keyword;
	
	
	public AppointmentDTO getObjCheckAppointmentDTO() {
		return objCheckAppointmentDTO;
	}
	public void setObjCheckAppointmentDTO(AppointmentDTO objCheckAppointmentDTO) {
		this.objCheckAppointmentDTO = objCheckAppointmentDTO;
	}
	public String execute()
	{
		HttpServletRequest request  = (HttpServletRequest)ActionContext.getContext().get(org.apache.struts2.StrutsStatics.HTTP_REQUEST);
		HttpSession session=request.getSession(true);
		objCheckAppointmentDTO.setTelecallerName((String)session.getAttribute("username")); 
		objCheckAppointmentDTO.setTelecallerMobile((String)session.getAttribute("usermobile")); 
		
					
		
		boolean insert_status=false;
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		try
		{
			if(objCheckAppointmentDTO.getAppointmentTime().endsWith("M"))
			{
				String s[]=objCheckAppointmentDTO.getAppointmentTime().split(" ");
				objCheckAppointmentDTO.setAppointmentTime(s[0]+":00");
				
			}
		}catch(Exception e){}
		try{
			
			if(objCheckAppointmentDTO.getKeyword1()==null || objCheckAppointmentDTO.getKeyword2()==null || objCheckAppointmentDTO.getKeyword3()==null || objCheckAppointmentDTO.getKeyword4()==null || objCheckAppointmentDTO.getKeyword5()==null){
			if(!keyword.isEmpty() || keyword!=null)
			{
			keyword=keyword.replace("$",",");
			keyword=keyword.substring(0,keyword.length()-1);
			
			List<String> elephantList=new ArrayList<String>();
			
			
			elephantList =Arrays.asList(keyword.split(","));
			
			if(elephantList.get(0)!=null && elephantList.get(0)!="")
			{
				
				objCheckAppointmentDTO.setKeyword1(elephantList.get(0).trim());
			}
			 if(elephantList.get(1)!=null && elephantList.get(1)!="")
			{
		
				 objCheckAppointmentDTO.setKeyword2(elephantList.get(1).trim());
			}
			 if(elephantList.get(2)!=null && elephantList.get(2)!="")
			{
				
				 objCheckAppointmentDTO.setKeyword3(elephantList.get(2).trim());
			}
			 if(elephantList.get(3)!=null && elephantList.get(3)!="")
			{
				 objCheckAppointmentDTO.setKeyword4(elephantList.get(3).trim());
			}
			
			if(elephantList.get(4)!=null && elephantList.get(4)!="")
			{
				objCheckAppointmentDTO.setKeyword5(elephantList.get(4).trim());
			}
						
		}
			}
			
				
			
	
		}
	catch (Exception e)
		{
		
		}
		
		insert_status=objAppointmentDAO.storeAppointmentDetails(objCheckAppointmentDTO);
		if(insert_status)
		{
			success_msg="Your Appointment Details Has Been Submitted Successfully";
			return SUCCESS;
		}else
		{
			success_msg="Sorry Error Occured While Submitting Details....";
			return ERROR;
		}
		
	}
	public String getSuccess_msg() {
		return success_msg;
	}
	public void setSuccess_msg(String success_msg) {
		this.success_msg = success_msg;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
}
