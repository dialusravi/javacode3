package com.dialuz.salesTracking.action;

import java.util.ArrayList;


import com.dialuz.salesTracking.DAO.TargetDao;
import com.dialuz.salesTracking.DTO.TargetDTO;
import com.opensymphony.xwork2.ActionSupport;

public class ProblemsAction extends ActionSupport{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String subject;
	private String description;
	private String username;
	private String fdate;
	private String tdate;
	private String param;
	TargetDTO objTargetDTO=new TargetDTO();
	public String getFdate() {
		return fdate;
	}
	public void setFdate(String fdate) {
		this.fdate = fdate;
	}
	public String getTdate() {
		return tdate;
	}
	public void setTdate(String tdate) {
		this.tdate = tdate;
	}
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	public TargetDTO getObjTargetDTO() {
		return objTargetDTO;
	}
	public void setObjTargetDTO(TargetDTO objTargetDTO) {
		this.objTargetDTO = objTargetDTO;
	}
	ArrayList<TargetDTO> objArrayList=new ArrayList<TargetDTO>();
	public ArrayList<TargetDTO> getObjArrayList() {
		return objArrayList;
	}
	public void setObjArrayList(ArrayList<TargetDTO> objArrayList) {
		this.objArrayList = objArrayList;
	}
	public String getUsername()
	{
		return username;
	}
	public void setUsername(String username)
	{
		this.username = username;
	}
	public String getSubject() 
	{
		return subject;
	}
	public void setSubject(String subject)
	{
		this.subject = subject;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public String getDate()
	{
		return date;
	}
	public void setDate(String date)
	{
		this.date = date;
	}
	private String date;
	public String execute()
	{
TargetDao objTargetDao=new TargetDao();
		
		
		
		if(param==null)
		{
					if((subject==null) && (description==null))
					{
					objArrayList=objTargetDao.getProblems(username);
					objTargetDTO=objTargetDao.getProblemsCount(username);
					return "success";
					}
				else{
					objTargetDao.insertProblems(subject,description,username);
					}
					return "insert";
		}
	else{
			objArrayList=objTargetDao.getProblems1(fdate,tdate);
			objTargetDTO=objTargetDao.getProblemsCount1(fdate,tdate);
			return "success";
		}
		}
}
