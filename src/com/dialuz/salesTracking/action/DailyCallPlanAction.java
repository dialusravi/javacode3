package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.BusinessDetailDao;
import com.dialuz.salesTracking.DTO.BusinessDetailDTO;

public class DailyCallPlanAction {
	
  private String city; 
	
	public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}
	ArrayList<BusinessDetailDTO> objArrayList=new ArrayList<BusinessDetailDTO>();

	public ArrayList<BusinessDetailDTO> getObjArrayList() {
		return objArrayList;
	}

	public void setObjArrayList(ArrayList<BusinessDetailDTO> objArrayList) {
		this.objArrayList = objArrayList;
	}
public String execute(){
	
	
		BusinessDetailDao objBusinessDetailDao=new BusinessDetailDao();
		
		objArrayList=objBusinessDetailDao.getDailyCallplan(city);
		
		
		return "success";
	}
	
	

}
