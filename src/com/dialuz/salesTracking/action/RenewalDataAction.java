package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.RenewalDataDAO;
import com.dialuz.salesTracking.DTO.RenewalDataDTO;
import com.opensymphony.xwork2.ActionSupport;

public class RenewalDataAction extends ActionSupport {

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ArrayList<RenewalDataDTO> objArrayList=new ArrayList<RenewalDataDTO>();

	public String execute(){
		
		RenewalDataDAO objRenewalDataDAO=new RenewalDataDAO();
		
		objArrayList=objRenewalDataDAO.getRenewalData();
		
		
		
		return "success";
	}

	public ArrayList<RenewalDataDTO> getObjArrayList() {
		return objArrayList;
	}

	public void setObjArrayList(ArrayList<RenewalDataDTO> objArrayList) {
		this.objArrayList = objArrayList;
	}

	
}
