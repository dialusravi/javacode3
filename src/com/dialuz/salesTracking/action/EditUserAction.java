package com.dialuz.salesTracking.action;

import com.dialuz.salesTracking.DAO.UsersOperationsDAO;
import com.dialuz.salesTracking.DTO.UsersOperationsDTO;
import com.opensymphony.xwork2.ActionSupport;

public class EditUserAction extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8685725256236093761L;
	private String type;
	private String userId;
	private UsersOperationsDTO objUsersOperationsDTO=new UsersOperationsDTO();
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public UsersOperationsDTO getObjUsersOperationsDTO() {
		return objUsersOperationsDTO;
	}
	public void setObjUsersOperationsDTO(UsersOperationsDTO objUsersOperationsDTO) {
		this.objUsersOperationsDTO = objUsersOperationsDTO;
	}
	
	public String execute()
	{
		objUsersOperationsDTO.setUserId(userId);
		objUsersOperationsDTO.setType(type);
		UsersOperationsDAO objUsersOperationsDAO=new UsersOperationsDAO();
		
		if(type.equals("telecaller"))
		{
			objUsersOperationsDTO=objUsersOperationsDAO.getEditTelecallerDetails(objUsersOperationsDTO);
			return "telecallers";
		}
		else if(type.equals("teamleader"))
		{
		
	
			objUsersOperationsDTO=objUsersOperationsDAO.getEditTeamLeaderDetails(objUsersOperationsDTO);
		
			
			return "teamleader";
			
		}else {
			objUsersOperationsDTO=objUsersOperationsDAO.getEditSalesPersonsDetails(objUsersOperationsDTO);
			
			return "salespersons";
		}
		
	}
	
}
