package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionSupport;

public class UpdateAppointmentStatusAction extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2391961695396297417L;
	private AppointmentDTO objCheckAppointmentDTO=new AppointmentDTO();
	private String success_msg;
	
	public AppointmentDTO getObjCheckAppointmentDTO() {
		return objCheckAppointmentDTO;
	}
	public void setObjCheckAppointmentDTO(AppointmentDTO objCheckAppointmentDTO) {
		this.objCheckAppointmentDTO = objCheckAppointmentDTO;
	}
	public String getSuccess_msg() {
		return success_msg;
	}
	public void setSuccess_msg(String success_msg) {
		this.success_msg = success_msg;
	}
	
	
	public String execute()
	{
		boolean update_status=false;
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		update_status=objAppointmentDAO.updateAppointmentStatus(objCheckAppointmentDTO); 
		if(update_status)
		{
			success_msg="Appointment Updation Done Successfully,.......";
			return SUCCESS;
		}else
		{
			success_msg="Sorry Error Occured During Updation,.......";
			return SUCCESS;
		}
		
	}

}
