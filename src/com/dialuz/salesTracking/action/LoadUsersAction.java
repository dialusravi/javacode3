package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.UsersOperationsDAO;
import com.dialuz.salesTracking.DTO.UsersOperationsDTO;
import com.opensymphony.xwork2.ActionSupport;

public class LoadUsersAction extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7364372803869445958L;
	/**
	 * 
	 */
	private String type;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public ArrayList<UsersOperationsDTO> getObjUsersOperationsDTOList() {
		return objUsersOperationsDTOList;
	}
	public void setObjUsersOperationsDTOList(ArrayList<UsersOperationsDTO> objUsersOperationsDTOList) {
		this.objUsersOperationsDTOList = objUsersOperationsDTOList;
	}
	private ArrayList<UsersOperationsDTO> objUsersOperationsDTOList=new ArrayList<UsersOperationsDTO>();
	
	public String execute()
	{
		UsersOperationsDAO objUsersOperationsDAO=new UsersOperationsDAO();
		if(type.equals("telecaller"))
		{
			
			objUsersOperationsDTOList=objUsersOperationsDAO.getTelecallers();
			
			return "telecallers";
			
		}
		if(type.equals("teamleader"))
		{
			
    objUsersOperationsDTOList=objUsersOperationsDAO.getTeamLeader();
   
			
			  return "teamleader";
		}
		else
		{
   			
  objUsersOperationsDTOList=objUsersOperationsDAO.getSalesPersons();
			
			return "salespersons";
		}
		
		
		
		
	}

}
