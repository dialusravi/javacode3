package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.TargetDao;
import com.dialuz.salesTracking.DTO.TargetDTO;
import com.opensymphony.xwork2.ActionSupport;



public class ProblemFixAction extends ActionSupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String user;
	private String status123;
	private String username;
	TargetDTO objTargetDTO=new TargetDTO();
	public TargetDTO getObjTargetDTO() {
		return objTargetDTO;
	}
	public void setObjTargetDTO(TargetDTO objTargetDTO) {
		this.objTargetDTO = objTargetDTO;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	ArrayList<TargetDTO> objArrayList=new ArrayList<TargetDTO>();
	public ArrayList<TargetDTO> getObjArrayList() {
		return objArrayList;
	}
	public void setObjArrayList(ArrayList<TargetDTO> objArrayList) {
		this.objArrayList = objArrayList;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getStatus123() {
		return status123;
	}
	public void setStatus123(String status123)
	{
		this.status123 = status123;
	}
	public String execute()
	{
		TargetDao dao=new TargetDao();
		int status=dao.fixProblem(id,user);
		if(status>0)
		{
		status123="fixed";	
		}
		objArrayList=dao.getProblems(username);
		objTargetDTO=dao.getProblemsCount(username);
		return "success";
	}

}
