package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.BusinessDetailDao;
import com.dialuz.salesTracking.DTO.BusinessDetailDTO;
import com.opensymphony.xwork2.ActionSupport;

public class ShowSmsAction extends ActionSupport
{

	private static final long serialVersionUID = -3491380003377567150L;
	private String mobile;
	private ArrayList<BusinessDetailDTO> objArrayList=new ArrayList<BusinessDetailDTO>();
	private int today_smscount; 

	public ArrayList<BusinessDetailDTO> getObjArrayList() {
		return objArrayList;
	}

	public void setObjArrayList(ArrayList<BusinessDetailDTO> objArrayList) {
		this.objArrayList = objArrayList;
	}
	public String execute(){
	BusinessDetailDao objBusinessDetailDao=new BusinessDetailDao();
	objArrayList=objBusinessDetailDao.getShowSms(mobile);
	
	
	return "success";
}

	public int getToday_smscount() {
		return today_smscount;
	}

	public void setToday_smscount(int today_smscount) {
		this.today_smscount = today_smscount;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

}
