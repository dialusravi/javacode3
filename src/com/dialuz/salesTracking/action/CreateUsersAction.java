package com.dialuz.salesTracking.action;

import com.dialuz.salesTracking.DAO.UsersOperationsDAO;
import com.dialuz.salesTracking.DTO.UsersOperationsDTO;
import com.opensymphony.xwork2.ActionSupport;

public class CreateUsersAction extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1596980242650499146L;
	/**
	 * 
	 */
	private UsersOperationsDTO objUsersOperationsDTO=new UsersOperationsDTO();
	private String usertype;
	private String success_msg;
	public UsersOperationsDTO getObjUsersOperationsDTO() {
		return objUsersOperationsDTO;
	}
	public void setObjUsersOperationsDTO(UsersOperationsDTO objUsersOperationsDTO) {
		this.objUsersOperationsDTO = objUsersOperationsDTO;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String getSuccess_msg() {
		return success_msg;
	}
	public void setSuccess_msg(String success_msg) {
		this.success_msg = success_msg;
	}
	
	public String execute()
	{
		UsersOperationsDAO objUsersOperationsDAO=new UsersOperationsDAO();
		boolean update_status=false;
		if(objUsersOperationsDTO.getUsertype().equals("telecaller"))
		{
			update_status=objUsersOperationsDAO.createTelecallers(objUsersOperationsDTO);
			
		}else if(objUsersOperationsDTO.getUsertype().equals("teamleader"))
		{
			update_status=objUsersOperationsDAO.createTeamLeader(objUsersOperationsDTO);
		}else {
			update_status=objUsersOperationsDAO.createSalesPersons(objUsersOperationsDTO);
		}
		
		
		
		if(update_status)
		{
			success_msg="User Created Successfully";
		}else
		{
			success_msg="Sorry Error Occured While Creating User";
		}
		return SUCCESS;
	}
	
	
}
