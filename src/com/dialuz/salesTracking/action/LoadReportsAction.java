package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class LoadReportsAction extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5537417368592918004L;
	/**
	 * 
	 */
	private String type;
	private String report;
	private String todaydate;
	private ArrayList<AppointmentDTO> objAppointmentDTOsList=new ArrayList<AppointmentDTO>();
	public ArrayList<AppointmentDTO> getObjAppointmentDTOsList() {
		return objAppointmentDTOsList;
	}
	public void setObjAppointmentDTOsList(ArrayList<AppointmentDTO> objAppointmentDTOsList) {
		this.objAppointmentDTOsList = objAppointmentDTOsList;
	}
	public String getType()
	{
		return type;
	}
	public void setType(String type) 
	{
		this.type = type;
	}
	
	public String execute()
	{
		
	
		HttpServletRequest request  = (HttpServletRequest)ActionContext.getContext().get(org.apache.struts2.StrutsStatics.HTTP_REQUEST);
		HttpSession session=request.getSession(true);
		
		String resultpage;
		AppointmentDTO objAppointmentDTO=new AppointmentDTO();
		
		objAppointmentDTO.setReportType(type);
		objAppointmentDTO.setTodaydate(todaydate);
		objAppointmentDTO.setAppointmentType(report);
		objAppointmentDTO.setTelecallerName((String)session.getAttribute("username"));
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		objAppointmentDTOsList=objAppointmentDAO.getReports(objAppointmentDTO);
		if(type.equals("daily"))
		{
			resultpage="daily";
		}else if(type.equals("weekly"))
		{
			resultpage="weekly";
		}
		else if(type.equals("monthly")){
			resultpage="monthly";
			}
		else
		{
			resultpage="callback";
		}
		return resultpage;
	}
	public String getReport() {
		return report;
	}
	public void setReport(String report) {
		this.report = report;
	}
	public String getTodaydate() {
		return todaydate;
	}
	public void setTodaydate(String todaydate) {
		this.todaydate = todaydate;
	}
	
	
}
