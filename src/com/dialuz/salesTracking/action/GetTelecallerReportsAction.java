package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionSupport;

public class GetTelecallerReportsAction extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5537417368592918004L;
	/**
	 * 
	 */
	private String report;
	private String person;
	private String type;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getReport() {
		return report;
	}
	public void setReport(String report) {
		this.report = report;
	}
	public String getPerson() {
		return person;
	}
	public void setPerson(String person) {
		this.person = person;
	}


	private ArrayList<AppointmentDTO> objAppointmentDTOsList=new ArrayList<AppointmentDTO>();
	public ArrayList<AppointmentDTO> getObjAppointmentDTOsList() {
		return objAppointmentDTOsList;
	}
	public void setObjAppointmentDTOsList(ArrayList<AppointmentDTO> objAppointmentDTOsList) {
		this.objAppointmentDTOsList = objAppointmentDTOsList;
	}
	
	
	public String execute()
	{
		 String resultpage=""; 
		AppointmentDTO objAppointmentDTO=new AppointmentDTO();
		objAppointmentDTO.setReportType(report);
		objAppointmentDTO.setTelecallerName(person);
		objAppointmentDTO.setAppointmentType(type);
		
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		if(type.equals("caller"))
		{
			objAppointmentDTOsList=objAppointmentDAO.getTelecallerReports(objAppointmentDTO);
			resultpage="callerreport";
		}else
		{
			objAppointmentDTOsList=objAppointmentDAO.getSalesPersonsReports(objAppointmentDTO);
			resultpage="salesreport";
		}
		
		return resultpage;
	}
	
	
	
}
