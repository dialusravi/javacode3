package com.dialuz.salesTracking.action;

import java.io.File;
import java.util.ArrayList;

import com.dialuz.salesTracking.DTO.AppointmentDTO;

import jxl.Workbook;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;


public class ExcelCreate {

	public void create(ArrayList<AppointmentDTO> objAppointmentDTOList) 
	{

        try {
           
        	ArrayList<String> list = new ArrayList<String>();
         	
           	
        	String input = "";

        	for (AppointmentDTO s : objAppointmentDTOList)
        	{
        		list.add(s.getAppointmentId());
        		list.add(s.getBusinessName());
        		list.add(s.getAppointmentDateTime());
        		list.add(s.getTelecallerName());
        		list.add(s.getBusinessCity());
        		list.add(s.getBusinessArea());
        		
        	}

        	for (String s : list)
        	{
        		input += s +","+ "\t";
        	}

        	
        	
        	
            int counter = 0;
 
            WritableWorkbook workbook =
                Workbook.createWorkbook(new File("C:/Users/venkateswararao.ch/Desktop/raj.xls"));
            WritableSheet sheet = workbook.createSheet("Page1", 0);
 
            //Set Header
 
            String Header[] = new String[6];
            Header[0] = "AppointmentId";
            Header[1] = "BusinessName";
            Header[2] = "AppointmentDateTime";
            Header[3] = "TelecallerName";
            Header[4] = "BusinessCity";
            Header[5] = "BusinessArea";
             
            
            //Setting Background colour for Cells
            Colour bckcolor = Colour.DARK_GREEN;
            WritableCellFormat cellFormat = new WritableCellFormat();
            cellFormat.setBackground(bckcolor);
 
            //Setting Colour & Font for the Text
 
            WritableFont font = new WritableFont(WritableFont.ARIAL);
            font.setColour(Colour.GOLD);
            cellFormat.setFont(font);
 
            // Write the Header to the excel file
            for (int i = 0; i < Header.length; i++)
            {
                Label label = new Label(i, 0, Header[i]);
                sheet.addCell(label);
                WritableCell cell = sheet.getWritableCell(i, 0);
                cell.setCellFormat(cellFormat);
 
             }
 
            String inputArray[] = input.split("END");
    
            for (int i = 0; i < inputArray.length; i++) 
            {
 
                counter++;
 
                String fieldValuesArray[] = inputArray[i].split(",");
 
                for (int j = 0; j <fieldValuesArray.length; j++)
                {
                	
                	
                    Label label =new Label(j, counter, fieldValuesArray[j].toString());
                    
                   
                    sheet.addCell(label);
                   
                   
                }
                
            }
 
            workbook.write();
            workbook.close();
 
        } catch (Exception e) {
           
        }
    
		
	}
}
