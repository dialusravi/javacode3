package com.dialuz.salesTracking.action;

import com.dialuz.salesTracking.DAO.AutoCompleteDao;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;

public class TargetUserAutoComleteAction extends ActionSupport
{

    public TargetUserAutoComleteAction()
    {
        list = new ArrayList();
    }

    public String getTargetuser()
    {
        return targetuser;
    }

    public void setTargetuser(String targetuser)
    {
        this.targetuser = targetuser;
    }

    public ArrayList getList()
    {
        return list;
    }

    public void setList(ArrayList list)
    {
        this.list = list;
    }

    public String execute()
    {
        HttpServletRequest request = (HttpServletRequest)ActionContext.getContext().get("com.opensymphony.xwork2.dispatcher.HttpServletRequest");
        targetuser = request.getParameter("q");
        AutoCompleteDao objAutocomplteDao = new AutoCompleteDao();
        list = objAutocomplteDao.getTargetUserType(targetuser);
        return "success";
    }

    private static final long serialVersionUID = 0x1f786053aea7c585L;
    private String targetuser;
    ArrayList list;
}