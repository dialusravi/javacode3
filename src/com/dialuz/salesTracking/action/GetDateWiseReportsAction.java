package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionSupport;

public class GetDateWiseReportsAction extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3243623210008819710L;
	private String reportTodate;
	private String reportFromdate;
	private String reportUserType;
	private String reportUserName;
	private AppointmentDTO objAppointmentDTO=new AppointmentDTO();
	private ArrayList<AppointmentDTO> objAppointmentDTOsList=new ArrayList<AppointmentDTO>();
	public String getReportTodate() {
		return reportTodate;
	}
	public void setReportTodate(String reportTodate) {
		this.reportTodate = reportTodate;
	}
	public String getReportFromdate() {
		return reportFromdate;
	}
	public void setReportFromdate(String reportFromdate) {
		this.reportFromdate = reportFromdate;
	}
	public String getReportUserType() {
		return reportUserType;
	}
	public void setReportUserType(String reportUserType) {
		this.reportUserType = reportUserType;
	}
	public String getReportUserName() {
		return reportUserName;
	}
	public void setReportUserName(String reportUserName) {
		this.reportUserName = reportUserName;
	}
	public ArrayList<AppointmentDTO> getObjAppointmentDTOsList() {
		return objAppointmentDTOsList;
	}
	public void setObjAppointmentDTOsList(ArrayList<AppointmentDTO> objAppointmentDTOsList) {
		this.objAppointmentDTOsList = objAppointmentDTOsList;
	}
	public AppointmentDTO getObjAppointmentDTO() {
		return objAppointmentDTO;
	}
	public void setObjAppointmentDTO(AppointmentDTO objAppointmentDTO) {
		this.objAppointmentDTO = objAppointmentDTO;
	}
	
	public String execute()
	{
		objAppointmentDTO.setFromDate(reportFromdate);
		objAppointmentDTO.setToDate(reportTodate);
		
		return SUCCESS;
	}
	

}
