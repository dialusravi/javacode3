package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionSupport;

public class AllotAppointmentsAction extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2986589400811216530L;
	private AppointmentDTO objCheckAppointmentDTO=new AppointmentDTO();
	private boolean update_status=false;
	private String success_msg;
	private String operation;
	private String telecallername;
	
	private ArrayList<AppointmentDTO> objAppointmentDTOList=new ArrayList<AppointmentDTO>();
	
	public ArrayList<AppointmentDTO> getObjAppointmentDTOList() {
		return objAppointmentDTOList;
	}
	public void setObjAppointmentDTOList(
			ArrayList<AppointmentDTO> objAppointmentDTOList) {
		this.objAppointmentDTOList = objAppointmentDTOList;
	}
	public boolean isUpdate_status() {
		return update_status;
	}
	public void setUpdate_status(boolean update_status) {
		this.update_status = update_status;
	}
	public AppointmentDTO getObjCheckAppointmentDTO() {
		return objCheckAppointmentDTO;
	}
	public void setObjCheckAppointmentDTO(AppointmentDTO objCheckAppointmentDTO) {
		this.objCheckAppointmentDTO = objCheckAppointmentDTO;
	}
	private String todaydate;
	public String getTodaydate() {
		return todaydate;
	}
	public void setTodaydate(String todaydate) {
		this.todaydate = todaydate;
	}
	public String execute()
	{
		objCheckAppointmentDTO.setTelecallerName(telecallername);
		operation="allot";
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		if(telecallername!=null||telecallername!="")
		{
	
		update_status=objAppointmentDAO.allotAppointments(objCheckAppointmentDTO);
		
		objAppointmentDTOList=objAppointmentDAO.getAllAppointmentsByName(objCheckAppointmentDTO); 	
		
		  return "success";
		}
	
	else{
		
		update_status=objAppointmentDAO.allotAppointments(objCheckAppointmentDTO);
		
		if(update_status)
		{
			success_msg="Your Appointment Details Has Been Submitted Successfully";
		}else
		{
			success_msg="Sorry Error Occured While Submitting Details........";
		}
		return SUCCESS;
		}
	
	}
	public String getSuccess_msg() {
		return success_msg;
	}
	public void setSuccess_msg(String success_msg) {
		this.success_msg = success_msg;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getTelecallername() {
		return telecallername;
	}
	public void setTelecallername(String telecallername) {
		this.telecallername = telecallername;
	}
}
