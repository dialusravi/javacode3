package com.dialuz.salesTracking.action;

import com.dialuz.salesTracking.DAO.MobStatusDAO;
import com.opensymphony.xwork2.ActionSupport;

public class MobStatusAction extends ActionSupport
{
	private static final long serialVersionUID = -948521988247612307L;
	private String salesperson_mobile;
	private String type;
	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}
	public String getSalesperson_mobile() {
		return salesperson_mobile;
	}


	public void setSalesperson_mobile(String salesperson_mobile) {
		this.salesperson_mobile = salesperson_mobile;
	}


	private String mobStatus;
	boolean status=false;
	

	public String getMobStatus() {
		return mobStatus;
	}


	public void setMobStatus(String mobStatus) {
		this.mobStatus = mobStatus;
	}

	
	public String execute()
	{
		MobStatusDAO objCreateuserDAO=new MobStatusDAO();
		status=objCreateuserDAO.checkbusinessMob(salesperson_mobile,type);
		if(status)
		{
			mobStatus="Sorry Already user Existed Try With Other User Name";
			return SUCCESS;
		}else
		{
			mobStatus="";
			return ERROR;
		}
		
	}


	
}
