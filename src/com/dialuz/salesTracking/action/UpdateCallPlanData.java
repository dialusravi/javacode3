package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.BusinessDetailDao;
import com.dialuz.salesTracking.DTO.BusinessDetailDTO;
import com.opensymphony.xwork2.ActionSupport;

public class UpdateCallPlanData extends ActionSupport {
	
	
	private int business_id;
	
	public int getBusiness_id() {
		return business_id;
	}

	public void setBusiness_id(int business_id) {
		this.business_id = business_id;
	}
	ArrayList<BusinessDetailDTO> list=new ArrayList<BusinessDetailDTO>();
	BusinessDetailDTO objBusinessDetailDTO=new BusinessDetailDTO();
	public ArrayList<BusinessDetailDTO> getList()
	{
		return list;
	}
	public void setList(ArrayList<BusinessDetailDTO> list)
	{
		this.list = list;
	}
	
	public String execute(){
		
		
		BusinessDetailDao objBusinessDetailDao=new BusinessDetailDao();
		int status=objBusinessDetailDao.updateCallPlanStatus(business_id);
		objBusinessDetailDTO=objBusinessDetailDao.getUpdateCallPlanLeads(business_id);
		
		return "success";
	
		
	}

	public BusinessDetailDTO getObjBusinessDetailDTO() {
		return objBusinessDetailDTO;
	}

	public void setObjBusinessDetailDTO(BusinessDetailDTO objBusinessDetailDTO) {
		this.objBusinessDetailDTO = objBusinessDetailDTO;
	}

	

}
