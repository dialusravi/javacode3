package com.dialuz.salesTracking.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class SubmitCallStatusAction extends ActionSupport
{

	/**
	 * 
	 */
	/**
	 * 
	 */
	private static final long serialVersionUID = 1760433064307060690L;
	private AppointmentDTO objCheckAppointmentDTO=new AppointmentDTO();
	private String success_msg;
	private boolean insert_status=false;
	private String operation;
	public AppointmentDTO getObjCheckAppointmentDTO() {
		return objCheckAppointmentDTO;
	}
	public void setObjCheckAppointmentDTO(AppointmentDTO objCheckAppointmentDTO) {
		this.objCheckAppointmentDTO = objCheckAppointmentDTO;
	}
	public String getSuccess_msg() {
		return success_msg;
	}
	public void setSuccess_msg(String success_msg) {
		this.success_msg = success_msg;
	}
	public boolean isInsert_status() {
		return insert_status;
	}
	public void setInsert_status(boolean insert_status) {
		this.insert_status = insert_status;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation)
	{
		this.operation = operation;
	}
	public String execute()
	{

		operation="callstatus";
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		HttpServletRequest request  = (HttpServletRequest)ActionContext.getContext().get(org.apache.struts2.StrutsStatics.HTTP_REQUEST);
		HttpSession session=request.getSession(true);
		objCheckAppointmentDTO.setTelecallerName((String)session.getAttribute("username"));
		
		insert_status=objAppointmentDAO.insertCallStatusDetails(objCheckAppointmentDTO);
		if(insert_status)
		{
			success_msg="Call Status Has Been Update Successfully.....";
			return SUCCESS;
		}else
		{
			success_msg="Sorry Error Occured While Updating Call Status.....";
			return SUCCESS;
		}
		
	}
	
	
	

}
