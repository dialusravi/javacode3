package com.dialuz.salesTracking.commonUtilities;


import java.util.*; 
import javax.mail.*; 
import javax.mail.internet.*; 
import javax.activation.*; 

public class SendMail 
{
	public static void sendMail(String filename,String Subject,String Content) throws MessagingException 
	{
			
			
				String mailTO="anu.t@dialuz.com"; 
				final String user="mailbox@dialus.com"; 
				final String password="mailbox@123"; 
				
				//1) get the session object 
				Properties pro=new Properties(); 
				pro.put("mail.smtp.host","mail.dialus.com"); 
				/*pro.put("mail.smtp.socketFactory.port","465"); 
				pro.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); */
				pro.put("mail.smtp.auth", "true"); 
				pro.put("mail.smtp.port", "25"); 
			
				Session session = Session.getDefaultInstance(pro, 
				new javax.mail.Authenticator()
				{ 
					protected PasswordAuthentication getPasswordAuthentication() { 
					return new PasswordAuthentication(user,password); 
				} 
				}); 
			
				//2) compose message 
				try{ 
					MimeMessage message = new MimeMessage(session); 
					message.setFrom(new InternetAddress(user)); 
					message.addRecipient(Message.RecipientType.TO,new InternetAddress(mailTO)); 
					message.setSubject(Subject); 
				
					//3) create MimeBodyPart object and set your message content 
					MimeBodyPart messageBodyPart1 = new MimeBodyPart(); 
					messageBodyPart1.setContent("<h1>Greetings of The Day....<br>Dear Sir<br>"+Content+"</h1>","text/html"); 
					
					
					//4) set file to datasource
					MimeBodyPart messageBodyPart3 = new MimeBodyPart(); 
					DataSource source1 = new FileDataSource(filename); 
					messageBodyPart3.setDataHandler(new DataHandler(source1)); 
					messageBodyPart3.setFileName(filename); 
					
				
				
				
					//5) create Multipart object and add MimeBodyPart objects to this object 
					Multipart multipart = new MimeMultipart(); 
					multipart.addBodyPart(messageBodyPart1); 
					//multipart.addBodyPart(messageBodyPart2); 
					multipart.addBodyPart(messageBodyPart3); 
				
				
					//6) set the multiplart object to the message object 
					message.setContent(multipart); 
				
					//7) send message 
					Transport.send(message); 
				
				}catch (MessagingException ex) {ex.printStackTrace();} 
				} 
	
	public static boolean sendMail(String mailid,String content) throws MessagingException 
	{
		        boolean status=false;
				
				
				final String user="mailbox@dialus.com"; 
				final String password="mailbox@123"; 
				
				//1) get the session object 
				Properties pro=new Properties(); 
				pro.put("mail.smtp.host","mail.dialus.com"); 
				/*pro.put("mail.smtp.socketFactory.port","465"); 
				pro.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); */
				pro.put("mail.smtp.auth", "true"); 
				pro.put("mail.smtp.port", "25"); 
			
				Session session = Session.getDefaultInstance(pro, 
				new javax.mail.Authenticator()
				{ 
					protected PasswordAuthentication getPasswordAuthentication() { 
					return new PasswordAuthentication(user,password); 
				} 
				}); 
			
				//2) compose message 
				try{ 
					MimeMessage message = new MimeMessage(session); 
					message.setFrom(new InternetAddress(user)); 
					message.addRecipient(Message.RecipientType.TO,new InternetAddress(mailid)); 
					message.setSubject("Infomation from Dialus."); 
				
					//3) create MimeBodyPart object and set your message content 
					MimeBodyPart messageBodyPart1 = new MimeBodyPart(); 
					messageBodyPart1.setContent(content,"text/html"); 
					
					
				
					//5) create Multipart object and add MimeBodyPart objects to this object 
					Multipart multipart = new MimeMultipart(); 
					multipart.addBodyPart(messageBodyPart1); 
					//multipart.addBodyPart(messageBodyPart2); 
					
				
				
					//6) set the multiplart object to the message object 
					message.setContent(multipart); 
				
					//7) send message 
					Transport.send(message); 
					status=true;
				
				}catch (MessagingException ex) {ex.printStackTrace();} 
				return status;
				} 
	
		}



