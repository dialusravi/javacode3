package com.dialuz.salesTracking.DTO;

import java.io.Serializable;

public class TrackFieldDTO  implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2386211117112498302L;
	
	private int id;
	private String business_name;
	private String business_owner;
	private String longitude;
	private String latitude;
	private String mobile;
	private String city;
	private String area;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBusiness_name() {
		return business_name;
	}
	public void setBusiness_name(String business_name) {
		this.business_name = business_name;
	}
	public String getBusiness_owner() {
		return business_owner;
	}
	public void setBusiness_owner(String business_owner) {
		this.business_owner = business_owner;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	
	
	
	

}
