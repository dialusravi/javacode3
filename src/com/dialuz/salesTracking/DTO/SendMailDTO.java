package com.dialuz.salesTracking.DTO;

import java.io.Serializable;

public class SendMailDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3936005429963662165L;
	
	String userName;
	String password;
	String to;
	String from;
	String subject;
	String body;
	String storeMail;
	String bcc;
	String cc;
	String applicationType;
	String poinOfMail;
	String countryCode;
	String ipAddress;
	
	
	
	
	public String getApplicationType() {
		return applicationType;
	}
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}
	public String getPoinOfMail() {
		return poinOfMail;
	}
	public void setPoinOfMail(String poinOfMail) {
		this.poinOfMail = poinOfMail;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getBcc() {
		return bcc;
	}
	public void setBcc(String bcc) {
		this.bcc = bcc;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	
	public String getStoreMail() {
		return storeMail;
	}
	public void setStoreMail(String storeMail) {
		this.storeMail = storeMail;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	

}
