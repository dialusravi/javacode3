package com.dialuz.salesTracking.DTO;

import java.io.Serializable;
import java.sql.Timestamp;

public class BusinessDetailDTO implements Serializable {
	
	
    private static final long serialVersionUID = 7026979705948982769L;
	
	
	private int business_id;
	private int sms_id;
	private String name;
	private String email;
	private String mobile;
	private String city;
	private String area;
	private String cat;
	private String business_name;
	private String dbMobile;
	private String msgContent;
	private int priority;
	private Timestamp st;
	private int id;
	private int view_count;
	private String leads_count;
	private String ph1;
	private String keyword1;
	
	public String getKeyword1() {
		return keyword1;
	}
	public void setKeyword1(String keyword1) {
		this.keyword1 = keyword1;
	}
	public String getPh1() {
		return ph1;
	}
	public void setPh1(String ph1) {
		this.ph1 = ph1;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	private String date_time;
	public String getMsgContent() {
		return msgContent;
	}
	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}
	public String getCat() {
		return cat;
	}
	public void setCat(String cat) {
		this.cat = cat;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public int getBusiness_id() {
		return business_id;
	}
	public void setBusiness_id(int business_id) {
		this.business_id = business_id;
	}
	public String getBusiness_name() {
		return business_name;
	}
	public void setBusiness_name(String business_name) {
		this.business_name = business_name;
	}
	public String getDbMobile() {
		return dbMobile;
	}
	public void setDbMobile(String dbMobile) {
		this.dbMobile = dbMobile;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public int getSms_id() {
		return sms_id;
	}
	public void setSms_id(int sms_id) {
		this.sms_id = sms_id;
	}
	public Timestamp getSt() {
		return st;
	}
	public void setSt(Timestamp st) {
		this.st = st;
	}
	public String getDate_time() {
		return date_time;
	}
	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}
	public int getView_count() {
		return view_count;
	}
	public void setView_count(int view_count) {
		this.view_count = view_count;
	}
	public String getLeads_count() {
		return leads_count;
	}
	public void setLeads_count(String leads_count) {
		this.leads_count = leads_count;
	}

}
