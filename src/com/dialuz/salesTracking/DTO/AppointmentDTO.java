package com.dialuz.salesTracking.DTO;

import java.io.Serializable;

public class AppointmentDTO  implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2386211117112498302L;
	
	private String appointmentId;
	private String businessName;
	private String businessCategory;
	private String businessOtherCategory;
	private String businessMobile;
	private String businessPhone;
	private String businessPhone1;
	private String contactNumber;
	private String contactPerson;
	private String appointmentPlace;
	private String appointmentTime;
	private String appointmentDate;
	private String salesPersonName;
	private String salesPersonContactNo;
	private String businessMail;
	private String callStatus;
	private String comments;
	private String businessArea;
	private String telecallerName;
	private String telecallerMobile;
	private String appointmentDateTime;
	private String appointmentCity;
	private String appointmentState;
	private String salesPersonArea;
	private String salesPersonCity;
	private String salesPersonState;
	private String salesPersonLatitude;
	private String salesPersonLongitude;
	private String appointmentType;
	private String usertype;
	private String businessCity;
	private String businessState;
	private String username;
	private String status;
	private String filepath;
	private String reportType;
	private String businessContact;
	private String callBackDate;
	private String businesssubcategory;
	private String businessCategory_old;
	private String businesssubcategory_old;
	private String businessOtherCategory_old;
	private String fromDate;
	private String toDate;
	private String userName;
	private String todaydate;
	private String savedtodaydate;
	private String name;
	private String date;
	private String keyword1;
	private String keyword2;
	private String keyword3;
	private String keyword4;
	private String keyword5;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getSavedtodaydate() {
		return savedtodaydate;
	}
	public void setSavedtodaydate(String savedtodaydate) {
		this.savedtodaydate = savedtodaydate;
	}
	public String getTodaydate() {
		return todaydate;
	}
	public void setTodaydate(String todaydate) {
		this.todaydate = todaydate;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getSalesPersonArea() {
		return salesPersonArea;
	}
	public void setSalesPersonArea(String salesPersonArea) {
		this.salesPersonArea = salesPersonArea;
	}
	public String getSalesPersonCity() {
		return salesPersonCity;
	}
	public void setSalesPersonCity(String salesPersonCity) {
		this.salesPersonCity = salesPersonCity;
	}
	public String getSalesPersonLatitude() {
		return salesPersonLatitude;
	}
	public void setSalesPersonLatitude(String salesPersonLatitude) {
		this.salesPersonLatitude = salesPersonLatitude;
	}
	public String getSalesPersonLongitude() {
		return salesPersonLongitude;
	}
	public void setSalesPersonLongitude(String salesPersonLongitude) {
		this.salesPersonLongitude = salesPersonLongitude;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getBusinessCategory() {
		return businessCategory;
	}
	public void setBusinessCategory(String businessCategory) {
		this.businessCategory = businessCategory;
	}
	public String getBusinessOtherCategory() {
		return businessOtherCategory;
	}
	public void setBusinessOtherCategory(String businessOtherCategory) {
		this.businessOtherCategory = businessOtherCategory;
	}
	public String getBusinessMobile() {
		return businessMobile;
	}
	public void setBusinessMobile(String businessMobile) {
		this.businessMobile = businessMobile;
	}
	public String getBusinessPhone() {
		return businessPhone;
	}
	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}
	public String getBusinessPhone1() {
		return businessPhone1;
	}
	public void setBusinessPhone1(String businessPhone1) {
		this.businessPhone1 = businessPhone1;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getAppointmentPlace() {
		return appointmentPlace;
	}
	public void setAppointmentPlace(String appointmentPlace) {
		this.appointmentPlace = appointmentPlace;
	}
	public String getAppointmentTime() {
		return appointmentTime;
	}
	public void setAppointmentTime(String appointmentTime) {
		this.appointmentTime = appointmentTime;
	}
	public String getSalesPersonName() {
		return salesPersonName;
	}
	public void setSalesPersonName(String salesPersonName) {
		this.salesPersonName = salesPersonName;
	}
	public String getSalesPersonContactNo() {
		return salesPersonContactNo;
	}
	public void setSalesPersonContactNo(String salesPersonContactNo) {
		this.salesPersonContactNo = salesPersonContactNo;
	}
	public String getBusinessMail() {
		return businessMail;
	}
	public void setBusinessMail(String businessMail) {
		this.businessMail = businessMail;
	}
	public String getAppointmentId() {
		return appointmentId;
	}
	public void setAppointmentId(String appointmentId) {
		this.appointmentId = appointmentId;
	}
	public String getCallStatus() {
		return callStatus;
	}
	public void setCallStatus(String callStatus) {
		this.callStatus = callStatus;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getBusinessArea() {
		return businessArea;
	}
	public void setBusinessArea(String businessArea) {
		this.businessArea = businessArea;
	}
	public String getAppointmentDate() {
		return appointmentDate;
	}
	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}
	public String getTelecallerName() {
		return telecallerName;
	}
	public void setTelecallerName(String telecallerName) {
		this.telecallerName = telecallerName;
	}
	public String getAppointmentDateTime() {
		return appointmentDateTime;
	}
	public void setAppointmentDateTime(String appointmentDateTime) {
		this.appointmentDateTime = appointmentDateTime;
	}
	public String getAppointmentCity() {
		return appointmentCity;
	}
	public void setAppointmentCity(String appointmentCity) {
		this.appointmentCity = appointmentCity;
	}
	public String getAppointmentType() {
		return appointmentType;
	}
	public void setAppointmentType(String appointmentType) {
		this.appointmentType = appointmentType;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String getAppointmentState() {
		return appointmentState;
	}
	public void setAppointmentState(String appointmentState) {
		this.appointmentState = appointmentState;
	}
	public String getBusinessCity() {
		return businessCity;
	}
	public void setBusinessCity(String businessCity) {
		this.businessCity = businessCity;
	}
	public String getBusinessState() {
		return businessState;
	}
	public void setBusinessState(String businessState) {
		this.businessState = businessState;
	}
	public String getSalesPersonState() {
		return salesPersonState;
	}
	public void setSalesPersonState(String salesPersonState) {
		this.salesPersonState = salesPersonState;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	public String getReportType() {
		return reportType;
	}
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	public String getBusinessContact() {
		return businessContact;
	}
	public void setBusinessContact(String businessContact) {
		this.businessContact = businessContact;
	}
	public String getCallBackDate() {
		return callBackDate;
	}
	public void setCallBackDate(String callBackDate) {
		this.callBackDate = callBackDate;
	}
	public String getBusinesssubcategory() {
		return businesssubcategory;
	}
	public void setBusinesssubcategory(String businesssubcategory) {
		this.businesssubcategory = businesssubcategory;
	}
	public String getTelecallerMobile() {
		return telecallerMobile;
	}
	public void setTelecallerMobile(String telecallerMobile) {
		this.telecallerMobile = telecallerMobile;
	}
	public String getBusinessCategory_old() {
		return businessCategory_old;
	}
	public void setBusinessCategory_old(String businessCategory_old) {
		this.businessCategory_old = businessCategory_old;
	}
	public String getBusinesssubcategory_old() {
		return businesssubcategory_old;
	}
	public void setBusinesssubcategory_old(String businesssubcategory_old) {
		this.businesssubcategory_old = businesssubcategory_old;
	}
	public String getBusinessOtherCategory_old() {
		return businessOtherCategory_old;
	}
	public void setBusinessOtherCategory_old(String businessOtherCategory_old) {
		this.businessOtherCategory_old = businessOtherCategory_old;
	}
	public String getKeyword1() {
		return keyword1;
	}
	public void setKeyword1(String keyword1) {
		this.keyword1 = keyword1;
	}
	public String getKeyword2() {
		return keyword2;
	}
	public void setKeyword2(String keyword2) {
		this.keyword2 = keyword2;
	}
	public String getKeyword3() {
		return keyword3;
	}
	public void setKeyword3(String keyword3) {
		this.keyword3 = keyword3;
	}
	public String getKeyword4() {
		return keyword4;
	}
	public void setKeyword4(String keyword4) {
		this.keyword4 = keyword4;
	}
	public String getKeyword5() {
		return keyword5;
	}
	public void setKeyword5(String keyword5) {
		this.keyword5 = keyword5;
	}
	
	

}
