package com.dialuz.salesTracking.DTO;

public class RenewalDataDTO {

	private String business_name;
	
	private String contact_person;
	private String comments;
	private String business_mobile ;
	private String callback_date;
	public String getBusiness_name() {
		return business_name;
	}
	public void setBusiness_name(String business_name) {
		this.business_name = business_name;
	}
	public String getContact_person() {
		return contact_person;
	}
	public void setContact_person(String contact_person) {
		this.contact_person = contact_person;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getBusiness_mobile() {
		return business_mobile;
	}
	public void setBusiness_mobile(String business_mobile) {
		this.business_mobile = business_mobile;
	}
	public String getCallback_date() {
		return callback_date;
	}
	public void setCallback_date(String callback_date) {
		this.callback_date = callback_date;
	}
	
	
	
	
}
