package com.dialuz.salesTracking.DAO;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import com.dialuz.salesTracking.DB.DBConfig;
import com.dialuz.salesTracking.DTO.AppointmentDTO;

public class SendEodDAO 
{

	
	
	private Connection conn;
	private PreparedStatement ps;
	private ResultSet rs = null;
	private PreparedStatement ps2;
	private ResultSet rs2= null;
	private File file;
	String Name="";
	
	public String sendDailyEodDetails(AppointmentDTO objCheckAppointmentDTO)
	{
		
			ArrayList<String> count=new ArrayList<String>();
			List<String> details = null;
			String query;
			int numberOfColumns = 0;
			ResultSetMetaData rsmd=null;
			Label lbl;
			Iterator<String> itr;
			try {

				conn =DBConfig.connect();
				if(objCheckAppointmentDTO.getAppointmentType().equals("daily"))
				{
					query="select business_name,appointment_time,appointment_place,comments," +
						"salesPersonName,salesPersonContactNo,appointment_date,status_flag" +
						" from appointment_details_IN where salesPersonContactNo=? and  appointment_date=CURDATE()";
				}else if(objCheckAppointmentDTO.getAppointmentType().equals("weekly"))
				{
					query="select business_name,appointment_time,appointment_place,comments," +
							"salesPersonName,salesPersonContactNo,appointment_date,status_flag" +
							" from appointment_details_IN where salesPersonContactNo=? and  appointment_date " +
							"BETWEEN SUBDATE(CURDATE(), INTERVAL 7 DAY) AND NOW()";
					
				}else
				{
					query="select business_name,appointment_time,appointment_place,comments," +
							"salesPersonName,salesPersonContactNo,appointment_date,status_flag" +
							" from appointment_details_IN where salesPersonContactNo=? and  appointment_date" +
							" BETWEEN SUBDATE(CURDATE(), INTERVAL 1 MONTH) AND NOW()";
				}
				
				
				//"BETWEEN SUBDATE(CURDATE(), INTERVAL 7 DAY) AND NOW()
				try
				{
				ps = conn.prepareStatement(query);
				ps.setString(1, objCheckAppointmentDTO.getTelecallerName());
				rs = ps.executeQuery();
				rsmd = rs.getMetaData();
				numberOfColumns = rsmd.getColumnCount();
				details = new ArrayList<String>();
				// Read and store data in list variable.
				while (rs.next()) {
					for (int n = 1; n <= numberOfColumns; n++) {
						details.add(rs.getString(n));
					}
				}
				}catch (Exception e) {
					// TODO: handle exception
				}
				try{
					ps2 = conn.prepareStatement(query);
					rs2 = ps2.executeQuery();
					while(rs2.next())
					{
						
						count.add(rs2.getString("business_name"));
						count.add(rs2.getString("appointment_time"));
						count.add(rs2.getString("comments"));
						count.add(rs2.getString("salesPersonName"));
						Name=rs2.getString("salesPersonName");
						count.add(rs2.getString("salesPersonContactNo"));
						count.add(rs2.getString("appointment_date"));
						if(rs2.getString("status_flag")=="2")
						count.add("Completed");
						else if(rs2.getString("status_flag")=="3")
							count.add("Pending");
						else
							count.add("Rejected");
						
					}
				}catch (Exception e) {
					// TODO: handle exception
				}
				// Excel file properties
				file=new File(objCheckAppointmentDTO.getFilepath());
				if(!file.exists()){
					file.createNewFile();
				}
				WorkbookSettings wbSettings = new WorkbookSettings();

				wbSettings.setLocale(new Locale("en", "EN"));
				WritableWorkbook w = Workbook.createWorkbook(file, wbSettings);
				if(objCheckAppointmentDTO.getAppointmentType().equals("daily"))
				{
					w.createSheet("End Of The Day Report  Of:"+Name, 0);
				}else if(objCheckAppointmentDTO.getAppointmentType().equals("weekly"))
				{
					w.createSheet("End Of The Week Report Of:"+Name, 0);
				}else
				{
					w.createSheet("End Of The Month Report Of:"+Name, 0);
				}
				WritableSheet s = w.getSheet(0);
				
				WritableFont wf = new WritableFont(WritableFont.ARIAL, 10,WritableFont.NO_BOLD);
				WritableFont wf1 = new WritableFont(WritableFont.ARIAL, 10,WritableFont.BOLD);
				
				WritableCellFormat cf = new WritableCellFormat(wf);
				WritableCellFormat cf1 = new WritableCellFormat(wf1);
				
				cf.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
				cf.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THICK, jxl.format.Colour.BLUE2);
				cf.setWrap(true);
				
				cf1.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
				cf1.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THICK, jxl.format.Colour.BLUE2);
				cf1.setWrap(true);
				
				itr = details.iterator();
				// Write column header
				CellView cv = s.getColumnView(0);
				cv.setAutosize(true);
				cv.setSize(2000);
				s.setColumnView(0, cv);
				s.setColumnView(1, cv);
				s.setColumnView(2, cv);
				s.setColumnView(3, cv);
				s.setColumnView(4, cv);
				s.setColumnView(5, cv);
				s.setColumnView(6, cv);
				s.setColumnView(7, cv);
				s.setColumnView(8, cv);
				
				int col = 0;
				for (int j = 1; j <= numberOfColumns; j++) 
				{
					lbl = new Label(col, 0, rsmd.getColumnName(j), cf1);
					
					s.addCell(lbl);
					col++;
				}
				// Write content
				int row = 1;
				while (itr.hasNext()) {
					for (int column = 0; column < numberOfColumns; column++) {
						lbl = new Label(column, row, (String) itr.next(), cf);
						s.addCell(lbl);
					}
					row++;
				}
				
				
				
				w.write();
				w.close();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				
				DBConfig.disconnect(null, ps, rs);
				DBConfig.disconnect(conn, ps2, rs2);
				
			}
			return objCheckAppointmentDTO.getFilepath();
			
			
			
		}

	
	
	public String sendDailyTelecallerEodDetails(AppointmentDTO objCheckAppointmentDTO)
	{
		
			ArrayList<String> count=new ArrayList<String>();
			List<String> details = null;
			int numberOfColumns = 0;
			ResultSetMetaData rsmd=null;
			Label lbl;
			String sql="";
			Iterator<String> itr;
			try {

				conn =DBConfig.connect();
				if(objCheckAppointmentDTO.getAppointmentType().equals("daily"))
				{
					sql="select businesscontact as BusinessName,call_time as  CallTime,comments as Comments,callstatus as CallStatus,call_date as CallDate from telecaller_callstatus_details" +
							" where call_date=CURDATE() and telecaller=?";
				}else if(objCheckAppointmentDTO.getAppointmentType().equals("weekly"))
				{
					sql="select businesscontact as BusinessName,call_time as  CallTime,comments as Comments,callstatus as CallStatus,call_date as CallDate  from telecaller_callstatus_details" +
							" where call_date BETWEEN SUBDATE(CURDATE(), INTERVAL 7 DAY) AND NOW() and telecaller=?";
				}else
				{
					sql="select businesscontact as BusinessName,call_time as  CallTime,comments as Comments,callstatus as CallStatus,call_date as CallDate from telecaller_callstatus_details" +
							" where call_date BETWEEN SUBDATE(CURDATE(), INTERVAL 1 MONTH) AND NOW() and telecaller=?";
				}
				
				//"BETWEEN SUBDATE(CURDATE(), INTERVAL 7 DAY) AND NOW()
				try
				{
				ps = conn.prepareStatement(sql);
				ps.setString(1, objCheckAppointmentDTO.getTelecallerName());
				rs = ps.executeQuery();
				rsmd = rs.getMetaData();
				numberOfColumns = rsmd.getColumnCount();
				details = new ArrayList<String>();
				// Read and store data in list variable.
				while (rs.next()) {
					for (int n = 1; n <= numberOfColumns; n++) {
						details.add(rs.getString(n));
					}
				}
				}catch (Exception e) {
					// TODO: handle exception
				}
				try{
					ps2 = conn.prepareStatement(sql);
					rs2 = ps2.executeQuery();
					while(rs2.next())
					{
						
						count.add(rs2.getString("BusinessName"));
						count.add(rs2.getString("CallTime"));
						count.add(rs2.getString("Comments"));
						Name=rs2.getString("CallDate");
						count.add(rs2.getString("salesPersonContactNo"));
						count.add(rs2.getString("appointment_date"));
						if(rs2.getString("CallStatus")=="0")
						count.add("Completed");
						else if(rs2.getString("CallStatus")=="1")
							count.add("Pending");
						else if(rs2.getString("CallStatus")=="2")
							count.add("Rejected");
						else if(rs2.getString("CallStatus")=="3")
							count.add("Not Reachable");
						else if(rs2.getString("CallStatus")=="4")
							count.add("Others");
						else
							count.add("Call Back");
						
					}
				}catch (Exception e) {
					// TODO: handle exception
				}
				// Excel file properties
				file=new File(objCheckAppointmentDTO.getFilepath());
				if(!file.exists()){
					file.createNewFile();
				}
				WorkbookSettings wbSettings = new WorkbookSettings();

				wbSettings.setLocale(new Locale("en", "EN"));
				WritableWorkbook w = Workbook.createWorkbook(file, wbSettings);
				if(objCheckAppointmentDTO.getAppointmentType().equals("daily"))
				{
					w.createSheet("End Of The Day Report  Of:"+Name, 0);
				}else if(objCheckAppointmentDTO.getAppointmentType().equals("weekly"))
				{
					w.createSheet("End Of The Week Report Of:"+Name, 0);
				}else
				{
					w.createSheet("End Of The Month Report Of:"+Name, 0);
				}
				WritableSheet s = w.getSheet(0);
				
				WritableFont wf = new WritableFont(WritableFont.ARIAL, 10,WritableFont.NO_BOLD);
				WritableFont wf1 = new WritableFont(WritableFont.ARIAL, 10,WritableFont.BOLD);
				
				WritableCellFormat cf = new WritableCellFormat(wf);
				WritableCellFormat cf1 = new WritableCellFormat(wf1);
				
				cf.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
				cf.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THICK, jxl.format.Colour.BLUE2);
				cf.setWrap(true);
				
				cf1.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
				cf1.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THICK, jxl.format.Colour.BLUE2);
				cf1.setWrap(true);
				
				itr = details.iterator();
				// Write column header
				CellView cv = s.getColumnView(0);
				cv.setAutosize(true);
				cv.setSize(2000);
				s.setColumnView(0, cv);
				s.setColumnView(1, cv);
				s.setColumnView(2, cv);
				s.setColumnView(3, cv);
				s.setColumnView(4, cv);
				s.setColumnView(5, cv);
				s.setColumnView(6, cv);
				s.setColumnView(7, cv);
				s.setColumnView(8, cv);
				
				int col = 0;
				for (int j = 1; j <= numberOfColumns; j++) 
				{
					lbl = new Label(col, 0, rsmd.getColumnName(j), cf1);
					
					s.addCell(lbl);
					col++;
				}
				// Write content
				int row = 1;
				while (itr.hasNext()) {
					for (int column = 0; column < numberOfColumns; column++) {
						lbl = new Label(column, row, (String) itr.next(), cf);
						s.addCell(lbl);
					}
					row++;
				}
				
				
				
				w.write();
				w.close();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				
				DBConfig.disconnect(null, ps, rs);
				DBConfig.disconnect(conn, ps2, rs2);
				
			}
			
			return objCheckAppointmentDTO.getFilepath();
			
			
			
		}
	
	
	
	

}
