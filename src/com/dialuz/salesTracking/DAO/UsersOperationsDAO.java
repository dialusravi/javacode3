package com.dialuz.salesTracking.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.dialuz.salesTracking.DB.DBConfig;
import com.dialuz.salesTracking.DTO.UsersOperationsDTO;

public class UsersOperationsDAO 
{

	public boolean createTelecallers(UsersOperationsDTO objUsersOperationsDTO)
	{
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		PreparedStatement objPreparedStatement1=null;
		ResultSet objResultSet=null;
		int insert_count=0;
		boolean insert_status=false;
		String sql;
				try{
					objConnection=DBConfig.connect();
					sql="select telecaller_name from telecaller_details where  telecaller_mobileno=? and status_flag=0 ";
					objPreparedStatement1=objConnection.prepareStatement(sql);
					objPreparedStatement1.setString(1,objUsersOperationsDTO.getUserMobile());
					objResultSet=objPreparedStatement1.executeQuery();
					if(objResultSet.next())
					{
						insert_status=false;
					}else
					{
						sql="insert into telecaller_details(telecaller_name,telecaller_mobileno,telecaller_mailid," +
								"telecaller_username,telecaller_password) values(?,?,?,?,?)";
						try
						{
							
							objPreparedStatement=objConnection.prepareStatement(sql);
							objPreparedStatement.setString(1,objUsersOperationsDTO.getUserName());
							objPreparedStatement.setString(2,objUsersOperationsDTO.getUserMobile());
							objPreparedStatement.setString(3,objUsersOperationsDTO.getUserMailId());
							objPreparedStatement.setString(4,objUsersOperationsDTO.getUserName());
							objPreparedStatement.setString(5,objUsersOperationsDTO.getUserPassword());
							
							insert_count=objPreparedStatement.executeUpdate();
							
						}catch(Exception e){}
						finally{DBConfig.disconnect(null, objPreparedStatement, null);}
						if(insert_count>0)
						{
								insert_status=true;
							
						}else
						{
							insert_status=false;
						}
					
						
					}
					
				}catch (Exception e) {
					// TODO: handle exception
				}finally{DBConfig.disconnect(objConnection, objPreparedStatement1, objResultSet);}
			
				
					
					
		
		return insert_status;
		
	}
	public boolean createSalesPersons(UsersOperationsDTO objUsersOperationsDTO)
	{
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		PreparedStatement objPreparedStatement1=null;
		ResultSet objResultSet=null;
		int insert_count=0;
		boolean insert_status=false;
		String sql;
				try{
					objConnection=DBConfig.connect();
					sql="select salesperson_name from sales_user_details_IN  where   mobile_no=? and status_flag=0";
					objPreparedStatement1=objConnection.prepareStatement(sql);
					objPreparedStatement1.setString(1,objUsersOperationsDTO.getUserMobile());
					objResultSet=objPreparedStatement1.executeQuery();
					if(objResultSet.next())
					{
						insert_status=false;
					}else
					{
							sql="insert into sales_user_details_IN(salesperson_name,mobile_no,mobile_no2," +
									"city,area,state,password,usertype,email_id) values(?,?,?,?,?,?,?,?,?)";
							try
							{
								objPreparedStatement=objConnection.prepareStatement(sql);
								objPreparedStatement.setString(1,objUsersOperationsDTO.getUserName());
								objPreparedStatement.setString(2,objUsersOperationsDTO.getUserMobile());
								objPreparedStatement.setString(3,objUsersOperationsDTO.getUserMobile1());
								objPreparedStatement.setString(4,objUsersOperationsDTO.getUserCity());
								objPreparedStatement.setString(5,objUsersOperationsDTO.getUserArea());
								objPreparedStatement.setString(6,objUsersOperationsDTO.getUserState());
								objPreparedStatement.setString(7,objUsersOperationsDTO.getUserPassword());
								objPreparedStatement.setString(8,"sales");
								objPreparedStatement.setString(9,objUsersOperationsDTO.getUserMailId());
								insert_count=objPreparedStatement.executeUpdate();
								
							}catch(Exception e){objConnection.rollback();}
							finally{DBConfig.disconnect(null, objPreparedStatement, null);}
							if(insert_count>0)
							{
								insert_status=true;
										
							}else
							{
								insert_status=false;
							}
					}
				
				}catch (Exception e) {
				}finally{DBConfig.disconnect(objConnection, null, null);}
			
		return insert_status;
}

	public boolean createTeamLeader(UsersOperationsDTO objUsersOperationsDTO)
	{
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		PreparedStatement objPreparedStatement1=null;
		ResultSet objResultSet=null;
		int insert_count=0;
		boolean insert_status=false;
		String sql;
				try{
					objConnection=DBConfig.connect();
					sql="select tl_name from teamlead_details where  tl_mobileno=? and status_flag=0 ";
					objPreparedStatement1=objConnection.prepareStatement(sql);
					objPreparedStatement1.setString(1,objUsersOperationsDTO.getUserMobile());
					objResultSet=objPreparedStatement1.executeQuery();
					if(objResultSet.next())
					{
						insert_status=false;
					}else
					{
						sql="insert into teamlead_details(tl_name,tl_mobileno,tl_mailid," +
								"tl_username,tl_password) values(?,?,?,?,?)";
						try
						{
							
							objPreparedStatement=objConnection.prepareStatement(sql);
							objPreparedStatement.setString(1,objUsersOperationsDTO.getUserName());
							objPreparedStatement.setString(2,objUsersOperationsDTO.getUserMobile());
							objPreparedStatement.setString(3,objUsersOperationsDTO.getUserMailId());
							objPreparedStatement.setString(4,objUsersOperationsDTO.getUserName());
							objPreparedStatement.setString(5,objUsersOperationsDTO.getUserPassword());
							
							insert_count=objPreparedStatement.executeUpdate();
							
						}catch(Exception e){}
						finally{DBConfig.disconnect(null, objPreparedStatement, null);}
						if(insert_count>0)
						{
								insert_status=true;
							
						}else
						{
							insert_status=false;
						}
					
						
					}
					
				}catch (Exception e) {
					// TODO: handle exception
				}finally{DBConfig.disconnect(objConnection, objPreparedStatement1, objResultSet);}
			
				
					
					
		
		return insert_status;
}
		
	
	
	
	
	
	public ArrayList<UsersOperationsDTO> getTelecallers() 
	{
		ArrayList<UsersOperationsDTO> objUsersOperationsDTOList =new ArrayList<UsersOperationsDTO>();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="select telecaller_id,telecaller_name,telecaller_mobileno,telecaller_mailid from" +
				" telecaller_details where status_flag=0";
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objResultSet=objPreparedStatement.executeQuery();
			while(objResultSet.next())
			{
				UsersOperationsDTO objUsersOperationsDTO=new UsersOperationsDTO();
				objUsersOperationsDTO.setUserId(objResultSet.getString("telecaller_id"));
				objUsersOperationsDTO.setUserName(objResultSet.getString("telecaller_name"));
				objUsersOperationsDTO.setUserMobile(objResultSet.getString("telecaller_mobileno"));
				objUsersOperationsDTO.setUserMailId(objResultSet.getString("telecaller_mailid"));
				
				objUsersOperationsDTOList.add(objUsersOperationsDTO);
				
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		// TODO Auto-generated method stub
		return objUsersOperationsDTOList;
	}
	public boolean deleteTelecallers(UsersOperationsDTO objUsersOperationsDTO) 
	{
		boolean update_status=false;
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		int update_count=0;
		String sql="update telecaller_details set status_flag=1 where  telecaller_id=?";
		try{
			objConnection=DBConfig.connect();
			
			
				update_count=0;
				objPreparedStatement=objConnection.prepareStatement(sql);
				objPreparedStatement.setString(1, objUsersOperationsDTO.getUserId());
				update_count=objPreparedStatement.executeUpdate();
				
				if(update_count>0)
				{
					
					update_status=true;
					
				}else
				{
					
					update_status=false;
				}
			
		}catch(Exception e){e.printStackTrace();}
		finally{DBConfig.disconnect(objConnection, objPreparedStatement, null);}
		
		
		return update_status;
	}
	public boolean deleteSalesPersons(UsersOperationsDTO objUsersOperationsDTO)
	{

		boolean update_status=false;
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		int update_count=0;
		String sql="update sales_user_details_IN  set status_flag=1 where id=? and  usertype='sales' ";
		
		try{
			objConnection=DBConfig.connect();
			objConnection.setAutoCommit(false);
			objPreparedStatement=objConnection.prepareStatement(sql);
			objPreparedStatement.setString(1, objUsersOperationsDTO.getUserId());
			update_count=objPreparedStatement.executeUpdate();
			if(update_count>0)
			{
				objConnection.commit();
				update_status=true;
			}else
			{
				objConnection.rollback();
				update_status=false;
			}
						
		}catch (Exception e) {e.printStackTrace();
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, null);}
			
		return update_status;
	}
	public ArrayList<UsersOperationsDTO> getTeamLeader() 
	{
		
		

		
		ArrayList<UsersOperationsDTO> objUsersOperationsDTOList =new ArrayList<UsersOperationsDTO>();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="select tl_id,tl_name,tl_mobileno,tl_mailid from" +
				" teamlead_details where status_flag=0";
		try{
			
			
			
			
			
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objResultSet=objPreparedStatement.executeQuery();
			while(objResultSet.next())
			{
				UsersOperationsDTO objUsersOperationsDTO=new UsersOperationsDTO();
				objUsersOperationsDTO.setUserId(objResultSet.getString("tl_id"));
				objUsersOperationsDTO.setUserName(objResultSet.getString("tl_name"));
				objUsersOperationsDTO.setUserMobile(objResultSet.getString("tl_mobileno"));
				objUsersOperationsDTO.setUserMailId(objResultSet.getString("tl_mailid"));
				
				objUsersOperationsDTOList.add(objUsersOperationsDTO);
				
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		// TODO Auto-generated method stub
		return objUsersOperationsDTOList;
	}
	public boolean deleteTeamLeader(UsersOperationsDTO objUsersOperationsDTO) 
	{
		boolean update_status=false;
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		int update_count=0;
		String sql="update teamlead_details set status_flag=1 where  tl_id=?";
		try{
			objConnection=DBConfig.connect();
						
				update_count=0;
				objPreparedStatement=objConnection.prepareStatement(sql);
				objPreparedStatement.setString(1, objUsersOperationsDTO.getUserId());
				update_count=objPreparedStatement.executeUpdate();
				
				if(update_count>0)
				{
					
					update_status=true;
					
				}else
				{
					
					update_status=false;
				}
			
		}catch(Exception e){e.printStackTrace();}
		finally{DBConfig.disconnect(objConnection, objPreparedStatement, null);}
		
		
		return update_status;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public ArrayList<UsersOperationsDTO> getSalesPersons()
	{
		ArrayList<UsersOperationsDTO> objUsersOperationsDTOList =new ArrayList<UsersOperationsDTO>();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="select id,salesperson_name,mobile_no,city,area,email_id,password from" +
				" sales_user_details_IN  where status_flag=0";
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objResultSet=objPreparedStatement.executeQuery();
			while(objResultSet.next())
			{
				UsersOperationsDTO objUsersOperationsDTO=new UsersOperationsDTO();
				objUsersOperationsDTO.setUserId(objResultSet.getString("id"));
				objUsersOperationsDTO.setUserName(objResultSet.getString("salesperson_name"));
				objUsersOperationsDTO.setUserMobile(objResultSet.getString("mobile_no"));
				objUsersOperationsDTO.setUserCity(objResultSet.getString("city"));
				objUsersOperationsDTO.setUserArea(objResultSet.getString("area"));
				objUsersOperationsDTO.setUserMailId(objResultSet.getString("email_id"));
				objUsersOperationsDTO.setUserPassword(objResultSet.getString("password"));
				objUsersOperationsDTOList.add(objUsersOperationsDTO);
				
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		// TODO Auto-generated method stub
		return objUsersOperationsDTOList;
	}
	public UsersOperationsDTO getEditTelecallerDetails(UsersOperationsDTO objUsersOperationsDTO)
	{
		UsersOperationsDTO objUsersOperationsDTO1=new UsersOperationsDTO();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="select * from telecaller_details  where telecaller_id=?";
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objPreparedStatement.setString(1, objUsersOperationsDTO.getUserId());
			objResultSet=objPreparedStatement.executeQuery();
			if(objResultSet.next())
			{
				objUsersOperationsDTO1.setUserId(objResultSet.getString("telecaller_id"));
				objUsersOperationsDTO1.setUserName(objResultSet.getString("telecaller_name"));
				objUsersOperationsDTO1.setUserMobile(objResultSet.getString("telecaller_mobileno"));
				objUsersOperationsDTO1.setUserMailId(objResultSet.getString("telecaller_mailid"));
				objUsersOperationsDTO1.setUserPassword(objResultSet.getString("telecaller_password"));
				
				
				
			}
			
			
			
			
		}catch (Exception e) {
			// TODO: handle exception
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		// TODO Auto-generated method stub
		return objUsersOperationsDTO1;
	}
	public UsersOperationsDTO getEditSalesPersonsDetails(UsersOperationsDTO objUsersOperationsDTO) 
	{
		UsersOperationsDTO objUsersOperationsDTO1=new UsersOperationsDTO();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="select * from sales_user_details_IN  where id=?";
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objPreparedStatement.setString(1, objUsersOperationsDTO.getUserId());
			objResultSet=objPreparedStatement.executeQuery();
			if(objResultSet.next())
			{
				objUsersOperationsDTO1.setUserId(objResultSet.getString("id"));
				objUsersOperationsDTO1.setUserName(objResultSet.getString("salesperson_name"));
				objUsersOperationsDTO1.setUserMailId(objResultSet.getString("email_id"));
				objUsersOperationsDTO1.setUserMobile(objResultSet.getString("mobile_no"));
				objUsersOperationsDTO1.setUserMobile1(objResultSet.getString("mobile_no2"));
				objUsersOperationsDTO1.setUserCity(objResultSet.getString("city"));
				objUsersOperationsDTO1.setUserArea(objResultSet.getString("area"));
				objUsersOperationsDTO1.setUserState(objResultSet.getString("state"));
				objUsersOperationsDTO1.setUserPassword(objResultSet.getString("password"));
				
				
				
			}
			
			
			
			
		}catch (Exception e) {
			// TODO: handle exception
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		// TODO Auto-generated method stub
		return objUsersOperationsDTO1;
	}
	public boolean updateEditTelecallerDetails(UsersOperationsDTO objUsersOperationsDTO)
	{
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		String sql="update telecaller_details set telecaller_name=?,telecaller_mobileno=?,telecaller_mailid=?,telecaller_password=? " +
				" where telecaller_id=?";
		boolean update_status=false;
		int update_count=0;
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objPreparedStatement.setString(1, objUsersOperationsDTO.getUserName());
			objPreparedStatement.setString(2, objUsersOperationsDTO.getUserMobile());
			objPreparedStatement.setString(3, objUsersOperationsDTO.getUserMailId());
			objPreparedStatement.setString(4, objUsersOperationsDTO.getUserPassword());
			objPreparedStatement.setString(5, objUsersOperationsDTO.getUserId());
			
			
			update_count=objPreparedStatement.executeUpdate();
			if(update_count>0)
			{
				update_status=true;
			}else
			{
				update_status=false;
			}
			
		}catch (Exception e) {e.printStackTrace();
			
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, null);}
		
		
		return update_status;
	}
	public UsersOperationsDTO getEditTeamLeaderDetails(UsersOperationsDTO objUsersOperationsDTO)
	{
		UsersOperationsDTO objUsersOperationsDTO1=new UsersOperationsDTO();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="select * from teamlead_details  where tl_id=?";
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objPreparedStatement.setString(1, objUsersOperationsDTO.getUserId());
			objResultSet=objPreparedStatement.executeQuery();
			if(objResultSet.next())
			{
				objUsersOperationsDTO1.setUserId(objResultSet.getString("tl_id"));
				objUsersOperationsDTO1.setUserName(objResultSet.getString("tl_name"));
				objUsersOperationsDTO1.setUserMobile(objResultSet.getString("tl_mobileno"));
				objUsersOperationsDTO1.setUserMailId(objResultSet.getString("tl_mailid"));
				objUsersOperationsDTO1.setUserPassword(objResultSet.getString("tl_password"));
				
				
				
			}
			
			
			
			
		}catch (Exception e) 
		{
			
		}
	finally{
		DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		
		return objUsersOperationsDTO1;
	      }
	
	
	
	
	
	public boolean updateEditSalesPersonDetails(UsersOperationsDTO objUsersOperationsDTO) 
	{
		
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		String sql="update sales_user_details_IN  set salesperson_name=?,email_id=?,mobile_no=?,mobile_no2=?," +
				"area=?,city=?,state=?,password=? where id=?";
		boolean update_status=false;
		int update_count=0;
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objPreparedStatement.setString(1, objUsersOperationsDTO.getUserName());
			objPreparedStatement.setString(2, objUsersOperationsDTO.getUserMailId());
			objPreparedStatement.setString(3, objUsersOperationsDTO.getUserMobile());
			objPreparedStatement.setString(4, objUsersOperationsDTO.getUserMobile1());
			objPreparedStatement.setString(5, objUsersOperationsDTO.getUserCity());
			objPreparedStatement.setString(6, objUsersOperationsDTO.getUserArea());
			objPreparedStatement.setString(7, objUsersOperationsDTO.getUserState());
			objPreparedStatement.setString(8, objUsersOperationsDTO.getUserPassword());
			objPreparedStatement.setString(9, objUsersOperationsDTO.getUserId());
			
			update_count=objPreparedStatement.executeUpdate();
			if(update_count>0)
			{
				update_status=true;
			}else
			{
				update_status=false;
			}
			
		}catch (Exception e) {e.printStackTrace();
			// TODO: handle exception
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, null);}
		
		
		return update_status;
	}
	public boolean updateEditTeamLeaderDetails(UsersOperationsDTO objUsersOperationsDTO)
	{
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		String sql="update teamlead_details set tl_name=?,tl_mobileno=?,tl_mailid=?,tl_password=? " +
				" where tl_id=?";
		boolean update_status=false;
		int update_count=0;
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objPreparedStatement.setString(1, objUsersOperationsDTO.getUserName());
			objPreparedStatement.setString(2, objUsersOperationsDTO.getUserMobile());
			objPreparedStatement.setString(3, objUsersOperationsDTO.getUserMailId());
			objPreparedStatement.setString(4, objUsersOperationsDTO.getUserPassword());
			objPreparedStatement.setString(5, objUsersOperationsDTO.getUserId());
			
			
			update_count=objPreparedStatement.executeUpdate();
			if(update_count>0)
			{
				update_status=true;
			}else
			{
				update_status=false;
			}
			
		}catch (Exception e) {e.printStackTrace();
			// TODO: handle exception
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, null);}
		
		
		return update_status;
	}
	
	
	public ArrayList<String> getAllUsers(UsersOperationsDTO objUsersOperationsDTO) 
	{
		ArrayList<String> usersList=new ArrayList<String>();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="";
		
		if(objUsersOperationsDTO.getUsertype().equals("sales"))
		{
			try
			{
				sql="select salesperson_name  from  sales_user_details_IN  where status_flag=0 and salesperson_name like ? limit 5";
				objConnection=DBConfig.connect();
				objPreparedStatement=objConnection.prepareStatement(sql);
				objPreparedStatement.setString(1, objUsersOperationsDTO.getName()+"%");
				objResultSet=objPreparedStatement.executeQuery();
				while(objResultSet.next())
				{
					if(objResultSet.getString("salesperson_name")!=null && !objResultSet.getString("salesperson_name").isEmpty())
					{
						usersList.add(objResultSet.getString("salesperson_name"));
					}
				}
			}catch (Exception e) {
			}finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
				
		}else
		{
			try{
				sql="select telecaller_name  from  telecaller_details  where status_flag=0 and telecaller_name like ? limit 5";
				objConnection=DBConfig.connect();
				objPreparedStatement=objConnection.prepareStatement(sql);
				objPreparedStatement.setString(1, objUsersOperationsDTO.getName()+"%");
				objResultSet=objPreparedStatement.executeQuery();
				while(objResultSet.next())
				{
					if(objResultSet.getString("telecaller_name")!=null && !objResultSet.getString("telecaller_name").isEmpty())
					{
						usersList.add(objResultSet.getString("telecaller_name"));
					}
				}
			}catch (Exception e) {
			}finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
			}
			
		
		return usersList;
	}

}
