package com.dialuz.salesTracking.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Random;




import com.dialuz.salesTracking.DB.DBConfig;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.dialuz.salesTracking.DTO.SendMailDTO;
import com.dialuz.salesTracking.commonUtilities.DateTimeUtilities;
import com.dialuz.salesTracking.commonUtilities.SendMail;
import com.dialuz.salesTracking.commonUtilities.SendSms;
import com.dialuz.webservice.client.SendMailServiceClient;

public class AppointmentDAO 
{
	
	
	public ArrayList<AppointmentDTO> getAppointments(AppointmentDTO objCheckAppointmentDTO)
	{
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="";
		ArrayList<AppointmentDTO> objAppointmentDTOsList=new ArrayList<AppointmentDTO>();
		try{
			objConnection=DBConfig.connect();
			if(!objCheckAppointmentDTO.getBusinessMobile().isEmpty() && objCheckAppointmentDTO.getBusinessMobile()!=null)
			{
				sql="select * from appointment_details_IN where business_mobile=?    order by appointment_id desc limit 10";
				objPreparedStatement=objConnection.prepareStatement(sql);
				objPreparedStatement.setString(1, objCheckAppointmentDTO.getBusinessMobile());
			}else if(!objCheckAppointmentDTO.getBusinessPhone().isEmpty() && objCheckAppointmentDTO.getBusinessPhone()!=null)
			{
				sql="select * from appointment_details_IN where business_phone=?   order by appointment_id desc  limit 10";
				objPreparedStatement=objConnection.prepareStatement(sql);
				objPreparedStatement.setString(1, objCheckAppointmentDTO.getBusinessPhone());
			}else if(!objCheckAppointmentDTO.getBusinessMail().isEmpty() && objCheckAppointmentDTO.getBusinessMail()!=null)
			{
				sql="select * from appointment_details_IN where business_email=?   order by appointment_id desc limit 10";
				objPreparedStatement=objConnection.prepareStatement(sql);
				objPreparedStatement.setString(1, objCheckAppointmentDTO.getBusinessMail());
			}
			
			objResultSet=objPreparedStatement.executeQuery();
			while(objResultSet.next())
			{
				AppointmentDTO objAppointmentDTO1=new AppointmentDTO();
				
				objAppointmentDTO1.setAppointmentId(objResultSet.getString("appointment_id"));
				objAppointmentDTO1.setBusinessName(objResultSet.getString("business_name"));
				objAppointmentDTO1.setBusinessArea(objResultSet.getString("business_place"));
				objAppointmentDTO1.setAppointmentTime(objResultSet.getString("appointment_time"));
				objAppointmentDTO1.setAppointmentPlace(objResultSet.getString("business_place"));
				objAppointmentDTO1.setCallBackDate(objResultSet.getString("callback_date"));
				objAppointmentDTO1.setAppointmentDate(objResultSet.getString("appointment_date"));
				objAppointmentDTO1.setContactPerson(objResultSet.getString("contact_person"));
				objAppointmentDTO1.setContactNumber(objResultSet.getString("contact_no"));
				objAppointmentDTO1.setCallStatus(objResultSet.getString("call_status"));
				objAppointmentDTO1.setComments(objResultSet.getString("comments"));
				objAppointmentDTO1.setBusinessMobile(objResultSet.getString("business_mobile"));
				objAppointmentDTO1.setBusinessPhone(objResultSet.getString("business_phone"));
				objAppointmentDTO1.setBusinessPhone1(objResultSet.getString("business_phone1"));
				objAppointmentDTO1.setBusinessMail(objResultSet.getString("business_email"));
				objAppointmentDTO1.setSalesPersonContactNo(objResultSet.getString("salesPersonContactNo"));
				objAppointmentDTO1.setSalesPersonName(objResultSet.getString("salesPersonName"));
				objAppointmentDTO1.setBusinessCategory(objResultSet.getString("business_category"));
				objAppointmentDTO1.setBusinessOtherCategory(objResultSet.getString("business_othercategory"));
				objAppointmentDTO1.setTelecallerName(objResultSet.getString("telecaller"));
				objAppointmentDTOsList.add(objAppointmentDTO1);
			}
			
			
			
			
		}catch(Exception e){e.printStackTrace();}
		finally{
			DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);
		}
		
		return objAppointmentDTOsList;
	}

	public boolean storeAppointmentDetails(AppointmentDTO objCheckAppointmentDTO) 
	{
		
	

		int insert_count=0;
		boolean insert_status=false;
		//boolean record_status=false;
		Connection objConnection=null;
		Connection objConnection1=null;
		PreparedStatement objPreparedStatement=null;
		PreparedStatement objPreparedStatement1=null;
		PreparedStatement objPreparedStatement2=null;
		PreparedStatement objPreparedStatement3=null;
		ResultSet objResultSet=null;
		objConnection=DBConfig.connect();
		
		String sql="";
		
	
			try{
				
				if((objCheckAppointmentDTO.getCallBackDate()==null ||  objCheckAppointmentDTO.getCallBackDate().isEmpty()) && (objCheckAppointmentDTO.getAppointmentDate()!=null && !objCheckAppointmentDTO.getAppointmentDate().isEmpty()))
				{
					
					
				
					
					
							sql="insert into appointment_details_IN(business_name, business_place, appointment_time, " +
								" contact_person, contact_no, call_status, comments, business_mobile, business_phone, " +
								"business_email,telecaller, business_phone1, appointment_date," +
								"business_city,business_state,callback_date,updated_date,keyword1,keyword2,keyword3,keyword4,keyword5) " +
								"values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," +
								"?,?,?,?,?,?)";
							
						objPreparedStatement=objConnection.prepareStatement(sql);
						objPreparedStatement.setString(1, objCheckAppointmentDTO.getBusinessName());
						objPreparedStatement.setString(2, objCheckAppointmentDTO.getBusinessArea());
						objPreparedStatement.setString(3, objCheckAppointmentDTO.getAppointmentTime());
						objPreparedStatement.setString(4, objCheckAppointmentDTO.getContactPerson());
						objPreparedStatement.setString(5, objCheckAppointmentDTO.getContactNumber());
						objPreparedStatement.setString(6, objCheckAppointmentDTO.getCallStatus());
						objPreparedStatement.setString(7, objCheckAppointmentDTO.getComments());
						objPreparedStatement.setString(8, objCheckAppointmentDTO.getBusinessMobile());
						objPreparedStatement.setString(9, objCheckAppointmentDTO.getBusinessPhone());
						objPreparedStatement.setString(10, objCheckAppointmentDTO.getBusinessMail());
						objPreparedStatement.setString(11, objCheckAppointmentDTO.getTelecallerName());
						objPreparedStatement.setString(12, objCheckAppointmentDTO.getBusinessPhone1());
						objPreparedStatement.setString(13, objCheckAppointmentDTO.getAppointmentDate());
						objPreparedStatement.setString(14, objCheckAppointmentDTO.getBusinessCity());
						objPreparedStatement.setString(15, objCheckAppointmentDTO.getBusinessState());
						objPreparedStatement.setString(16, "1900-01-01");
						objPreparedStatement.setString(17, DateTimeUtilities.getCurrentDate());
						
						objPreparedStatement.setString(18, objCheckAppointmentDTO.getKeyword1());
						objPreparedStatement.setString(19, objCheckAppointmentDTO.getKeyword2());
						objPreparedStatement.setString(20, objCheckAppointmentDTO.getKeyword3());
						objPreparedStatement.setString(21, objCheckAppointmentDTO.getKeyword4());
						objPreparedStatement.setString(22, objCheckAppointmentDTO.getKeyword5());
						
						insert_count=objPreparedStatement.executeUpdate();
						
						
				}else if((objCheckAppointmentDTO.getCallBackDate()!=null &&  !objCheckAppointmentDTO.getCallBackDate().isEmpty()) && (objCheckAppointmentDTO.getAppointmentDate()==null || objCheckAppointmentDTO.getAppointmentDate().isEmpty()))
				{
				
					
					
					sql="insert into appointment_details_IN( business_name, business_place, appointment_time, " +
							" contact_person, contact_no, call_status, comments, business_mobile, business_phone, " +
							"business_email,telecaller, business_phone1, appointment_date, " +
							"business_city,business_state,callback_date,updated_date,keyword1,keyword2,keyword3,keyword4,keyword5) " +
							"values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," +
							"?,?,?,?,?,?)";
					objPreparedStatement=objConnection.prepareStatement(sql);
					objPreparedStatement.setString(1, objCheckAppointmentDTO.getBusinessName());
					objPreparedStatement.setString(2, objCheckAppointmentDTO.getBusinessArea());
					objPreparedStatement.setString(3, "00:00:00");
					objPreparedStatement.setString(4, objCheckAppointmentDTO.getContactPerson());
					objPreparedStatement.setString(5, objCheckAppointmentDTO.getContactNumber());
					objPreparedStatement.setString(6, "5");
					objPreparedStatement.setString(7, objCheckAppointmentDTO.getComments());
					objPreparedStatement.setString(8, objCheckAppointmentDTO.getBusinessMobile());
					objPreparedStatement.setString(9, objCheckAppointmentDTO.getBusinessPhone());
					objPreparedStatement.setString(10, objCheckAppointmentDTO.getBusinessMail());
					objPreparedStatement.setString(11, objCheckAppointmentDTO.getTelecallerName());
					objPreparedStatement.setString(12, objCheckAppointmentDTO.getBusinessPhone1());
					objPreparedStatement.setString(13, "1900-01-01");
					objPreparedStatement.setString(14, objCheckAppointmentDTO.getBusinessCity());
					objPreparedStatement.setString(15, objCheckAppointmentDTO.getBusinessState());
					objPreparedStatement.setString(16, objCheckAppointmentDTO.getCallBackDate());
					objPreparedStatement.setString(17, DateTimeUtilities.getCurrentDate());
					
					objPreparedStatement.setString(18, objCheckAppointmentDTO.getKeyword1());
					objPreparedStatement.setString(19, objCheckAppointmentDTO.getKeyword2());
					objPreparedStatement.setString(20, objCheckAppointmentDTO.getKeyword3());
					objPreparedStatement.setString(21, objCheckAppointmentDTO.getKeyword4());
					objPreparedStatement.setString(22, objCheckAppointmentDTO.getKeyword5());
					
					
					insert_count=objPreparedStatement.executeUpdate();
					
				}
				else if((objCheckAppointmentDTO.getCallBackDate()==null ||  objCheckAppointmentDTO.getCallBackDate().isEmpty()) && (objCheckAppointmentDTO.getAppointmentDate()==null || objCheckAppointmentDTO.getAppointmentDate().isEmpty()))
				{
					
					
										
					
					sql="insert into appointment_details_IN( business_name, business_place, appointment_time, " +
							" contact_person, contact_no, call_status, comments, business_mobile, business_phone, " +
							"business_email,telecaller, business_phone1, appointment_date, " +
							"business_city,business_state,callback_date,updated_date,keyword1,keyword2,keyword3,keyword4,keyword5) " +
							"values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					try{
					objPreparedStatement=objConnection.prepareStatement(sql);
					objPreparedStatement.setString(1, objCheckAppointmentDTO.getBusinessName());
					objPreparedStatement.setString(2, objCheckAppointmentDTO.getBusinessArea());
					objPreparedStatement.setString(3, "00:00:00");
					objPreparedStatement.setString(4, objCheckAppointmentDTO.getContactPerson());
					objPreparedStatement.setString(5, objCheckAppointmentDTO.getContactNumber());
					objPreparedStatement.setString(6, "6");
					objPreparedStatement.setString(7, objCheckAppointmentDTO.getComments());
					objPreparedStatement.setString(8, objCheckAppointmentDTO.getBusinessMobile());
					objPreparedStatement.setString(9, objCheckAppointmentDTO.getBusinessPhone());
					objPreparedStatement.setString(10, objCheckAppointmentDTO.getBusinessMail());
					objPreparedStatement.setString(11, objCheckAppointmentDTO.getTelecallerName());
					objPreparedStatement.setString(12, objCheckAppointmentDTO.getBusinessPhone1());
					objPreparedStatement.setString(13, "1900-01-01");
					objPreparedStatement.setString(14, objCheckAppointmentDTO.getBusinessCity());
					objPreparedStatement.setString(15, objCheckAppointmentDTO.getBusinessState());
					objPreparedStatement.setString(16, "1900-01-01");
					objPreparedStatement.setString(17, DateTimeUtilities.getCurrentDate());
					
					objPreparedStatement.setString(18, objCheckAppointmentDTO.getKeyword1());
					objPreparedStatement.setString(19, objCheckAppointmentDTO.getKeyword2());
					objPreparedStatement.setString(20, objCheckAppointmentDTO.getKeyword3());
					objPreparedStatement.setString(21, objCheckAppointmentDTO.getKeyword4());
					objPreparedStatement.setString(22, objCheckAppointmentDTO.getKeyword5());
					
					
					insert_count=objPreparedStatement.executeUpdate();
					}catch (Exception e) {e.printStackTrace();
					}
				}
				
				
				if(insert_count>0)
				{
					
					sql="insert into" +
							" telecaller_callstatus_details(comments,businesscontact,telecaller,callstatus,call_date,callback_date) " +
							"values(?,?,?,?,?,?)";
					try{
							objPreparedStatement3=objConnection.prepareStatement(sql);
							objPreparedStatement3.setString(1, objCheckAppointmentDTO.getComments());
							try{
								
						
								
								if(objCheckAppointmentDTO.getBusinessMail()!=null && !objCheckAppointmentDTO.getBusinessMail().isEmpty())
								{
									objPreparedStatement3.setString(2, objCheckAppointmentDTO.getBusinessMail());
								}
							}catch (Exception e)
							{
								e.printStackTrace();
							}
							try{
								
								if(objCheckAppointmentDTO.getBusinessPhone()!=null && !objCheckAppointmentDTO.getBusinessPhone().isEmpty())
								{
									objPreparedStatement3.setString(2, objCheckAppointmentDTO.getBusinessPhone());
								}
							}catch (Exception e)
							{
								e.printStackTrace();
							}
							try{
								
								if(objCheckAppointmentDTO.getBusinessMobile()!=null && !objCheckAppointmentDTO.getBusinessMobile().isEmpty())
								{
									
									objPreparedStatement3.setString(2, objCheckAppointmentDTO.getBusinessMobile());
								}
							}catch (Exception e)
							{
								e.printStackTrace();
							}
							objPreparedStatement3.setString(3, objCheckAppointmentDTO.getTelecallerName());
							try{
								if((objCheckAppointmentDTO.getCallBackDate()==null ||  objCheckAppointmentDTO.getCallBackDate().isEmpty()) && (objCheckAppointmentDTO.getAppointmentDate()!=null && !objCheckAppointmentDTO.getAppointmentDate().isEmpty()))
								{
									objPreparedStatement3.setString(4, objCheckAppointmentDTO.getCallStatus());
									
								}
								if((objCheckAppointmentDTO.getCallBackDate()!=null &&  !objCheckAppointmentDTO.getCallBackDate().isEmpty()) && (objCheckAppointmentDTO.getAppointmentDate()==null || objCheckAppointmentDTO.getAppointmentDate().isEmpty()))
								{
									objPreparedStatement3.setString(4, "5");
								}
								 if((objCheckAppointmentDTO.getCallBackDate()==null ||  objCheckAppointmentDTO.getCallBackDate().isEmpty()) && (objCheckAppointmentDTO.getAppointmentDate()==null || objCheckAppointmentDTO.getAppointmentDate().isEmpty()))
								{
	
									objPreparedStatement3.setString(4, "6");
								}
							}catch (Exception e) {
								e.printStackTrace();
							}
							objPreparedStatement3.setString(5, DateTimeUtilities.getCurrentDate());
							try{
									if((objCheckAppointmentDTO.getCallBackDate()==null ||  objCheckAppointmentDTO.getCallBackDate().isEmpty()) && (objCheckAppointmentDTO.getAppointmentDate()!=null && !objCheckAppointmentDTO.getAppointmentDate().isEmpty()))
									{
										objPreparedStatement3.setString(6, objCheckAppointmentDTO.getAppointmentDate());
									}else if((objCheckAppointmentDTO.getCallBackDate()!=null &&  !objCheckAppointmentDTO.getCallBackDate().isEmpty()) && (objCheckAppointmentDTO.getAppointmentDate()==null || objCheckAppointmentDTO.getAppointmentDate().isEmpty()))
									{
										objPreparedStatement3.setString(6, objCheckAppointmentDTO.getCallBackDate());
									}else
									{
										objPreparedStatement3.setString(6, "1900-01-01");
									}
							}catch (Exception e) {
								e.printStackTrace();
							}
							insert_count=objPreparedStatement3.executeUpdate();

						
						
					}catch(Exception e)
					{
						e.printStackTrace();
					}finally{DBConfig.disconnect(null, objPreparedStatement3, null);}
					if((objCheckAppointmentDTO.getCallBackDate()==null ||  objCheckAppointmentDTO.getCallBackDate().isEmpty()) && (objCheckAppointmentDTO.getAppointmentDate()==null || objCheckAppointmentDTO.getAppointmentDate().isEmpty()))
					{
						
					}else
					{
						if((objCheckAppointmentDTO.getBusinessMobile()!=null&&!objCheckAppointmentDTO.getBusinessMobile().isEmpty()) && (objCheckAppointmentDTO.getContactPerson()!=null&&!objCheckAppointmentDTO.getContactPerson().isEmpty()))
						{
								Random randomGenerator = new Random();
								String username="";
								String password="";
								objConnection1=DBConfig.connectToMainDB();
								try{
									sql="select user_mob,user_pwd  from  user_detail_IN  where user_mob=?";
									objPreparedStatement1=objConnection1.prepareStatement(sql);
									objPreparedStatement1.setString(1, objCheckAppointmentDTO.getBusinessMobile());
									objResultSet=objPreparedStatement1.executeQuery();
									if(objResultSet.next())
									{
										username=objResultSet.getString("user_mob");
										password=objResultSet.getString("user_pwd");
										
									}else
									{
										sql="insert into user_detail_IN(user_name,user_mob,user_email_id,user_pwd,friends_list,login_status) values(?,?,?,?,?,?)";
										try{
											password=objCheckAppointmentDTO.getContactPerson().toLowerCase().replaceAll("\\s", "")+"@"+randomGenerator.nextInt(9999);
										}catch (Exception e) {
											password=objCheckAppointmentDTO.getContactPerson().toLowerCase()+"@"+randomGenerator.nextInt(9999);
										}
										username=objCheckAppointmentDTO.getBusinessMobile();
										objPreparedStatement2=objConnection1.prepareStatement(sql);
										objPreparedStatement2.setString(1, objCheckAppointmentDTO.getContactPerson());
										objPreparedStatement2.setString(2, objCheckAppointmentDTO.getBusinessMobile());
										objPreparedStatement2.setString(3, objCheckAppointmentDTO.getContactPerson()+objCheckAppointmentDTO.getBusinessMobile()+"@gmail.com");
										objPreparedStatement2.setString(4, password);
										objPreparedStatement2.setString(5, "null");
										objPreparedStatement2.setString(6, "telesales");
										objPreparedStatement2.executeUpdate();
										
									}
								}catch(Exception e)
								{
									e.printStackTrace();
								}
								finally{DBConfig.disconnect(null, objPreparedStatement1, null);
									DBConfig.disconnect(objConnection1, objPreparedStatement2, objResultSet);}
								SendSms objSendSms=new SendSms();
								String msgContent1="";
								if(objCheckAppointmentDTO.getCallBackDate()==null ||  objCheckAppointmentDTO.getCallBackDate().isEmpty())
								{
									msgContent1="Thank You  ,Dear "+objCheckAppointmentDTO.getContactPerson()+", For Giving Your Appointment to Dialuz " +
										"Your Appointment Details Shall be Sent to You Shortly For Any Details call 9666 888 888";
								}else
								{
									msgContent1="Thank You  ,Dear "+objCheckAppointmentDTO.getContactPerson()+", for your valuable time. This is  " +
											objCheckAppointmentDTO.getTelecallerName()+" from dialus.com.You can reach me on "+objCheckAppointmentDTO.getTelecallerMobile();
								}
								
								//Thank you UserName, for your valuable time. This is TelecallerName from dialus.com.You can reach me on MOBILE NUMBER.
								try{
									objSendSms.sendSms(msgContent1,objCheckAppointmentDTO.getContactNumber());
								}catch (Exception e){}
								msgContent1="Hi "+objCheckAppointmentDTO.getContactPerson()+",your login detials with Dialus.com are " +
										"  User: "+username+"& Pwd: "+password+"  Go to www.dialus.com, login and update your profile to get some free Gifts!!!";
								
								try{
									objSendSms.sendSms(msgContent1,objCheckAppointmentDTO.getContactNumber());
								}catch (Exception e){}
								
								try{
									
						if(!objCheckAppointmentDTO.getBusinessMail().trim().equals("") && objCheckAppointmentDTO.getBusinessMail()!=null){
								/*	charan*/
							
								//	SendMail.sendMail(objCheckAppointmentDTO.getBusinessMail(), msgContent1);
									
									 //webservice client mail code start
									 
								    SendMailDTO sendMailDtoObj=new SendMailDTO();
									
									sendMailDtoObj.setApplicationType("applicationType");
									sendMailDtoObj.setBody(msgContent1);
									sendMailDtoObj.setTo(objCheckAppointmentDTO.getBusinessMail());
									sendMailDtoObj.setFrom("donotreply@dialus.com");
									sendMailDtoObj.setCountryCode("IN");
									sendMailDtoObj.setPoinOfMail("TelesalesIndia");
									sendMailDtoObj.setSubject("TelesalesIndia");
									sendMailDtoObj.setIpAddress("127.0.0.1");
									sendMailDtoObj.setUserName("mailbox@dialus.com");
									sendMailDtoObj.setPassword("mailbox@123");
													
									SendMailServiceClient mailClientObj=new SendMailServiceClient();
									mailClientObj.sendMailToService(sendMailDtoObj);
								  
								
								 
								 
								 //webservice client mail code end
									
									
									
									
									
									
									}
								}catch (Exception e){}
						}
					}
				}
				
				
				else
				{
					
				}
				if(insert_count>0)
				{
					insert_status=true;
				}
				else
				{
					
					insert_status=false;
				}
			}
			catch(Exception e)
			{
		
			e.printStackTrace();
			}
			finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		/*}*/
		// TODO Auto-generated method stub
		return insert_status;
	}

	public ArrayList<String> getSalesPersons(String appointmentPlace,String appointmentCity) 
	{
		
		ArrayList<String> objSalesPersonsList=new ArrayList<String>();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		PreparedStatement objPreparedStatement1=null;
		ResultSet objResultSet=null;
		ResultSet objResultSet1=null;
		/*String sql="select salesperson_name from sales_user_details_IN where area=? and city=? and status_flag=0";*/
		String sql="select salesperson_name from sales_user_details_IN where status_flag=0";
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			/*objPreparedStatement.setString(1,appointmentPlace);
			objPreparedStatement.setString(2, appointmentCity);*/
			objResultSet=objPreparedStatement.executeQuery();
			while(objResultSet.next())
			{
				objSalesPersonsList.add(objResultSet.getString("salesperson_name"));
			}
		
		if(objSalesPersonsList.size()==0)
		{
			try{
			sql="select salesperson_name from sales_user_details_IN where  city=? and status_flag=0";
			objPreparedStatement1=objConnection.prepareStatement(sql);
			objPreparedStatement1.setString(1,appointmentCity);
			objResultSet1=objPreparedStatement1.executeQuery();
			while(objResultSet1.next())
			{
				objSalesPersonsList.add(objResultSet1.getString("salesperson_name"));
			}
			}catch (Exception e) {e.printStackTrace();
			}finally{DBConfig.disconnect(null, objPreparedStatement1, objResultSet1);}
			
		}
		}catch(Exception e){e.printStackTrace();}
		finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		// TODO Auto-generated method stub
		
		return objSalesPersonsList;
	}

	public ArrayList<AppointmentDTO> loadAppointments(AppointmentDTO objCheckAppointmentDTO) 
	{
		
		
		ArrayList<AppointmentDTO> objAppointmentDTOList=new ArrayList<AppointmentDTO>();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		
		String sql="";
		if(objCheckAppointmentDTO.getTodaydate()!="")
		{
			try{
				sql="select appointment_id,business_name,appointment_time,telecaller,appointment_date,business_place,business_city  from appointment_details_IN where status_flag=0 and call_status=0 and ! (appointment_date='1900-01-01')  and appointment_date=? order by appointment_id  desc";
				objConnection=DBConfig.connect();
				objPreparedStatement=objConnection.prepareStatement(sql);
				objPreparedStatement.setString(1, objCheckAppointmentDTO.getTodaydate());
				objResultSet=objPreparedStatement.executeQuery();
				while(objResultSet.next())
				{
					AppointmentDTO objCheckAppointmentDTO1=new AppointmentDTO();
					objCheckAppointmentDTO1.setAppointmentId(objResultSet.getString("appointment_id"));
					objCheckAppointmentDTO1.setBusinessName(objResultSet.getString("business_name"));
					objCheckAppointmentDTO1.setAppointmentDateTime(objResultSet.getString("appointment_date")+" "+objResultSet.getString("appointment_time"));
					objCheckAppointmentDTO1.setTelecallerName(objResultSet.getString("telecaller"));
					objCheckAppointmentDTO1.setBusinessArea(objResultSet.getString("business_place"));
					objCheckAppointmentDTO1.setBusinessCity(objResultSet.getString("business_city"));
					
					objAppointmentDTOList.add(objCheckAppointmentDTO1);
					
				}
			}catch(Exception e){e.printStackTrace();}
			finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		}else if(objCheckAppointmentDTO.getAppointmentType().equals("fixed"))
		{
			try
			{
					sql="select appointment_id,business_name,appointment_time,telecaller,appointment_date,business_place,business_city  from appointment_details_IN where status_flag=1 and  salesPersonContactNo=?  order by appointment_id  desc";
					objConnection=DBConfig.connect();
					objPreparedStatement=objConnection.prepareStatement(sql);
					objPreparedStatement.setString(1, objCheckAppointmentDTO.getUsername());
					objResultSet=objPreparedStatement.executeQuery();
					while(objResultSet.next())
					{
						AppointmentDTO objCheckAppointmentDTO1=new AppointmentDTO();
						objCheckAppointmentDTO1.setAppointmentId(objResultSet.getString("appointment_id"));
						objCheckAppointmentDTO1.setBusinessName(objResultSet.getString("business_name"));
						objCheckAppointmentDTO1.setAppointmentDateTime(objResultSet.getString("appointment_date")+" "+objResultSet.getString("appointment_time"));
						objCheckAppointmentDTO1.setTelecallerName(objResultSet.getString("telecaller"));
						objCheckAppointmentDTO1.setBusinessArea(objResultSet.getString("business_place"));
						objAppointmentDTOList.add(objCheckAppointmentDTO1);
						
					}
				}catch(Exception e){e.printStackTrace();}
				finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
			}
		else if(objCheckAppointmentDTO.getAppointmentType().equals("pending"))
		{
			try
			{
					sql="select appointment_id,business_name,appointment_time,telecaller,appointment_date,comments,business_place  from appointment_details_IN where status_flag=3 and  salesPersonContactNo=?  order by appointment_id  desc";
					objConnection=DBConfig.connect();
					objPreparedStatement=objConnection.prepareStatement(sql);
					objPreparedStatement.setString(1, objCheckAppointmentDTO.getUsername());
					objResultSet=objPreparedStatement.executeQuery();
					while(objResultSet.next())
					{
						AppointmentDTO objCheckAppointmentDTO1=new AppointmentDTO();
						objCheckAppointmentDTO1.setAppointmentId(objResultSet.getString("appointment_id"));
						objCheckAppointmentDTO1.setBusinessName(objResultSet.getString("business_name"));
						objCheckAppointmentDTO1.setAppointmentDateTime(objResultSet.getString("appointment_date")+" "+objResultSet.getString("appointment_time"));
						objCheckAppointmentDTO1.setTelecallerName(objResultSet.getString("telecaller"));
						objCheckAppointmentDTO1.setComments(objResultSet.getString("comments"));
						objCheckAppointmentDTO1.setBusinessArea(objResultSet.getString("business_place"));
						objAppointmentDTOList.add(objCheckAppointmentDTO1);
						
					}
				}catch(Exception e){e.printStackTrace();}
				finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
			}
		
			return objAppointmentDTOList;
	}

	public AppointmentDTO getAppointmentDetails(AppointmentDTO objAppointmentDTO) 
	{
				
		AppointmentDTO objCheckAppointmentDTO1=new AppointmentDTO();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="";
		try{
			objConnection=DBConfig.connect();
			
		if(objAppointmentDTO.getUsertype().equals("admin"))
		{
			sql="select *  from appointment_details_IN where  appointment_id=? ";
			objPreparedStatement=objConnection.prepareStatement(sql);
			objPreparedStatement.setString(1,objAppointmentDTO.getAppointmentId());
			objResultSet=objPreparedStatement.executeQuery();
		}else
		{
			sql="select *  from appointment_details_IN where  appointment_id=? ";
			objPreparedStatement=objConnection.prepareStatement(sql);
			objPreparedStatement.setString(1,objAppointmentDTO.getAppointmentId());
			objResultSet=objPreparedStatement.executeQuery();
		}
				
				if(objResultSet.next())
				{
					
					objCheckAppointmentDTO1.setAppointmentId(objResultSet.getString("appointment_id"));
					objCheckAppointmentDTO1.setBusinessName(objResultSet.getString("business_name"));
					objCheckAppointmentDTO1.setBusinessArea(objResultSet.getString("business_place"));
					objCheckAppointmentDTO1.setAppointmentTime(objResultSet.getString("appointment_time"));
					objCheckAppointmentDTO1.setAppointmentDate(objResultSet.getString("appointment_date"));
					objCheckAppointmentDTO1.setCallBackDate(objResultSet.getString("callback_date"));
					objCheckAppointmentDTO1.setContactPerson(objResultSet.getString("contact_person"));
					objCheckAppointmentDTO1.setContactNumber(objResultSet.getString("contact_no"));
					objCheckAppointmentDTO1.setCallStatus(objResultSet.getString("call_status"));
					objCheckAppointmentDTO1.setComments(objResultSet.getString("comments"));
					objCheckAppointmentDTO1.setBusinessMobile(objResultSet.getString("business_mobile"));
					objCheckAppointmentDTO1.setBusinessPhone(objResultSet.getString("business_phone"));
					objCheckAppointmentDTO1.setBusinessPhone1(objResultSet.getString("business_phone1"));
					objCheckAppointmentDTO1.setBusinessMail(objResultSet.getString("business_email"));
					objCheckAppointmentDTO1.setSalesPersonContactNo(objResultSet.getString("salesPersonContactNo"));
					objCheckAppointmentDTO1.setSalesPersonName(objResultSet.getString("salesPersonName"));
					objCheckAppointmentDTO1.setBusinessCategory(objResultSet.getString("business_category"));
					objCheckAppointmentDTO1.setBusinessOtherCategory(objResultSet.getString("business_othercategory"));
					objCheckAppointmentDTO1.setBusinessCity(objResultSet.getString("business_city"));
					objCheckAppointmentDTO1.setBusinessState(objResultSet.getString("business_state"));
					objCheckAppointmentDTO1.setBusinesssubcategory(objResultSet.getString("business_subcategory"));
					objCheckAppointmentDTO1.setTelecallerName(objResultSet.getString("telecaller"));
					objCheckAppointmentDTO1.setKeyword1(objResultSet.getString("keyword1"));
					objCheckAppointmentDTO1.setKeyword2(objResultSet.getString("keyword2"));
					objCheckAppointmentDTO1.setKeyword3(objResultSet.getString("keyword3"));
					objCheckAppointmentDTO1.setKeyword4(objResultSet.getString("keyword4"));
					objCheckAppointmentDTO1.setKeyword5(objResultSet.getString("keyword5"));
					
				}
			}catch(Exception e){e.printStackTrace();}
			finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		
			return objCheckAppointmentDTO1;
	}

	public boolean allotAppointments(AppointmentDTO objCheckAppointmentDTO) 
	{
		
		boolean update_status=false;
		int update_count=0;
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		
		String sql="update appointment_details_IN set salesPersonName=?,salesPersonContactNo=?,status_flag=? where appointment_id=?";
		try{
			
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objPreparedStatement.setString(1, objCheckAppointmentDTO.getSalesPersonName());
			objPreparedStatement.setString(2, objCheckAppointmentDTO.getSalesPersonContactNo());
			objPreparedStatement.setInt(3,1);
			objPreparedStatement.setString(4,objCheckAppointmentDTO.getAppointmentId());
			update_count=objPreparedStatement.executeUpdate();
			
			if(update_count>0)
			{
				String msgContent="A New Appointment is Fixed at "+objCheckAppointmentDTO.getAppointmentDate()+
							" On "+objCheckAppointmentDTO.getAppointmentTime()+"  with " +
							objCheckAppointmentDTO.getContactPerson()+" in "+objCheckAppointmentDTO.getBusinessArea()
							+"His/Him Contact Number "+objCheckAppointmentDTO.getContactNumber();
				
				String msgContent1="Your Appointment is Confirmed with "+objCheckAppointmentDTO.getSalesPersonName()+
						"On "+objCheckAppointmentDTO.getAppointmentDate()+" "+objCheckAppointmentDTO.getAppointmentTime()+"  for any  " +
					"Suggestions or Queries Please Contact Our Sales Manager 8099921450";
				
				SendSms objSendSms=new SendSms();
				
				try{
					
					if(!objCheckAppointmentDTO.getBusinessMail().trim().equals("") && objCheckAppointmentDTO.getBusinessMail()!=null){
					//SendMail.sendMail(objCheckAppointmentDTO.getBusinessMail(), msgContent1);
					
					
					 //webservice client mail code start
					 
				    SendMailDTO sendMailDtoObj=new SendMailDTO();
					
					sendMailDtoObj.setApplicationType("applicationType");
					sendMailDtoObj.setBody(msgContent1);
					sendMailDtoObj.setTo(objCheckAppointmentDTO.getBusinessMail());
					sendMailDtoObj.setFrom("donotreply@dialus.com");
					sendMailDtoObj.setCountryCode("IN");
					sendMailDtoObj.setPoinOfMail("TelesalesIndia");
					sendMailDtoObj.setSubject("TelesalesIndia");
					sendMailDtoObj.setIpAddress("127.0.0.1");
					sendMailDtoObj.setUserName("mailbox@dialus.com");
					sendMailDtoObj.setPassword("mailbox@123");
									
					SendMailServiceClient mailClientObj=new SendMailServiceClient();
					mailClientObj.sendMailToService(sendMailDtoObj);
				  
				
				 
				 
				 //webservice client mail code end
					
					
					
					
					}
				}catch (Exception e){}
				try{
					objSendSms.sendSms(msgContent1, objCheckAppointmentDTO.getBusinessMobile());
					objSendSms.sendSms(msgContent, objCheckAppointmentDTO.getSalesPersonContactNo());
				}catch (Exception e){}
				update_status=true;
			}else
			{
				update_status=false;
			}
	
			
		}catch(Exception e){e.printStackTrace();}
		finally{DBConfig.disconnect(objConnection, objPreparedStatement, null);}
		return update_status;
	}

	public AppointmentDTO getContactDetails(String salesPersonName) 
	{
		AppointmentDTO objCheckAppointmentDTO=new AppointmentDTO();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="select mobile_no,area,city,user_latitude,user_longitude,state   from sales_user_details_IN  where  salesperson_name=? ";
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objPreparedStatement.setString(1, salesPersonName);
			
			objResultSet=objPreparedStatement.executeQuery();
			if(objResultSet.next())
			{
				//id, salesperson_name, email_id, mobile_no, mobile_no2, address, designation,
				//user_latitude, user_longitude, user_image, area, city, state, password, usertype
				objCheckAppointmentDTO.setSalesPersonContactNo(objResultSet.getString("mobile_no"));
				objCheckAppointmentDTO.setSalesPersonArea(objResultSet.getString("area"));
				objCheckAppointmentDTO.setSalesPersonCity(objResultSet.getString("city"));
				objCheckAppointmentDTO.setSalesPersonLatitude(objResultSet.getString("user_latitude"));
				objCheckAppointmentDTO.setSalesPersonLongitude(objResultSet.getString("user_longitude"));
				objCheckAppointmentDTO.setSalesPersonState(objResultSet.getString("state"));
			}
		}catch(Exception e){}
		finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		
		
		// TODO Auto-generated method stub
		return objCheckAppointmentDTO;
	}

	public boolean updateAppointmentStatus(AppointmentDTO objCheckAppointmentDTO)
	{
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		String sql="update appointment_details_IN set comments=?,status_flag=? where appointment_id=?";
		int update_count=0;
		boolean update_status=false;
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			if(objCheckAppointmentDTO.getAppointmentType().equals("pending"))
			{
				objPreparedStatement.setString(1, objCheckAppointmentDTO.getComments());
				objPreparedStatement.setInt(2, 3);
				objPreparedStatement.setString(3, objCheckAppointmentDTO.getAppointmentId());
			}else if(objCheckAppointmentDTO.getAppointmentType().equals("complete"))
			{
				objPreparedStatement.setString(1, objCheckAppointmentDTO.getComments());
				objPreparedStatement.setInt(2, 2);
				objPreparedStatement.setString(3, objCheckAppointmentDTO.getAppointmentId());
			}else
			{
				objPreparedStatement.setString(1, objCheckAppointmentDTO.getComments());
				objPreparedStatement.setInt(2, 4);
				objPreparedStatement.setString(3, objCheckAppointmentDTO.getAppointmentId());
			}
			update_count=objPreparedStatement.executeUpdate();
			if(update_count>0)
			{
				update_status=true;
				
			}else
			{
				update_status=false;
			}
			
		}catch (Exception e) {e.printStackTrace();
			
			
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, null);}
		// TODO Auto-generated method stub
		return update_status;
		
	}

	public boolean insertCallStatusDetails(AppointmentDTO objCheckAppointmentDTO) 

	{
		
	
		
		
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		String sql="insert into telecaller_callstatus_details(comments,businesscontact,telecaller,callstatus,call_date,callback_date) values(?,?,?,?,?,?)";
		int insert_count=0;
		boolean insert_status=false;
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objPreparedStatement.setString(1, objCheckAppointmentDTO.getComments());
			try{
				if(objCheckAppointmentDTO.getBusinessMail()!=null && !objCheckAppointmentDTO.getBusinessMail().isEmpty())
				{
					objPreparedStatement.setString(2, objCheckAppointmentDTO.getBusinessMail());
				}
			}catch (Exception e)
			{
				
			}
			try{
				if(objCheckAppointmentDTO.getBusinessPhone()!=null && !objCheckAppointmentDTO.getBusinessPhone().isEmpty())
				{
					objPreparedStatement.setString(2, objCheckAppointmentDTO.getBusinessPhone());
				}
			}catch (Exception e)
			{
				
			}
			try{
				if(objCheckAppointmentDTO.getBusinessMobile()!=null && !objCheckAppointmentDTO.getBusinessMobile().isEmpty())
				{
					objPreparedStatement.setString(2, objCheckAppointmentDTO.getBusinessMobile());
				}
			}catch (Exception e)
			{
				
			}
			objPreparedStatement.setString(3, objCheckAppointmentDTO.getTelecallerName());
			objPreparedStatement.setString(4, objCheckAppointmentDTO.getCallStatus());
			objPreparedStatement.setString(5, DateTimeUtilities.getCurrentDate());
			if(objCheckAppointmentDTO.getAppointmentDate().isEmpty()||objCheckAppointmentDTO.getAppointmentDate()==null)
			{
				objPreparedStatement.setString(6,"1900-01-01");
			}else
			{
				objPreparedStatement.setString(6, objCheckAppointmentDTO.getAppointmentDate());
			}
			insert_count=objPreparedStatement.executeUpdate();
			if(insert_count>0)
			{
				insert_status=true;
			}else
			{
				insert_status=false;
			}
		}catch (Exception e) {e.printStackTrace();
			// TODO: handle exception
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, null);}
		return insert_status;
	}

	public ArrayList<AppointmentDTO> getAllRejectedAppointments(AppointmentDTO objAppointmentDTO1) 
	{
		ArrayList<AppointmentDTO> objAppointmentDTOList=new ArrayList<AppointmentDTO>();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="";
		if(objAppointmentDTO1.getAppointmentType().equals("reject"))
		{
			sql="select business_name,appointment_date,salesPersonName," +
				"salesPersonContactNo,appointment_time,comments,business_place,telecaller from appointment_details_IN where status_flag=4 order by" +
				" appointment_id desc";
		}else
		{

			sql="select business_name,appointment_date,salesPersonName," +
				"salesPersonContactNo,appointment_time,comments,business_place,telecaller from appointment_details_IN where status_flag=3 order by" +
				" appointment_id desc";
		}
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objResultSet=objPreparedStatement.executeQuery();
			while(objResultSet.next())
			{
				AppointmentDTO objAppointmentDTO=new AppointmentDTO();
				objAppointmentDTO.setBusinessName(objResultSet.getString("business_name"));
				objAppointmentDTO.setAppointmentDateTime(objResultSet.getString("appointment_date")+" "+objResultSet.getString("appointment_time"));
				objAppointmentDTO.setSalesPersonName(objResultSet.getString("salesPersonName"));
				objAppointmentDTO.setSalesPersonContactNo(objResultSet.getString("salesPersonContactNo"));
				objAppointmentDTO.setComments(objResultSet.getString("comments"));
				objAppointmentDTO.setBusinessArea(objResultSet.getString("business_place"));
				objAppointmentDTO.setTelecallerName(objResultSet.getString("telecaller"));
				
				objAppointmentDTOList.add(objAppointmentDTO);
			}
		}catch (Exception e) {
		}
		finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		return objAppointmentDTOList;
	}

	public ArrayList<AppointmentDTO> getDailyEodDetails(AppointmentDTO objCheckAppointmentDTO) 
	{
		ArrayList<AppointmentDTO> objAppointmentDTOList=new ArrayList<AppointmentDTO>();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="";
		if(objCheckAppointmentDTO.getAppointmentType().equals("daily"))
		{
			sql="select business_name,appointment_time,business_place,comments," +
				"salesPersonName,salesPersonContactNo,appointment_date,status_flag" +
				" from appointment_details_IN where salesPersonContactNo=? and  appointment_date=CURDATE() and (status_flag=2 or status_flag=3 or status_flag=4)";
		}else if(objCheckAppointmentDTO.getAppointmentType().equals("weekly"))
		{
			sql="select business_name,appointment_time,business_place,comments," +
					"salesPersonName,salesPersonContactNo,appointment_date,status_flag" +
					" from appointment_details_IN where salesPersonContactNo=? and  appointment_date " +
					"BETWEEN SUBDATE(CURDATE(), INTERVAL 7 DAY) AND NOW() and (status_flag=2 or status_flag=3 or status_flag=4)";
			
		}else
		{
			sql="select business_name,appointment_time,business_place,comments," +
					"salesPersonName,salesPersonContactNo,appointment_date,status_flag" +
					" from appointment_details_IN where salesPersonContactNo=? and  appointment_date" +
					" BETWEEN SUBDATE(CURDATE(), INTERVAL 1 MONTH) AND NOW() and (status_flag=2 or status_flag=3 or status_flag=4)";
		}
		
		
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objPreparedStatement.setString(1, objCheckAppointmentDTO.getTelecallerName());
			objResultSet=objPreparedStatement.executeQuery();
			while(objResultSet.next())
			{
				AppointmentDTO objAppointmentDTO=new AppointmentDTO();
				objAppointmentDTO.setBusinessName(objResultSet.getString("business_name"));
				objAppointmentDTO.setAppointmentDateTime(objResultSet.getString("appointment_date")+" "+objResultSet.getString("appointment_time"));
				objAppointmentDTO.setAppointmentPlace(objResultSet.getString("business_place"));
				objAppointmentDTO.setComments(objResultSet.getString("comments"));
				objAppointmentDTO.setSalesPersonName(objResultSet.getString("salesPersonName"));
				objAppointmentDTO.setSalesPersonContactNo(objResultSet.getString("salesPersonContactNo"));
				objAppointmentDTO.setStatus(objResultSet.getString("status_flag"));
				objAppointmentDTOList.add(objAppointmentDTO);
				
			}
			
			
		}catch(Exception e){e.printStackTrace();}
		finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		// TODO Auto-generated method stub
		return objAppointmentDTOList;
	}

	public ArrayList<String> getCategoryDetails()
	{
		
			
			String sqlqueary="SELECT cat FROM cat_subcat_othercat_list_IN";
			Connection objConnection=null;
			PreparedStatement objPreparedStatement=null;
			ResultSet objResultSet=null;
			ArrayList<String> categoryList=new ArrayList<String>();
			try
			{
				objConnection=DBConfig.connectToMainDB();
				
				objPreparedStatement = objConnection.prepareStatement(sqlqueary);
				objResultSet= objPreparedStatement.executeQuery();
				
				while (objResultSet.next())
				{			    
					categoryList.add(objResultSet.getString(1));
					
				}
				
			}
			catch(Exception e)
			{
				
				e.printStackTrace();
			}
			finally
			{
				try
				{  
					DBConfig.disconnect(objConnection,objPreparedStatement,objResultSet);
				}
				catch(Exception e)
				{
					e.printStackTrace();
					
				}
			}
			return categoryList;
	
	}

	public ArrayList<AppointmentDTO> getReports(AppointmentDTO objAppointmentDTO) 
	{
		ArrayList<AppointmentDTO> obAppointmentDTOsList=new ArrayList<AppointmentDTO>();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql;
		try{
			objConnection=DBConfig.connect();
			if(objAppointmentDTO.getReportType().equals("daily"))
			{
				sql="select comments,businesscontact,callstatus,call_time,call_date from telecaller_callstatus_details" +
						" where call_date=CURDATE() and telecaller=?";
				objPreparedStatement=objConnection.prepareStatement(sql);
				objPreparedStatement.setString(1, objAppointmentDTO.getTelecallerName());
				objResultSet=objPreparedStatement.executeQuery();
				while(objResultSet.next())
				{
					AppointmentDTO objAppointmentDTO1=new AppointmentDTO();
					objAppointmentDTO1.setComments(objResultSet.getString("comments"));
					if(objResultSet.getString("callstatus").equals("0"))
					{
						objAppointmentDTO1.setCallStatus("Answered");
						
					}else if(objResultSet.getString("callstatus").equals("1"))
					{
						objAppointmentDTO1.setCallStatus("Rejected");
					}else if(objResultSet.getString("callstatus").equals("2"))
					{
						objAppointmentDTO1.setCallStatus("Not Reachable");
					}
					else if(objResultSet.getString("callstatus").equals("3"))
					{
						objAppointmentDTO1.setCallStatus("Others");
					}else if(objResultSet.getString("callstatus").equals("5"))
					{
						objAppointmentDTO1.setCallStatus("Call Back");
					}
					else if(objResultSet.getString("callstatus").equals("6"))
					{
						objAppointmentDTO1.setCallStatus("Saved Data");
					}
					objAppointmentDTO1.setAppointmentDateTime(objResultSet.getString("call_time"));
					objAppointmentDTO1.setBusinessContact(objResultSet.getString("businesscontact"));
					obAppointmentDTOsList.add(objAppointmentDTO1);
					
				}
			}else if(objAppointmentDTO.getReportType().equals("weekly"))
			{
				sql="select comments,businesscontact,callstatus,call_time,call_date from telecaller_callstatus_details" +
						" where call_date BETWEEN SUBDATE(CURDATE(), INTERVAL 7 DAY) AND NOW() and telecaller=?";
				objPreparedStatement=objConnection.prepareStatement(sql);
				objPreparedStatement.setString(1, objAppointmentDTO.getTelecallerName());
				objResultSet=objPreparedStatement.executeQuery();
				while(objResultSet.next())
				{
					AppointmentDTO objAppointmentDTO1=new AppointmentDTO();
					objAppointmentDTO1.setComments(objResultSet.getString("comments"));
					if(objResultSet.getString("callstatus").equals("0"))
					{
						objAppointmentDTO1.setCallStatus("Answered");
						
					}else if(objResultSet.getString("callstatus").equals("1"))
					{
						objAppointmentDTO1.setCallStatus("Rejected");
					}else if(objResultSet.getString("callstatus").equals("2"))
					{
						objAppointmentDTO1.setCallStatus("Not Reachable");
					}
					else if(objResultSet.getString("callstatus").equals("3"))
					{
						objAppointmentDTO1.setCallStatus("Others");
					}else if(objResultSet.getString("callstatus").equals("5"))
					{
						objAppointmentDTO1.setCallStatus("Call Back");
					}else if(objResultSet.getString("callstatus").equals("6"))
					{
						objAppointmentDTO1.setCallStatus("Saved Data");
					}
					objAppointmentDTO1.setAppointmentDateTime(objResultSet.getString("call_time"));
					objAppointmentDTO1.setBusinessContact(objResultSet.getString("businesscontact"));
					obAppointmentDTOsList.add(objAppointmentDTO1);
					
				}
			}else if(objAppointmentDTO.getReportType().equals("monthly"))
			{
				sql="select comments,businesscontact,callstatus,call_time,call_date from telecaller_callstatus_details" +
						" where call_date BETWEEN SUBDATE(CURDATE(), INTERVAL 1 MONTH) AND NOW() and telecaller=?";
				objPreparedStatement=objConnection.prepareStatement(sql);
				objPreparedStatement.setString(1, objAppointmentDTO.getTelecallerName());
				objResultSet=objPreparedStatement.executeQuery();
				while(objResultSet.next())
				{
					AppointmentDTO objAppointmentDTO1=new AppointmentDTO();
					objAppointmentDTO1.setComments(objResultSet.getString("comments"));
					if(objResultSet.getString("callstatus").equals("0"))
					{
						objAppointmentDTO1.setCallStatus("Answered");
						
					}else if(objResultSet.getString("callstatus").equals("1"))
					{
						objAppointmentDTO1.setCallStatus("Rejected");
					}else if(objResultSet.getString("callstatus").equals("2"))
					{
						objAppointmentDTO1.setCallStatus("Not Reachable");
					}
					else if(objResultSet.getString("callstatus").equals("3"))
					{
						objAppointmentDTO1.setCallStatus("Others");
					}else if(objResultSet.getString("callstatus").equals("5"))
					{
						objAppointmentDTO1.setCallStatus("Call Back");
					}else if(objResultSet.getString("callstatus").equals("6"))
					{
						objAppointmentDTO1.setCallStatus("Saved Data");
					}
					objAppointmentDTO1.setAppointmentDateTime(objResultSet.getString("call_time"));
					objAppointmentDTO1.setBusinessContact(objResultSet.getString("businesscontact"));
					obAppointmentDTOsList.add(objAppointmentDTO1);
					
				}
			}else
			{
			
				sql="select comments,businesscontact,callback_date,call_time,call_date from telecaller_callstatus_details" +
						" where  telecaller=? and callstatus=5 and ! (callback_date='1900-01-01') and callback_date=? "  ;
				objPreparedStatement=objConnection.prepareStatement(sql);
				
				objPreparedStatement.setString(1, objAppointmentDTO.getTelecallerName());
				objPreparedStatement.setString(2, objAppointmentDTO.getAppointmentType());
				objResultSet=objPreparedStatement.executeQuery();
				while(objResultSet.next())
				{
					AppointmentDTO objAppointmentDTO1=new AppointmentDTO();
					objAppointmentDTO1.setComments(objResultSet.getString("comments"));
					objAppointmentDTO1.setCallBackDate(objResultSet.getString("callback_date"));
					objAppointmentDTO1.setAppointmentDateTime(objResultSet.getString("call_time"));
					objAppointmentDTO1.setBusinessContact(objResultSet.getString("businesscontact"));
					obAppointmentDTOsList.add(objAppointmentDTO1);
					
				}
			}
			
			
			
			
			
			
		}catch(Exception e)
		{
			
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		
		return obAppointmentDTOsList;
	}
	
	public ArrayList<AppointmentDTO> getTelecallerReports(AppointmentDTO objAppointmentDTO) 
	{
		ArrayList<AppointmentDTO> obAppointmentDTOsList=new ArrayList<AppointmentDTO>();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql;
		try{
			if(objAppointmentDTO.getReportType().equals("daily"))
			{
				sql="select comments,businesscontact,callstatus,call_time,call_date,telecaller  from telecaller_callstatus_details" +
						" where call_date=CURDATE() and telecaller=?";
			}else if(objAppointmentDTO.getReportType().equals("weekly"))
			{
				sql="select comments,businesscontact,callstatus,call_time,call_date,telecaller  from telecaller_callstatus_details" +
						" where call_date BETWEEN SUBDATE(CURDATE(), INTERVAL 7 DAY) AND NOW() and telecaller=?";
			}else
			{
				sql="select comments,businesscontact,callstatus,call_time,call_date,telecaller  from telecaller_callstatus_details" +
						" where call_date BETWEEN SUBDATE(CURDATE(), INTERVAL 1 MONTH) AND NOW() and telecaller=?";
			}
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objPreparedStatement.setString(1, objAppointmentDTO.getTelecallerName());
			objResultSet=objPreparedStatement.executeQuery();
			while(objResultSet.next())
			{
				AppointmentDTO objAppointmentDTO1=new AppointmentDTO();
				objAppointmentDTO1.setComments(objResultSet.getString("comments"));
				if(objResultSet.getString("callstatus").equals("0"))
				{
					objAppointmentDTO1.setCallStatus("Answered");
					
				}else if(objResultSet.getString("callstatus").equals("1"))
				{
					objAppointmentDTO1.setCallStatus("Rejected");
				}else if(objResultSet.getString("callstatus").equals("2"))
				{
					objAppointmentDTO1.setCallStatus("Not Reachable");
				}
				else if(objResultSet.getString("callstatus").equals("3"))
				{
					objAppointmentDTO1.setCallStatus("Others");
				}else if(objResultSet.getString("callstatus").equals("5"))
				{
					objAppointmentDTO1.setCallStatus("Call Back");
				}else if(objResultSet.getString("callstatus").equals("6"))
				{
					objAppointmentDTO1.setCallStatus("Saved Data");
				}
				objAppointmentDTO1.setAppointmentDateTime(objResultSet.getString("call_time"));
				objAppointmentDTO1.setBusinessContact(objResultSet.getString("businesscontact"));
				objAppointmentDTO1.setTelecallerName(objResultSet.getString("telecaller"));
				obAppointmentDTOsList.add(objAppointmentDTO1);
				
			}
			
			
			
			
			
		}catch(Exception e)
		{
			
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		
		return obAppointmentDTOsList;
	}
	
	
	public ArrayList<AppointmentDTO>  getSalesPersonsReports(AppointmentDTO objAppointmentDTO) 
	{
		ArrayList<AppointmentDTO> obAppointmentDTOsList=new ArrayList<AppointmentDTO>();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql;
		try{
			if(objAppointmentDTO.getReportType().equals("daily"))
			{
				sql="select business_name,business_place,appointment_time,comments,appointment_date,business_city,status_flag  from appointment_details_IN" +
						" where appointment_date=CURDATE() and salesPersonContactNo=?";
			}else if(objAppointmentDTO.getReportType().equals("weekly"))
			{
				sql="select business_name,business_place,appointment_time,comments,appointment_date,business_city,status_flag  from appointment_details_IN" +
						" where appointment_date BETWEEN SUBDATE(CURDATE(), INTERVAL 7 DAY) AND NOW() and salesPersonContactNo=?";
			}else
			{
				sql="select business_name,business_place,appointment_time,comments,appointment_date,business_city,status_flag  from appointment_details_IN" +
						" where appointment_date  BETWEEN SUBDATE(CURDATE(), INTERVAL 1 MONTH) AND NOW() and salesPersonContactNo=?";
			}
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objPreparedStatement.setString(1, objAppointmentDTO.getTelecallerName());
			objResultSet=objPreparedStatement.executeQuery();
			while(objResultSet.next())
			{
				AppointmentDTO objAppointmentDTO1=new AppointmentDTO();
				objAppointmentDTO1.setBusinessName(objResultSet.getString("business_name"));
				if(objResultSet.getString("status_flag").equals("2"))
				{
					objAppointmentDTO1.setCallStatus("Completed");
					
				}else if(objResultSet.getString("status_flag").equals("3"))
				{
					objAppointmentDTO1.setCallStatus("Pending");
				}else if(objResultSet.getString("status_flag").equals("4"))
				{
					objAppointmentDTO1.setCallStatus("Rejected");
				}
				
				objAppointmentDTO1.setBusinessArea(objResultSet.getString("business_place"));
				objAppointmentDTO1.setComments(objResultSet.getString("comments"));
				objAppointmentDTO1.setAppointmentDateTime(objResultSet.getString("appointment_time")+" "+objResultSet.getString("appointment_date"));
				obAppointmentDTOsList.add(objAppointmentDTO1);
				
			}
			
			
			
			
			
		}catch(Exception e)
		{
			
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		
		return obAppointmentDTOsList;
	}

	public ArrayList<AppointmentDTO> getSavedData(AppointmentDTO objAppointmentDTO)
	{
		
		ArrayList<AppointmentDTO> objAppointmentDTOsList=new ArrayList<AppointmentDTO>();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="select comments,businesscontact,call_time,call_date from telecaller_callstatus_details" +
				" where  telecaller=? and callstatus=6 and callback_date=? order by call_id desc";
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objPreparedStatement.setString(1, objAppointmentDTO.getTelecallerName());
			objPreparedStatement.setString(2, objAppointmentDTO.getTodaydate());
			objResultSet=objPreparedStatement.executeQuery();
			while(objResultSet.next())
			{
				AppointmentDTO objAppointmentDTO1=new AppointmentDTO();
				objAppointmentDTO1.setComments(objResultSet.getString("comments"));
				objAppointmentDTO1.setAppointmentDateTime(objResultSet.getString("call_time"));
				objAppointmentDTO1.setBusinessContact(objResultSet.getString("businesscontact"));
				objAppointmentDTOsList.add(objAppointmentDTO1);
				
			}
			
		}catch (Exception e) {
		}finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		return objAppointmentDTOsList;
	}

	public ArrayList<AppointmentDTO> getDatewiseReports(AppointmentDTO objAppointmentDTO) 
	{
		ArrayList<AppointmentDTO> objAppointmentDTOList=new ArrayList<AppointmentDTO>();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		PreparedStatement objPreparedStatement1=null;
		ResultSet objResultSet1=null;
		String mobile_no=null;
		String sql="";
		try{
			objConnection=DBConfig.connect();
			if(objAppointmentDTO.getAppointmentType().equals("sales"))
			{
					try{
						sql="select mobile_no from  sales_user_details_IN where salesperson_name=? limit 1";
						objPreparedStatement1=objConnection.prepareStatement(sql);
						objPreparedStatement1.setString(1, objAppointmentDTO.getUsername());
						objResultSet1=objPreparedStatement1.executeQuery();
						while(objResultSet1.next())
						{
							mobile_no=objResultSet1.getString("mobile_no");
						}
					}catch (Exception e) {e.printStackTrace();
					}finally{DBConfig.disconnect(null, objPreparedStatement1, objResultSet1);}
					try{
					sql="select business_name,business_place,appointment_time,appointment_date,status_flag   from appointment_details_IN  where salesPersonContactNo=? and  updated_date between ? and ? ";
					objPreparedStatement=objConnection.prepareStatement(sql);
					objPreparedStatement.setString(1, mobile_no);
					objPreparedStatement.setString(2, objAppointmentDTO.getFromDate());
					objPreparedStatement.setString(3, objAppointmentDTO.getToDate());
					objResultSet=objPreparedStatement.executeQuery();
					
					while(objResultSet.next())
					{
						
						AppointmentDTO objAppointmentDTO1=new AppointmentDTO();
						objAppointmentDTO1.setBusinessName(objResultSet.getString("business_name"));
						objAppointmentDTO1.setBusinessArea(objResultSet.getString("business_place"));
						objAppointmentDTO1.setAppointmentDateTime(objResultSet.getString("appointment_date")+" "+objResultSet.getString("appointment_time"));
						if(objResultSet.getString("status_flag").equals("2"))
						{
							objAppointmentDTO1.setCallStatus("Completed");
						}else if(objResultSet.getString("status_flag").equals("3"))
						{
							objAppointmentDTO1.setCallStatus("Pending");
						}else if(objResultSet.getString("status_flag").equals("4"))
						{
							objAppointmentDTO1.setCallStatus("Rejected");
						}
						else if(objResultSet.getString("status_flag").equals("1"))
						{
							objAppointmentDTO1.setCallStatus("Not Processed Till Now");
						}
						
						objAppointmentDTOList.add(objAppointmentDTO1);
					}
					}catch (Exception e) {e.printStackTrace();
					}
			}else
			{
				sql="select businesscontact,call_time,callstatus,comments   from telecaller_callstatus_details   where telecaller=? and call_date between ? and ?";
				objPreparedStatement=objConnection.prepareStatement(sql);
				objPreparedStatement.setString(1, objAppointmentDTO.getUsername());
				objPreparedStatement.setString(2, objAppointmentDTO.getFromDate());
				objPreparedStatement.setString(3, objAppointmentDTO.getToDate());
				objResultSet=objPreparedStatement.executeQuery();
				while(objResultSet.next())
				{
					AppointmentDTO objAppointmentDTO1=new AppointmentDTO();
					objAppointmentDTO1.setBusinessContact(objResultSet.getString("businesscontact"));
					objAppointmentDTO1.setComments(objResultSet.getString("comments"));
					objAppointmentDTO1.setAppointmentDateTime(objResultSet.getString("call_time"));
					if(objResultSet.getString("callstatus").equals("0"))
					{
						objAppointmentDTO1.setCallStatus("Answered");
					}else if(objResultSet.getString("callstatus").equals("1"))
					{
						objAppointmentDTO1.setCallStatus("Rejected");
					}else if(objResultSet.getString("callstatus").equals("2"))
					{
						objAppointmentDTO1.setCallStatus("Not Reachble");
					}
					else if(objResultSet.getString("callstatus").equals("3"))
					{
						objAppointmentDTO1.setCallStatus("Other");
					}
					else if(objResultSet.getString("callstatus").equals("4"))
					{
						objAppointmentDTO1.setCallStatus("Call Back");
					}
					else if(objResultSet.getString("callstatus").equals("5"))
					{
						objAppointmentDTO1.setCallStatus("Answered And Call Back");
					}else if(objResultSet.getString("callstatus").equals("6"))
					{
						objAppointmentDTO1.setCallStatus("Answered And Saved Data");
					}
					objAppointmentDTOList.add(objAppointmentDTO1);
				}
			}
			
		}catch (Exception e) {
		}
		finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		return objAppointmentDTOList;
	}

	public ArrayList<AppointmentDTO> getAllAppointments(AppointmentDTO objCheckAppointmentDTO) 
			{
		
		ArrayList<AppointmentDTO> objAppointmentDTOList=new ArrayList<AppointmentDTO>();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="";
		
			try{
				sql="select appointment_id,business_name,appointment_time,telecaller,appointment_date,business_place,business_city  from appointment_details_IN where status_flag=0 and call_status=0 and ! (appointment_date='1900-01-01')   order by appointment_date  desc";
				objConnection=DBConfig.connect();
				objPreparedStatement=objConnection.prepareStatement(sql);

				objResultSet=objPreparedStatement.executeQuery();
				while(objResultSet.next())
				{
					AppointmentDTO objCheckAppointmentDTO1=new AppointmentDTO();
					objCheckAppointmentDTO1.setAppointmentId(objResultSet.getString("appointment_id"));
					objCheckAppointmentDTO1.setBusinessName(objResultSet.getString("business_name"));
					objCheckAppointmentDTO1.setAppointmentDateTime(objResultSet.getString("appointment_date")+" "+objResultSet.getString("appointment_time"));
					objCheckAppointmentDTO1.setTelecallerName(objResultSet.getString("telecaller"));
					objCheckAppointmentDTO1.setBusinessArea(objResultSet.getString("business_place"));
					objCheckAppointmentDTO1.setBusinessCity(objResultSet.getString("business_city"));
					objAppointmentDTOList.add(objCheckAppointmentDTO1);
					
				}
				
			}catch(Exception e){e.printStackTrace();}
			finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		
			
		
			return objAppointmentDTOList;
	       }

	public ArrayList<String> getTelecallerName(String name) 
	{
		
		
		 ArrayList<String> SearchCityList=new ArrayList<String>();
			Connection objConnection = null;
			PreparedStatement objPreparedStatement = null;
			ResultSet objResultSet = null;
			
			 try {
					 objConnection = DBConfig.connect();
					 String query="SELECT distinct telecaller FROM   appointment_details_IN  where telecaller  like ?  LIMIT 0,5 ";
					 objPreparedStatement = objConnection.prepareStatement(query);
					 objPreparedStatement.setString(1,name+"%");
					 objResultSet = objPreparedStatement.executeQuery();
					 
					while(objResultSet.next())
					 {
						
						SearchCityList.add(objResultSet.getString("telecaller"));
			    			
					 }
					
			 } 
			 catch (Exception e) {
				e.printStackTrace();
			 } 
			 finally {
				
				 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

	      }
			 return SearchCityList;
}

	public ArrayList<AppointmentDTO> getAllAppointmentsByName(
			AppointmentDTO objCheckAppointmentDTO) {
		ArrayList<AppointmentDTO> objAppointmentDTOList=new ArrayList<AppointmentDTO>();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="";
		
			try{
				sql="select appointment_id,business_name,appointment_time,telecaller,appointment_date,business_place,business_city  from appointment_details_IN where status_flag=0 and call_status=0 and ! (appointment_date='1900-01-01') and telecaller=?  order by appointment_date  desc";
				objConnection=DBConfig.connect();
				objPreparedStatement=objConnection.prepareStatement(sql);
				objPreparedStatement.setString(1, objCheckAppointmentDTO.getTelecallerName());
				objResultSet=objPreparedStatement.executeQuery();
				while(objResultSet.next())
				{
					AppointmentDTO objCheckAppointmentDTO1=new AppointmentDTO();
					objCheckAppointmentDTO1.setAppointmentId(objResultSet.getString("appointment_id"));
					objCheckAppointmentDTO1.setBusinessName(objResultSet.getString("business_name"));
					objCheckAppointmentDTO1.setAppointmentDateTime(objResultSet.getString("appointment_date")+" "+objResultSet.getString("appointment_time"));
					objCheckAppointmentDTO1.setTelecallerName(objResultSet.getString("telecaller"));
					objCheckAppointmentDTO1.setBusinessArea(objResultSet.getString("business_place"));
					objCheckAppointmentDTO1.setBusinessCity(objResultSet.getString("business_city"));
					objAppointmentDTOList.add(objCheckAppointmentDTO1);
					
				}
			
			}catch(Exception e){e.printStackTrace();}
			finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		
			
		
			return objAppointmentDTOList;
	}
	
	public ArrayList<AppointmentDTO> getAllocatedNames(
			AppointmentDTO objCheckAppointmentDTO) {
		
		ArrayList<AppointmentDTO> objAppointmentDTOList=new ArrayList<AppointmentDTO>();
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="";
	
			try{
				objConnection=DBConfig.connect();
				if(!objCheckAppointmentDTO.getName().isEmpty() && objCheckAppointmentDTO.getName()!=null)
				{
					
				sql="select * from appointment_details_IN where status_flag=1 and   salesPersonName=? order by appointment_date desc limit 0,20";
				objPreparedStatement=objConnection.prepareStatement(sql);
				objPreparedStatement.setString(1, objCheckAppointmentDTO.getName());
				
				
				}
				else if(!objCheckAppointmentDTO.getDate().isEmpty() && objCheckAppointmentDTO.getDate()!=null)
				
				{
					
					sql="select * from appointment_details_IN where status_flag=1 and   appointment_date=? ";
					objPreparedStatement=objConnection.prepareStatement(sql);
					objPreparedStatement.setString(1, objCheckAppointmentDTO.getDate());
				}
				
				/*objPreparedStatement=objConnection.prepareStatement(sql);
				if(objCheckAppointmentDTO.getName()!=null && objCheckAppointmentDTO.getName()!=""){
				objPreparedStatement.setString(1, objCheckAppointmentDTO.getName());
				}
				
				
				System.out.println(objCheckAppointmentDTO.getDate()+"second if");
				
				if(objCheckAppointmentDTO.getDate()!=null && objCheckAppointmentDTO.getDate()!=""){
				objPreparedStatement.setString(1, objCheckAppointmentDTO.getDate());
				}*/
				objResultSet=objPreparedStatement.executeQuery();
				while(objResultSet.next())
				{
					AppointmentDTO objCheckAppointmentDTO1=new AppointmentDTO();
					objCheckAppointmentDTO1.setAppointmentId(objResultSet.getString("appointment_id"));
					objCheckAppointmentDTO1.setBusinessName(objResultSet.getString("business_name"));
					objCheckAppointmentDTO1.setDate(objResultSet.getString("appointment_date"));
					objCheckAppointmentDTO1.setName(objResultSet.getString("salesPersonName"));
					objCheckAppointmentDTO1.setBusinessArea(objResultSet.getString("business_place"));
					objCheckAppointmentDTO1.setBusinessCity(objResultSet.getString("business_city"));
					objAppointmentDTOList.add(objCheckAppointmentDTO1);
					
					
				}
				
			}catch(Exception e){e.printStackTrace();}
			finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		
			
		
			return objAppointmentDTOList;
	}


	public ArrayList<AppointmentDTO> getTodayAppointmentDetails() 
	{
		
		
		
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		ArrayList<AppointmentDTO> objTodayAppointmentlist =new ArrayList<AppointmentDTO>();
		String sql="";
		try{
			objConnection=DBConfig.connect();
						
			sql="select * from appointment_details_IN where  appointment_date=curdate() ";
			objPreparedStatement=objConnection.prepareStatement(sql);
			
			objResultSet=objPreparedStatement.executeQuery();
		
				
				while(objResultSet.next())
				{
					
					AppointmentDTO objCheckAppointmentDTO1=new AppointmentDTO();
					
					objCheckAppointmentDTO1.setBusinessName(objResultSet.getString("business_name"));
					objCheckAppointmentDTO1.setBusinessArea(objResultSet.getString("business_place"));
					objCheckAppointmentDTO1.setBusinessMobile(objResultSet.getString("business_mobile"));
					objCheckAppointmentDTO1.setTelecallerName(objResultSet.getString("telecaller"));
					
					objCheckAppointmentDTO1.setKeyword1(objResultSet.getString("keyword1"));
					objCheckAppointmentDTO1.setAppointmentDateTime(objResultSet.getString("appointment_date"));
					
					
					
					objTodayAppointmentlist.add(objCheckAppointmentDTO1);
				}
			}catch(Exception e){e.printStackTrace();}
			finally{DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);}
		
			return objTodayAppointmentlist;
	}
	
	public boolean rejectAppointments(String id,String telename,String businessname,String reason) 
	{
		
		boolean update_status=false;
		int update_count=0;
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		
		String sql="update appointment_details_IN set status_flag=5,reason=? where appointment_id=? and telecaller=?";
		try{
			objConnection=DBConfig.connect();
			objPreparedStatement=objConnection.prepareStatement(sql);
			objPreparedStatement.setString(1,reason);
			objPreparedStatement.setString(2,id);
			objPreparedStatement.setString(3,telename);
			update_count=objPreparedStatement.executeUpdate();
			if(update_count>0)
			{
			update_status=true;
			}
	
			
		}catch(Exception e){e.printStackTrace();}
		finally{DBConfig.disconnect(objConnection, objPreparedStatement, null);}
		return update_status;
	}

}