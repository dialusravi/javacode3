package com.dialuz.salesTracking.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.dialuz.salesTracking.DB.DBConfig;
import com.dialuz.salesTracking.DTO.TargetDTO;




public class TargetDao {
	
public boolean insertTargets(String targetuser,int targets,String tlname)
{
	Connection objconnection=DBConfig.connect();
	PreparedStatement pstmt=null;
	int count=0;
	boolean insert_status=false;
	String sql="insert into targets(user_name,targets,assigned_by) values(?,?,?)";
	try{
		pstmt=objconnection.prepareStatement(sql);
		pstmt.setString(1,targetuser);
		pstmt.setInt(2,targets);
		pstmt.setString(3,tlname);
		count=pstmt.executeUpdate();
		if(count>0)
		{
			insert_status=true;
		}
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	finally
	{
		DBConfig.disconnect(objconnection, pstmt, null);
	}
	return insert_status;
}

	public TargetDTO GetUserCount(String user)
	{

		
		
		
		Connection objConnection=null;
		PreparedStatement pstmt1=null;
		PreparedStatement pstmt2=null;
		PreparedStatement pstmt3=null;
	
		ResultSet rs1=null;
		ResultSet rs2=null;
		ResultSet rs3=null;
	
	  TargetDTO objTargetDTO=new TargetDTO();
		int today_count=0;
		int week_count=0;
		
		int monthly_count=0;
		try{
			 objConnection=DBConfig.connect();
			 String sql1="SELECT count(updated_date) as count FROM appointment_details_IN where updated_date=curdate() and telecaller=?";
			 //String sql2="SELECT count(updated_date) as count FROM dataentry_record_details where updated_date <=curdate() and updated_date >=(subdate(curdate(),6)) and update_by=?";
			 String sql2="SELECT count(updated_date) as count FROM appointment_details_IN where updated_date <=curdate() and updated_date >subdate(curdate(),dayofweek(curdate())) and telecaller=?";
			 String sql3="SELECT count(updated_date) as count FROM appointment_details_IN where updated_date <=last_DAY(curdate())"+
	                               "and updated_date >subdate(last_DAY(curdate()),dayofmonth(last_DAY(curdate()))) and telecaller=? ";
					
				pstmt1=objConnection.prepareStatement(sql1);
				
				pstmt1.setString(1,user);
				rs1=pstmt1.executeQuery();
				if(rs1.next())
				{
				today_count=rs1.getInt(1);	
				objTargetDTO.setDay_count(today_count);
				
				}
				
				
				
				
				

				pstmt2=objConnection.prepareStatement(sql2);
				pstmt2.setString(1, user);

				rs2=pstmt2.executeQuery();
				if(rs2.next())
				{
				week_count=rs2.getInt(1);
				objTargetDTO.setWeek_count(week_count);
				
					
				}
				
				
				
				
				pstmt3=objConnection.prepareStatement(sql3);
				pstmt3.setString(1, user);

				
				rs3=pstmt3.executeQuery();
				
				if(rs3.next())
				{
					monthly_count=rs3.getInt(1);
				objTargetDTO.setMonth_count(monthly_count);
				}
				
			
			
		}catch(Exception e)
		{
			
		}
		finally{
			DBConfig.disconnect(objConnection, pstmt1, rs1);
			DBConfig.disconnect(objConnection, pstmt2, rs2);
			DBConfig.disconnect(objConnection, pstmt3, rs3);
		
		       }
		return objTargetDTO;
	}

	public TargetDTO GetTargets(String user)
	{

		
		Connection objConnection=null;
		PreparedStatement pstmt1=null;
		
		ResultSet rs1=null;
		
	
	  TargetDTO objTargetDTO1=new TargetDTO();
		int today_target=0;
		int week_target=0;
		
		int monthly_target=0;
		try{
			 objConnection=DBConfig.connect();
		/*	 String sql1="select t.targets from targets t inner join dataentry_record_details d on t.user_name=d.update_by and d.updated_date=date(t.date_time) where d.update_by=?";
			 */
			  String sql1="select distinct t.targets,t.date_time  from targets t inner join appointment_details_IN d on t.user_name='"+user+"' and date(t.date_time)=curdate()";
					
				pstmt1=objConnection.prepareStatement(sql1);
				
				//pstmt1.setString(1,user);
				rs1=pstmt1.executeQuery();
				
				if(rs1.next())
				{
					today_target=rs1.getInt("targets");	
				objTargetDTO1.setToday_target(today_target);
				
				}
				
				week_target=today_target*6;
				objTargetDTO1.setWeek_target(week_target);
				monthly_target=today_target*24;
				objTargetDTO1.setMonth_target(monthly_target);
				
				

			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			DBConfig.disconnect(objConnection, pstmt1, rs1);
			
		       }
		return objTargetDTO1;
	}

	public ArrayList<TargetDTO> getAllTargets(String user) 
	{
		Connection objConnection=null;
		PreparedStatement pstmt1=null;
		ResultSet rs1=null;
		ArrayList<TargetDTO> list=new ArrayList<TargetDTO>();
	
	 		
		try{
			 objConnection=DBConfig.connect();
		     String sql1="SELECT user_name,targets,date(date_time) as date FROM targets  where assigned_by=? and date(date_time)=curdate();";
			 pstmt1=objConnection.prepareStatement(sql1);
			 pstmt1.setString(1, user);	
			 rs1=pstmt1.executeQuery();
				
				while(rs1.next())
				{ 
				 TargetDTO objTargetDTO1=new TargetDTO();
				 objTargetDTO1.setUsername(rs1.getString("user_name"));
				 objTargetDTO1.setTargets(rs1.getInt("targets"));
				 objTargetDTO1.setDate(rs1.getString("date"));
				 list.add(objTargetDTO1);
				}
				
				
				

			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			DBConfig.disconnect(objConnection, pstmt1, rs1);
			
		       }
		return list;
	}

	public int updateTarget(String user,String targets)
	{
		Connection objConnection=null;
		PreparedStatement pstmt1=null;
		int status=0;
		try{
			 objConnection=DBConfig.connect();
		     String sql1="update targets set targets=? where user_name=?";
		     
			 pstmt1=objConnection.prepareStatement(sql1);
			
			 pstmt1.setString(1,targets);
			 pstmt1.setString(2,user);	
			 				
			 status=pstmt1.executeUpdate();	
				if(status>0)
				{
					status=1;	
				}

			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			DBConfig.disconnect(objConnection, pstmt1, null);
			   }
		return status;
	}
	
	
	public int insertProblems(String subject,String description,String user)
	{
		Connection objConnection=null;
		PreparedStatement pstmt1=null;
		int status=0;
	try{
			 objConnection=DBConfig.connect();
		     String sql1="insert into problems(subject,description,user_name) values(?,?,?)";
		     
			 pstmt1=objConnection.prepareStatement(sql1);
			
			 pstmt1.setString(1,subject);
			 pstmt1.setString(2,description);	
			 pstmt1.setString(3,user);	
			 
			 status=pstmt1.executeUpdate();	
				if(status>0)
				{
					status=1;	
				}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			DBConfig.disconnect(objConnection, pstmt1, null);
			   }
		return status;
	}
	public ArrayList<TargetDTO> getProblems(String user)
	{
		Connection objConnection=null;
		PreparedStatement pstmt1=null;
		ResultSet rs1=null;
		ArrayList<TargetDTO> list=new ArrayList<TargetDTO>();
		 String sql1=null;
	try{
			 objConnection=DBConfig.connect();
		     if(user==null)
		     {
		     sql1="select * from problems  order by date_time desc";	 
		     }
		else{
			 sql1="select subject,description,date_time,status_flag from problems where user_name=? order by date_time desc";
		     }
			 pstmt1=objConnection.prepareStatement(sql1);
			 if(user==null)
			 {
				 
			 }
		else{
			 pstmt1.setString(1,user);
			 }
			 rs1=pstmt1.executeQuery();
			if(user==null)
			{
				 while(rs1.next())
					{ 
					 TargetDTO objTargetDTO1=new TargetDTO();
					 objTargetDTO1.setSubject(rs1.getString("subject"));
					 objTargetDTO1.setDescription(rs1.getString("description"));
					 Date date = rs1.getTimestamp("date_time");
					 objTargetDTO1.setDate(new SimpleDateFormat("yyyy-MM-dd").format(date));
					 objTargetDTO1.setStatus(rs1.getString("status_flag"));
					 objTargetDTO1.setUsername(rs1.getString("user_name"));
					 objTargetDTO1.setId(rs1.getInt("id"));
					 list.add(objTargetDTO1);
					}
			}
			else
			{
				 while(rs1.next())
					{ 
					 TargetDTO objTargetDTO1=new TargetDTO();
					 objTargetDTO1.setSubject(rs1.getString("subject"));
					 objTargetDTO1.setDescription(rs1.getString("description"));
					 Date date = rs1.getTimestamp("date_time");
					 objTargetDTO1.setDate(new SimpleDateFormat("yyyy-MM-dd").format(date));
					 objTargetDTO1.setStatus(rs1.getString("status_flag"));
					 
					 list.add(objTargetDTO1);
					}	
			}
			
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			DBConfig.disconnect(objConnection, pstmt1, null);
			   }
		return list;
	}

	public ArrayList<TargetDTO> getProblems1(String fdate,String tdate)
	{
		Connection objConnection=null;
		PreparedStatement pstmt1=null;
		ResultSet rs1=null;
		ArrayList<TargetDTO> list=new ArrayList<TargetDTO>();
		 String sql1=null;
	try{
			 objConnection=DBConfig.connect();
		     
		     sql1="select * from problems  where date(date_time)>=? and date(date_time)<=?";	 
		    
			 pstmt1=objConnection.prepareStatement(sql1);
			 pstmt1.setString(1,fdate);
			 pstmt1.setString(2,tdate);
			 
			 rs1=pstmt1.executeQuery();
			 while(rs1.next())
					{ 
					 TargetDTO objTargetDTO1=new TargetDTO();
					 objTargetDTO1.setSubject(rs1.getString("subject"));
					 objTargetDTO1.setDescription(rs1.getString("description"));
					 Date date = rs1.getTimestamp("date_time");
					 objTargetDTO1.setDate(new SimpleDateFormat("yyyy-MM-dd").format(date));
					 objTargetDTO1.setStatus(rs1.getString("status_flag"));
					 objTargetDTO1.setUsername(rs1.getString("user_name"));
					 objTargetDTO1.setId(rs1.getInt("id"));
					 list.add(objTargetDTO1);
					}
			
		
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			DBConfig.disconnect(objConnection, pstmt1, null);
			   }
		return list;
	}
	public int fixProblem(int id, String user) 
	{
		Connection objConnection=null;
		PreparedStatement pstmt1=null;
		int status=0;
	try{
			 objConnection=DBConfig.connect();
		     String sql1="update problems set status_flag=1 where id=? and user_name=?";
		     
			 pstmt1=objConnection.prepareStatement(sql1);
			
			 pstmt1.setInt(1,id);
			 pstmt1.setString(2,user);	
			 status=pstmt1.executeUpdate();	
				if(status>0)
				{
					status=1;	
				}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			DBConfig.disconnect(objConnection, pstmt1, null);
			   }
		return status;
	}

	public TargetDTO getProblemsCount(String user)
    {
		Connection objConnection=null;
		PreparedStatement pstmt1=null;
		PreparedStatement pstmt2=null;
		ResultSet rs1=null;
		ResultSet rs2=null;
		TargetDTO objTargetDTO1=new TargetDTO();
		 String sql1=null;
		 String sql2=null;
	try{
			 objConnection=DBConfig.connect();
		     
			 if(user==null)
			 {
		     sql1="select count(*) from problems  where status_flag=0";	 
		     sql2="select count(*) from problems  where status_flag=1";
			 }else
			 {
				 sql1="select count(*) from problems  where status_flag=0 and user_name=?";	 
			     sql2="select count(*) from problems  where status_flag=1 and user_name=?"; 
			 }
			 if(user==null)
			 {
		 	 pstmt1=objConnection.prepareStatement(sql1);
			 pstmt2=objConnection.prepareStatement(sql2);
			 }else
			 {
				 pstmt1=objConnection.prepareStatement(sql1);
				 pstmt2=objConnection.prepareStatement(sql2);
				 pstmt1.setString(1, user); 
				 pstmt2.setString(1, user);
			 }
			 
			 rs1=pstmt1.executeQuery();
			 rs2=pstmt2.executeQuery();
			 
			 if(rs1.next())
			  {
				 objTargetDTO1.setUnfixcount(rs1.getInt(1));
			  }
			 if(rs2.next())
			 {
				 objTargetDTO1.setFixcount(rs2.getInt(1));
			 }
			
		}catch(Exception e)
		{
		  e.printStackTrace();
		}
 finally{
		 DBConfig.disconnect(objConnection, pstmt1, null);
		}
		return objTargetDTO1;
	}
	public TargetDTO getProblemsCount1(String fdate, String tdate)
	{
		Connection objConnection=null;
		PreparedStatement pstmt1=null;
		PreparedStatement pstmt2=null;
		ResultSet rs1=null;
		ResultSet rs2=null;
		TargetDTO objTargetDTO1=new TargetDTO();
		 String sql1=null;
		 String sql2=null;
	try{
			 objConnection=DBConfig.connect();
		     
		     sql1="select count(*) from problems  where status_flag=0 and (date(date_time)>=? and date(date_time)<=?)";	 
		     sql2="select count(*) from problems  where status_flag=1 and (date(date_time)>=? and date(date_time)<=?)";
		    
			 pstmt1=objConnection.prepareStatement(sql1);
			 pstmt1.setString(1,fdate);
			 pstmt1.setString(2, tdate);
			 pstmt2=objConnection.prepareStatement(sql2);
			 pstmt2.setString(1,fdate);
			 pstmt2.setString(2, tdate);
			 rs1=pstmt1.executeQuery();
			 rs2=pstmt2.executeQuery();
			 if(rs1.next())
			  {
				 objTargetDTO1.setUnfixcount(rs1.getInt(1));
			  }
			 if(rs2.next())
			 {
				 objTargetDTO1.setFixcount(rs2.getInt(1));
			 }
			
		}catch(Exception e)
		{
		  e.printStackTrace();
		}
 finally{
		 DBConfig.disconnect(objConnection, pstmt1, null);
		}
		return objTargetDTO1;
	}
	}
