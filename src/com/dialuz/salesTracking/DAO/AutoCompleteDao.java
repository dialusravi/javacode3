package com.dialuz.salesTracking.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.dialuz.salesTracking.DB.DBConfig;



public class AutoCompleteDao {
	
	
	
	public LinkedHashMap<Integer,String> getAutoComplteBusinessName(String name)
	{

		LinkedHashMap<Integer,String> map=new LinkedHashMap<Integer,String>();
			Connection objConnection = null;
			PreparedStatement objPreparedStatement = null;
			ResultSet objResultSet = null;

		 try {					 
					 objConnection = DBConfig.connect();
					 String query="SELECT business_id,business_name FROM business_master_IN  where business_name like '"+name+"%' LIMIT 0,5";
					 objPreparedStatement = objConnection.prepareStatement(query);
					
					 objResultSet = objPreparedStatement.executeQuery();
					
					while(objResultSet.next())
					 {
						
						map.put(objResultSet.getInt("business_id"),objResultSet.getString("business_name"));
			    			
					 }
					
			 } 
			 catch (Exception e) {
				e.printStackTrace();
			 } 
			 finally
			 {
				
				 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

			 }

			 return map;
		
		
		
		
	}
	

	public ArrayList<String> getAutoComplte(String name)
	{

		  ArrayList<String> list=new ArrayList<String>();
			Connection objConnection = null;
			PreparedStatement objPreparedStatement = null;
			ResultSet objResultSet = null;

		 try {				 				 
					 objConnection = DBConfig.connectToMainDB();
					 String query="SELECT distinct keyword FROM keywords_IN  where  keyword like '"+name+"%' LIMIT 0,5 ";
					 objPreparedStatement = objConnection.prepareStatement(query);
					 objResultSet = objPreparedStatement.executeQuery();
					
					while(objResultSet.next())
					 {
						
						list.add(objResultSet.getString("keyword"));
			    			
					 }
					
			 } 
			 catch (Exception e) {
				e.printStackTrace();
			 } 
			 finally {
				
				 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

			 }

			 return list;
		
		
		
		
	}

	public ArrayList<String> getAllDetails()
	{
		  ArrayList<String> list=new ArrayList<String>();
			Connection objConnection = null;
			PreparedStatement objPreparedStatement = null;
			ResultSet objResultSet = null;

		 try {
				 				 
					 objConnection = DBConfig.connectToMainDB();
					 String query="SELECT distinct keyword FROM keywords_IN";
					
					 objPreparedStatement = objConnection.prepareStatement(query);
					
					 objResultSet = objPreparedStatement.executeQuery();
					
					while(objResultSet.next())
					 {
						
					list.add(objResultSet.getString("keyword"));
			    			
					 }
					
			 } 
			 catch (Exception e) {
				e.printStackTrace();
			 } 
			 finally {
				
				 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

			 }

			 return list;
	}

	public String insertCountry(String country,String city)
	{
		 Connection objConnection = null;
	     PreparedStatement objPreparedStatement = null;
	     int result=0;
	     try {
			 objConnection = DBConfig.connectToMainDB();
			 String query="insert into keywords_IN(keyword,business_city) values(?,?)";
			 objPreparedStatement = objConnection.prepareStatement(query);
			 objPreparedStatement.setString(1,country);
			 objPreparedStatement.setString(2,city);
			 result=objPreparedStatement.executeUpdate();
			
			
			
	 } 
	 catch (Exception e) 
	 {
		e.printStackTrace();
	 } 
	     
	 finally
     {
		
		 DBConfig.disconnect(objConnection, objPreparedStatement, null);

	 }if(result>0)
     {
    	 return "true";
     }

	     return "false";
	}

	public int deleteKeyword(String delstr,String city) {
		 Connection objConnection = null;
	     PreparedStatement objPreparedStatement = null;
	     int result=0;
	     try {
				 
			 objConnection = DBConfig.connectToMainDB();
			 String query="delete from keywords_IN where keyword=? and business_city=?";
			
			 objPreparedStatement = objConnection.prepareStatement(query);
			 objPreparedStatement.setString(1, delstr);
			 objPreparedStatement.setString(2, city);
			 result=objPreparedStatement.executeUpdate();
			
			
			
	 } 
	 catch (Exception e) 
	 {
		e.printStackTrace();
	 } 
	     
	 finally
     {
		
		 DBConfig.disconnect(objConnection, objPreparedStatement, null);

	 }
	     if(result>0)
     {
    	 return 1;
     }

	     return 0;
	}

	public ArrayList getTargetUserType(String targetuser)
	  {
	    ArrayList<String> list = new ArrayList();
	    Connection objConnection = null;
	    PreparedStatement objPreparedStatement = null;
	    ResultSet objResultSet = null;
	    try
	    {
	      objConnection = DBConfig.connect();
	      String query = "SELECT telecaller_username FROM  telecaller_details where  telecaller_username like '" + targetuser + "%' LIMIT 0,5 ";
	      objPreparedStatement = objConnection.prepareStatement(query);
	      objResultSet = objPreparedStatement.executeQuery();
	      while (objResultSet.next()) {
	        list.add(objResultSet.getString("telecaller_username"));
	      }
	    }
	    catch (Exception e)
	    {
	      e.printStackTrace();
	    }
	    finally
	    {
	      DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);
	    }
	    return list;
	  }
	public ArrayList getDialyCallPlanCity(String city)
	  {
	    ArrayList<String> list = new ArrayList();
	    Connection objConnection = null;
	    PreparedStatement objPreparedStatement = null;
	    ResultSet objResultSet = null;
	    try
	    {
	      objConnection = DBConfig.connectToMainDB();
	      String query = "SELECT distinct business_city FROM business_master_IN  where business_city like '"+ city + "%' LIMIT 0,5 ";
	      objPreparedStatement = objConnection.prepareStatement(query);
	      objResultSet = objPreparedStatement.executeQuery();
	      while (objResultSet.next()) {
	        list.add(objResultSet.getString("business_city"));
	      }
	    }
	    catch (Exception e)
	    {
	      e.printStackTrace();
	    }
	    finally
	    {
	      DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);
	    }
	    return list;
	  }

}
