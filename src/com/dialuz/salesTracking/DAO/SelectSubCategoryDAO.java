package com.dialuz.salesTracking.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.dialuz.salesTracking.DB.DBConfig;
import com.dialuz.salesTracking.DTO.SelectSubCategoryDTO;


public class SelectSubCategoryDAO {
	
	
	
	Connection objcon;
	PreparedStatement objpstm;
	Statement objstm;
	ResultSet objrs;
	
	public ArrayList getSubCategory(String subCat) 
	
	{
		
		ArrayList objArrayListSubCat=new ArrayList();
		
		try
		{
		
		
		
			
		
		String subCatTemp=subCat.trim();
		
		String subCatQuery="Select cat,sub_cat,other_cat from cat_subcat_othercat_list_IN  where cat='"+subCatTemp+"'";
			objcon=DBConfig.connectToMainDB();
		objpstm = objcon.prepareStatement(subCatQuery);
		objrs= objpstm.executeQuery();
		SelectSubCategoryDTO objSelectSubCategoryDTO=new SelectSubCategoryDTO();
		while(objrs.next())
		{
			
			
			
			objSelectSubCategoryDTO.setCatName(objrs.getString("cat"));
			
			objSelectSubCategoryDTO.setSubCategoryName(objrs.getString("sub_cat"));
			
			objSelectSubCategoryDTO.setSubSubCategoryName(objrs.getString("other_cat"));
		    objArrayListSubCat.add(objSelectSubCategoryDTO);
		
		}
		
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
			
		}
		finally
		{
			try
			{
				
				objcon.close();
				DBConfig.disconnect(objcon,objpstm,objrs);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				
			}
		}
	
		return   objArrayListSubCat;
	}
	

}
